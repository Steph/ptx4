# -*- encoding: UTF-8 -*-

import uno
import re
import sys
import os
import traceback

from com.sun.star.linguistic2 import SingleProofreadingError
from com.sun.star.text.TextMarkupType import PROOFREADING
from com.sun.star.beans import PropertyValue

import conjfr
import mfspfr
import cregex as cr

pkg = u"grammalecte"
lang = u"fr"
locales = {'fr-BE': ['fr', 'BE', ''], 'fr-CA': ['fr', 'CA', ''], 'fr-FR': ['fr', 'FR', ''], 'fr-CH': ['fr', 'CH', ''], 'fr-LU': ['fr', 'LU', ''], 'fr-MC': ['fr', 'MC', '']}
version = u"0.4.10.1"
author = u"Olivier R."
name = u"Grammalecte"

#import lightproof_handler_grammalecte as loopt
import Options as loopt

# end of sentence regex
reEOS = re.compile(u'([.?!:;…][ .?!… »”")]*|.$)')
# begin of paragraph regex
reBOP = re.compile(u"^\W*")
# end of paragraph regex
reEOP = re.compile(u"\W*$")

# loaded rules (check for Update mechanism of the editor)
try:
    langrule
except NameError:
    langrule = {}

# ignored rules
aIgnoredRules = set()

# cache for morphogical analyses
analyses = {}
stems = {}
suggestions = {}

# assign Calc functions
calcfunc = None

GLOBALS = globals()

def echo (s):
    """ Print for Windows. Windows console is crap. You can’t print something without being sure it won’t make Python crash.
        Windows console don’t understand UTF-8. Encoding depends on Windows locale. No useful standard."""
    try:
        print(s.replace(u"’", "'"))
    except:
        print(s.encode('ascii', 'replace').decode('ascii', 'replace'))


def option (s):
    # replace the very slow lightproof option
    return loopt.options.get(s, False)


def morph (rLoc, word, pattern, strict=True, noword=False):
    "if the pattern matches analyses of the input word, return the last matched substring"
    if not word:
        return noword
    if word not in analyses and not seekMorphFromHunspell(rLoc, word):
        return None
    a = analyses[word]
    result = None
    p = re.compile(pattern)
    if strict :
        for i in a:
            result = p.search(i)
            if result:
                result = result.group(0)
            else:
                return None
        return result
    else:
        for i in a:
            result = p.search(i)
            if result:
                return result.group(0)
        return None

def spell (rLoc, word):
    if not word:
        return None
    return spellchecker.isValid(word, rLoc, ())

def stem (rLoc, word):
    "get the tuple of the stem of the word or an empty list"
    global stems
    if not word:
        return []
    if not word in stems or len(stems[word]) == 0:
        x = spellchecker.spell(u"<?xml?><query type='stem'><word>" + word + "</word></query>", rLoc, ())
        if not x:
            return []
        t = x.getAlternatives()
        if not t:
            stems[word] = []
        else:
            stems[word] = list(t)
    return stems[word]

def generate (rLoc, word, example):
    "get the tuple of the morphological generation of a word or an empty list"
    if not word:
        return []
    x = spellchecker.spell(u"<?xml?><query type='generate'><word>" + word + "</word><word>" + example + "</word></query>", rLoc, ())
    if not x:
        return []
    t = x.getAlternatives()
    if not t:
        return []
    return list(t)

def suggest (rLoc, word):
    "get suggestions from the spellchecker"
    global suggestions
    if not word:
        return word
    if word not in suggestions:
        x = spellchecker.spell("_" + word, rLoc, ())
        if not x:
            return word
        t = x.getAlternatives()
        suggestions[word] = "|".join(t)
    return suggestions[word]

def word (s, n):
    "get the nth word of the input string or empty string"
    a = re.match(u"(?u)( +[-\w%]+){" + str(n-1) + u"} +([-\w%]+)", s)
    if not a:
        return ''
    return a.group(2)

def wordmin(s, n):
    "get the (-)nth word of the input string or empty string"
    a = re.search(u"(?u)([-\w%]+) +([-\w%]+ +){" + str(n-1) + u"}$", s)
    if not a:
        return ''
    return a.group(1)

def calc (funcname, par):
    global calcfunc
    global SMGR
    if calcfunc == None:
        calcfunc = SMGR.createInstance( "com.sun.star.sheet.FunctionAccess")
        if calcfunc == None:
                return None
    return calcfunc.callFunction(funcname, par)

def parse (sText, rLocale, nStartOfSentencePos, nSuggestedSentenceEndPos, rProperties):
    aErr = []
    # analyze by paragraph
    try:
        s, aErr = proofread(sText, rLocale, nStartOfSentencePos, nSuggestedSentenceEndPos, rProperties, 0)
        if s:
            sText = s
    except:
        raise

    # analyze by sentence
    lTuplePos = []
    nBOS = reBOP.match(sText).end() # nStartOfSentencePos
    for m in reEOS.finditer(sText):
        lTuplePos.append((nBOS, m.end()))
        nBOS = m.end()
    for tPos in lTuplePos:
        nl = tPos[1] - tPos[0]
        if nl > 4 and nl < 2000:
            for i in range(1, get_npass()):
                try:
                    s, errs = proofread(sText, rLocale, tPos[0], tPos[1], rProperties, i)
                    if s:
                        sText = sText[:tPos[0]] + s + sText[tPos[1]:]
                    aErr += errs
                except:
                    raise
    return tuple(aErr)

def proofread (TEXT, LOCALE, nStartOfSentencePos, nSuggestedSentenceEndPos, rProperties, nPass):
    aErrs = []
    s = TEXT[nStartOfSentencePos:nSuggestedSentenceEndPos]
    bChange = False
    
    if nPass == 1:
        if u" " in s:
            s = s.replace(u" ", u' ') # nbsp
            bChange = True
        if u" " in s:
            s = s.replace(u" ", u' ') # snbsp
            bChange = True
        if u"@" in s:
            s = s.replace(u"@", u' ')
            bChange = True
        if u"'" in s:
            s = s.replace(u"'", u"’")
            bChange = True
        if u"‑" in s:
            s = s.replace(u"‑", u"-") # nobreakdash
            bChange = True
    if nPass == 3:
        if u"@" in s:
            s = s.replace(u"@", u' ')
            bChange = True

    bIdRule = option('idrule')
    bConsole = option('console')

    for i in get_pp(nPass):
        # 0: regex,  1: replacement,  2: message,  3: condition,  4: ngroup,  5: ruleid numline/pass (str), 6: option, 7: URL, 8: case sensitive?
        for m in i[0].finditer(s):
            try:
                #if not i[3] or eval(i[3]):
                if not i[3] or GLOBALS[i[3]](LOCALE, s, m):
                    ln = m.end(i[4]) - m.start(i[4])
                    if i[1] == "*":
                        rep = " " * ln
                    elif i[1] == ">" or i[1] == "_" or i[1] == u"~":
                        rep = i[1] + " " * (ln-1)
                    elif i[1] == "@":
                        rep = "@" * ln
                    elif i[1][0:1] == "=":
                        if i[1][1:2] != "@":
                            #rep = eval(i[1][1:])
                            rep = GLOBALS[i[1][1:]](LOCALE, s, m)
                            rep = rep + " " * (ln-len(rep))
                        else:
                            #rep = eval(i[1][2:])
                            rep = GLOBALS[i[1][2:]](LOCALE, s, m)
                            rep = rep + "@" * (ln-len(rep))
                        if i[-1] and m.group(i[4])[0:1].isupper():
                            rep = rep.capitalize()
                    else:
                        rep = m.expand(i[1])
                        rep = rep + " " * (ln-len(rep))
                    s = s[0:m.start(i[4])] + rep + s[m.end(i[4]):]
                    bChange = True
                    if bConsole:
                        echo(u"> " + s + " -- "+ m.group(i[4]) + "  # " + i[5])
            except Exception as e:
                raise Exception(str(e), i[5])
    
    for i in get_rule(nPass):
        # 0: regex,  1: replacement,  2: message,  3: condition,  4: ngroup,  5: ruleid numline/pass (str), 6: option, 7: URL, 8: case insensitive?
        if i[0] and (not i[6] or option(i[6])) and not i[5] in aIgnoredRules:
            for m in i[0].finditer(s):
                try:
                    #if not i[3] or eval(i[3]):
                    if not i[3] or GLOBALS[i[3]](LOCALE, s, m):
                        xErr = SingleProofreadingError()
                        #xErr = uno.createUnoStruct( "com.sun.star.linguistic2.SingleProofreadingError" )
                        xErr.nErrorStart        = nStartOfSentencePos + m.start(i[4])
                        xErr.nErrorLength       = m.end(i[4]) - m.start(i[4])
                        xErr.nErrorType         = PROOFREADING
                        xErr.aRuleIdentifier    = i[5]
                        # suggestions
                        if i[1][0:1] == "=":
                            #sugg = eval(i[1][1:])
                            sugg = GLOBALS[i[1][1:]](LOCALE, s, m)
                            if sugg:
                                if i[-1] and m.group(i[4])[0:1].isupper():
                                    xErr.aSuggestions = tuple(cap(sugg.split("|")))
                                else:
                                    xErr.aSuggestions = tuple(sugg.split("|"))
                            else:
                                xErr.aSuggestions = ()
                        elif i[1] == "_":
                            xErr.aSuggestions = ()
                        else:
                            if i[-1] and m.group(i[4])[0:1].isupper():
                                xErr.aSuggestions = tuple(cap(m.expand(i[1]).split("|")))
                            else:
                                xErr.aSuggestions = tuple(m.expand(i[1]).split("|"))
                        # comment
                        if i[2][0:1] == "=":
                            #comment = eval(i[2][1:])
                            comment = GLOBALS[i[2][1:]](LOCALE, s, m)
                        else:
                            comment = m.expand(i[2])
                        xErr.aShortComment      = comment   # comment.split("|")[0]     # in context menu
                        xErr.aFullComment       = comment   # comment.split("|")[-1]    # in dialog
                        if bIdRule:
                            xErr.aShortComment += "  # " + i[5]
                        # URL
                        if i[7]:
                            p = PropertyValue()
                            p.Name = "FullCommentURL"
                            p.Value = i[7]
                            xErr.aProperties    = (p,)
                        else:
                            xErr.aProperties    = ()
                        aErrs.append(xErr)
                except Exception as e:
                    raise Exception(str(e), i[5])
    if bChange:
        return (s, tuple(aErrs))
    return (False, tuple(aErrs))

def cap (a):
    for i in range(0, len(a)):
        a[i] = a[i].capitalize()
    return a

def compile_rules (dic):
    for i in dic:
        try:
            i[0] = re.compile(i[0])
        except:
            echo("Lightproof: bad regular expression: ", traceback.format_exc())
            i[0] = None

def get_npass ():
    try:
        return len(langrule[pkg].dic)
    except:
        langrule[pkg] = __import__("lightproof_" + pkg)
        for i in range(len(langrule[pkg].dic)):
            compile_rules(langrule[pkg].dic[i])
            compile_rules(langrule[pkg].pp[i])
    return len(langrule[pkg].dic)

def get_rule (n):
    try:
        return langrule[pkg].dic[n]
    except:
        langrule[pkg] = __import__("lightproof_" + pkg)
        for i in range(len(langrule[pkg].dic)):
            compile_rules(langrule[pkg].dic[i])
            compile_rules(langrule[pkg].pp[i])
    return langrule[pkg].dic[n]

def get_pp (n):
    try:
        return langrule[pkg].pp[n]
    except:
        langrule[pkg] = __import__("lightproof_" + pkg)
        for i in range(len(langrule[pkg].dic)):
            compile_rules(langrule[pkg].dic[i])
            compile_rules(langrule[pkg].pp[i])
    return langrule[pkg].pp[n]

def get_path():
    return os.path.join(os.path.dirname(sys.modules[__name__].__file__), __name__ + ".py")


#### GRAMMALECTE

def seekMorphFromHunspell (rLoc, word):
    "retrieves morphologies list from Hunspell -> analyses"
    global analyses
    x = spellchecker.spell(u"<?xml?><query type='analyze'><word>" + word + "</word></query>", rLoc, ())
    if x:
        t = x.getAlternatives()
        if not t:
            if not analyses: # fix synchronization problem (missing alternatives with unloaded dictionary)
                return False
            analyses[word] = []
        else:
            analyses[word] = t[0].split("</a>")[:-1]
        return True
    return False

def rewriteSubject (rLoc, s1, s2):
    # s1 is supposed to be prn/patr/npr
    if s2 == "lui":
        return "ils"
    if s2 == "moi":
        return "nous"
    if s2 == "toi":
        return "vous"
    if s2 == "nous":
        return "nous"
    if s2 == "vous":
        return "vous"
    if s2 == "eux":
        return "ils"
    if s2 == "elle" or s2 == "elles":
        # We don’t check if word exists in analyses, for it is assumed it has been done before
        if cr.mbNprMasNotFem(analyses.get(s1, "")):
            return "ils"
        # si épicène, indéterminable, mais OSEF, le féminin l’emporte
        return "elles"
    return s1 + " et " + s2

def word1 (s):
    "get next word (optimization)"
    m = cr.NextWord.match(s)
    if not m:
        return ''
    return m.group(1)

def wordmin1 (s):
    "get previous word (optimization)"
    m = cr.PrevWord.search(s)
    if not m:
        return ''
    return m.group(1)

def look (s, p, np=None):
    "seek pattern p in s (before/after/fulltext), if antipattern np not in s"
    if np and re.search(np, s):
        return False
    if re.search(p, s):
        return True
    return False

def lookchk1 (rLoc, s, p, pmg1, negpmg1=None):
    "returns True if s has pattern p and m.group(1) has pattern pmg1"
    m = re.search(p, s)
    if not m:
        return False
    try:
        word = m.group(1)
    except:
        #print("Missing group 1")
        return False
    if word not in analyses and not seekMorphFromHunspell(rLoc, word):
        return False
    a = analyses[word]
    if not a:
        return False
    if negpmg1:
        # check negative condition
        np = re.compile(negpmg1)
        for s in a:
            if np.search(s):
                return False
    # search pattern
    rp = re.compile(pmg1)
    for s in a:
        if rp.search(s):
            return True
    return False

def stemchk (rLoc, word, pattern, strict=True, noword=False):
    "check pattern on stem(s) of word"
    global stems
    if not word:
        return noword
    if word not in stems or len(stems[word]) == 0:
        x = spellchecker.spell(u"<?xml?><query type='stem'><word>" + word + "</word></query>", rLoc, ())
        if not x:
            return False
        t = x.getAlternatives()
        if not t:
            t = ['']
        stems[word] = t
    a = stems[word]
    p = re.compile(pattern)
    if strict:
        for s in a:
            if not p.search(s):
                return False
        return True
    else:
        for s in a:
            if p.search(s):
                return True
        return False

def morphex (rLoc, word, pattern, negpattern, noword=False):
    "returns True if not negpattern in word and pattern in word"
    if not word:
        return noword
    if word not in analyses and not seekMorphFromHunspell(rLoc, word):
        return False
    a = analyses[word]
    if not a:
        return False
    # check negative condition
    np = re.compile(negpattern)
    for s in a:
        if np.search(s):
            return False
    # search pattern
    p = re.compile(pattern)
    for s in a:
        if p.search(s):
            return True
    return False

def apposition (word1, word2):
    "returns True if nom + nom (no agreement required)"
    # We don’t check if word exists in analyses, for it is assumed it has been done before
    return cr.mbNomNotAdj(analyses.get(word2, "")) and cr.mbPpasNomNotAdj(analyses.get(word1, ""))

def isAmbiguousNAV (rLoc, word):
    "words which are nom|adj and verb are ambiguous (except être and avoir)"
    if word not in analyses and not seekMorphFromHunspell(rLoc, word):
        return False
    a = analyses[word]
    if not a:
        return False
    if not cr.mbNomAdj(a):
        return False
    if cr.mbVconj123(a) and not cr.mbMG(a):
        return True
    return False

def isAmbiguousAndWrong (sWord1, sWord2, sReqMorphNA, sReqMorphConj):
    "use it if sWord1 won’t be a verb; word2 is assumed to be True via isAmbiguousNAV"
    # We don’t check if word exists in analyses, for it is assumed it has been done before
    a2 = analyses.get(sWord2, [])
    if not a2:
        return False
    if cr.checkConjVerb(a2, sReqMorphConj):
        # verb word2 is ok
        return False
    a1 = analyses.get(sWord1, [])
    if not a1:
        return False
    if cr.checkAgreement(a1, a2, sReqMorphNA) and (cr.mbAdj(a2) or cr.mbAdj(a1)):
        return False
    return True

def isVeryAmbiguousAndWrong (sWord1, sWord2, sReqMorphNA, sReqMorphConj, bLastHopeCond):
    "use it if sWord1 can be also a verb; word2 is assumed to be True via isAmbiguousNAV"
    # We don’t check if word exists in analyses, for it is assumed it has been done before
    a2 = analyses.get(sWord2, [])
    if not a2:
        return False
    if cr.checkConjVerb(a2, sReqMorphConj):
        # verb word2 is ok
        return False
    a1 = analyses.get(sWord1, [])
    if not a1:
        return False
    if cr.checkAgreement(a1, a2, sReqMorphNA) and (cr.mbAdj(a2) or cr.mbAdjNb(a1)):
        return False
    # now, we know there no agreement, and conjugation is also wrong
    if cr.isNomAdj(a1):
        return True
    #if cr.isNomAdjVerb(a1): # considered True
    if bLastHopeCond:
        return True
    return False

def undoLigature (c):
    if c == u"ﬁ":
        return "fi"
    elif c == u"ﬂ":
        return "fl"
    elif c == u"ﬀ":
        return "ff"
    elif c == u"ﬃ":
        return "ffi"
    elif c == u"ﬄ":
        return "ffl"
    elif c == u"ﬅ":
        return "ft"
    elif c == u"ﬆ":
        return "st"
    return "_"

#### Syntagmes

reENDOFNG1 = re.compile(u" +(?:, +|)(?:n(?:’|e |o(?:u?s|tre) )|l(?:’|e(?:urs?|s|) |a )|j(?:’|e )|m(?:’|es? |a |on )|t(?:’|es? |a |u )|s(?:’|es? |a )|c(?:’|e(?:t|tte|s|) )|ç(?:a |’)|ils? |vo(?:u?s|tre) )")
reENDOFNG2 = re.compile(r" +(\w[\w-]+)")
reENDOFNG3 = re.compile(r" *, +(\w[\w-]+)")

def isEndOfNG (rLoc, s):
    if reENDOFNG1.match(s):
        return True
    m = reENDOFNG2.match(s)
    if m and morphex(rLoc, m.group(1), "po:(?:v|prep)", "po:(?:nom|adj|pp)"):
        return True
    m = reENDOFNG3.match(s)
    if m and not morph(rLoc, m.group(1), "po:(?:nom|adj)", False):
        return True
    return False


#### Suggestion mechanisms

## verbs

def suggVerb (rLoc, sFlex, sWho, funcSugg2=None):
    aSugg = set()
    for sStem in stem(rLoc, sFlex):
        tTags = conjfr.getTags(sStem)
        if tTags:
            # we get the tense
            aTense = set()
            for sMorph in analyses.get(sFlex, []): # we don’t check if word exists in analyses, for it is assumed it has been done before
                for m in re.finditer(sStem+" .*po:(i(?:pre|imp|psi|fut|nfi)|s(?:pre|imp)|cond)", sMorph):
                    # stem must be used in regex to prevent confusion between different verbs (i.e. sauras has 2 stems: savoir and saurer)
                    if m:
                        if m.group(1) == "infi":
                            aTense.add("ipre")
                            aTense.add("iimp")
                            aTense.add("ipsi")
                        else:
                            aTense.add(m.group(1))
            for sTense in aTense:
                if sWho == "1isg" and not conjfr.hasConjWithTags(tTags, sTense, "1isg"):
                    sWho = "1sg"
                if conjfr.hasConjWithTags(tTags, sTense, sWho):
                    aSugg.add(conjfr.getConjWithTags(sStem, tTags, sTense, sWho))
    if funcSugg2:
        aSugg2 = funcSugg2(rLoc, sFlex)
        if aSugg2:
            aSugg.add(aSugg2)
    if aSugg:
        return u"|".join(aSugg)
    return ""

def suggVerbPpas (rLoc, sFlex, sWho=None):
    aSugg = set()
    for sStem in stem(rLoc, sFlex):
        tTags = conjfr.getTags(sStem)
        if tTags:
            aSugg.add(conjfr.getConjWithTags(sStem, tTags, "pp", "ppas1"))
            aSugg.add(conjfr.getConjWithTags(sStem, tTags, "pp", "ppas2"))
            aSugg.add(conjfr.getConjWithTags(sStem, tTags, "pp", "ppas3"))
            aSugg.add(conjfr.getConjWithTags(sStem, tTags, "pp", "ppas4"))
            aSugg.discard("")
    if aSugg:
        return u"|".join(aSugg)
    return ""

def suggVerbInfi (rLoc, sFlex):
    return u"|".join(stem(rLoc, sFlex))

dWHOIS = { "je": "1sg", u"j’": "1sg", "tu": "2sg", "il": "3sg", "on": "3sg", "elle": "3sg", "nous": "1pl", "vous": "2pl", "ils": "3pl", "elles": "3pl" }
lMODEINDIC = ["ipre", "iimp", "ipsi", "ifut"]
lMODESUBJ = ["spre", "simp"]

def suggVerbMode (rLoc, sFlex, cMode, sSuj):
    if cMode == "i":
        lMode = lMODEINDIC
    elif cMode == "s":
        lMode = lMODESUBJ
    else:
        return ""
    sWho = dWHOIS.get(sSuj.lower(), None)
    if not sWho:
        if sSuj[0:1].islower():
            return ""
        sWho = "3sg"
    aSugg = set()
    for sStem in stem(rLoc, sFlex):
        tTags = conjfr.getTags(sStem)
        if tTags:
            for sTense in lMode:
                if conjfr.hasConjWithTags(tTags, sTense, sWho):
                    aSugg.add(conjfr.getConjWithTags(sStem, tTags, sTense, sWho))
    if aSugg:
        return u"|".join(aSugg)
    return ""

## Nouns and adjectives

def suggPlur (rLoc, sFlex, sWordToAgree=None):
    "returns plural forms assuming sFlex is singular"
    if sWordToAgree:
        if sWordToAgree not in analyses and not seekMorphFromHunspell(rLoc, sWordToAgree):
            return ""
        sGender = cr.getGender(analyses[sWordToAgree])
        if sGender == "mas":
            return suggMasPlur(rLoc, sFlex)
        elif sGender == "fem":
            return suggFemPlur(rLoc, sFlex)
    aSugg = set()
    if "-" not in sFlex:
        if sFlex.endswith("l"):
            if sFlex.endswith("al") and len(sFlex) > 2 and spellchecker.isValid(sFlex[:-1]+"ux", rLoc, ()):
                aSugg.add(sFlex[:-1]+"ux")
            if sFlex.endswith("ail") and len(sFlex) > 3 and spellchecker.isValid(sFlex[:-2]+"ux", rLoc, ()):
                aSugg.add(sFlex[:-2]+"ux")
        if spellchecker.isValid(sFlex+"s", rLoc, ()):
            aSugg.add(sFlex+"s")
        if spellchecker.isValid(sFlex+"x", rLoc, ()):
            aSugg.add(sFlex+"x")
    if mfspfr.isMiscPlural(sFlex):
        aSugg.add(mfspfr.getMiscPlural(sFlex))
    if aSugg:
        return u"|".join(aSugg)
    return ""

def suggSing (rLoc, sFlex):
    "returns singular forms assuming sFlex is plural"
    if "-" in sFlex:
        return ""
    aSugg = set()
    if sFlex.endswith("ux"):
        if spellchecker.isValid(sFlex[:-2]+"l", rLoc, ()):
            aSugg.add(sFlex[:-2]+"l")
        if spellchecker.isValid(sFlex[:-2]+"il", rLoc, ()):
            aSugg.add(sFlex[:-2]+"il")
    if spellchecker.isValid(sFlex[:-1], rLoc, ()):
        aSugg.add(sFlex[:-1])
    if aSugg:
        return u"|".join(aSugg)
    return ""

def suggMasSing (rLoc, sFlex):
    "returns masculine singular forms"
    aSugg = set()
    for sMorph in analyses.get(sFlex, []): # we don’t check if word exists in analyses, for it is assumed it has been done before
        if not "po:v" in sMorph:
            # not a verb
            if "is:mas" in sMorph or "is:epi" in sMorph:
                aSugg.add(suggSing(rLoc, sFlex))
            else:
                sStem = cr.getLemmaOfMorph(sMorph)
                if mfspfr.isFemForm(sStem):
                    aSugg.add(mfspfr.getMasForm(sStem, False))
        else:
            # a verb
            sVerb = cr.getLemmaOfMorph(sMorph)
            if conjfr.hasConj(sVerb, "pp", "ppas1"):
                aSugg.add(conjfr.getConj(sVerb, "pp", "ppas1"))
    if aSugg:
        return u"|".join(aSugg)
    return ""

def suggMasPlur (rLoc, sFlex):
    "returns masculine plural forms"
    aSugg = set()
    for sMorph in analyses.get(sFlex, []): # we don’t check if word exists in analyses, for it is assumed it has been done before
        if not "po:v" in sMorph:
            # not a verb
            if "is:mas" in sMorph or "is:epi" in sMorph:
                aSugg.add(suggPlur(rLoc, sFlex))
            else:
                sStem = cr.getLemmaOfMorph(sMorph)
                if mfspfr.isFemForm(sStem):
                    aSugg.add(mfspfr.getMasForm(sStem, True))
        else:
            # a verb
            sVerb = cr.getLemmaOfMorph(sMorph)
            if conjfr.hasConj(sVerb, "pp", "ppas2"):
                aSugg.add(conjfr.getConj(sVerb, "pp", "ppas2"))
            elif conjfr.hasConj(sVerb, "pp", "ppas1"):
                aSugg.add(conjfr.getConj(sVerb, "pp", "ppas1"))
    if aSugg:
        return u"|".join(aSugg)
    return ""

def suggFemSing (rLoc, sFlex):
    "returns feminine singular forms"
    aSugg = set()
    for sMorph in analyses.get(sFlex, []): # we don’t check if word exists in analyses, for it is assumed it has been done before
        if not "po:v" in sMorph:
            # not a verb
            if "is:fem" in sMorph or "is:epi" in sMorph:
                aSugg.add(suggSing(rLoc, sFlex))
            else:
                sStem = cr.getLemmaOfMorph(sMorph)
                if mfspfr.isFemForm(sStem):
                    aSugg.add(sStem)
        else:
            # a verb
            sVerb = cr.getLemmaOfMorph(sMorph)
            if conjfr.hasConj(sVerb, "pp", "ppas3"):
                aSugg.add(conjfr.getConj(sVerb, "pp", "ppas3"))
    if aSugg:
        return u"|".join(aSugg)
    return ""

def suggFemPlur (rLoc, sFlex):
    "returns feminine plural forms"
    aSugg = set()
    for sMorph in analyses.get(sFlex, []): # we don’t check if word exists in analyses, for it is assumed it has been done before
        if not "po:v" in sMorph:
            # not a verb
            if "is:fem" in sMorph or "is:epi" in sMorph:
                aSugg.add(suggPlur(rLoc, sFlex))
            else:
                sStem = cr.getLemmaOfMorph(sMorph)
                if mfspfr.isFemForm(sStem):
                    aSugg.add(sStem+"s")
        else:
            # a verb
            sVerb = cr.getLemmaOfMorph(sMorph)
            if conjfr.hasConj(sVerb, "pp", "ppas4"):
                aSugg.add(conjfr.getConj(sVerb, "pp", "ppas4"))
    if aSugg:
        return u"|".join(aSugg)
    return ""

def switchGender (rLoc, sFlex, bPlur=None):
    # we don’t check if word exists in analyses, for it is assumed it has been done before
    aSugg = set()
    if bPlur == None:
        for sMorph in analyses.get(sFlex, []): 
            if "is:fem" in sMorph:
                if "is:sg" in sMorph:
                    aSugg.add(suggMasSing(rLoc, sFlex))
                elif "is:pl" in sMorph:
                    aSugg.add(suggMasPlur(rLoc, sFlex))
            elif "is:mas" in sMorph:
                if "is:sg" in sMorph:
                    aSugg.add(suggFemSing(rLoc, sFlex))
                elif "is:pl" in sMorph:
                    aSugg.add(suggFemPlur(rLoc, sFlex))
                else:
                    aSugg.add(suggFemSing(rLoc, sFlex))
                    aSugg.add(suggFemPlur(rLoc, sFlex))
    elif bPlur:
        for sMorph in analyses.get(sFlex, []):
            if "is:fem" in sMorph:
                aSugg.add(suggMasPlur(rLoc, sFlex))
            elif "is:mas" in sMorph:
                aSugg.add(suggFemPlur(rLoc, sFlex))
    else:
        for sMorph in analyses.get(sFlex, []):
            if "is:fem" in sMorph:
                aSugg.add(suggMasSing(rLoc, sFlex))
            elif "is:mas" in sMorph:
                aSugg.add(suggFemSing(rLoc, sFlex))

    if aSugg:
        return u"|".join(aSugg)
    return ""

def switchPlural (rLoc, sFlex):
    aSugg = set()
    for sMorph in analyses.get(sFlex, []): # we don’t check if word exists in analyses, for it is assumed it has been done before
        if "is:sg" in sMorph:
            aSugg.add(suggPlur(rLoc, sFlex))
        elif "is:pl" in sMorph:
            aSugg.add(suggSing(rLoc, sFlex))
    if aSugg:
        return u"|".join(aSugg)
    return ""


#### Check date validity

lDAY = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
dMONTH = { "janvier":1, u"février":2, "mars":3, "avril":4, "mai":5, "juin":6, "juillet":7, u"août":8, "aout":8, "septembre":9, "octobre":10, "novembre":11, u"décembre":12 }

import datetime

def checkDate (day, month, year):
    "to use if month is a number"
    try:
        return datetime.date(int(year), int(month), int(day))
    except ValueError:
        return False
    except:
        #traceback.print_exc()
        return True

def checkDateWithString (day, month, year):
    "to use if month is a noun"
    try:
        return datetime.date(int(year), dMONTH.get(month, ""), int(day))
    except ValueError:
        return False
    except:
        #traceback.print_exc()
        return True

def checkDay (weekday, day, month, year):
    "to use if month is a number"
    dt = checkDate(day, month, year)
    if dt and lDAY[dt.weekday()] != weekday.lower():
        return False
    return True
        
def checkDayWithString (weekday, day, month, year):
    "to use if month is a noun"
    dt = checkDate(day, dMONTH.get(month, ""), year)
    if dt and lDAY[dt.weekday()] != weekday.lower():
        return False
    return True

def getDay (day, month, year):
    "to use if month is a number"
    return lDAY[datetime.date(int(year), int(month), int(day)).weekday()]

def getDayWithString (day, month, year):
    "to use if month is a noun"
    return lDAY[datetime.date(int(year), dMONTH.get(month, ""), int(day)).weekday()]


# [code]


# Exceptions
aREGULARPLURAL = frozenset(["abricot", "amarante", "aubergine", "acajou", "anthracite", "brique", "caca", u"caf\xe9", "carotte", "cerise", "chataigne", "corail", "citron", u"cr\xe8me", "grave", "groseille", "jonquille", "marron", "pervenche", "prune", "sable"])
aSHOULDBEVERB = frozenset(["aller", "manger"]) 

def show (s):
    "Debug: \xe0 utiliser dans les conditions pour affichage"
    echo("SHOW: " + s)
    return True

def ceOrCet (s):
    if re.match("(?i)[ae\xe9\xe8\xeaiouy\xe2\xee\xef]", s):
        return "cet"
    if s[0:1] == "h" or s[0:1] == "H":
        return "ce|cet"
    return "ce"

def formatNumber (s):
    nLen = len(s)
    if nLen == 10:
        sRes = s[0] + u"\xa0" + s[1:4] + u"\xa0" + s[4:7] + u"\xa0" + s[7:]                                  # nombre ordinaire
        if s.startswith("0"):
            sRes += u"|" + s[0:2] + u"\xa0" + s[2:4] + u"\xa0" + s[4:6] + u"\xa0" + s[6:8] + u"\xa0" + s[8:]    # t\xe9l\xe9phone fran\xe7ais
            if s[1] == "4" and (s[2]=="7" or s[2]=="8" or s[2]=="9"):
                sRes += u"|" + s[0:4] + u"\xa0" + s[4:6] + u"\xa0" + s[6:8] + u"\xa0" + s[8:]                # mobile belge
            sRes += u"|" + s[0:3] + u"\xa0" + s[3:6] + u"\xa0" + s[6:8] + u"\xa0" + s[8:]                    # t\xe9l\xe9phone suisse
        sRes += u"|" + s[0:4] + u"\xa0" + s[4:7] + "-" + s[7:]                                         # t\xe9l\xe9phone canadien ou am\xe9ricain
        return sRes 
    elif nLen == 9:
        sRes = s[0:3] + u"\xa0" + s[3:6] + u"\xa0" + s[6:]                                                # nombre ordinaire
        if s.startswith("0"):
            sRes += "|" + s[0:3] + u"\xa0" + s[3:5] + u"\xa0" + s[5:7] + u"\xa0" + s[7:9]                    # fixe belge 1
            sRes += "|" + s[0:2] + u"\xa0" + s[2:5] + u"\xa0" + s[5:7] + u"\xa0" + s[7:9]                    # fixe belge 2
        return sRes
    elif nLen < 4:
        return ""
    sRes = ""
    nEnd = nLen
    while nEnd > 0:
        nStart = max(nEnd-3, 0)
        if not sRes:
            sRes = s[nStart:nEnd]
        else:
            sRes = s[nStart:nEnd] + u"\xa0" + sRes
        nEnd = nEnd - 3
    return sRes

def formatNF (s):
    try:
        m = re.match(u"NF[ \xa0-]?(C|E|P|Q|S|X|Z|EN(?:[ \xa0-]ISO|))[ \xa0-]?([0-9]+(?:[/\u2011-][0-9]+|))", s)
        if not m:
            return ""
        return u"NF\xa0" + m.group(1).upper().replace(" ", u"\xa0").replace("-", u"\xa0") + u"\xa0" + m.group(2).replace("/", u"\u2011").replace("-", u"\u2011")
    except:
        traceback.print_exc()
        return ""
    



# generated code, do not edit
def r63 (LOCALE, s, m):
    return m.group(1).replace(".", "")+"."
def r64 (LOCALE, s, m):
    return m.group(0).replace(".", "")
def c66 (LOCALE, s, m):
    return not re.match("(?i)etc", m.group(1))
def c87 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "po:(?:det|prep)", False)
def c92 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:prn|nom)", False)
def r104 (LOCALE, s, m):
    return m.group(2).capitalize()
def c104 (LOCALE, s, m):
    return not re.match(u"(?i)(etc|[A-Z]|chap|cf|fig|hab|litt|circ|coll|ref|étym|suppl|bibl|bibliogr|cit|op|vol|déc|nov|oct|janv|juil|avr|sept)$", m.group(1)) and morph(LOCALE,m.group(1), "po:") and morph(LOCALE,m.group(2), "po:")
def c114 (LOCALE, s, m):
    return not m.group(1).isdigit()
def r118 (LOCALE, s, m):
    return m.group(0).replace("...", u"…").rstrip(".")
def c123 (LOCALE, s, m):
    return not re.match(u"(etc|[A-Z]|fig|hab|litt|circ|coll|ref|étym|suppl|bibl|bibliogr|cit|vol|déc|nov|oct|janv|juil|avr|sept)$", m.group(1))
def r132 (LOCALE, s, m):
    return m.group(1)[1]
def r155 (LOCALE, s, m):
    return undoLigature(m.group(0))
def r180 (LOCALE, s, m):
    return m.group(2)[:-1]+u"’"
def c180 (LOCALE, s, m):
    return not option("mapos") and morph(LOCALE,m.group(3), "po:v", False)
def r183 (LOCALE, s, m):
    return m.group(1)[:-1]+u"’"
def c183 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v", False)
def r188 (LOCALE, s, m):
    return m.group(1)[:-1]+u"’"
def c188 (LOCALE, s, m):
    return option("mapos") and not look(s[:m.start()],u"(?i)(?:lettre|caractère|glyphe|dimension|variable|fonction|point) *$")
def c195 (LOCALE, s, m):
    return not re.match(u"([oO]nz|[éÉ]nième|[AÂEÉÊIÎOÔU])", m.group(2)) and not morph(LOCALE,m.group(2), "po:mg", False)
def c197 (LOCALE, s, m):
    return not re.match(u"(?i)(onz|énième)", m.group(2)) and morph(LOCALE,m.group(2), "is:(?:mas|epi)")
def r204 (LOCALE, s, m):
    return formatNF(m.group(0))
def c204 (LOCALE, s, m):
    return not re.match(u"NF (?:C|E|P|Q|S|X|Z|EN(?: ISO|)) [0-9]+(?:‑[0-9]+|)", m.group(0))
def r209 (LOCALE, s, m):
    return m.group(0).replace("2", u"₂").replace("3", u"₃").replace("4", u"₄")
def c215 (LOCALE, s, m):
    return not checkDate(m.group(1),m.group(2),m.group(3)) and not re.match("(?i)versions?", wordmin1(s[:m.start()]))
def c218 (LOCALE, s, m):
    return not checkDateWithString(m.group(1),m.group(2),m.group(3))
def r221 (LOCALE, s, m):
    return getDay(m.group(2),m.group(3),m.group(4))
def c221 (LOCALE, s, m):
    return not checkDay(m.group(1),m.group(2),m.group(3),m.group(4))
def r224 (LOCALE, s, m):
    return getDayWithString(m.group(2),m.group(3),m.group(4))
def c224 (LOCALE, s, m):
    return not checkDayWithString(m.group(1),m.group(2),m.group(3),m.group(4))
def c242 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), ":v0", False) or m.group(1) == "en"
def c247 (LOCALE, s, m):
    return spell(LOCALE,m.group(1)+"-"+m.group(2)) and morph(LOCALE,m.group(1)+"-"+m.group(2), "po:", False)
def c250 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:n(?:om|b)", False)
def c251 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:n(?:om|b)", False) and not word1(s[m.end():])
def c254 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:nom") and not re.match(u"(?i)(aequo|nihilo|cathedra|absurdo|abrupto)", m.group(1))
def c256 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj|ppas)", False)
def c257 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:nom", "po:(?:adj|mg|adv)")
def c260 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj|ppas)", False)
def c262 (LOCALE, s, m):
    return spell(LOCALE,m.group(1)+"-"+m.group(2)) and morph(LOCALE,m.group(1)+"-"+m.group(2), "po:", False)
def c266 (LOCALE, s, m):
    return spell(LOCALE,m.group(1)+"-"+m.group(2)) and morph(LOCALE,m.group(1)+"-"+m.group(2), "po:", False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:det", False, not bool(re.match("(?i)s(?:ans|ous)$",m.group(1))))
def c270 (LOCALE, s, m):
    return spell(LOCALE,m.group(1)+"-"+m.group(2)) and morph(LOCALE,m.group(1)+"-"+m.group(2), "po:nom", False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:det|v0e)", False, True) and not (morph(LOCALE,m.group(1), "po:mg", False) and morph(LOCALE,m.group(2), "po:(?:mg|infi|nb)", False))
def r277 (LOCALE, s, m):
    return m.group(0).replace(" ", "-")
def r278 (LOCALE, s, m):
    return m.group(0).replace(" ", "-")
def c289 (LOCALE, s, m):
    return morph(LOCALE,wordmin1(s[:m.start()]), "po:cjsub", False, True)
def r294 (LOCALE, s, m):
    return m.group(0).replace(" ", "-")
def c300 (LOCALE, s, m):
    return not word1(s[m.end():])
def c302 (LOCALE, s, m):
    return morph(LOCALE,wordmin1(s[:m.start()]), "po:mg")
def c306 (LOCALE, s, m):
    return re.match("(?i)(les?|du|des|un|ces?|[mts]on)", wordmin1(s[:m.start()]))
def c312 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), ":det", False)
def c314 (LOCALE, s, m):
    return not (morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False) and look(s[m.end():]," *qu[e’]"))
def r361 (LOCALE, s, m):
    return m.group(0).replace(" ", "-").replace("vingts", "vingt")
def r363 (LOCALE, s, m):
    return m.group(0).replace(" ", "-")
def r365 (LOCALE, s, m):
    return m.group(0).replace(" ", "-").replace("vingts", "vingt")
def c376 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:det", False, False)
def r391 (LOCALE, s, m):
    return formatNumber(m.group(0))
def c391 (LOCALE, s, m):
    return not look(s[:m.start()],u"NF[  -]?(C|E|P|Q|X|Z|EN(?:[  -]ISO|)) *")
def r397 (LOCALE, s, m):
    return m.group(0).replace("O", "0")
def r398 (LOCALE, s, m):
    return m.group(0).replace("O", "0")
def c413 (LOCALE, s, m):
    return not re.match(u"(?i)([nv]ous|faire|en|la|lui|donnant|œuvre|h[éo]|olé|joli|Bora|couvent|dément|sapiens|très|vroum|[0-9]+)$", m.group(1)) and not (re.match("(?:est|une?)$", m.group(1)) and look(s[:m.start()],u"[’']$")) and not (m.group(1) == "mieux" and look(s[:m.start()],"(?i)qui +$"))
def c445 (LOCALE, s, m):
    return not re.match("(?i)avoir$", m.group(1)) and "avoir" in stem(LOCALE,m.group(1))
def c452 (LOCALE, s, m):
    return not (word1(s[m.end():]) == "en" and "avoir" in stem(LOCALE,word(s[m.end():],2))) and not (word(s[m.end():],2) == "en" and "avoir" in stem(LOCALE,word(s[m.end():],3)))
def c483 (LOCALE, s, m):
    return "abolir" in stem(LOCALE,m.group(1))
def c485 (LOCALE, s, m):
    return "achever" in stem(LOCALE,m.group(1))
def c486 (LOCALE, s, m):
    return not look(s[m.end():],r" +de?\b")
def c495 (LOCALE, s, m):
    return morph(LOCALE,wordmin1(s[:m.start()]), "(po:adj|st:un)", False)
def c501 (LOCALE, s, m):
    return stem(LOCALE,m.group(1)) == ["comparer"]
def c503 (LOCALE, s, m):
    return stem(LOCALE,m.group(1)) == ["contraindre"]
def c514 (LOCALE, s, m):
    return stem(LOCALE,m.group(1)) == ["joindre"]
def c540 (LOCALE, s, m):
    return stem(LOCALE,m.group(1)) == ["suffire"]
def c546 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(prévenir|prévoir|prédire|présager|préparer|pressentir|pronostiquer|avertir|devancer|réserver)$", False)
def c551 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(ajourner|différer|reporter)$", False)
def c560 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), u"(?:po:(?:mg|v0)|st:(?:t(?:antôt|emps|rès)|loin|souvent|parfois|quelquefois|côte) )", False) and not m.group(1)[0].isupper()
def c591 (LOCALE, s, m):
    return not morph(LOCALE,m.group(0), "po:", False)
def c594 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:det", False)
def c595 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:adj", False) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:det", False)
def r621 (LOCALE, s, m):
    return m.group(1)[:-1]+"’"
def c621 (LOCALE, s, m):
    return option("mapos")
def c636 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v.* po:(?:infi|[123](?:sg|pl))") and m.group(2)[0].islower()
def c640 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v.* po:(?:infi|[123](?:sg|pl))") and m.group(1)[0].islower() and not wordmin1(s[:m.start()])
def c644 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:[1-3](?:sg|pl)", "po:(?:nom|adj)")
def c648 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:infi|[123](?:sg|pl))") and not look(s[:m.start()],"(?i)(dont|sauf) +$")
def c650 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v.* po:[123](?:sg|pl)") and m.group(1)[0].islower()
def c654 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v.* po:[123](?:sg|pl)") and m.group(1)[0].islower()
def c658 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:mas|epi)", "po:(?:infi|mg)") and m.group(2)[0].islower()
def c664 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "po:ppre", False)
def c665 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj)")
def c670 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj)")
def c679 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:adv", "po:(?:3pl|mg)") and morphex(LOCALE,m.group(2), "po:(?:[123](?:sg|pl))", "po:(?:mg|adj|ppas|adv)")
def c683 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:infi|[123](?:sg|pl))", "po:(?:mg|adj|ppas|adv)")
def c687 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:[123](?:sg|pl))", "po:(?:mg|nom|adj|ppas|adv|prn|patr)")
def c691 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:[123](?:sg|pl))", "po:(?:mg|nom|adj|ppas|adv|prn|patr|titr)")
def c695 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:infi|[123](?:sg|pl))", "po:(?:mg|adj|ppas|adv)") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:v[123].* po:[123](?:sg|pl)", False, False)
def c701 (LOCALE, s, m):
    return morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:v|nom)", False, True)
def c702 (LOCALE, s, m):
    return not wordmin1(s[:m.start()])
def c705 (LOCALE, s, m):
    return not re.match("(?i)([lmts]a|leur|une)", wordmin1(s[:m.start()]))
def c707 (LOCALE, s, m):
    return stem(LOCALE,m.group(1)) == [u"être"]
def c724 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:3sg", False, False)
def c727 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:3sg|prep)", False, False) and not morph(LOCALE,word1(s[m.end():]), "po:properobj", False)
def c732 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:ppas", "po:(?:prn|patr|npr)")
def c735 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj)", "po:infi")
def c738 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj)", "po:infi")
def c744 (LOCALE, s, m):
    return not look(s[:m.start()],r"(?i)\bce que?\b")
def c746 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:prn|patr|det|properobj)")
def c749 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:[123](?:sg|pl)", True)
def c753 (LOCALE, s, m):
    return not look(s[:m.start()],u"(?i)[ln]’$|(?<!-)\\b(?:il|elle|on|y|n’en) +$")
def c756 (LOCALE, s, m):
    return not look(s[:m.start()],u"(?i)(\\bque?\\b|[ln]’$|(?<!-)\\b(?:il|elle|on|y|n’en) +$)")
def c759 (LOCALE, s, m):
    return not look(s[:m.start()],u"(?i)(\\bque?\\b|[ln]’$|(?<!-)\\b(?:il|elle|on|y|n’en) +$)")
def c762 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:infi", False) and not look(s[:m.start()],u"(?i)\\b(que?)\\b|(?:il|elle|on|n’(?:en|y)) +$")
def c768 (LOCALE, s, m):
    return not wordmin1(s[:m.start()])
def c773 (LOCALE, s, m):
    return (not word1(s[m.end():]) or re.match("(?i)que?", word1(s[m.end():])))
def c774 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:mg", u"(?:st:(tr(?:ès|op)|peu|bien)\\b|po:(?:nom|adj).* is:fem)")
def c778 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem") and not re.match("seule?s?", m.group(2))
def c780 (LOCALE, s, m):
    return not re.match("(?i)(?:oh|ah)$", wordmin1(s[:m.start()]))
def c782 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep")
def c785 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj)", "po:([123](?:sg|pl)|infi|pp)|st:la")
def c787 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "is:infi")  and m.group(1) != "CE"
def c788 (LOCALE, s, m):
    return (m.group(2).startswith(",") or morphex(LOCALE,m.group(3), ":mg", "po:adj"))
def c791 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v[123].* po:(?:infi|[123](?:sg|pl))") and not stemchk(LOCALE,m.group(2), "^(?:devoir|pouvoir)$") and m.group(2)[0].islower() and m.group(1) != "CE"
def c799 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v", "po:(?:nom|adj).* is:(?:mas|epi)")
def c802 (LOCALE, s, m):
    return morph(LOCALE,wordmin1(s[:m.start()]), "po:cjsub", False, True) and not look(s[:m.start()],", +$") and not look(s[m.end():],r"^ +(?:ils?|elles?)\b")
def c806 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:nom.* is:sg", "po:adj.* is:(?:pl|inv)")
def c810 (LOCALE, s, m):
    return m.group(1).endswith("en") or look(s[:m.start()],"^ *$")
def c816 (LOCALE, s, m):
    return not wordmin1(s[:m.start()])
def c819 (LOCALE, s, m):
    return not m.group(1).startswith("B")
def c827 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:impe", False, False) and not look(s[:m.start()],r"(?<!-)\ble +$")
def c832 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:[123](?:sg|pl)|infi)", "po:(?:mg|nom|adj|prn|patr|npr)") and not look(s[:m.start()],r"(?i)\bles *$")
def c840 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:adv", False) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:v.* po:3sg", False, False)
def r845 (LOCALE, s, m):
    return m.group(2).replace("pal", u"pâl")
def r847 (LOCALE, s, m):
    return m.group(1).replace("pal", u"pâl")
def r856 (LOCALE, s, m):
    return m.group(1).replace(" ", "")
def c856 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj)", "po:(?:mg|[123](?:sg|pl)|adv)")
def c861 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), ":v0e", False)
def c867 (LOCALE, s, m):
    return not wordmin1(s[:m.start()]) and morph(LOCALE,m.group(3), "po:v", False)
def c870 (LOCALE, s, m):
    return not wordmin1(s[:m.start()]) and morph(LOCALE,m.group(2), "po:v", False) and not ( m.group(1) == "sans" and morph(LOCALE,m.group(2), "po:(?:nom|infi)", False) )
def c886 (LOCALE, s, m):
    return not re.match(u"(?iu)(d[eu]|avant|après|sur|malgré)$", wordmin1(s[:m.start()]))
def c888 (LOCALE, s, m):
    return not re.match(u"(?iu)(d[eu]|avant|après|sur|malgré)$", wordmin1(s[:m.start()]))
def c893 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "is:fem") and not re.match(u"(?iu)(à|pas|de|[nv]ous|eux)$", wordmin1(s[:m.start()]))
def c895 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "is:mas") and not re.match(u"(?iu)(à|pas|de|[nv]ous|eux)$", wordmin1(s[:m.start()]))
def r898 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(1))
def c898 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:nom.* is:(?:fem|pl)", "(?:po:(?:ad[jv]|mg|prn|patr|npr|infi)|is:(?:mas|epi) is:inv)") and morph(LOCALE,wordmin1(s[:m.start()]), "(?:po:prep|st:de)", False, True)
def r902 (LOCALE, s, m):
    return suggFemSing(LOCALE,m.group(1))
def c902 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:mas|pl)") and morph(LOCALE,wordmin1(s[:m.start()]), "(?:po:prep|st:de)", False, True)
def r906 (LOCALE, s, m):
    return suggMasPlur(LOCALE,m.group(1))
def c906 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:fem|sg)") and morph(LOCALE,wordmin1(s[:m.start()]), "(?:po:prep|st:de)", False, True)
def r910 (LOCALE, s, m):
    return suggFemPlur(LOCALE,m.group(1))
def c910 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:mas|sg)") and morph(LOCALE,wordmin1(s[:m.start()]), "(?:po:prep|st:de)", False, True)
def c917 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:[123](?:sg|pl)", False) and not (re.match("(?i)(?:jamais|rien)", m.group(3)) and look(s[:m.start()],"\\b(?:que?|plus|moins)\\b"))
def c921 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:[123](?:sg|pl)", False) and not (re.match("(?i)(?:jamais|rien)", m.group(3)) and look(s[:m.start()],"\\b(?:que?|plus|moins)\\b"))
def c925 (LOCALE, s, m):
    return morph(LOCALE,m.group(3), "po:[123](?:sg|pl)", False) and not (re.match("(?i)(?:jamais|rien)", m.group(3)) and look(s[:m.start()],"\\b(?:que?|plus|moins)\\b"))
def c929 (LOCALE, s, m):
    return morph(LOCALE,m.group(3), "po:[123](?:sg|pl)", False) and not (re.match("(?i)(?:jamais|rien)", m.group(3)) and look(s[:m.start()],"\\b(?:que?|plus|moins)\\b"))
def c936 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v", False)
def c957 (LOCALE, s, m):
    return not morph(LOCALE,m.group(2), "po:(?:nom|adj)", False)
def c1192 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:nom|adj)", "po:mg")
def c1199 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False)
def c1209 (LOCALE, s, m):
    return not re.match(u"(?i)(?:janvier|février|mars|avril|mai|juin|juillet|ao[ûu]t|septembre|octobre|novembre|décembre|vendémiaire|brumaire|frimaire|nivôse|pluviôse|ventôse|germinal|floréal|prairial|messidor|thermidor|fructidor)s?", m.group(3))
def c1239 (LOCALE, s, m):
    return "faire" in stem(LOCALE,m.group(1))
def c1240 (LOCALE, s, m):
    return "faire" in stem(LOCALE,m.group(1))
def c1269 (LOCALE, s, m):
    return "prendre" in stem(LOCALE,m.group(1))
def c1277 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(?:sembler|para[îi]tre)$") and morphex(LOCALE,m.group(3), "po:adj", "po:mg")
def c1280 (LOCALE, s, m):
    return "tenir" in stem(LOCALE,m.group(1))
def c1282 (LOCALE, s, m):
    return "trier" in stem(LOCALE,m.group(1))
def c1284 (LOCALE, s, m):
    return "venir" in stem(LOCALE,m.group(1))
def c1295 (LOCALE, s, m):
    return morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:(?:pl|inv)", "po:(?:mg|3pl)")
def c1302 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:nb", False)
def c1303 (LOCALE, s, m):
    return morph(LOCALE,wordmin1(s[:m.start()]), ":v0", False) or not morph(LOCALE,word1(s[m.end():]), "po:adj", False)
def c1304 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:adv", False)
def c1305 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:adj .*is:mas is:sg", False)
def c1307 (LOCALE, s, m):
    return morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:prep|cjsub|cjco)", False, True)
def c1308 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:nb", False) or re.match("(?i)(?:plusieurs|maintes)", m.group(1))
def c1309 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:nom|adj|ppas)", ":v0")
def c1311 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "po:det", False)
def c1312 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "po:det.* is:(?:mas is:(?:sg|inv)|epi is:(?:sg|inv))", False)
def c1313 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), "^(?:croire|devoir|estimer|imaginer|penser)$")
def c1314 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:prep|det|[1-3]sg|negadv)", False)
def c1315 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:det", False, False)
def c1316 (LOCALE, s, m):
    return not look(s[:m.start()],u"(?i)\\bt(?:u|oi qui)\\b")
def c1317 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:det", False, False)
def c1318 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:adj", False)
def c1319 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:det", False, False)
def c1320 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:adv", False)
def c1321 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:ad[jv]", "po:mg")
def c1322 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:ad[jv]", False)
def c1323 (LOCALE, s, m):
    return not morph(LOCALE,m.group(2), "po:infi", False)
def c1325 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:nom|v)", "po:det")
def c1326 (LOCALE, s, m):
    return not morph(LOCALE,m.group(2), "po:(?:3sg|negadv)", False)
def c1327 (LOCALE, s, m):
    return morph(LOCALE,m.group(3), "is:(?:mas|epi)", False)
def c1332 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:patr|prn)", False) and (morph(LOCALE,m.group(2), "po:(?:patr|prn|v)", False) or not spell(LOCALE,m.group(2)))
def c1333 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:patr|prn|npr)", False) and morph(LOCALE,m.group(2), "po:(?:patr|prn|npr)", False)
def c1334 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:patr|prn|npr)", False)
def c1335 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:patr|prn|nom)") and morph(LOCALE,m.group(2), "po:(?:patr|prn|nom)")
def c1336 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|npr)") and m.group(1) != u"République"
def c1337 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(patr|prn)", False)
def c1338 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:patr|prn)", False)
def r1340 (LOCALE, s, m):
    return rewriteSubject(LOCALE,m.group(1),m.group(2))
def c1340 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:prn|patr|npr|titr)", False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:cjsub", False, True) and not look(s[:m.start()],u"(?:plus|moins|aussi) .* que +$")
def c1346 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0e", False)
def c1347 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0e", False)
def c1348 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(v0e|nom)", False) and morph(LOCALE,m.group(3), "po:(?:adj|ppas)", False)
def c1349 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0", False)
def c1350 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0", False) and morph(LOCALE,m.group(3), "po:(?:ppas|infi)", False)
def c1352 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False)
def c1354 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False) and morph(LOCALE,m.group(3), "po:nb", False) and morph(LOCALE,m.group(4), "po:(?:ppas|v1.* po:infi)", False)
def c1358 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v", False)
def c1359 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v[123]")
def c1360 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v[1-3]", False)
def c1361 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v", False)
def c1364 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:mg")
def c1367 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:mas|epi)", "po:mg") and morph(LOCALE,m.group(3), "po:(?:adj|ppas).* is:(?:mas|epi)", False)
def c1368 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:fem|epi)", "po:mg") and morph(LOCALE,m.group(3), "po:(?:adj|ppas).* is:(?:fem|epi)", False)
def c1369 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:pl|inv)", "po:[123](?:sg|pl)") and morph(LOCALE,m.group(3), "po:(?:adj|ppas).* is:(?:pl|inv)", False)
def c1371 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:ad[jv]")
def c1373 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:ad[jv]", False)
def c1375 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:ad[jv]", False)
def c1376 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:adv", "po:3pl")
def c1378 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:ad[jv]", "po:[1-3](?:sg|pl)")
def c1383 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj|ppas)", False) and morph(LOCALE,m.group(3), "po:adv", False) and morph(LOCALE,m.group(4), "po:(?:adj|ppas)", False)
def c1385 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:det", False, True)
def c1386 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), r"po:adv\b")
def c1389 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj|ppas)", False)
def c1393 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj|ppas|v0e)", False)
def c1448 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:1sg", False, False)
def c1449 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:2sg", False, False)
def c1450 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:3sg", False, False)
def c1451 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:1pl", False, False)
def c1452 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:2pl", False, False)
def c1453 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:3pl", False, False)
def c1454 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:[123](?:sg|pl)")
def c1461 (LOCALE, s, m):
    return isAmbiguousNAV(LOCALE,m.group(3)) and morph(LOCALE,m.group(1), "po:(?:nom|adj)", False)
def c1464 (LOCALE, s, m):
    return isAmbiguousNAV(LOCALE,m.group(4)) and morph(LOCALE,m.group(2), "po:(?:nom|adj)", False)
def c1467 (LOCALE, s, m):
    return isAmbiguousNAV(LOCALE,m.group(3)) and ( morphex(LOCALE,m.group(1), "po:(?:nom|adj)", "po:3(?:sg|pl)") or (morph(LOCALE,m.group(1), "po:(?:nom|adj)", False) and not wordmin1(s[:m.start()])) )
def c1482 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "po:(?:mg|v0)", False)
def c1493 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj)", False)
def c1494 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj)", False)
def c1495 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", False)
def c1501 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", ":(?:epi|mas|ppre|mg|adv|[1-3](?:sg|pl)|infi)")
def c1504 (LOCALE, s, m):
    return morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem", ":(?:epi|mas|ppre|mg|adv|[1-3](?:sg|pl)|infi)") or ( morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem", "is:(?:mas|epi)") and morphex(LOCALE,m.group(1), "po:(?:prep|cj)", "st:(?:e[tn]|ou)") and not (morph(LOCALE,m.group(1), "po:prepv", False) and morph(LOCALE,m.group(3), "po:infi", False)) )
def c1508 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", ":(?:epi|mas|ppre|mg|adv|infi)")
def c1512 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", ":(?:epi|mas|mg|adv)")
def c1515 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", ":(?:epi|mas|mg|adv|v0|3sg)")
def c1518 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", ":(?:epi|mas|mg|adv|ppre)")
def c1521 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", ":(?:epi|mas|mg|adv)")
def c1524 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", ":(?:epi|mas|mg|adv)")
def c1527 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem is:sg", ":(?:epi|mas|mg|adv)")
def c1531 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:epi|fem|ppre|mg|adv|[1-3](?:sg|pl)|infi)")
def c1534 (LOCALE, s, m):
    return morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas", ":(?:epi|fem|ppre|mg|adv|[1-3](?:sg|pl)|infi)") or ( morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas", "is:(?:fem|epi)") and morphex(LOCALE,m.group(1), "po:(?:prep|cj)", "st:(?:e[tn]|ou)") and not (morph(LOCALE,m.group(1), "po:(?:prepv|cj)", False) and morph(LOCALE,m.group(3), "po:infi", False)) )
def c1538 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:epi|fem|ppre|mg|adv|infi)")
def c1542 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:epi|fem|mg|adv)")
def c1545 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:epi|fem|mg|adv|v0|3sg|ppre)")
def c1548 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:epi|fem|mg|adv|v0|3sg)")
def c1551 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:epi|fem|mg|adv|ppre)")
def c1554 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:epi|fem|mg|adv)")
def r1557 (LOCALE, s, m):
    return ceOrCet(m.group(2))
def c1557 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:epi|fem|mg|adv)")
def c1561 (LOCALE, s, m):
    return morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem", ":(?:epi|mas|mg|adv)")
def c1564 (LOCALE, s, m):
    return morphex(LOCALE,m.group(3), "is:fem", ":(?:epi|mg|adv)")
def c1567 (LOCALE, s, m):
    return morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas", ":(?:epi|fem|mg|adv)")
def c1572 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:epi|fem|mg|adv)")
def r1578 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(1))
def c1578 (LOCALE, s, m):
    return (morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:sg") and not (morph(LOCALE,word1(s[m.end():]), "st:(?:et|ou)", False, False) and morph(LOCALE,word(s[m.end():],2), "po:(?:nom|adj)", True, False))) or m.group(1) in aREGULARPLURAL
def r1583 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c1583 (LOCALE, s, m):
    return (morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg") or (morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg", "(?:is:(?:pl|inv)|st:avoir)") and morphex(LOCALE,m.group(1), "po:(?:prep|cj)", "st:(?:e[tn]|ou)") and not (morph(LOCALE,m.group(1), "po:prepv", False) and morph(LOCALE,m.group(2), "po:infi", False)))) and not (morph(LOCALE,word1(s[m.end():]), "st:(?:et|ou)", False, False) and morph(LOCALE,word(s[m.end():],2), "po:(?:nom|adj)", True, False))
def r1587 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(1))
def c1587 (LOCALE, s, m):
    return (morphex(LOCALE,m.group(1), "po:(?:nom|adj).* is:sg", ":(inv|pl|infi|ppre|mg|adv)") and not (morph(LOCALE,word1(s[m.end():]), "st:(?:et|ou)", False, False) and morph(LOCALE,word(s[m.end():],2), "po:(?:nom|adj)", True, False))) or m.group(1) in aREGULARPLURAL
def r1592 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c1592 (LOCALE, s, m):
    return (morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg", ":(?:inv|pl|mg|adv)") and not (morph(LOCALE,word1(s[m.end():]), "st:(?:et|ou)", False, False) and morph(LOCALE,word(s[m.end():],2), "po:(?:nom|adj)", True, False))) or m.group(2) in aREGULARPLURAL
def r1597 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(1))
def c1597 (LOCALE, s, m):
    return (morphex(LOCALE,m.group(1), "po:(?:nom|adj).* is:sg", ":(?:inv|pl|mg|adv|ppre)") and not (morph(LOCALE,word1(s[m.end():]), "st:(?:et|ou)", False, False) and morph(LOCALE,word(s[m.end():],2), "po:(?:nom|adj)", True, False))) or m.group(1) in aREGULARPLURAL
def r1603 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(1))
def c1603 (LOCALE, s, m):
    return (morphex(LOCALE,m.group(1), "po:(?:nom|adj).* is:sg", "is:(?:inv|pl)") and morphex(LOCALE,wordmin1(s[:m.start()]), "po:(?:mg|[123](?:sg|pl))", "po:(?:adj|det)", True)) or m.group(1) in aREGULARPLURAL
def r1608 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c1608 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg", "is:(?:inv|pl)") or m.group(2) in aREGULARPLURAL
def r1612 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c1612 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg", "is:(?:inv|pl)") or m.group(2) in aREGULARPLURAL
def r1616 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(1))
def c1616 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:pl")
def r1619 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(2))
def c1619 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl") or ( morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl", "is:(?:sg|inv)") and morphex(LOCALE,m.group(1), "po:(?:prep|cj)", "st:(?:e[tn]|ou)") and not (morph(LOCALE,m.group(1), "po:prepv", False) and morph(LOCALE,m.group(3), "po:infi", False)) )
def r1623 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(1))
def c1623 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:nom|adj).* is:pl", ":(?:inv|sg|mg|adv)")
def r1627 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(2))
def c1627 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl", ":(?:inv|sg|mg|adv)")
def c1631 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl", ":(?:inv|sg|mg|adv)")
def c1635 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl", ":(?:mg|sg|inv)")
def r1639 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(1))
def c1639 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:nom|adj).* is:pl", ":(?:inv|sg|mg|adv)") and not morph(LOCALE,wordmin(s[:m.start()],2), "po:nb", False)
def r1643 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c1643 (LOCALE, s, m):
    return (morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg") and not re.match(u"(?i)(janvier|février|mars|avril|mai|juin|juillet|ao[ûu]t|septembre|octobre|novembre|décembre|rue|route|ruelle|place|boulevard|avenue|allée|chemin|sentier|square|impasse|cour|quai|chaussée|côte|vendémiaire|brumaire|frimaire|nivôse|pluviôse|ventôse|germinal|floréal|prairial|messidor|thermidor|fructidor)$", m.group(2))) or m.group(2) in aREGULARPLURAL
def r1649 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c1649 (LOCALE, s, m):
    return (morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:nom", False) and not re.match(u"(?i)(janvier|février|mars|avril|mai|juin|juillet|ao[ûu]t|septembre|octobre|novembre|décembre|rue|route|ruelle|place|boulevard|avenue|allée|chemin|sentier|square|impasse|cour|quai|chaussée|côte|vendémiaire|brumaire|frimaire|nivôse|pluviôse|ventôse|germinal|floréal|prairial|messidor|thermidor|fructidor)$", m.group(2))) or m.group(2) in aREGULARPLURAL
def r1655 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(1))
def c1655 (LOCALE, s, m):
    return (morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:sg") or m.group(1) in aREGULARPLURAL) and not re.match("(?i)(le|un|ce|du)$", wordmin1(s[:m.start()]))
def r1659 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(1))
def c1659 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:pl") and not re.match(u"(?i)(janvier|février|mars|avril|mai|juin|juillet|ao[ûu]t|septembre|octobre|novembre|décembre|rue|route|ruelle|place|boulevard|avenue|allée|chemin|sentier|square|impasse|cour|quai|chaussée|côte|vendémiaire|brumaire|frimaire|nivôse|pluviôse|ventôse|germinal|floréal|prairial|messidor|thermidor|fructidor)$", m.group(1))
def r1663 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c1663 (LOCALE, s, m):
    return (m.group(1) != "1" and m.group(1) != "0" and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg") and not re.match(u"(?i)(janvier|février|mars|avril|mai|juin|juillet|ao[ûu]t|septembre|octobre|novembre|décembre|rue|route|ruelle|place|boulevard|avenue|allée|chemin|sentier|square|impasse|cour|quai|chaussée|côte|vendémiaire|brumaire|frimaire|nivôse|pluviôse|ventôse|germinal|floréal|prairial|messidor|thermidor|fructidor)$", m.group(2))) or m.group(1) in aREGULARPLURAL
def c1671 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem is:pl")
def c1672 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas is:pl")
def c1673 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem is:(?:sg|inv)")
def c1674 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem is:sg")
def c1675 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg")
def c1676 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem")
def c1677 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas is:pl")
def c1678 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem is:pl")
def c1679 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas is:(?:sg|inv)")
def c1680 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas is:sg")
def c1681 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg")
def c1682 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas")
def c1684 (LOCALE, s, m):
    return not look(s[:m.start()],r"\btel(?:le|)s? +$")
def r1687 (LOCALE, s, m):
    return m.group(1)[:-1]
def c1687 (LOCALE, s, m):
    return not look(s[:m.start()],r"\btel(?:le|)s? +$")
def c1691 (LOCALE, s, m):
    return not look(s[:m.start()],r"\btel(?:le|)s? +$") and morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem", "is:(?:mas|inv)")
def c1695 (LOCALE, s, m):
    return not look(s[:m.start()],r"\btel(?:le|)s? +$") and morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas", "is:(?:fem|inv)")
def c1699 (LOCALE, s, m):
    return not look(s[:m.start()],r"\btel(?:le|)s? +$") and morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem", "is:(?:mas|inv)")
def c1703 (LOCALE, s, m):
    return not look(s[:m.start()],r"\btel(?:le|)s? +$") and morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas", "is:(?:fem|inv)")
def r1710 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(3))
def c1710 (LOCALE, s, m):
    return "trouver" in stem(LOCALE,m.group(1)) and morphex(LOCALE,m.group(3), "po:adj.* is:(?:fem|mas is:pl)", "po:(?:mg|3(?:sg|pl)|prn|patr|npr)")
def r1719 (LOCALE, s, m):
    return switchGender(LOCALE,m.group(2))
def c1719 (LOCALE, s, m):
    return ((morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:mas") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem")) or (morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:fem") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas"))) and not apposition(m.group(1), m.group(2))
def r1724 (LOCALE, s, m):
    return switchPlural(LOCALE,m.group(2))
def c1724 (LOCALE, s, m):
    return ((morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:sg") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl")) or (morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:pl") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg"))) and not apposition(m.group(1), m.group(2))
def r1732 (LOCALE, s, m):
    return switchGender(LOCALE,m.group(3))
def c1732 (LOCALE, s, m):
    return ((morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem")) or (morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas"))) and not apposition(m.group(2), m.group(3)) and morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:v|prep|proneg)", True, True)
def r1739 (LOCALE, s, m):
    return switchGender(LOCALE,m.group(3))
def c1739 (LOCALE, s, m):
    return ((morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", "(?:po:(?:mg|infi)|is:(?:fem|epi))") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem")) or (morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", "(?:po:(?:mg|infi)|is:(?:mas|epi))") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas"))) and not apposition(m.group(2), m.group(3)) and morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:v|prep|proneg)", True, True)
def r1746 (LOCALE, s, m):
    return switchGender(LOCALE,m.group(3))
def c1746 (LOCALE, s, m):
    return ((morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", "(?:po:mg|is:(?:fem|epi))") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem")) or (morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", "(?:po:mg|is:(?:mas|epi))") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas"))) and not apposition(m.group(2), m.group(3)) and morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:v|prep|proneg)", True, True)
def r1754 (LOCALE, s, m):
    return switchPlural(LOCALE,m.group(3))
def c1754 (LOCALE, s, m):
    return ((morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:sg")) or (morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl"))) and not apposition(m.group(2), m.group(3)) and morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:v|prep|proneg)", True, True)
def r1761 (LOCALE, s, m):
    return switchPlural(LOCALE,m.group(3))
def c1761 (LOCALE, s, m):
    return ((morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl", "(?:po:(?:mg|infi)|is:(?:sg|inv))") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:sg")) or (morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg", "(?:po:(?:mg|infi)|is:(?:pl|inv))") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl"))) and not apposition(m.group(2), m.group(3)) and morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:v|prep|proneg)", True, True)
def r1768 (LOCALE, s, m):
    return switchPlural(LOCALE,m.group(3))
def c1768 (LOCALE, s, m):
    return ((morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl", "(?:po:mg|is:(?:sg|inv))") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:sg")) or (morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg", "(?:po:mg|is:(?:pl|inv))") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl"))) and not apposition(m.group(2), m.group(3)) and morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:v|prep|proneg)", True, True)
def r1777 (LOCALE, s, m):
    return switchGender(LOCALE,m.group(2), False)
def c1777 (LOCALE, s, m):
    return not re.match("(?i)air$", m.group(1)) and not m.group(2).startswith("seul") and ((morph(LOCALE,m.group(1), "is:mas") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem")) or (morph(LOCALE,m.group(1), "is:fem") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas"))) and not apposition(m.group(1), m.group(2)) and not look(s[:m.start()],r"\b(et|ou|de) +$")
def r1782 (LOCALE, s, m):
    return switchGender(LOCALE,m.group(2), False)
def c1782 (LOCALE, s, m):
    return not m.group(2).startswith("seul") and ((morph(LOCALE,m.group(1), "is:mas") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem")) or (morph(LOCALE,m.group(1), "is:fem") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas"))) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:nom|adj)", False, False) and not apposition(m.group(1), m.group(2))
def r1789 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(2))
def c1789 (LOCALE, s, m):
    return not re.match("(?i)air$", m.group(1)) and not m.group(2).startswith("seul") and morph(LOCALE,m.group(1), "is:(?:sg|inv)") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl") and not apposition(m.group(1), m.group(2)) and not look(s[:m.start()],r"\b(et|ou|de) +$")
def r1794 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(2))
def c1794 (LOCALE, s, m):
    return not re.match("(?i)air$", m.group(1)) and not m.group(2).startswith("seul") and morph(LOCALE,m.group(1), "is:(?:sg|inv)") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:nom|adj)", False, False) and not apposition(m.group(1), m.group(2))
def r1803 (LOCALE, s, m):
    return switchGender(LOCALE,m.group(2), True)
def c1803 (LOCALE, s, m):
    return m.group(1) != "fois" and not m.group(2).startswith("seul") and ((morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:mas") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem")) or (morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:fem") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas"))) and morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:v|prep|nb|proneg)", True, True) and not apposition(m.group(1), m.group(2))
def r1810 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c1810 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:pl|inv)", False) and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg") and morph(LOCALE,wordmin1(s[:m.start()]), "po:(v|prep|nb|proneg)", True, True) and not apposition(m.group(1), m.group(2))
def r1818 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(2))
def c1818 (LOCALE, s, m):
    return m.group(1) != "fois" and morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:sg|inv)", False) and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl") and not m.group(2).startswith("seul") and not apposition(m.group(1), m.group(2)) and not look(s[:m.start()],r"\b(et|ou|d’) *$")
def r1822 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(2))
def c1822 (LOCALE, s, m):
    return m.group(1) != "fois" and morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:sg|inv)", False) and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl") and not m.group(2).startswith("seul") and not apposition(m.group(1), m.group(2)) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:nom|adj|ppas|nb)", False, False)
def r1831 (LOCALE, s, m):
    return suggMasPlur(LOCALE,m.group(3))  if m.group(1)=="certains"  else suggMasSing(LOCALE,m.group(3))
def c1831 (LOCALE, s, m):
    return not m.group(3).startswith("seul") and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:mas|epi)", ":(?:nb|mg|v0|fem)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem") and not apposition(m.group(2), m.group(3)) and not look(s[:m.start()],r"\b(et|ou|de) +$")
def r1836 (LOCALE, s, m):
    return suggMasPlur(LOCALE,m.group(3))  if m.group(1)=="certains"  else suggMasSing(LOCALE,m.group(3))
def c1836 (LOCALE, s, m):
    return not m.group(3).startswith("seul") and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:mas|epi)", ":(?:nb|mg|v0|fem)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem") and not apposition(m.group(2), m.group(3)) and not morph(LOCALE,wordmin1(s[:m.start()]), ":(?:nom|adj|ppas|et|ou)", False, False)
def r1843 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(3))
def c1843 (LOCALE, s, m):
    return not m.group(3).startswith("seul") and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:nb|mg|epi|v0|fem)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem") and not apposition(m.group(2), m.group(3)) and not look(s[:m.start()],r"\b(et|ou|de) +$")
def r1848 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(3))
def c1848 (LOCALE, s, m):
    return not m.group(3).startswith("seul") and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:nb|mg|epi|v0|fem)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem") and not apposition(m.group(2), m.group(3)) and not morph(LOCALE,wordmin1(s[:m.start()]), ":(?:nom|adj|ppas|et|ou)", False, False)
def r1855 (LOCALE, s, m):
    return suggFemPlur(LOCALE,m.group(3))  if m.group(1)=="certaines"  else suggFemSing(LOCALE,m.group(3))
def c1855 (LOCALE, s, m):
    return m.group(2) != "fois" and not m.group(3).startswith("seul") and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:fem|epi)", ":(?:nb|mg|v0|mas)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas") and not apposition(m.group(2), m.group(3)) and not look(s[:m.start()],r"\b(et|ou|de) +$")
def r1860 (LOCALE, s, m):
    return suggFemPlur(LOCALE,m.group(3))  if m.group(1)=="certaines"  else suggFemSing(LOCALE,m.group(3))
def c1860 (LOCALE, s, m):
    return m.group(2) != "fois" and not m.group(3).startswith("seul") and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:fem|epi)", ":(?:nb|mg|v0|mas)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas") and not apposition(m.group(2), m.group(3)) and not morph(LOCALE,wordmin1(s[:m.start()]), ":(?:nom|adj|ppas|et|ou)", False, False)
def r1867 (LOCALE, s, m):
    return switchGender(LOCALE,m.group(3), m.group(1).endswith("s"))
def c1867 (LOCALE, s, m):
    return m.group(2) != "fois" and not m.group(3).startswith("seul") and not re.match("(?i)quelque chose", m.group(0)) and ((morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:nb|epi|mg|v0|fem)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem")) or (morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", ":(?:nb|epi|mg|v0|mas)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas"))) and not apposition(m.group(2), m.group(3)) and not look(s[:m.start()],r"\b(et|ou|de) +$")
def r1872 (LOCALE, s, m):
    return switchGender(LOCALE,m.group(3), m.group(1).endswith("s"))
def c1872 (LOCALE, s, m):
    return m.group(2) != "fois" and not m.group(3).startswith("seul") and not re.match("(?i)quelque chose", m.group(0)) and ((morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", ":(?:nb|epi|mg|v0|fem)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem")) or (morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", ":(?:nb|epi|mg|v0|mas)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas"))) and not apposition(m.group(2), m.group(3)) and not morph(LOCALE,wordmin1(s[:m.start()]), r":(?:nom|adj|ppas|et|ou)\b", False, False)
def r1881 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(3))
def c1881 (LOCALE, s, m):
    return not m.group(3).startswith("seul") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:sg|inv)", False) and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl") and not apposition(m.group(2), m.group(3)) and not look(s[:m.start()],r"\b(et|ou|de) +$")
def r1886 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(3))
def c1886 (LOCALE, s, m):
    return not m.group(3).startswith("seul") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:sg|inv)", False) and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl") and not apposition(m.group(2), m.group(3)) and not morph(LOCALE,wordmin1(s[:m.start()]), ":(?:nom|adj|ppas|et|ou)", False, False)
def r1893 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(3))
def c1893 (LOCALE, s, m):
    return not m.group(3).startswith("seul") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:sg|inv)", False) and morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl", ":(?:inv|mg|adv)") and not apposition(m.group(2), m.group(3)) and not look(s[:m.start()],r"\b(et|ou|de) +$")
def r1898 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(3))
def c1898 (LOCALE, s, m):
    return not m.group(3).startswith("seul") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:sg|inv)", False) and morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl", ":(?:inv|mg|adv)") and not apposition(m.group(2), m.group(3)) and not morph(LOCALE,wordmin1(s[:m.start()]), ":(?:nom|adj|ppas|et|ou)", False, False)
def r1905 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c1905 (LOCALE, s, m):
    return not m.group(2).startswith("seul") and morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:pl|inv)") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg") and not apposition(m.group(1), m.group(2))
def r1910 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c1910 (LOCALE, s, m):
    return not m.group(2).startswith("seul") and morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:pl|inv)") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg") and not morph(LOCALE,m.group(3), "po:adj", False) and not apposition(m.group(1), m.group(2))
def r1916 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(3))
def c1916 (LOCALE, s, m):
    return not m.group(3).startswith("seul") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:pl|inv)", False) and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:sg") and not apposition(m.group(2), m.group(3)) and not look(s[:m.start()],r"(?i)\bune? de ")
def r1928 (LOCALE, s, m):
    return switchPlural(LOCALE,m.group(3))
def c1928 (LOCALE, s, m):
    return (morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:pl") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:pl|inv)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:sg")) or (morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:sg") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:sg|inv)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl"))
def r1933 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(3))
def c1933 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:pl|inv)") and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:pl|inv)") and morph(LOCALE,m.group(3), "po:(?:nom|adj).* is:sg")
def r1937 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(4))
def c1937 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:pl|inv)", False) and morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:(?:pl|inv)", "po:mg") and morph(LOCALE,m.group(4), "po:(?:nom|adj).* is:sg") and not look(s[:m.start()],r"(?i)\bune? de ")
def r1942 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(4))
def c1942 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:sg|inv)", False) and morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:(?:sg|inv)", "po:mg") and morph(LOCALE,m.group(4), "po:(?:nom|adj).* is:pl")
def c1951 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v0e", False)
def r1954 (LOCALE, s, m):
    return m.group(1).replace("lle", "l")
def c1954 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v0e", False) and morphex(LOCALE,m.group(4), "po:(?:nom|adj).* is:mas", "is:(?:fem|epi)")
def c1960 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v0e", False)
def r1963 (LOCALE, s, m):
    return m.group(1).replace("l", "lle")
def c1963 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v0e", False) and morphex(LOCALE,m.group(4), "po:(?:nom|adj).* is:fem", "is:(?:mas|epi)")
def c1974 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:nb.* is:pl", False)
def c1996 (LOCALE, s, m):
    return not wordmin1(s[:m.start()])
def c1997 (LOCALE, s, m):
    return not wordmin1(s[:m.start()])
def c1998 (LOCALE, s, m):
    return not wordmin1(s[:m.start()])
def c2004 (LOCALE, s, m):
    return not look(s[:m.start()],r"(?i)\bquatre ")
def c2006 (LOCALE, s, m):
    return not morph(LOCALE,word1(s[m.end():]), "po:nb") and not look(s[:m.start()],u"(?i)\\b(?:numéro|page|chapitre|référence|année|test|série)s? +$")
def c2017 (LOCALE, s, m):
    return not morph(LOCALE,word1(s[m.end():]), "(?:po:nb|st:une?)", False, True) and not look(s[:m.start()],u"(?i)\\b(?:numéro|page|chapitre|référence|année|test|série)s? +$")
def c2020 (LOCALE, s, m):
    return morph(LOCALE,word1(s[m.end():]), "(?:po:nb|st:une?)", False, False)
def c2023 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:pl|inv)", "po:mg") and morphex(LOCALE,wordmin1(s[:m.start()]), "po:(?:v|prep)", "po:nb", True)
def c2028 (LOCALE, s, m):
    return morph(LOCALE,word1(s[m.end():]), "po:nb") or (morph(LOCALE,wordmin1(s[:m.start()]), "po:nb") and morph(LOCALE,word1(s[m.end():]), "po:(?:nom|adj)", False))
def c2036 (LOCALE, s, m):
    return "faire" in stem(LOCALE,m.group(1))
def c2038 (LOCALE, s, m):
    return "faire" in stem(LOCALE,m.group(1)) and morph(LOCALE,m.group(3), "po:(?:nom|npr)")
def c2064 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:v0e|adv)", False) or m.group(1) == u"très"
def c2068 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(co[ûu]ter|payer)$", False)
def c2081 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), "^donner$", False)
def c2088 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), "^(mettre|mise)$", False)
def c2096 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), "^(avoir|perdre)$", False)
def c2098 (LOCALE, s, m):
    return not look(s[:m.start()],u"(?i)\\b(lit|fauteuil|armoire|commode|guéridon|tabouret|chaise)s?\\b")
def c2103 (LOCALE, s, m):
    return morphex(LOCALE,wordmin1(s[:m.start()]), "po:(?:v|(?:nom|adj).* is:sg)", "is:(?:pl|inv)", True)
def c2133 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), "^(aller|partir)$", False)
def c2146 (LOCALE, s, m):
    return not re.match(u"(?i)([lmtsn]|soussignée?s?|seule?s?)$", m.group(2)) and morph(LOCALE,m.group(2), "po:(?:nom|adj|ppas)") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:v0")
def c2153 (LOCALE, s, m):
    return stem(LOCALE,m.group(1)) == ["tenir"]
def c2163 (LOCALE, s, m):
    return "mettre" in stem(LOCALE,m.group(1))
def c2164 (LOCALE, s, m):
    return "faire" in stem(LOCALE,m.group(1))
def c2179 (LOCALE, s, m):
    return not morph(LOCALE,word1(s[m.end():]), "po:(?:adj|ppas)")
def c2189 (LOCALE, s, m):
    return "faire" in stem(LOCALE,m.group(1))
def c2195 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False)
def c2204 (LOCALE, s, m):
    return stem(LOCALE,m.group(1)) == ["couper"]
def c2205 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), "^(?:avoir|donner)$", False)
def c2212 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v.\w+ is:(?!ppas)")
def c2218 (LOCALE, s, m):
    return not re.match("(?i)[dlmts]es|[nv]os|leurs$", wordmin1(s[:m.start()]))
def c2221 (LOCALE, s, m):
    return morphex(LOCALE,word1(s[m.end():]), "po:(?:mg|v)", "po:(?:nom|adj|ppas)", True)
def c2223 (LOCALE, s, m):
    return morphex(LOCALE,word1(s[m.end():]), "po:(?:mg|v)", "po:(?:nom|adj)")
def c2225 (LOCALE, s, m):
    return morphex(LOCALE,word1(s[m.end():]), "po:(?:mg|v)", "po:(?:nom|adj)", True)
def c2227 (LOCALE, s, m):
    return morphex(LOCALE,word1(s[m.end():]), "po:mg", "po:(?:nom|adj)")
def c2229 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0e", False)
def c2233 (LOCALE, s, m):
    return morph(LOCALE,wordmin1(s[:m.start()]), "po:v0e", False)
def c2236 (LOCALE, s, m):
    return not stemchk(LOCALE,wordmin1(s[:m.start()]), u"^(?:abandonner|céder|résister)$", False)
def r2245 (LOCALE, s, m):
    return m.group(1).replace("nt", "mp")
def r2252 (LOCALE, s, m):
    return m.group(2).replace("sens", "cens")
def c2252 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0e", False) and morph(LOCALE,m.group(3), "po:infi", False)
def r2257 (LOCALE, s, m):
    return m.group(2).replace("o", u"ô")
def c2264 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False)
def r2267 (LOCALE, s, m):
    return m.group(2).replace("descell", u"décel").replace("dessell", u"décel")
def c2273 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0", False)
def r2279 (LOCALE, s, m):
    return m.group(1).replace("and", "ant")
def c2282 (LOCALE, s, m):
    return not ( m.group(1) == "bonne" and look(s[:m.start()],"(?i)\\bune +$") and look(s[m.end():],"(?i)^ +pour toute") )
def c2285 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), "^(?:faire|perdre)$")
def c2294 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:det")
def c2326 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v", "po:(?:nom|adj)")
def c2327 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj)", "po:[123](?:sg|pl)")
def c2331 (LOCALE, s, m):
    return morphex(LOCALE,m.group(3), "po:[1-3](?:sg|pl)", "po:(?:ppas|mg)")
def c2334 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:[1-3](?:sg|pl)", "po:(?:ppas|mg)")
def c2336 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:[1-3](?:sg|pl)", "po:(?:ppas|mg)")
def c2343 (LOCALE, s, m):
    return morph(LOCALE,m.group(3), "is:infi") and not re.match("(?i)[ld]es", m.group(1))
def c2350 (LOCALE, s, m):
    return "soulever" in stem(LOCALE,m.group(1))
def c2357 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), "^(?:être|habiter|trouver|situer|rester|demeurer?)$")
def r2371 (LOCALE, s, m):
    return m.group(1).replace("otre", u"ôtre")
def r2372 (LOCALE, s, m):
    return m.group(0).replace(u"ôtre", "otre").replace("s", "")
def c2372 (LOCALE, s, m):
    return not re.match("(?i)les?|la|du|des|aux?", wordmin1(s[:m.start()])) and morph(LOCALE,word1(s[m.end():]), "po:(?:nom|adj)")
def c2379 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:det")
def c2382 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False)
def c2389 (LOCALE, s, m):
    return not wordmin1(s[:m.start()])
def c2392 (LOCALE, s, m):
    return ( re.match("[nmts]e$", m.group(2)) or (not re.match(u"(?i)(?:confiance|envie|peine|prise|crainte|affaire|hâte|force|recours)$", m.group(2)) and morphex(LOCALE,m.group(2), "po:[1-3](?:sg|pl)", "po:(?:adj|mg)")) ) and not wordmin1(s[:m.start()])
def c2399 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:prep|v)", False, False) and not look(s[m.end():],"^ +>") and not morph(LOCALE,word1(s[m.end():]), "po:3sg", False)
def c2403 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v.\w+ is:(?!infi)")
def c2404 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v0e", "po:infi")
def c2407 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:det", False, False)
def c2410 (LOCALE, s, m):
    return "aller" in stem(LOCALE,m.group(1))
def r2413 (LOCALE, s, m):
    return m.group(2).replace("pal", u"pâl")
def c2413 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0e", False)
def r2415 (LOCALE, s, m):
    return m.group(2).replace("pal", u"pâl")
def c2419 (LOCALE, s, m):
    return "prendre" in stem(LOCALE,m.group(1))
def c2420 (LOCALE, s, m):
    return "tirer" in stem(LOCALE,m.group(1))
def c2421 (LOCALE, s, m):
    return "faire" in stem(LOCALE,m.group(1))
def c2423 (LOCALE, s, m):
    return "prendre" in stem(LOCALE,m.group(1))
def c2429 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj)")
def c2430 (LOCALE, s, m):
    return not wordmin1(s[:m.start()])
def c2436 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:adj") and not re.match("(?i)seule?s?$", m.group(2))
def c2440 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:(?:nom|adj|ppas|mg|npr)")
def c2453 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:infi|patr|prn|npr)")
def c2454 (LOCALE, s, m):
    return wordmin1(s[:m.start()]) != "peu" and morph(LOCALE,m.group(2), "po:infi")
def c2461 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False)
def c2470 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:ppas")
def c2474 (LOCALE, s, m):
    return not morph(LOCALE,word1(s[m.end():]), "po:adj", False)
def c2492 (LOCALE, s, m):
    return not word1(s[m.end():])
def r2495 (LOCALE, s, m):
    return m.group(2).replace(u"résonn", "raisonn")
def c2495 (LOCALE, s, m):
    return u"résonner" in stem(LOCALE,m.group(2))
def c2502 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v.* po:(?:infi|[123](?:sg|pl))") or m.group(2) == "va"
def c2505 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:prn", False)
def r2514 (LOCALE, s, m):
    return m.group(2).replace("scep","sep")
def c2516 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0e", False)
def c2519 (LOCALE, s, m):
    return "suivre" in stem(LOCALE,m.group(1))
def c2524 (LOCALE, s, m):
    return morph(LOCALE,word1(s[m.end():]), "po:(?:mg|infi)", True, True) and not look(s[:m.start()],"quel(?:s|les?|) qu $")
def c2533 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:nom.* is:mas is:sg", "po:mg") and (look(s[:m.start()],u"(?i)^ *$|\b(?:par|avec|dès|sur|contre|devant|derrière)$"))
def r2545 (LOCALE, s, m):
    return m.group(1).replace("sur", u"sûr")
def r2547 (LOCALE, s, m):
    return m.group(1).replace("sur", u"sûr")
def c2551 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:prn", False)
def r2554 (LOCALE, s, m):
    return m.group(1).replace("sur", u"sûr")
def c2554 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:infi", False)
def c2561 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:nom", "po:(prn|patr|npr)")
def c2572 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0e", False) and morph(LOCALE,m.group(3), "is:infi", False)
def c2578 (LOCALE, s, m):
    return "avoir" in stem(LOCALE,m.group(1))
def c2588 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj).* is:(?:mas|epi).* is:sg")
def c2601 (LOCALE, s, m):
    return "ouvrir" in stem(LOCALE,m.group(1))
def c2615 (LOCALE, s, m):
    return not m.group(1).isdigit() and not m.group(2).isdigit() and not morph(LOCALE,m.group(0), "po:", False) and not morph(LOCALE,m.group(2), "po:mg", False) and spell(LOCALE,m.group(1)+m.group(2))
def c2618 (LOCALE, s, m):
    return m.group(2) != u"là" and not re.match("(?i)(?:ex|mi|quasi|semi|non|demi|pro|anti|multi|pseudo|proto|extra)$", m.group(1)) and not m.group(1).isdigit() and not m.group(2).isdigit() and not morph(LOCALE,m.group(2), "po:mg", False) and not morph(LOCALE,m.group(0), "po:", False) and not spell(LOCALE,m.group(1)+m.group(2))
def c2629 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:mg|nom|adj|infi)", ":ppas") and not re.match(u"(?i)(?:priori|post[eé]riori|contrario|capella)", m.group(2))
def r2639 (LOCALE, s, m):
    return m.group(0).lower()
def r2643 (LOCALE, s, m):
    return m.group(0).lower()
def c2643 (LOCALE, s, m):
    return not( ( m.group(0)=="Juillet" and look(s[:m.start()],"(?i)monarchie +de +$") ) or ( m.group(0)=="Octobre" and look(s[:m.start()],"(?i)révolution +d’$") ) )
def r2662 (LOCALE, s, m):
    return m.group(2).capitalize()
def c2662 (LOCALE, s, m):
    return not look(s[:m.start()],u"(?i)\\b(?:parl|appr|enseign|étudi|exprim)") and not m.group(2).startswith("canadienne")
def r2666 (LOCALE, s, m):
    return m.group(2).capitalize()
def r2670 (LOCALE, s, m):
    return m.group(2).lower()
def c2670 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(?:parler|cours|leçon|apprendre|étudier|traduire|enseigner|professeur|enseignant|dictionnaire|méthode)$", False)
def r2674 (LOCALE, s, m):
    return m.group(1).lower()
def c2682 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:det", False)
def c2689 (LOCALE, s, m):
    return look(s[:m.start()],r"\w")
def r2694 (LOCALE, s, m):
    return m.group(1).capitalize()
def r2695 (LOCALE, s, m):
    return m.group(1).capitalize()
def r2699 (LOCALE, s, m):
    return m.group(2).lower()
def c2699 (LOCALE, s, m):
    return re.match(u"(?:Mètre|Watt|Gramme|Seconde|Ampère|Kelvin|Mole|Cand[eé]la|Hertz|Henry|Newton|Pascal|Joule|Coulomb|Volt|Ohm|Farad|Tesla|W[eé]ber|Radian|Stéradian|Lumen|Lux|Becquerel|Gray|Sievert|Siemens|Katal)s?|(?:Exa|P[ée]ta|Téra|Giga|Méga|Kilo|Hecto|Déc[ai]|Centi|Mi(?:lli|cro)|Nano|Pico|Femto|Atto|Ze(?:pto|tta)|Yo(?:cto|etta))(?:mètre|watt|gramme|seconde|ampère|kelvin|mole|cand[eé]la|hertz|henry|newton|pascal|joule|coulomb|volt|ohm|farad|tesla|w[eé]ber|radian|stéradian|lumen|lux|becquerel|gray|sievert|siemens|katal)s?", m.group(2))
def c2710 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "po:infi", False)
def c2711 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v1") and not look(s[:m.start()],u"(?i)(quelqu(?:e chose|’une?) d(?:e |’)|(?:l(es?|a)|nous|vous|me|te|se)[ @]trait|(?:quelqu(?:e chose|’une?)|personne|rien(?: +\w+|)) +$)")
def c2714 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v1", "po:(?:prn|npr|patr)")
def c2715 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v1", False)
def c2716 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:[123](?:sg|pl)")
def r2717 (LOCALE, s, m):
    return suggVerbInfi(LOCALE,m.group(1))
def c2717 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v1", "po:(?:nom|prn|npr|patr)") and not stemchk(LOCALE,wordmin1(s[:m.start()]), "^(?:tenir|passer)$", False)
def c2720 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v1", False)
def c2721 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v1", "po:(?:nom|prn|npr|patr)")
def c2722 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:ppas", False)
def c2723 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:ppas|2pl)", False)
def c2724 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:ppas", False) and not morph(LOCALE,wordmin1(s[:m.start()]), "v0.*[12]pl", False)
def r2731 (LOCALE, s, m):
    return m.group(2)[:-1]
def c2757 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), ":v", False)
def c2761 (LOCALE, s, m):
    return "sembler" in stem(LOCALE,m.group(1))
def c2776 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj)", False) and isEndOfNG(LOCALE,s[m.end():])
def c2778 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), ":v[1-3]_i_._") and isEndOfNG(LOCALE,s[m.end():])
def c2779 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:adj", False) and morphex(LOCALE,m.group(2), "po:adj", "po:(?:mg|prn|patr|npr)")
def c2780 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:adj", False)
def c2781 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj) .*is:sg", False) and morphex(LOCALE,m.group(2), "po:(?:nom|adj) .*is:sg", "po:(?:mg|v)") and isEndOfNG(LOCALE,s[m.end():])
def c2782 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:nom", "po:(?:mg|infi)") and isEndOfNG(LOCALE,s[m.end():])
def c2782 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:nom", "po:(?:mg|infi)") and isEndOfNG(LOCALE,s[m.end():])
def c2785 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj)", ":v0") and morphex(LOCALE,m.group(3), "po:(?:nom|adj)", "po:(?:mg|[1-3](?:sg|pl)|ppre)")
def c2785 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj)", ":v0") and morphex(LOCALE,m.group(3), "po:(?:nom|adj)", "po:(?:mg|[1-3](?:sg|pl)|ppre)")
def c2798 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj)", False) and isEndOfNG(LOCALE,s[m.end():])
def c2811 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:nom|adj)", "po:mg") and isEndOfNG(LOCALE,s[m.end():])
def c2814 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj)", False) and isEndOfNG(LOCALE,s[m.end():])
def c2782 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:nom", "po:(?:mg|infi)") and isEndOfNG(LOCALE,s[m.end():])
def c2782 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:nom", "po:(?:mg|infi)") and isEndOfNG(LOCALE,s[m.end():])
def c2821 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj)", False) and isEndOfNG(LOCALE,s[m.end():])
def c2821 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:nom|adj)", False) and isEndOfNG(LOCALE,s[m.end():])
def c2823 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:nom|adj)", "po:infi") and isEndOfNG(LOCALE,s[m.end():])
def c2844 (LOCALE, s, m):
    return morph(LOCALE,wordmin1(s[:m.start()]), "po:cj", False, True)
def c2847 (LOCALE, s, m):
    return look(s[m.end():],"^ +d(?:e |’)") or not word1(s[m.end():])
def c2850 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:adj", "po:(?:mg|v|adv)") and morph(LOCALE,wordmin1(s[:m.start()]), "po:cj", False, True)
def c2857 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), "^(faire|vouloir)$", False) and not re.match("(?i)(en|[mtsld]es?|[nv]ous|un)$", wordmin1(s[:m.start()])) and morphex(LOCALE,m.group(2), "po:v", "po:(patr|prn)")
def c2860 (LOCALE, s, m):
    return "savoir" in stem(LOCALE,m.group(1)) and morph(LOCALE,m.group(2), "po:v", False) and not re.match("(?i)([mts]e|[vn]ous|les?|la|un)$", wordmin1(s[:m.start()]))
def r2863 (LOCALE, s, m):
    return suggVerbInfi(LOCALE,m.group(1))
def c2863 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:ppas|2pl)", False)
def r2866 (LOCALE, s, m):
    return suggVerbInfi(LOCALE,m.group(1))
def c2866 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:ppas|2pl)", "po:nom")
def r2873 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(3))
def c2873 (LOCALE, s, m):
    return (stemchk(LOCALE,m.group(2), u"^(?:être|sembler|devenir|re(?:ster|devenir)|para[îi]tre)$", False) or m.group(2).endswith(u" été")) and morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl", ":(?:mg|adv|sg|inv|infi)")
def r2877 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(2))
def c2877 (LOCALE, s, m):
    return (stemchk(LOCALE,m.group(1), u"^(?:être|sembler|devenir|re(?:ster|devenir)|para[îi]tre)$", False) or m.group(1).endswith(u" été")) and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl", ":(?:mg|adv|sg|inv|infi)")
def r2881 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(3))
def c2881 (LOCALE, s, m):
    return (stemchk(LOCALE,m.group(2), u"^(?:être|sembler|devenir|re(?:ster|devenir)|para[îi]tre)$", False) or m.group(2).endswith(u" été")) and (morphex(LOCALE,m.group(3), "po:(adj|ppas|nom).* is:pl", ":(mg|adv|sg|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:fem", ":(?:mg|adv|mas|epi|infi)"))
def r2886 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(1))
def c2886 (LOCALE, s, m):
    return (morphex(LOCALE,m.group(1), "po:(adj|ppas|nom).* is:pl", ":(mg|adv|sg|inv|infi)") or morphex(LOCALE,m.group(1), "po:(?:adj|ppas).* is:fem", ":(?:mg|adv|mas|epi|infi)"))
def r2890 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(1))
def c2890 (LOCALE, s, m):
    return (morphex(LOCALE,m.group(1), "po:(adj|ppas|nom).* is:pl", ":(mg|adv|sg|inv|infi)") or morphex(LOCALE,m.group(1), "po:(?:adj|ppas).* is:fem", ":(?:mg|adv|mas|epi|infi)"))
def r2894 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(3))
def c2894 (LOCALE, s, m):
    return (stemchk(LOCALE,m.group(2), u"^(?:être|sembler|devenir|re(?:ster|devenir)|para[îi]tre)$", False) or m.group(2).endswith(u" été")) and (morphex(LOCALE,m.group(3), "po:(adj|ppas|nom).* is:pl", ":(mg|adv|sg|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:fem", ":(?:mg|adv|mas|epi|infi)")) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False)
def r2900 (LOCALE, s, m):
    return suggFemSing(LOCALE,m.group(3))
def c2900 (LOCALE, s, m):
    return (stemchk(LOCALE,m.group(2), u"^(?:être|sembler|devenir|re(?:ster|devenir)|para[îi]tre)$", False) or m.group(2).endswith(u" été")) and (morphex(LOCALE,m.group(3), "po:(adj|ppas|nom).* is:pl", ":(mg|adv|sg|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:mas", ":(?:mg|adv|fem|epi|infi)")) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False)
def r2906 (LOCALE, s, m):
    return suggFemSing(LOCALE,m.group(3))
def c2906 (LOCALE, s, m):
    return (stemchk(LOCALE,m.group(2), u"^(?:être|sembler|devenir|re(?:ster|devenir)|para[îi]tre)$", False) or m.group(2).endswith(u" été")) and (morphex(LOCALE,m.group(3), "po:(adj|ppas|nom).* is:pl", ":(mg|adv|sg|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:mas", ":(?:mg|adv|fem|epi|infi)"))
def r2911 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c2911 (LOCALE, s, m):
    return not re.match(u"(?i)légion$", m.group(2)) and ((stemchk(LOCALE,m.group(1), u"^(?:être|sembler|devenir|re(?:ster|devenir)|para[îi]tre)$", False) and morph(LOCALE,m.group(1), "po:1pl", False)) or m.group(1).endswith(u" été")) and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg", ":(?:mg|adv|pl|inv|infi)")
def r2917 (LOCALE, s, m):
    return suggMasPlur(LOCALE,m.group(3))
def c2917 (LOCALE, s, m):
    return not re.match(u"(?i)légion$", m.group(3)) and (stemchk(LOCALE,m.group(2), u"^(?:être|sembler|devenir|re(?:ster|devenir)|para[îi]tre)$", False) or m.group(2).endswith(u" été")) and (morphex(LOCALE,m.group(3), "po:(?:adj|ppas|nom).* is:sg", ":(?:mg|adv|pl|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:fem", ":(?:mg|adv|mas|epi|infi)")) and (not re.match(u"(?:ceux-(?:ci|là)|lesquels)$", m.group(1)) or not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False))
def r2923 (LOCALE, s, m):
    return suggFemPlur(LOCALE,m.group(3))
def c2923 (LOCALE, s, m):
    return not re.match(u"(?i)légion$", m.group(3)) and (stemchk(LOCALE,m.group(2), u"^(?:être|sembler|devenir|re(?:ster|devenir)|para[îi]tre)$", False) or m.group(2).endswith(u" été")) and (morphex(LOCALE,m.group(3), "po:(adj|ppas|nom).* is:sg", ":(?:mg|adv|pl|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:mas", ":(?:mg|adv|fem|epi|infi)")) and (not re.match(u"(?i)(?:elles|celles-(?:ci|là)|lesquelles)$", m.group(1)) or not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False))
def r2929 (LOCALE, s, m):
    return suggVerbPpas(LOCALE,m.group(2))
def c2929 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^avoir$", False) and morphex(LOCALE,m.group(2), "po:[123]sg", ":(?:mg|ad[vj]|infi|ppas)")
def r2935 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(3))
def c2935 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(2), u"^(?:sembler|para[îi]tre|pouvoir|penser|préférer|croire|d(?:evoir|éclarer|ire)|vouloir|affirmer)$", False) and morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl", ":(?:mg|adv|sg|inv|infi)")
def r2939 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(2))
def c2939 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(?:sembler|para[îi]tre|pouvoir|penser|préférer|croire|d(?:evoir|éclarer|ire)|vouloir|affirmer)$", False) and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl", ":(?:mg|adv|sg|inv|infi)")
def r2943 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(3))
def c2943 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(2), u"^(?:sembler|para[îi]tre|pouvoir|penser|préférer|croire|d(?:evoir|éclarer|ire)|vouloir|affirmer)$", False) and (morphex(LOCALE,m.group(3), "po:(?:adj|ppas|nom).* is:pl", ":(?:mg|adv|sg|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:fem", ":(?:mg|adv|mas|epi|infi)"))
def r2948 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(3))
def c2948 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(2), u"^(?:sembler|para[îi]tre|pouvoir|penser|préférer|croire|d(?:evoir|éclarer|ire)|vouloir|affirmer)$", False) and (morphex(LOCALE,m.group(3), "po:(?:adj|ppas|nom).* is:pl", ":(?:mg|adv|sg|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:fem", ":(?:mg|adv|mas|epi|infi)")) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False)
def r2954 (LOCALE, s, m):
    return suggFemSing(LOCALE,m.group(3))
def c2954 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(2), u"^(?:sembler|para[îi]tre|pouvoir|penser|préférer|croire|d(?:evoir|éclarer|ire)|vouloir|affirmer)$", False) and (morphex(LOCALE,m.group(3), "po:(?:adj|ppas|nom).* is:pl", ":(?:mg|adv|sg|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:mas", ":(?:mg|adv|fem|epi|infi)")) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False)
def r2960 (LOCALE, s, m):
    return suggFemSing(LOCALE,m.group(3))
def c2960 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(2), u"^(?:sembler|para[îi]tre|pouvoir|penser|préférer|croire|d(?:evoir|éclarer|ire)|vouloir|affirmer)$", False) and (morphex(LOCALE,m.group(3), "po:(?:adj|ppas|nom).* is:pl", ":(?:mg|adv|sg|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:mas", ":(?:mg|adv|fem|epi|infi)"))
def r2965 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c2965 (LOCALE, s, m):
    return not re.match(u"(?i)légion$", m.group(2)) and stemchk(LOCALE,m.group(1), u"^(?:sembler|para[îi]tre|pouvoir|penser|préférer|croire|d(?:evoir|éclarer|ire)|vouloir|affirmer)$", False) and morph(LOCALE,m.group(1), "po:1pl", False) and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg", ":(?:mg|adv|pl|inv|infi)")
def r2970 (LOCALE, s, m):
    return suggMasPlur(LOCALE,m.group(3))
def c2970 (LOCALE, s, m):
    return not re.match(u"(?i)légion$", m.group(3)) and stemchk(LOCALE,m.group(2), u"^(?:sembler|para[îi]tre|pouvoir|penser|préférer|croire|d(?:evoir|éclarer|ire)|vouloir|affirmer)$", False) and (morphex(LOCALE,m.group(3), "po:(?:adj|ppas|nom).* is:sg", ":(?:mg|adv|pl|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:fem", ":(?:mg|adv|mas|epi|infi)")) and (not re.match(u"(?:ceux-(?:ci|là)|lesquels)$", m.group(1)) or not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False))
def r2976 (LOCALE, s, m):
    return suggFemPlur(LOCALE,m.group(3))
def c2976 (LOCALE, s, m):
    return not re.match(u"(?i)légion$", m.group(3)) and stemchk(LOCALE,m.group(2), u"^(?:sembler|para[îi]tre|pouvoir|penser|préférer|croire|d(?:evoir|éclarer|ire)|vouloir|affirmer)$", False) and (morphex(LOCALE,m.group(3), "po:(?:adj|ppas|nom).* is:sg", ":(?:mg|adv|pl|inv|infi)") or morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:mas", ":(?:mg|adv|fem|epi|infi)")) and (not re.match(u"(?:elles|celles-(?:ci|là)|lesquelles)$", m.group(1)) or not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False))
def r2984 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(2))
def c2984 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:adj|ppas|nom).* is:pl", ":(?:mg|adv|sg|inv|infi)") and not morph(LOCALE,m.group(1), "po:mg", False)
def r2988 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c2988 (LOCALE, s, m):
    return not re.match("(?i)légion$", m.group(2)) and morphex(LOCALE,m.group(2), "po:(?:adj|ppas|nom).* is:sg", ":(?:mg|adv|pl|inv|infi)") and not morph(LOCALE,m.group(1), "po:mg", False)
def r2993 (LOCALE, s, m):
    return switchGender(LOCALE,m.group(3))
def c2993 (LOCALE, s, m):
    return not re.match(u"(?i)légion$", m.group(3)) and ((morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:fem", ":(?:mg|adv|mas|epi)") and morphex(LOCALE,m.group(2), "is:mas", ":(?:mg|epi|fem)")) or (morphex(LOCALE,m.group(3), "po:(?:adj|ppas).* is:mas", ":(?:mg|adv|fem|epi)") and morphex(LOCALE,m.group(2), "is:fem", ":(?:mg|mas|epi)"))) and not ( morph(LOCALE,m.group(3), "is:pl", False) and morph(LOCALE,m.group(2), "is:sg", False) ) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:prep|pp|infi|[1-3](?:sg|pl))", False, False) and not look(s[:m.start()],r"\b(?:et|ou|de) +$")
def r3001 (LOCALE, s, m):
    return switchGender(LOCALE,m.group(2))
def c3001 (LOCALE, s, m):
    return not re.match(u"(?i)légion$", m.group(2)) and ((morphex(LOCALE,m.group(1), "po:(?:prn|npr).* is:fem", ":(mg|adv|mas|epi)") and morphex(LOCALE,m.group(2), "is:mas", ":(?:mg|epi|fem|adv)")) or (morphex(LOCALE,m.group(1), "po:(?:prn|npr).* is:mas", ":(?:mg|adv|fem|epi)") and morphex(LOCALE,m.group(2), "is:fem", ":(?:mg|mas|epi|adv)"))) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:prep|pp|infi|[1-3](?:sg|pl))", False, False) and not look(s[:m.start()],r"\b(?:et|ou|de) +$")
def r3010 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(1))
def c3010 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:adj.* is:pl", "(?:is:(?:sg|inv)|po:(?:mg|impe|prn))")
def r3014 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(1))
def c3014 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:adj.* is:(?:pl|fem)", "(?:is:mas is(?:sg|inv)|po:(?:mg|impe|prn))")
def r3018 (LOCALE, s, m):
    return suggFemSing(LOCALE,m.group(1))
def c3018 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:adj.* is:(?:pl|mas)", "(?:is:fem is(?:sg|inv)|po:(?:mg|impe|prn))")
def r3022 (LOCALE, s, m):
    return suggMasPlur(LOCALE,m.group(1))
def c3022 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:adj.* is:(?:sg|fem)", "(?:is:mas is(?:pl|inv)|po:(?:mg|impe|prn))")
def r3026 (LOCALE, s, m):
    return suggFemPlur(LOCALE,m.group(1))
def c3026 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:adj.* is:(?:sg|mas)", "(?:is:fem is(?:pl|inv)|po:(?:mg|impe|prn))")
def c3032 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "v0e", False)
def r3036 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(1))
def c3036 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:[1-3](?:sg|pl)|infi|(?:nom|adj|ppas).* is:pl)", ":(?:mg|adv|sg|inv)")
def r3039 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(1))
def c3039 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:[1-3](?:sg|pl)|infi|(?:nom|adj|ppas).* is:pl)", ":(?:mg|adv|sg|inv)")
def r3042 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(1))
def c3042 (LOCALE, s, m):
    return (morphex(LOCALE,m.group(1), "po:(?:[1-3](?:sg|pl)|infi|(?:nom|adj|ppas).* is:pl)", ":(?:mg|adv|sg|inv)") or morphex(LOCALE,m.group(1), "po:(?:[1-3](?:sg|pl)|(?:adj|ppas).* is:fem)", ":(?:mg|adv|mas|epi)"))
def r3045 (LOCALE, s, m):
    return suggFemSing(LOCALE,m.group(1))
def c3045 (LOCALE, s, m):
    return (morphex(LOCALE,m.group(1), "po:(?:[1-3](?:sg|pl)|infi|(?:nom|adj|ppas).* is:pl)", ":(?:mg|adv|sg|inv)") or morphex(LOCALE,m.group(1), "po:(?:[1-3](?:sg|pl)|(?:adj|ppas).* is:mas)", ":(?:mg|adv|fem|epi)"))
def r3048 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(1))
def c3048 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:[1-3](?:sg|pl)|infi|(?:nom|adj|ppas).* is:sg)", ":(?:mg|adv|pl|inv)")
def r3051 (LOCALE, s, m):
    return suggMasPlur(LOCALE,m.group(1))
def c3051 (LOCALE, s, m):
    return not re.match(u"(?i)légion$", m.group(1)) and (morphex(LOCALE,m.group(1), "po:(?:[1-3](?:sg|pl)|infi|(?:nom|adj|ppas).* is:sg)", ":(?:mg|adv|pl|inv)") or morphex(LOCALE,m.group(1), "po:(?:[1-3](?:sg|pl)|(?:adj|ppas).* is:fem)", ":(?:mg|adv|mas|epi)"))
def r3054 (LOCALE, s, m):
    return suggFemPlur(LOCALE,m.group(1))
def c3054 (LOCALE, s, m):
    return not re.match(u"(?i)légion$", m.group(1)) and (morphex(LOCALE,m.group(1), "po:(?:[1-3](?:sg|pl)|infi|(?:nom|adj|ppas).* is:sg)", ":(?:mg|adv|pl|inv)") or morphex(LOCALE,m.group(1), "po:(?:[1-3](?:sg|pl)|(?:adj|ppas).* is:mas)", ":(?:mg|adv|fem|epi)"))
def r3059 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(1))
def c3059 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:nom|adj)", "(?:po:(?:ppas|adv|mg|nb|patr|prn)|is:(?:pl|inv))") and not re.match(u"(?i)(?:légion|nombre|cause)$", m.group(1))
def c3065 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:(?:nom|ad[jv]|ppas|mg|3pl)") and not look(s[:m.start()],r"(?i)\bce que?\b")
def r3072 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(2))
def c3072 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(?:croire|considérer|montrer|penser|savoir|sentir|voir|vouloir)$", False) and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl", ":(?:mg|adv|sg|inv)")
def r3076 (LOCALE, s, m):
    return suggSing(LOCALE,m.group(2))
def c3076 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(?:croire|considérer|montrer|penser|savoir|sentir|voir|vouloir)$", False) and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:pl", ":(?:mg|adv|sg|inv)")
def r3080 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(3))
def c3080 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(2), u"^(?:croire|considérer|montrer|penser|savoir|sentir|voir|vouloir)$", False) and (morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl", ":(?:mg|adv|sg|inv)") or morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem", ":(?:mg|adv|mas|epi)")) and (not re.match(u"(?:celui-(?:ci|là)|lequel)$", m.group(1)) or not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False))
def r3086 (LOCALE, s, m):
    return suggFemSing(LOCALE,m.group(3))
def c3086 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(2), u"^(?:croire|considérer|montrer|penser|savoir|sentir|voir|vouloir)$", False) and (morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl", ":(?:mg|adv|sg|inv)") or morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas", ":(?:mg|adv|fem|epi)")) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False)
def r3092 (LOCALE, s, m):
    return suggFemSing(LOCALE,m.group(3))
def c3092 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(2), u"^(?:croire|considérer|montrer|penser|savoir|sentir|voir|vouloir)$", False) and (morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:pl", ":(?:mg|adv|sg|inv)") or morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas", ":(?:mg|adv|fem|epi)"))
def r3097 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c3097 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(?:croire|considérer|montrer|penser|savoir|sentir|voir|vouloir)$", False) and morphex(LOCALE,m.group(2), "po:(?:nom|adj).* is:sg", ":(?:mg|adv|pl|inv)")
def r3101 (LOCALE, s, m):
    return suggMasPlur(LOCALE,m.group(3))
def c3101 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(2), u"^(?:croire|considérer|montrer|penser|savoir|sentir|voir|vouloir)$", False) and (morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:sg", ":(?:mg|adv|pl|inv)") or morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:fem", ":(?:mg|adv|mas|epi)")) and (not re.match(u"(?:ceux-(?:ci|là)|lesquels)$", m.group(1)) or not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False))
def r3107 (LOCALE, s, m):
    return suggFemPlur(LOCALE,m.group(3))
def c3107 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(2), u"^(?:croire|considérer|montrer|penser|savoir|sentir|voir|vouloir)$", False) and (morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:sg", ":(?:mg|adv|pl|inv)") or morphex(LOCALE,m.group(3), "po:(?:nom|adj).* is:mas", ":(?:mg|adv|fem|epi)")) and (not re.match(u"(?:elles|celles-(?:ci|là)|lesquelles)$", m.group(1)) or not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False))
def r3119 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(3))
def c3119 (LOCALE, s, m):
    return not re.match(u"(?i)(?:confiance|cours|envie|peine|prise|crainte|cure|affaire|hâte|force|recours)$", m.group(3)) and morph(LOCALE,m.group(2), "po:v0a", False) and morphex(LOCALE,m.group(3), "po:(?:[1-3](?:sg|pl)|ppas.* is:(?:pl|fem))", "po:(?:mg|adv|ppas.* is:mas is:(?:sg|inv))")
def r3124 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(4))
def c3124 (LOCALE, s, m):
    return not re.match(u"(?i)(?:confiance|cours|envie|peine|prise|crainte|cure|affaire|hâte|force|recours)$", m.group(4)) and morph(LOCALE,m.group(3), "po:v0a", False) and morphex(LOCALE,m.group(4), "po:(?:[1-3](?:sg|pl)|ppas.* is:(?:pl|fem))", "po:(?:mg|adv|ppas.* is:mas is:(?:sg|inv))")
def r3129 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c3129 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False) and morphex(LOCALE,m.group(2), "po:v[0-3]..t.* po:ppas.* is:sg", ":(?:mg|adv|pl|inv)") and not morph(LOCALE,word1(s[m.end():]), "po:v", False)
def r3133 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(3))
def c3133 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v0a", False) and morphex(LOCALE,m.group(3), "po:v[0-3]..t_.* po:ppas.* is:sg", ":(?:mg|adv|pl|inv)") and not look(s[:m.start()],r"\bque?\b")
def r3137 (LOCALE, s, m):
    return m.group(2)[:-1]
def c3137 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False) and morphex(LOCALE,m.group(2), "po:v[0-3]..t.* po:ppas.* is:pl", ":(?:mg|adv|sg|inv)") and not morph(LOCALE,word1(s[m.end():]), "po:v", False)
def r3141 (LOCALE, s, m):
    return m.group(3)[:-1]
def c3141 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v0a", False) and morphex(LOCALE,m.group(3), "po:v[0-3]..t_.* po:ppas.* is:pl", ":(?:mg|adv|sg|inv)") and not look(s[:m.start()],r"\bque?\b") and not morph(LOCALE,word1(s[m.end():]), "po:v", False)
def r3145 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(1))
def c3145 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:ppas.* is:(?:fem|mas is:pl)", "is:(?:mas is:(?:sg|inv)|fem)")
def r3149 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(1))
def c3149 (LOCALE, s, m):
    return not re.match(u"(?i)(?:confiance|cours|envie|peine|prise|crainte|cure|affaire|hâte|force|recours)$", m.group(1)) and morphex(LOCALE,m.group(1), "po:ppas.* is:(?:fem|mas is:pl)", "is:mas is:(?:sg|inv)") and look(s[:m.start()],u"(?i)(?:après +$|sans +$|pour +$|que? +$|quand +$|, +$|^ *$)")
def r3156 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(4), m.group(2))
def c3156 (LOCALE, s, m):
    return morph(LOCALE,m.group(3), "po:v0a", False) and not (re.match(u"(?:décidé|essayé|tenté)$", m.group(4)) and look(s[m.end():],u" +d(?:e |’)")) and morph(LOCALE,m.group(2), "po:(?:nom|adj)", False) and morphex(LOCALE,m.group(4), "po:v[0-3]..t.* po:ppas.* is:sg", ":(?:mg|adv|pl|inv)") and not morph(LOCALE,word1(s[m.end():]), "po:(?:infi|properobj)", False)
def r3163 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(4))
def c3163 (LOCALE, s, m):
    return morph(LOCALE,m.group(3), "po:v0a", False) and not (re.match(u"(?:décidé|essayé|tenté)$", m.group(4)) and look(s[m.end():],u" +d(?:e |’)")) and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:mas", False) and (morphex(LOCALE,m.group(4), "po:v[0-3]..t.* po:ppas.* is:fem", ":(?:mg|adv|mas|epi)") or morphex(LOCALE,m.group(4), "po:v[0-3]..t.* po:ppas.* is:pl", ":(?:mg|adv|sg|inv)")) and not morph(LOCALE,word1(s[m.end():]), "po:(?:infi|properobj)", False)
def r3171 (LOCALE, s, m):
    return suggFemSing(LOCALE,m.group(4))
def c3171 (LOCALE, s, m):
    return morph(LOCALE,m.group(3), "po:v0a", False) and not (re.match(u"(?:décidé|essayé|tenté)$", m.group(4)) and look(s[m.end():],u" +d(?:e |’)")) and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:fem", False) and (morphex(LOCALE,m.group(4), "po:v[0-3]..t.* po:ppas.* is:mas", ":(?:mg|adv|fem|epi)") or morphex(LOCALE,m.group(4), "po:v[0-3]..t.* po:ppas.* is:pl", ":(?:mg|adv|sg|inv)")) and not morph(LOCALE,word1(s[m.end():]), "(?:po:(?:infi|properobj)|st:que?)", False)
def c3179 (LOCALE, s, m):
    return not re.match("(A|avions)$", m.group(1)) and morph(LOCALE,m.group(1), "po:v0a", False) and morph(LOCALE,m.group(2), "po:v(?!.* po:ppas)")
def c3184 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False) and (morph(LOCALE,m.group(3), "po:infi") or re.match("(?:[mtsn]e|[nv]ous|leur|lui)$", m.group(3)))
def c3190 (LOCALE, s, m):
    return not morph(LOCALE,word1(s[m.end():]), "po:(?:nom|adj).* is:mas", False)
def c3192 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0e", False)
def r3196 (LOCALE, s, m):
    return suggMasSing(LOCALE,m.group(2))
def c3196 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False) and morphex(LOCALE,m.group(2), "po:(?:infi|2pl|ppas.* is:(?:fem|pl))", "is:mas is:(?:sg|inv)") and m.group(2) != "prise" and not morph(LOCALE,wordmin1(s[:m.start()]), "(?:st:(?:les|[nv]ous)|po:(?:nom|adj).* is:(?:fem|pl))", False) and not look(s[:m.start()],r"(?i)\bquel(?:le|)s?\b")
def r3201 (LOCALE, s, m):
    return suggPlur(LOCALE,m.group(2))
def c3201 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False) and morphex(LOCALE,m.group(2), "po:v[1-3]..t.* po:ppas.* is:sg", ":(?:mg|adv|pl|inv)")
def r3206 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "1pl")
def c3206 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:(?:mg|infi|1pl|3(?:sg|pl))") and not look(s[m.end():],"^ +(?:je|tu|ils?|elles?|on|[vn]ous)")
def r3211 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "2pl")
def c3211 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:(?:mg|infi|2pl|3(?:sg|pl))") and not look(s[m.end():],"^ +(?:je|ils?|elles?|on|[vn]ous)")
def c3256 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:(?:nom|adj)", "po:mg")
def c3264 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v[13].* po:ipre.* po:2sg", "po:(?:mg|nom|adj)")
def c3267 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v[13].* po:ipre.* po:2sg", "po:mg")
def c3272 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v[23].* po:ipre.* po:3sg", "po:(mg|nom|adj)") and morph(LOCALE,m.group(2)+"s", "po:impe po:2sg", False) and not re.match("(?i)doit", m.group(1)) and not (re.match("(?i)vient$", m.group(1)) and look(s[m.end():]," +l[ea]")) and morph(LOCALE,m.group(2)+"s", "po:impe po:2sg", False)
def c3276 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v[23].* po:ipre.* po:3sg", "po:mg") and morph(LOCALE,m.group(2)+"s", "po:impe po:2sg", False)
def c3281 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v3.* po:ipre.* po:3sg", "po:(?:mg|nom|adj)")
def c3284 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v3.* po:ipre.* po:3sg", "po:mg")
def c3294 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:adj", "po:mg") and not look(s[m.end():],r"\bsoit\b")
def c3305 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "(?:po:impe|st:chez)", False) and spell(LOCALE,m.group(1))
def c3310 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:impe", "po:(?:prn|patr|mg)")
def c3314 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:impe", "po:(?:prn|patr|mg)") and not morph(LOCALE,word1(s[m.end():]), "po:infi", False, False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:cjco", False, True)
def c3318 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:impe", "po:(?:prn|patr|mg)") and not morph(LOCALE,word1(s[m.end():]), "po:(?:nom|adj|ppas|infi|nb|3(?:sg|pl))", False, False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:cjco", False, True)
def c3322 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:impe", "po:(?:prn|patr|mg)") and not morph(LOCALE,word1(s[m.end():]), "po:(?:nom|adj|ppas|infi|npr)", False, False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:cjco", False, True)
def c3328 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:impe", "po:(?:prn|patr|mg)") and not morph(LOCALE,word1(s[m.end():]), "po:(?:infi|[1-3])", False, False)
def c3332 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:impe", False)
def c3336 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:impe", False) and morphex(LOCALE,word1(s[m.end():]), "po:(?:prep|cj)", "po:(?:nom|adj)", True)
def c3340 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:impe", False) and morphex(LOCALE,word1(s[m.end():]), "po:(?:prep|cj)", "po:infi", True)
def c3345 (LOCALE, s, m):
    return not morph(LOCALE,word1(s[m.end():]), "po:infi", False, False)
def c3347 (LOCALE, s, m):
    return not wordmin1(s[:m.start()]) and not morph(LOCALE,word1(s[m.end():]), "po:infi", False, False)
def c3375 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, True)
def c3376 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False)
def c3378 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False)
def c3380 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:[123](?:sg|pl)")
def c3381 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:[123]sg", False, False)
def c3382 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:[123]sg|prep)", False, False)
def c3383 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:[123]pl|prep)", False, False)
def c3384 (LOCALE, s, m):
    return not morph(LOCALE,wordmin1(s[:m.start()]), "po:3pl", False, False)
def c3385 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:[123](?:sg|pl)", False)
def c3386 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:[123](?:sg|pl)", "po:(?:(?:nom|adj).* is:mas is:(?:sg|inv)|mg|prn|patr|npr)")
def c3387 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:[123](?:sg|pl)", "po:(?:(?:nom|adj).* is:fem is:(?:sg|inv)|mg|prn|patr|npr)")
def c3388 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:[123](?:sg|pl)", "po:(?:(?:nom|adj).* is:(?:sg|inv)|mg|prn|patr|npr)")
def c3390 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:[123](?:sg|pl)", "po:(?:adj|mg|prn|patr|npr|1pl)")
def c3391 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:[123](?:sg|pl)", "po:(?:adj|mg|prn|patr|npr|2pl)")
def c3393 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v", False)
def c3394 (LOCALE, s, m):
    return morph(LOCALE,m.group(3), "po:v", False)
def c3395 (LOCALE, s, m):
    return not morph(LOCALE,m.group(2), "po:2sg", False) or look(s[:m.start()],"(?i)\\b(?:je|tu|on|ils?|elles?|nous) +$")
def c3396 (LOCALE, s, m):
    return not morph(LOCALE,m.group(2), "po:2sg", False) or look(s[:m.start()],"(?i)\\b(?:je|tu|on|ils?|elles?|nous) +$")
def c3419 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:v", False)
def c3422 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:infi")
def c3430 (LOCALE, s, m):
    return not look(s[:m.start()],r"\bce que?\b")
def c3437 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v", False) and not (m.group(1).endswith("ez") and look(s[m.end():]," +vous"))
def c3440 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:ppas|2pl)", False)
def c3443 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(?:aimer|aller|désirer|devoir|espérer|pouvoir|préférer|souhaiter|venir)$", False) and not morph(LOCALE,m.group(1), "po:(?:mg|nom)", False) and morph(LOCALE,m.group(2), "po:v", False)
def c3446 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), "^devoir$", False) and morph(LOCALE,m.group(2), "po:v", False) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:det", False)
def c3449 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(?:cesser|décider|défendre|suggérer|commander|essayer|tenter|choisir|permettre|interdire)$", False) and morph(LOCALE,m.group(2), "po:(?:ppas|2pl)", False)
def c3452 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:ppas|2pl)", False)
def c3455 (LOCALE, s, m):
    return "valoir" in stem(LOCALE,m.group(1)) and morphex(LOCALE,m.group(2), "po:(?:ppas|2pl)", "po:(?:mg|patr|prn)")
def c3457 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v1", "po:nom") and m.group(1) != u"pâté"
def c3458 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v1", "po:nom")
def c3466 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0e", False) and (morphex(LOCALE,m.group(2), "po:infi", "po:(?:adj|nom)") or m.group(2) in aSHOULDBEVERB) and not re.match(u"(?i)(soit|été)", m.group(1)) and not morph(LOCALE,wordmin1(s[:m.start()]), "(?:po:infi|st:ce)", False, False) and not look(s[:m.start()],"(?i)ce (?:>|qu|que >) $") and not lookchk1(LOCALE,s[:m.start()],u"(\w[\w-]+) +> $", "po:infi") and not lookchk1(LOCALE,s[:m.start()],u"^ +>? *(\w[\w-]+)", "po:infi")
def c3470 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:v0a", False) and morph(LOCALE,m.group(2), "po:infi", False)
def r3478 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "1sg")
def c3478 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:1sg|mg)") and not (morph(LOCALE,m.group(2), "po:pp", False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:v0.* po:1sg", False, False))
def r3481 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "1sg")
def c3481 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:1sg|mg|1pl)")
def r3484 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "1sg")
def c3484 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:1sg|mg|1pl)")
def r3487 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "1sg")
def c3487 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:1sg|mg|1pl|3pl!)")
def r3492 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "2sg")
def c3492 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:mg|(?:i(?:pre|imp|psi|fut)|s(?:pre|imp)|cond).* po:2sg)") and not (morph(LOCALE,m.group(2), "po:pp", False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:v0.* po:2sg", False, False))
def r3495 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "2sg")
def c3495 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:mg|(?:i(?:pre|imp|psi|fut)|s(?:pre|imp)|cond).* po:2sg)")
def r3498 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "2sg")
def c3498 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:mg|2pl|3pl!|(?:i(?:pre|imp|psi|fut)|s(?:pre|imp)|cond).* po:2sg)")
def r3503 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3sg")
def c3503 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:3sg|ppre|mg)") and not (morph(LOCALE,m.group(2), "po:pp", False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:v0.* po:3sg", False, False))
def r3506 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3sg")
def c3506 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:3sg|ppre|mg)")
def r3510 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3sg")
def c3510 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:nom|adj|3sg|pp|mg|v0e.* po:3pl)")
def r3514 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3sg")
def c3514 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:3sg|pp|mg)")
def r3518 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3sg")
def c3518 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:3sg|pp|mg|3pl!)") and not morph(LOCALE,wordmin1(s[:m.start()]), "(?:po:(?:prep|v)|st:de)", False, False) and not(m.group(1).endswith("out") and morph(LOCALE,m.group(2), "po:infi", False))
def r3522 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "3sg")
def c3522 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:(?:3sg|ppre|mg|3pl!)") and not morph(LOCALE,wordmin1(s[:m.start()]), "(?:po:prep|st:(?:et|ou))", False, False) and not (morph(LOCALE,m.group(1), "po:pp", False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:v0.* po:3sg", False, False))
def r3526 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "3sg")
def c3526 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:(?:3sg|ppre|mg|3pl!)") and not morph(LOCALE,wordmin1(s[:m.start()]), "(?:po:prep|st:(?:et|ou))", False, False)
def c3530 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:3pl", "po:(?:mg|3sg)")
def c3533 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:3sg", "po:(?:mg|3pl)")
def r3537 (LOCALE, s, m):
    return m.group(1)[:-1]+"t"
def r3539 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3sg")
def c3539 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:3sg|ppre|mg)") and morphex(LOCALE,wordmin1(s[:m.start()]), "po:cj", "po:(?:infi|pp|[1-3](?:sg|pl)|prep)", True) and not( m.group(1).endswith("ien") and look(s[:m.start()],"> +$") and morph(LOCALE,m.group(2), "po:infi", False) )
def r3545 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3sg")
def c3545 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:infi", False) and morph(LOCALE,m.group(2), "po:v.\w+(?!.* po:(?:3sg|pp|infi|3pl!))")
def r3550 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(3), "3sg")
def c3550 (LOCALE, s, m):
    return morphex(LOCALE,wordmin1(s[:m.start()]), "po:cj", "po:(?:infi|ppre)", True) and morph(LOCALE,m.group(2), "po:(?:nom|adj|ppas).* is:(?:sg|inv)", False) and morph(LOCALE,m.group(3), "po:v.\w+(?!.* po:(?:3sg|pp|infi|3pl!))") and not (re.match("(?i)et|ou$", wordmin1(s[:m.start()])) and morph(LOCALE,m.group(3), "po:[1-3]pl", False)) and not look(s[:m.start()],r"(?i)\bni .* ni\b")
def r3554 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(3), "3sg")
def c3554 (LOCALE, s, m):
    return morphex(LOCALE,wordmin1(s[:m.start()]), "po:cj", "is:(?:infi|ppre)", True) and morph(LOCALE,m.group(2), "po:(?:nom|adj|ppas).* is:(?:sg|inv)", False) and morphex(LOCALE,m.group(3), "po:v", "po:(?:3sg|1pl|pp|infi|3pl!|mg)") and not (re.match("(?i)et|ou$", wordmin1(s[:m.start()])) and morph(LOCALE,m.group(3), "po:[1-3]pl", False)) and not look(s[:m.start()],r"(?i)\bni .* ni\b")
def r3560 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(3), "3sg")
def c3560 (LOCALE, s, m):
    return morphex(LOCALE,wordmin1(s[:m.start()]), "po:cj", "po:(?:infi|ppre)", True) and isAmbiguousAndWrong(m.group(2), m.group(3), "sg", "3sg") and not (re.match("(?i)et|ou$", wordmin1(s[:m.start()])) and morph(LOCALE,m.group(3), "(?:po:[1-3]pl|is:pl)", False)) and not look(s[:m.start()],r"(?i)\bni .* ni\b")
def r3565 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(3), "3sg")
def c3565 (LOCALE, s, m):
    return morphex(LOCALE,wordmin1(s[:m.start()]), "po:cj", "po:(?:infi|ppre)", True) and isVeryAmbiguousAndWrong(m.group(2), m.group(3), "sg", "3sg", not wordmin1(s[:m.start()])) and not (re.match("(?i)et|ou$", wordmin1(s[:m.start()])) and morph(LOCALE,m.group(3), "(?:po:[1-3]pl|is:pl)", False)) and not look(s[:m.start()],r"(?i)\bni .* ni\b")
def r3571 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(0), "3sg")
def c3571 (LOCALE, s, m):
    return ( morph(LOCALE,m.group(0), "po:1sg") or ( look(s[:m.start()],"> +$") and morph(LOCALE,m.group(0), "po:1sg", False) ) ) and not (m.group(0)[0:1].isupper() and look(s[:m.start()],r"\w")) and not look(s[:m.start()],u"(?i)\\b(?:j(?:e |’)|moi,? qui )")
def r3575 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(0), "3sg")
def c3575 (LOCALE, s, m):
    return morphex(LOCALE,m.group(0), "po:2sg", "po:(?:impe|mg|adv|prn|patr|npr|interj|[13](?:sg|pl)|2pl)") and not m.group(0)[0:1].isupper() and not look(s[:m.start()],"^ *$") and ( not morph(LOCALE,m.group(0), "po:(?:nom|adj)", False) or look(s[:m.start()],"> +$") ) and not look(s[:m.start()],u"(?i)\\bt(?:u |’|oi,? qui )")
def r3580 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(0), "3sg")
def c3580 (LOCALE, s, m):
    return morphex(LOCALE,m.group(0), "po:2sg", "po:(?:mg|adv|prn|patr|npr|interj|[13](?:sg|pl)|2pl)") and not (m.group(0)[0:1].isupper() and look(s[:m.start()],r"\w")) and ( not morph(LOCALE,m.group(0), "po:(?:nom|adj)", False) or look(s[:m.start()],"> +$") ) and not look(s[:m.start()],u"(?i)\\bt(?:u |’|oi,? qui )")
def r3585 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(0), "3sg")
def c3585 (LOCALE, s, m):
    return morphex(LOCALE,m.group(0), "po:[12]sg", "po:(?:impe|mg|adv|prn|patr|npr|interj|3(?:sg|pl)|2pl|1pl)") and not (m.group(0)[0:1].isupper() and look(s[:m.start()],r"\w")) and ( not morph(LOCALE,m.group(0), "po:(?:nom|adj)", False) or look(s[:m.start()],"> +$") or ( re.match(u"(?i)étais$", m.group(0)) and not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:det|adj).* is:pl", False, True) ) ) and not look(s[:m.start()],u"(?i)\\b(?:j(?:e |’)|moi,? qui |t(?:u |’|oi,? qui ))")
def r3590 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(0), "3sg")
def c3590 (LOCALE, s, m):
    return not (m.group(0)[0:1].isupper() and look(s[:m.start()],r"\w")) and not look(s[:m.start()],u"(?i)\\b(?:j(?:e |’)|moi,? qui |t(?:u |’|oi,? qui ))")
def r3593 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(0), "3sg")
def c3593 (LOCALE, s, m):
    return not (m.group(0)[0:1].isupper() and look(s[:m.start()],r"\w")) and not look(s[:m.start()],u"(?i)\\b(?:j(?:e |’)|moi,? qui |t(?:u |’|oi,? qui ))")
def r3598 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "1pl")
def c3598 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:(?:1pl|3(?:sg|pl))") and not look(s[m.end():],"^ +(?:je|tu|ils?|elles?|on|[vn]ous)")
def r3601 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "1pl")
def c3601 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:1pl") and not look(s[m.end():],"^ +(?:je|tu|ils?|elles?|on|[vn]ous)")
def r3604 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "1pl")
def c3604 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:1pl") and not look(s[m.end():],"^ +(?:ils|elles)")
def r3609 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "2pl")
def c3609 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:(?:2pl|3(?:sg|pl))") and not look(s[m.end():],"^ +(?:je|ils?|elles?|on|[vn]ous)")
def r3612 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "2pl")
def c3612 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:2pl") and not look(s[m.end():],"^ +(?:je|ils?|elles?|on|[vn]ous)")
def r3617 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3pl")
def c3617 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:3pl|pp|mg)") and not (morph(LOCALE,m.group(2), "po:pp", False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:v0.* po:3pl", False, False))
def r3620 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3pl")
def c3620 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:3pl|pp|mg)")
def r3624 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3pl")
def c3624 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:3pl|pp|mg)")
def r3628 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3pl")
def c3628 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:3pl|pp|mg)") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:prep|v)", False, False)
def r3632 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "3pl")
def c3632 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:(?:3pl|pp|mg)") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False) and not (morph(LOCALE,m.group(1), "po:pp", False) and morph(LOCALE,wordmin1(s[:m.start()]), "po:v0.* is:3pl", False, False))
def r3635 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "3pl")
def c3635 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v", "po:(?:3pl|pp|mg)") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False)
def c3643 (LOCALE, s, m):
    return not re.match(u"(à|avec|sur|chez|par|dans)$", wordmin1(s[:m.start()]))
def r3647 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3pl")
def c3647 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:3pl|pp|mg)") and not morph(LOCALE,wordmin1(s[:m.start()]), "(?:po:(?:prep|v)|st:de )", False, False)
def r3651 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3pl")
def c3651 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:3pl|pp|mg)") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:prep|v)", False, False)
def r3655 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3pl")
def c3655 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v", "po:(?:mg|adj|nom|3pl|pp)") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:prep|v)", False, False)
def r3659 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(3), "3pl")
def c3659 (LOCALE, s, m):
    return morphex(LOCALE,wordmin1(s[:m.start()]), "po:cj", "po:(?:infi|ppre)", True) and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:pl|inv)", False) and morph(LOCALE,m.group(3), "po:v.\w+(?!.* po:(?:[13]pl|pp|infi))")
def r3662 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(3), "3pl")
def c3662 (LOCALE, s, m):
    return morphex(LOCALE,wordmin1(s[:m.start()]), "po:cj", "po:(?:infi|ppre)", True) and morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:pl|inv)", False) and morphex(LOCALE,m.group(3), "po:v", "po:(?:[13]pl|ppre|infi|mg)")
def r3667 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(3), "3pl")
def c3667 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:pl|inv)", False) and morphex(LOCALE,m.group(3), "po:v", "po:(?:[13]pl|ppre|mg)") and morph(LOCALE,word1(s[m.end():]), "po:(?:prep|det.* is:pl)|st:au ", False, True)
def r3670 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(3), "3pl")
def c3670 (LOCALE, s, m):
    return morph(LOCALE,m.group(2), "po:(?:nom|adj).* is:(?:pl|inv)", False) and morphex(LOCALE,m.group(3), "po:v", "po:(?:[13]pl|ppre|mg)")
def r3676 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(3), "3pl")
def c3676 (LOCALE, s, m):
    return morphex(LOCALE,wordmin1(s[:m.start()]), "po:cj", "po:(?:infi|ppre)", True) and isAmbiguousAndWrong(m.group(2), m.group(3), "pl", "3pl")
def r3680 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3pl")
def c3680 (LOCALE, s, m):
    return morphex(LOCALE,wordmin1(s[:m.start()]), "po:cj", "po:(?:infi|ppre)", True) and isVeryAmbiguousAndWrong(m.group(1), m.group(2), "pl", "3pl", not wordmin1(s[:m.start()]))
def r3684 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3pl")
def c3684 (LOCALE, s, m):
    return morphex(LOCALE,wordmin1(s[:m.start()]), "po:cj", "po:(?:infi|ppre)", True) and isVeryAmbiguousAndWrong(m.group(1), m.group(2), "mas pl", "3pl", not wordmin1(s[:m.start()]))
def r3688 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3pl")
def c3688 (LOCALE, s, m):
    return morphex(LOCALE,wordmin1(s[:m.start()]), "po:cj", "po:(?:infi|ppre)", True) and isVeryAmbiguousAndWrong(m.group(1), m.group(2), "fem pl", "3pl", not wordmin1(s[:m.start()]))
def r3696 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "3sg")
def c3696 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v0e", "po:3sg")
def r3700 (LOCALE, s, m):
    return m.group(1)[:-1]
def c3700 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v0e.* po:3sg", "po:3pl")
def r3705 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "3pl")
def c3705 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v0e", "po:3pl")
def c3709 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:v0e.* po:3pl", "po:3sg")
def r3717 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(2), "3sg")
def c3717 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:prn|patr|npr)", False) and morphex(LOCALE,m.group(2), "po:[123](?:sg|pl)", "po:(?:mg|3sg|3pl!|ppre|(?:adj|ppas).* is:(?:sg|inv))") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:(?:prep|v|det)", False, False) and not look(s[:m.start()],u"([A-ZÉÈ][\w-]+), +([A-ZÉÈ][\w-]+), +$")
def r3723 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(3), "3pl")
def c3723 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:prn|patr|npr)", False) and morph(LOCALE,m.group(2), "po:(?:prn|patr|npr)", False) and morphex(LOCALE,m.group(3), "po:[123](?:sg|pl)", "po:(?:mg|3pl|ppre|ppas.* is:(?:pl|inv))") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:prep", False, False)
def r3732 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "3sg")
def c3732 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:[12]sg|3pl)", "po:(?:3sg|mg|adv|3pl!)")
def r3736 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "3pl")
def c3736 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:[123]sg", "po:(?:3pl|mg|adv)")
def c3740 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:[12](?:sg|pl)", "po:(?:mg|adv|3(?:sg|pl)|infi|pp)")
def c3744 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:[12](?:sg|pl)", "po:(?:mg|adv|3(?:sg|pl))")
def c3751 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v.* po:1sg", "po:(?:mg|adv|nom)") and not look(s[:m.start()],r"(?i)\bje +>? *$")
def c3754 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:v.* po:1sg", "po:(?:mg|adv|nom)") and not look(s[:m.start()],r"(?i)\bje +>? *$")
def c3757 (LOCALE, s, m):
    return not look(s[m.end():],"^ +(?:en|y|ne|>)") and morphex(LOCALE,m.group(1), "po:v.* po:2sg", "po:(?:mg|adv|nom)") and not look(s[:m.start()],r"(?i)\b(je|tu) +>? *$")
def c3760 (LOCALE, s, m):
    return not look(s[m.end():],"^ +(?:en|y|ne|>)") and morphex(LOCALE,m.group(1), "po:v.* po:3sg", "(po:(?:mg|adv|nom)|dp:ca)") and not look(s[:m.start()],r"(?i)\b(ce|il|elle|on) +>? *$")
def c3763 (LOCALE, s, m):
    return not look(s[m.end():],"^ +(?:en|y|ne|aussi|>)") and morphex(LOCALE,m.group(1), "po:v.* po:3sg", "(po:(?:mg|adv|nom)|dp:ca)") and not look(s[:m.start()],r"(?i)\b(ce|il|elle|on) +>? *$")
def c3766 (LOCALE, s, m):
    return not look(s[m.end():],"^ +(?:en|y|ne)") and morphex(LOCALE,m.group(1), "po:v.* po:3sg", "po:(?:mg|adv|nom)")
def c3769 (LOCALE, s, m):
    return not look(s[m.end():],"^ +(?:en|y|ne|aussi|>)") and morphex(LOCALE,m.group(1), "po:v.* po:1pl", "po:(?:mg|adv|nom)") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:propersuj", False, False) and not morph(LOCALE,word1(s[m.end():]), "po:infi", False, False)
def c3773 (LOCALE, s, m):
    return not look(s[m.end():],"^ +(?:en|y|ne|aussi|>)") and not m.group(1).endswith("euillez") and morphex(LOCALE,m.group(1), "po:v.* is:2pl", "po:(?:mg|adv|nom)") and not morph(LOCALE,wordmin1(s[:m.start()]), "po:propersuj", False, False) and not morph(LOCALE,word1(s[m.end():]), "po:infi", False, False)
def c3777 (LOCALE, s, m):
    return not look(s[m.end():],"^ +(?:en|y|ne|aussi|>)") and morphex(LOCALE,m.group(1), "po:v.* po:3pl", "po:(?:mg|adv|nom)") and not look(s[:m.start()],r"(?i)\b(ce|ils|elles) +>? *$")
def r3782 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "1isg")
def c3782 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "po:1[ij]?sg", False) and spell(LOCALE,m.group(1)) and not re.match("(?i)vite$", m.group(1))
def r3784 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "2sg")
def c3784 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "po:(?:[is](?:pre|imp)|ipsi|ifut|cond).* po:2sg", False) and spell(LOCALE,m.group(1)) and not re.match("(?i)vite$", m.group(1))
def r3786 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "3sg")
def c3786 (LOCALE, s, m):
    return m.group(1) != "t" and not morph(LOCALE,m.group(1), "po:3sg", False) and (not m.group(1).endswith(u"oilà") or m.group(2) != "il") and spell(LOCALE,m.group(1)) and not re.match("(?i)vite$", m.group(1))
def c3788 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:3pl", "po:3sg") and spell(LOCALE,m.group(1))
def c3790 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "po:(?:1pl|2sg|2pl)", False) and spell(LOCALE,m.group(1)) and not re.match("(?i)(vite|chez)$", m.group(1))
def c3792 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "po:2pl", False) and spell(LOCALE,m.group(1)) and not re.match("(?i)(tes|vite)$", m.group(1)) and not spell(LOCALE,m.group(0))
def r3794 (LOCALE, s, m):
    return suggVerb(LOCALE,m.group(1), "3pl")
def c3794 (LOCALE, s, m):
    return m.group(1) != "t" and not morph(LOCALE,m.group(1), "po:3pl", False) and spell(LOCALE,m.group(1))
def c3797 (LOCALE, s, m):
    return not morph(LOCALE,m.group(1), "po:v", False) and not m.group(1).endswith(u"oilà") and not re.match("(?i)vite$", m.group(1)) and spell(LOCALE,m.group(1))
def c3811 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:propersuj|patr|prn|npr)", False) and morphex(LOCALE,m.group(2), "po:(?:cond|spre|simp)", "po:(?:mg|v0|i)") and not wordmin1(s[:m.start()])
def c3814 (LOCALE, s, m):
    return morphex(LOCALE,m.group(1), "po:(?:cond|spre|simp)", "po:(?:mg|v0|i(?:pre|imp|fut|psi))") and not wordmin1(s[:m.start()])
def r3819 (LOCALE, s, m):
    return suggVerbMode(LOCALE,m.group(3), "s", m.group(2))
def c3819 (LOCALE, s, m):
    return stemchk(LOCALE,m.group(1), u"^(afin|pour|quoi|permettre|falloir|vouloir|ordonner|exiger|désirer|suffire)$", False) and morph(LOCALE,m.group(2), "po:(?:prn|patr|npr|propersuj)", False) and not morph(LOCALE,m.group(3), "po:(?:spre|simp|mg|infi)", False)
def r3825 (LOCALE, s, m):
    return suggVerbMode(LOCALE,m.group(2), "i", m.group(1))
def c3825 (LOCALE, s, m):
    return morphex(LOCALE,m.group(2), "po:spre", "po:(?:i(?:pre|imp|fut|psi)|cond|mg)")
def r3830 (LOCALE, s, m):
    return suggVerbMode(LOCALE,m.group(2), "i", m.group(1))
def c3830 (LOCALE, s, m):
    return morph(LOCALE,m.group(1), "po:(?:propersuj|patr|prn|npr)", False) and morph(LOCALE,m.group(2), "po:v.* po:s(?:pre|imp)")

