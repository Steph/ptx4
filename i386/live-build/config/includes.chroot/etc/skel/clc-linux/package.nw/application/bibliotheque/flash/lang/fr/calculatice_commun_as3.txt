//Français
//Les boutons
p1_1=Commencer
p1_2=Options
p1_3=Valider
p1_4=Suite
p1_5=Résultats
p1_6=Recommencer
p1_7=Valider options
p1_8=Tester options
p1_9=Suivant
//Messages info
p3_1 =Tu n'as pas donné de réponse.
p3_2 =Bravo !
p3_3 =Bravo ! Cet exercice est terminé.
p3_4 =Faux ! Regarde bien la correction.
p3_5 =C'est faux ! Mais tu as encore un essai.
p3_6 =C'est faux ! Cet exercice est terminé. Clique sur le bouton "Suite" pour afficher tes résultats.
p3_7 =Le temps est dépassé !
//message final d'appréciation.
//score=max
p4_1 =Voici tes résultats
//score > moyenne
p4_2 =Voici tes résultats
//score=moyenne
p4_3 =Voici tes résultats
//score<moyenne
p4_4 =Voici tes résultats
//Divers
p5_1 =Question n°
p5_2 =Mon score
p5_3 =sur
p5_4 =Score
p5_5 =Nombre de réussites au premier essai
p5_6=Exercice réalisé en
