var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.balance = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var aDonnee = [], tabMasse = [],tabx = [],taby = [],champReponse,sommeTotale,massMax, massMin,nbMasse,q,etiquette,fondbalance,textBalance;
// Référencer les ressources de l'exercice (textes, image, son)
exo.oRessources = { 
    txt : "balance/textes/balance_fr.json",
    balance : "balance/images/balance.png",
    masse1: "balance/images/masse.png",
    illustration : "balance/images/illustration.png"
};

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        tempsExo:0,
        temps_question:60,
        typeCalcul:0,
        nbreCaisse :2
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    aDonnee = [];
	var i, nA,nB,nC,nD,aNombre;
	//Ecart entre des dizaines
	if(exo.options.typeCalcul === 0 ){
		aNombre = [20,30,40,50,60,70,80,90];
	} else if(exo.options.typeCalcul==1 ){
		aNombre = [200,300,400,500,600,700,800,900];
	} else if(exo.options.typeCalcul==2 ){
		aNombre = [20,30,40,50,60,70,80,90,100,200,300,400,500];
	}
	var aTemp = [];
        //console.log(exo.options.nbreCaisse);
	for(i=0;i<exo.options.totalQuestion;i++){
        util.shuffleArray(aNombre);
		//aNombre.sort(function(){return Math.floor(Math.random()*3)-1});
		var max = Math.max.apply(this,aNombre.slice(0,exo.options.nbreCaisse));
		var min = Math.min.apply(this,aNombre.slice(0,exo.options.nbreCaisse));
		var ecart = max - min;
		if (
                (exo.options.typeCalcul<2 && aTemp.indexOf(ecart) < 0)
                || (exo.options.typeCalcul == 2 && aTemp.indexOf(ecart) < 0 && max > 99 && min < 100 )
            )
		{
			aTemp.push(ecart);
            var tepm = aNombre.slice(0,exo.options.nbreCaisse);
			aDonnee.push(tepm);
		} else {
			i--;
		}
	}
};
//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};
//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    exo.keyboard.config({
		arrow: "disabled"
    });
    exo.blocInfo.css({
        left:560,
        top:140
    });
    //typeQ : 0 somme 1 écart
    var typeQ=0;
    //reponse en g 0 ou kg 1
    var typeRep=0;
    //valeurs utilisées dans cette question
    massMax=0;
    massMin=0;
    //
    var afficheB="";
    q = exo.indiceQuestion;
    nbMasse=exo.options.nbreCaisse;
    typeQ=1; //on demande l'écart dans cette version
    typeRep=0;
    massMax=Math.max.apply(this,aDonnee[q]);
    massMin=Math.min.apply(this,aDonnee[q]);
    sommeTotale=massMax-massMin;
    //la consigne
    etiquetteQuestion = disp.createTextLabel(exo.txt.consigne1);
    exo.blocAnimation.append(etiquetteQuestion);
    etiquetteQuestion.css({
        left:20,
        top:320
    });
    champReponse = disp.createTextField(exo,5);
    exo.blocAnimation.append(champReponse);
    champReponse.position({
            my:'left top',
            at:'right+5 top-5',
            of:etiquetteQuestion
    });
    champReponse.focus();
    champReponse.on("touchstart",function(e){
        champReponse.focus();
    });
    var symboleG = disp.createTextLabel("g");
    exo.blocAnimation.append(symboleG);
    symboleG.position({
            my:'left top',
            at:'left+115 top+5',
            of:champReponse
    });
    //la balance
    fondbalance = disp.createMovieClip(exo,"balance",190,105,100);
    fondbalance.gotoAndStop(2);
    fondbalance.conteneur.css({
        left:300,
        top:200
    });
    exo.blocAnimation.append(fondbalance.conteneur);
    textBalance = disp.createTextLabel("0 g");
    textBalance.css({
        fontSize:'22',
        fontWeight:'bold',
        textAlign:'right',
        //backgroundColor:'333',
        width:'120',
        //borderRadius:'4',
        color:'#9F9',
        left:350,
        top:235
    });
    exo.blocAnimation.append(textBalance);
    //les paquets
    tabx=[50,150,250,350];
    taby=[20,20,20,20];
    for (i=0; i<nbMasse; i++) {
        tabMasse[i] = disp.createMovieClip(exo,"masse1",90,130,100);
        tabMasse[i].gotoAndStop(Math.floor(5*Math.random())+1);
        tabMasse[i].conteneur.data("valeur",aDonnee[q][i]);
        tabMasse[i].conteneur.data("place",false);
        tabMasse[i].conteneur.data("indice",i);
        etiquette = disp.createTextLabel(aDonnee[q][i]+" g");
        etiquette.css({
            opacity:'0',
            x:10,
            y:90
        });
        tabMasse[i].conteneur.append(etiquette);
        tabMasse[i].conteneur.css({x:tabx[i],y:taby[i]});
        tabMasse[i].conteneur.on("touchstart.clc mousedown.clc",sourisCliquee);
        exo.blocAnimation.append(tabMasse[i].conteneur);
    }
    function sourisCliquee(e) {
        e.preventDefault();
        var cible = $(this);
        if (cible.data("place")===false) {
            fondbalance.gotoAndStop(2);
            textBalance.text("0 g");
            for (i=0; i<nbMasse; i++) {
                if (tabMasse[i].conteneur.data("indice")!=cible.data("indice")) {
                    if (tabMasse[i].conteneur.data("place")===true) {
                        tabMasse[i].conteneur.data("place",false);
                        tabMasse[i].conteneur.transition({x:tabx[i],y:taby[i]},500,"linear");
                    }
                } else {
                    tabMasse[i].conteneur.data("place",true);
                }
            }
            //textBalance.text(cible.data("valeur")+" g");
            afficheB = cible.data("valeur")+" g";
            cible.transition({x:350,y:113},500,"linear",paquetPoser);
        } else {
            fondbalance.gotoAndStop(2);
            textBalance.text("0 g");
            for (i=0; i<nbMasse; i++) {
                if (tabMasse[i].conteneur.data("place")===true) {
                    tabMasse[i].conteneur.data("place",false);
                    tabMasse[i].conteneur.transition({x:tabx[i],y:taby[i]},500,"linear");
                }
            }
        }
        champReponse.focus();
    }
    function paquetPoser() {
        textBalance.text(afficheB);
        fondbalance.gotoAndStop(1);
    }
};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    if (champReponse.val()==="") {
        return "rien";
    }
    if (Number(champReponse.val()) == sommeTotale){
        return "juste";
    } else {
        return "faux";
    }
};

// Correction (peut rester vide)
exo.corriger = function() {
    fondbalance.gotoAndStop(2);
    textBalance.text("0 g");
    var correction = disp.createCorrectionLabel(sommeTotale);
    exo.blocAnimation.append(correction);
    correction.position({
        my:"center center",
        at:"center center-30",
        of:champReponse,
    });
    var barre1 = disp.drawBar(champReponse);
    exo.blocAnimation.append(barre1);
    var textSolution = massMax+" g - "+massMin+" g = "+sommeTotale+" g";
    for (i=0; i<nbMasse; i++) {
        tabMasse[i].conteneur.css({x:tabx[i],y:taby[i]});
        etiquette = disp.createTextLabel(aDonnee[q][i]+" g");
        etiquette.css({
            opacity:'1',
            x:10,
            y:90
        });
        tabMasse[i].conteneur.append(etiquette);
        if(Number(aDonnee[q][i]) == massMax || Number(aDonnee[q][i])== massMin) {
			tabMasse[i].conteneur.css({opacity:1});
        } else {
			tabMasse[i].conteneur.css({opacity:0.5});
        }
		
    }
    var etiquetteSolution = disp.createTextLabel(textSolution);
    exo.blocAnimation.append(etiquetteSolution);
    etiquetteSolution.css({
        left:20,
        top:200
    });
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"typeCalcul",
        texte:exo.txt.opt4,
        aLabel:exo.txt.label4,
        aValeur:[0,1,2]
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"nbreCaisse",
        texte:exo.txt.opt5,
        aLabel:exo.txt.label5,
        aValeur:[2,3,4]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));