﻿class CadreResultat extends MovieClip {
	public var cadre_resultat:MovieClip;
	public var texte_appreciation:TextField;
	private var objExo:CalcExo;
	private var largeur:Number;
	private var x_resultat:Number;
	private var y_resultat:Number;
	private var corps:Number;
	private var gras:Boolean;
	public var btn_recommencer:Bouton;
	public var btn_suivant:Bouton;
	public function CadreResultat(obj) {
		this.objExo=obj;
		this.largeur = 400;
		this.x_resultat = (objExo.rootMovie._width-400)/2;
		this.y_resultat = 100;
		this.cadre_resultat = objExo.rootMovie.createEmptyMovieClip("cadre_resultat", objExo.rootMovie.getNextHighestDepth());
	}
	public function SetAppreciation(texte:String) {
		//on affiche l'appreciation
		if (this.texte_appreciation == undefined) {
			var d = this.cadre_resultat.getNextHighestDepth();
			this.texte_appreciation = this.cadre_resultat.createTextField("texte_appreciation", d, 10+this.x_resultat, 10+this.y_resultat, this.largeur-20, 0);
			this.texte_appreciation.embedFonts=true;
			this.texte_appreciation.antiAliasType = "advanced";
		}
		this.texte_appreciation.text = texte;
		this.texte_appreciation.multiline = true;
		this.texte_appreciation.wordWrap = true;
		var fmt = new TextFormat();
		this.texte_appreciation.autoSize = true;
		fmt.size = 14;
		fmt.bold = true;
		fmt.font = "emb_tahoma";
		this.texte_appreciation.setTextFormat(fmt);
		//on cree le bouton recommencer on gère son comportement dans la fonction avancer de la classe exo
		if (this.btn_recommencer == undefined) {
			this.btn_recommencer = new Bouton(this.cadre_resultat, "btn_recommencer", objExo.GetTradCommun("p1_4"));
			this.btn_recommencer.bouton._x = (objExo.rootMovie._width-this.btn_recommencer.bouton._width)/2;
			this.btn_recommencer.bouton._y = this.texte_appreciation._y+this.texte_appreciation._height+20;
		}
		this.btn_recommencer.Hide();
		//on cree le bouton suivant on gère son comportement dans la fonction avancer de la classe exo
		if (this.btn_suivant == undefined) {
			this.btn_suivant = new Bouton(this.cadre_resultat, "btn_suivant", objExo.GetTradCommun("p1_5"));
			btn_suivant.bouton._x = (objExo.rootMovie._width-this.btn_suivant.bouton._width)/2;
			btn_suivant.bouton._y = btn_recommencer.bouton._y;
		}
		this.btn_suivant.Hide();
		//on dessine le cadre
		var cadrex = this.x_resultat;
		var cadrey = this.y_resultat;
		var largeur = this.largeur;
		var hauteur = this.texte_appreciation._height+20+this.btn_recommencer.bouton._height+20;
		var couleur1 = 0xDFE4FF;
		var couleur2 = 0xDFE4FF;
		var rayon = 10;
		with (this.cadre_resultat) {
			lineStyle(1, couleur1);
			beginFill(couleur2);
			moveTo(cadrex+rayon, cadrey);
			lineTo(cadrex+largeur-rayon, cadrey);
			curveTo(cadrex+largeur, cadrey, largeur+cadrex, rayon+cadrey);
			lineTo(cadrex+largeur, hauteur-rayon+cadrey);
			curveTo(cadrex+largeur, hauteur+cadrey, cadrex+largeur-rayon, hauteur+cadrey);
			lineTo(cadrex+rayon, hauteur+cadrey);
			curveTo(cadrex, hauteur+cadrey, cadrex, hauteur-rayon+cadrey);
			lineTo(cadrex, rayon+cadrey);
			curveTo(cadrex, 0+cadrey, rayon+cadrex, 0+cadrey);
			endFill();
		}
	}
	public function Hide() {
		this.cadre_resultat._visible = false;
	}
	public function Show() {
		this.cadre_resultat._visible = true;
	}
}
