############################################################################
# Copyright (C) 2004 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : multiplications.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 01/11/2004
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: multiplications.tcl,v 1.4 2004/12/23 12:52:32 david Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#multiplications.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

	global plateforme user listdata arg1 listgen indfich

	package require Img

	

	# Change la presentation de tous les boutons
	# option add *Button.background red widgetDefault
 	# option add *Button.foreground green widgetDefault

	set plateforme $tcl_platform(platform)
	set ident $tcl_platform(user)

	source fonts.tcl
	source path.tcl
	source eval.tcl
	source msg.tcl

	inithome
	initlog $plateforme $ident
	changehome


	set c .frame.c
	set a .frame.a
	set arg1 [lindex $argv 0]
	

	#interface
	. configure -background $sysColor(color_pingoin_fond)

	frame .frame -width 640 -height 520 -background $sysColor(color_pingoin_fond)
	pack .frame -side top -fill both -expand yes

	# Met la fen�tre en haut � gauche
	wm geometry . +0+0
	

	# NSE : nouveau : tableau pour les historiques
	canvas $a -width 240 -height 500 -background $sysColor(color_hist_fond) -highlightbackground $sysColor(color_hist_fond)
	pack $a -side left
	
	# Zone pour l'operation
	canvas $c -width 420 -height 500 -background $sysColor(color_mult_fond) -highlightbackground $sysColor(color_mult_fond)
	pack $c -side left

	set cmilieu [expr 420 / 2]


	#ouverture du fichier multiplications.conf
	set f [open [file join $Home reglages multiplications.conf] "r"]
	set listgen [gets $f]
	close $f

	image create photo bilan -file [file join sysdata bilan_60.gif]
	image create photo bien -file [file join sysdata pbien_40.gif] 
	image create photo pass -file [file join sysdata ppass_40.gif]
	image create photo mal -file [file join sysdata pmal_40.gif]
	image create photo neutre -file [file join sysdata pneutre_40.gif]

	# NSE 200908 : pouce remplac� par pingoin ok
	image create photo ok1 -file [file join sysdata ok2-50-rouge.gif]
	image create photo ok0 -file [file join sysdata ok2-50-rouge.gif]



	catch {destroy .aaframe}
	frame .aaframe -width 440 -height 1 -background $sysColor(color_pingoin_fond)
	pack .aaframe -side top
	
	catch {destroy .aframe}
	frame .aframe -width 440 -height 1 -background $sysColor(color_pingoin_fond)
	pack .aframe -side top

	catch {destroy .bframe}
	frame .bframe -width 440 -height 100 -background $sysColor(color_pingoin_fond)
	pack .bframe -side left


	# NSE : nouveau : Creation des pingoins neutres
	set xlistdata [lindex $listgen $arg1]
	set xdata [lindex $xlistdata 2]
	set xtotal [llength $xdata]
	set xscen [lindex $xlistdata 0]

	for {set ii 1} {$ii <= $xtotal} {incr ii} {
		# MAJ des pingoins neutre
		catch {destroy .bframe.pneutre$ii}
		button .bframe.pneutre$ii -background $sysColor(color_pingoin_fond) -image neutre -text [mc "$ii"] -compound top -width 20 -height 45
		grid .bframe.pneutre$ii -column $ii -padx 1 -row 0
	}


	# NSE : MAJ de Titres Historique 
	# ...................................
	$a create text 90 90 -text "Bilan" -font $sysFont(historique) -anchor w
	$a create text 90 120 -text $xscen -font $sysFont(historique_scen) -tags xscenario -anchor w

	# NSE : Ajout de l'image du serveur
	# ...................................
	set myimage1 [image create photo -file sysdata/bilan_80.png]
	label $a.bilan -image $myimage1 -background $sysColor(color_hist_fond)
	place $a.bilan -x 0 -y 60 -anchor nw

	# NSE : MAJ de la L�gende du tableau 
	# ...................................
	$a create text 20 450 -anchor w -text "P = erreur de placement d'un chiffre" -font $sysFont(legende)
	$a create text 20 470 -anchor w -text "C = erreur de calcul" -font $sysFont(legende)
	$a create text 20 490 -anchor w -text "(V = erreur de placement de la virgule)" -font $sysFont(legende)

	# NSE : Creation du tableau de Historique 
	# ................................................
	set xnbrow [expr $xtotal + 1]
	set xnbcol 5

	# taille des cases
	set xtaillerect $sysFont(taillerect)

	set taillecol1 [expr ($xtaillerect * 4)]
	set taillecol234 [expr ($xtaillerect / 2 * 1.2)]
	set taillecol5 [expr ($xtaillerect * 1)]

	set taillehaut [expr $sysFont(taillerect) * 0.75]

	# point de d�part du tableau
	set xtaille [expr ($taillecol1) + ($taillecol234*3)]
	set xorg0 [expr 120 - ($xtaille/2)]

	# position du tableau depuis le haut de la fenetre
	set xorg1 180

	# NSE : Ligne 0 avec les titres des colonnes
	# .................................................
	set x 0
	set z 0
	set xlarg $taillecol234
	set xhaut $taillehaut
	set xorg00 [expr $xorg0 + $taillecol1 - ($taillecol234*1)]
	set xorg11 [expr $xorg1 - $xhaut]

	for {set x 1} {$x <= [expr $xnbcol - 2]} {incr x} {
		# Pour chaque colonne
		$a create rect [expr $xorg00 + $x*$xlarg] [expr $xorg11 + $z*$xhaut]  [expr $xorg00 + ($x+1)*$xlarg]  [expr $xorg11 + ($z+1)*$xhaut] -width 1 -fill grey -tag histcol$x

		if {$x == 1} {set xtext "P"}
		if {$x == 2} {set xtext "C"}
		if {$x == 3} {set xtext "V"}

		$a create text [expr $xorg00 + $x*$xlarg + int($xlarg/2)] [expr $xorg11 + $z*$xhaut + int($xhaut/2)] -tags textcol[expr \$x]row[expr \$z] -font $sysFont(tableau_hist) -text $xtext
	}


	# NSE : Parcours des Operations
	# ..................................................
	for {set z 0} {$z <= [expr $xtotal - 1]} {incr z} {

		set xop [lindex $xdata $z]
		set xtmp [lindex $xop 0]
		set xnbop [llength $xop]
		for {set w 1} {$w <= [expr $xnbop - 1]} {incr w} {
			set xtmp "$xtmp x [lindex $xop $w]"
		}
		# L'op�ration z
		set op $xtmp

		# Colonne 1 : Operation
		set x 0
		set xlarg $taillecol1
		set xhaut $taillehaut
		$a create rect [expr $xorg0 + $x*$xlarg] [expr $xorg1 + $z*$xhaut]  [expr $xorg0 + ($x+1)*$xlarg]  [expr $xorg1 + ($z+1)*$xhaut] -width 1 -fill $sysFont(couleur_col_text_op) -tag histcol$x$z

		# Colonne 1 � xnbcol - 2 : Nb erreurs
		set xlarg1 $xlarg
		set xlarg $taillecol234
		set xhaut $taillehaut
		set xorg00 [expr $xorg0 + $xlarg1 - $xlarg]

		for {set x 1} {$x <= [expr $xnbcol - 2]} {incr x} {
			# Pour chaque colonne
			$a create rect [expr $xorg00 + $x*$xlarg] [expr $xorg1 + $z*$xhaut]  [expr $xorg00 + ($x+1)*$xlarg]  [expr $xorg1 + ($z+1)*$xhaut] -width 1 -fill $sysFont(couleur_col_resultat_op) -tag histcol$x$z
		}
	}


	catch {destroy .cframe}
	frame .cframe -width 100 -height 100 -background $sysColor(color_pingoin_fond)
	pack .cframe -side left

	catch {destroy .dframe}
	frame .dframe -width 200 -height 100 -background $sysColor(color_pingoin_fond)
	pack .dframe -side right

	# Creation des boutons "Quitter, Echelle, ..."
  	button .dframe.but_quitter -command exit -image [image create photo -file [file join sysdata quitter_minus.gif]]
  	pack .dframe.but_quitter -side right

  	button .dframe.but_clear -command exit -image [image create photo -file [file join sysdata fgauche.gif]]
  	pack .dframe.but_clear -side right

  	button .dframe.but_echelle -command exit -image [image create photo -file [file join sysdata echelle.gif]]
  	pack .dframe.but_echelle -side right


set indfich 0


# procedure principale
proc xplace {c a} {
	global listdata arg1 listgen user basedir progaide mode data indfich total sysFont sysColor nbcol nbrow curcol currow plateforme taillerect org0 org1 donnee resultf decim erreurscalcul1 erreurscalcul2 erreurvirg erreursplacement partdec1 partdec2 cmilieu

	#recup de l'activit� dans listdata
	set listdata [lindex $listgen $arg1]
	set message ""

	# Descriptif de l'activite
	# la nouvelle version permet la presence d'espace dans le nom de l'activit�
	set nline [lrange $listdata 0 0]
	set nline_size [string length $nline]
	set scen [string range [lrange $listdata 0 0] 1 [expr $nline_size - 2]]
	append message [mc "Multiplications"] "     " $scen

	wm title . $message
	
	set erreurscalcul 0
	set erreurscalcul1 0
	set erreursplacement 0
	set erreurvirg 0
	set erreurscalcul2 0
	set mode [lindex $listdata 1]
	set data [lindex $listdata 2]
	set donnee [lindex $data $indfich]


	catch {destroy .aaframe.consigne}
	catch {destroy .aaframe.ok}
	catch {destroy $c.ok}
	
	bind $c <Return> "verifplace $c $a"
	#return du pave numerique$a
	bind $c <KP_Enter> "verifplace $c $a"

	set total [llength $data]
	$c delete all

	set decim 0
	set tmp 0
	set partdec1 0
	set partdec2 0


	for {set i 0} {$i < [llength $donnee]} {incr i 1} {
	  set l$i [string length [lindex $donnee $i]]
	  if {[expr \$l$i] > $tmp} {set tmp [expr \$l$i]}
	  if {[string first , [lindex $donnee $i]] != -1} {
	    set decim 1
	    set partdec[expr \$i +1] [expr [string length [lindex $donnee $i]] - [string first , [lindex $donnee $i]] -1]
	  }
	}

	set minv 0
	if {[string first , [lindex $donnee 1]] != -1} {set minv 1}

	set nbcol [expr [string length [lindex $donnee 0]] + [string length [lindex $donnee 1]] -1 - $minv]
	set tmp [lindex $donnee 0]
	set tmp2 [lindex $donnee 0]
	for {set i 1} {$i < [llength $donnee]} {incr i 1} {
		set tmp "$tmp * [lindex $donnee $i]"
		set tmp2 "$tmp2 x [lindex $donnee $i]"
	}
	
	if {$nbcol < [expr [string length [expr [string map {, .} $tmp]]] - $decim]} {
	  set nbcol [expr [string length [expr [string map {, .} $tmp]]] - $decim]
	}
	
	set nbrow [expr [llength $donnee] ]
	set currow 0
	set curcol $nbcol
	# Taille des cellules grises
	set taillerect $sysFont(taillerect)
	set org0 [expr $cmilieu - int((($nbcol +1)*$taillerect)/2)]
	# Position du tableau depuis le haut de la fenetre
	set org1 150


	# Operation � resoudre
	# ----------------------------------------
	$c create text [expr $cmilieu] [expr 60] -text $tmp2 -font $sysFont(exercice) -tags operation
	set resultf [expr [string map {, .} $tmp]]

	for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
	  for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
	    $c create rect [expr $org0 + $i*$taillerect] [expr $org1 + $j*$taillerect ]  [expr $org0 + ($i+1)*$taillerect]  [expr $org1 + ($j+1)*$taillerect] -width 2 -fill gray -tags rectcol[expr \$i]row[expr \$j]
	    $c create text [expr $org0 + $i*$taillerect + int($taillerect/2)] [expr $org1 + $j*$taillerect + int($taillerect/2)] -tags textcol[expr \$i]row[expr \$j] -font $sysFont(tableau)

	  }
	}
	for {set i 1} {$i <= [expr $nbcol]} {incr i 1} {
	  for {set j 0} {$j <= [expr $nbrow -1]} {incr j 1} {
	    
	    # Pas de . dans les celulles
	    $c itemconf  textcol[expr \$i]row[expr \$j] -text 
	    $c bind rectcol[expr \$i]row[expr \$j] <ButtonRelease-1> "changefocus $c $a"
	    $c bind textcol[expr \$i]row[expr \$j] <ButtonRelease-1> "changefocus $c $a"
	  }
	}

	bind $c <KeyPress> "changenum $c $a %K"
	bind $c <Right> "changecell $c $a right 1 [expr $nbrow -1] 1 $nbcol"
	bind $c <Up> "changecell $c $a up 0 [expr $nbrow -1] 1 $nbcol"
	bind $c <Left> "changecell $c $a left 1 [expr $nbrow -1] 1 $nbcol"
	bind $c <Down> "changecell $c $a down 0 [expr $nbrow -1] 1 $nbcol"


	focus -force .frame.c
	for {set i 1} {$i < [expr $nbrow]} {incr i 1} {
	  $c itemconf textcol0row$i -text X -fill blue
	}
	$c create line [expr $org0] [expr $org1 + $nbrow*$taillerect ]  [expr $org0 + [expr $nbcol +1]*$taillerect]  [expr $org1 + $nbrow*$taillerect]  -width 4 -fill red -tags line
	$c itemconf rectcol[expr \$nbcol]row0 -fill white


	# Ajout de la consigne
	# .......................................................
	$c create text $cmilieu 480 -text "Pose l'op�ration et appuie sur la touche Entr�e" -font $sysFont(messbas) -tag consigne
	
}


#appel de la proc�dure principale
xplace $c $a



proc changefocus {c a} {
	global nbcol nbrow curcol currow taillerect org0 org1
	for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
	  for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
	    $c itemconf  rectcol[expr \$i]row[expr \$j] -fill grey
	  }
	}
	set coord [$c coords current]
	set xc [expr int(([lindex $coord 0]- $org0)/$taillerect)]
	set yc [expr int(([lindex $coord 1]-$org1)/$taillerect)]
	$c itemconf rectcol[expr \$xc]row[expr \$yc] -fill white
	set curcol $xc
	set currow $yc
}

proc changecell {c a where rmin rmax cmin cmax} {
   global nbcol nbrow curcol currow taillerect
   switch $where {
        right { 
		  set tmp [expr $curcol +1]
		    if {$tmp <= $cmax} {
			set curcol $tmp
		    }
		  }
	left { 
		  set tmp [expr $curcol -1]
		    if {$tmp >= $cmin} {
			set curcol $tmp
		    }
		  }
	down { 
		  set tmp [expr $currow +1]
		    if {$tmp <= $rmax} {
			set currow $tmp
		    }
		  }
	up { 
		  set tmp [expr $currow -1]
		    if {$tmp >= $rmin} {
			set currow $tmp
		    }
		  }

  }
	for {set i 1} {$i <= [expr $nbcol]} {incr i 1} {
	  for {set j 0} {$j <= [expr $nbrow -1]} {incr j 1} {
		 $c itemconf  rectcol[expr \$i]row[expr \$j] -fill grey
	
		 # Sauf pour l'op�rateur
		 # .........................................................

	  }
	}
	$c itemconf rectcol[expr \$curcol]row[expr \$currow] -fill white

}

proc changenum {c a key} {
    global curcol currow
    focus -force .frame.c
    
    switch $key {
	
	BackSpace - Delete {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text ""}
	0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9  {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text $key}
	KP_Insert {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "0"}
	KP_End {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "1"}
	KP_Down {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "2"}
	KP_Next {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "3"}
	KP_Left {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "4"}
	KP_Begin {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "5"}
	KP_Right {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "6"}
	KP_Home {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "7"}
	KP_Up {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "8"}
	KP_Prior {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "9"}
	KP_Delete {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text ,}
	comma {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text ,}
    }
}



proc verifplace {c a} {
global nbcol donnee nbrow currow curcol sysFont activecol decim erreursplacement retenue ancretenue curligne resultat decalage actcol cmilieu
set retenue 0
set ancretenue 0
set resultat 0
set decalage 0
set actcol $nbcol
set curligne $nbcol

	for {set j 0} {$j < [expr $nbrow]} {incr j 1} {
		set nomb ""
		set saisie ""
		for {set i 1} {$i <= [expr $nbcol]} {incr i 1} {
			set saisie [$c itemcget textcol[expr \$i]row[expr \$j] -text]
			set nomb $nomb$saisie
		}

		set tmp [string trim $nomb .]
		if {[string first . $tmp] != -1} {
			# pas de . au milieu du nombre
			set message ""
			append message [mc "Erreur au nombre"] " " [expr $j +1]
			set answer [tk_messageBox -message $message -type ok]
			incr erreursplacement 
			return
		}

		if {[string equal [string trimleft [lindex $donnee [expr $j]] 0] [string trimleft $nomb .]] != 1} {
			incr erreursplacement
			set message ""
			append message [mc "Erreur au nombre"] " " [expr $j +1]
			set answer [tk_messageBox -message $message -type ok] 
			return
		} 
	}


	# ajout de la consigne (decouper en 2)
	# .......................................................
	$c delete consigne
	$c create text $cmilieu 480 -text "Effectue et valide en appuyant sur Entr�e \r (Pense aux retenues !)" -font $sysFont(messbas) -tag consigne
	

	for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
	  for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
		 $c itemconf  rectcol[expr \$i]row[expr \$j] -fill grey
	  }
	}
	set curcol $nbcol
	set activecol $nbcol
	set currow $nbrow
	$c itemconf rectcol[expr \$curcol]row[expr \$currow] -fill white
	for {set i 1} {$i <= [expr $nbcol]} {incr i 1} {
	  for {set j 0} {$j <= [expr $nbrow -1]} {incr j 1} {
		 $c bind rectcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
		 $c bind textcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
	  }
	}

	catch {destroy .aaframe.ok}
	catch {destroy $c.ok}
	bind $c <Return> "verifope $c $a"
	#return du pave numerique
	bind $c <KP_Enter> "verifope $c $a"

	bind $c <Right> ""
	bind $c <Up> ""
	bind $c <Left> ""
	bind $c <Down> ""


	$c bind rectcol[expr \$curcol]row[expr \$currow] <ButtonRelease-1> "changefocus $c $a"
	$c bind textcol[expr \$curcol]row[expr \$currow] <ButtonRelease-1> "changefocus $c $a"
	$c itemconf textcol[expr \$curcol]row0 -fill blue
	$c itemconf textcol[expr \$curligne]row1 -fill blue
	set tmp1 0
	set tmp2 0
	if {[$c itemcget textcol[expr \$curcol]row0 -text] != "" && [$c itemcget textcol[expr \$curcol]row0 -text] != ","} {
		set tmp1 [string map {. 0} [$c itemcget textcol[expr \$curcol]row0 -text]]
	}
	if {[$c itemcget textcol[expr \$curligne]row1 -text] != "" && [$c itemcget textcol[expr \$curligne]row1 -text] != ","} {
		set tmp2 [string map {. 0} [$c itemcget textcol[expr \$curligne]row1 -text]]
	}
	set tmp [expr $tmp1 * $tmp2]
	if {[string length $tmp] > 1} {
		set retenue [string index $tmp 0]
		set resultat [string index $tmp 1]
	} else {
		set resultat $tmp
	}


	for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
		 # Pas de . dans les cellules
		 $c itemconf  textcol[expr \$i]row[expr $nbrow] -text ""
	}

	set message ""
	append message [mc "Retenue"] " : "

	$c create text 350 100 -text $message -font $sysFont(ret) -tag efface
	catch {entry $c.retenue -font $sysFont(ret) -width 2}

	$c create window 400 100 -window $c.retenue -tag efface
	$c.retenue insert end "0"

	set message ""
	append message "(" [mc "Ancienne retenue"] " : " $ancretenue ")"

	$c create text 340 130 -text $message -tag "efface ancret"
	focus $c
	bind $c.retenue <KeyRelease> "verifret $c $a %A"
	bind $c.retenue <1> "$c.retenue delete 0 end"
}

proc verifret {c a key} {
	switch $key {
		0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9  {}
		default {
			$c.retenue delete 0 end
			$c.retenue insert end "0"}
		}
	focus $c
}

proc verifope {c a} {
	global curcol currow nbcol nbrow activecol indfich resultf total decim listdata erreurscalcul1 erreursplacement user curligne retenue resultat taillerect org0 org1 sysFont decalage actcol ancretenue
	
	set result 0
	set averif 0
	set message ""
	append message "(" [mc "Ancienne retenue"] " : " $ancretenue ")"
	$c itemconf ancret -text $message

	if {[$c itemcget textcol[expr \$actcol]row[expr \$currow] -text] != "" && [$c itemcget textcol[expr \$actcol]row[expr \$currow] -text] != "," && [$c itemcget textcol[expr \$actcol]row[expr \$currow] -text] != "."} {
		set result [$c itemcget textcol[expr \$actcol]row[expr \$currow] -text]
	} else {
		if {$decalage == 0 } {
			incr erreurscalcul1
			set answer [tk_messageBox -message [mc "Erreur de calcul"] -type ok] 
			return
		}
	} 

	set tmpresult $result
	if {[$c.retenue get] != ""} {set result [$c.retenue get]$result}

	set result1 [string trimleft $retenue$resultat 0]
	if {$result1 == ""} {set result1 0}
	set result [string trimleft $result 0]
	if {$result == ""} {set result 0}

	if {$decalage ==0} {
		if {[expr $result1] != [expr $result]} {
			incr erreurscalcul1
			set answer [tk_messageBox -message [mc "Erreur de calcul"] -type ok] 
			return
		} 
	} else {
		if {[expr $tmpresult] != "0" && $tmpresult != "." && $tmpresult != ""} {
			incr erreurscalcul1
			set answer [tk_messageBox -message [mc "Erreur : d�calage des lignes"] -type ok] 
			return
		} else {
			for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
				for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
					$c itemconf  rectcol[expr \$i]row[expr \$j] -fill grey
					$c bind rectcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
					$c bind textcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
				}
			}
		}
	set actcol [expr $actcol -1]
	set decalage [expr $decalage -1]

	$c itemconf rectcol[expr \$actcol]row[expr \$currow] -fill white
	$c bind rectcol[expr \$actcol]row[expr \$currow] <ButtonRelease-1> "changefocus $c $a"
	$c bind textcol[expr \$actcol]row[expr \$currow] <ButtonRelease-1> "changefocus $c $a"
	set curcol $actcol
	return
}


set activecol [expr $activecol -1]
set actcol [expr $actcol -1]

if {[$c itemcget textcol[expr \$activecol]row0 -text] == ","} {set activecol [expr $activecol -1]}
	if {([$c itemcget textcol[expr \$activecol]row0 -text] == "" || [$c itemcget textcol[expr \$activecol]row0 -text] == ".") && $retenue == "0"} {
	set activecol $nbcol
	set actcol $nbcol
	set curligne [expr $curligne -1]
	if {[$c itemcget textcol[expr \$curligne]row1 -text] == ","} {set curligne [expr $curligne -1]}
		if {[$c itemcget textcol[expr \$curligne]row1 -text] == "" || [$c itemcget textcol[expr \$curligne]row1 -text] == "." || [$c itemcget textcol[expr \$curligne]row1 -text] == "X"} {
		# c'est fini
		for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
  			for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
    				$c itemconf  rectcol[expr \$i]row[expr \$j] -fill grey

				# Cas pour l'op�rateur en blue
				if {[$c itemcget textcol[expr \$i]row[expr \$j] -text] == "X"} {
					$c itemconf  textcol[expr \$i]row[expr \$j] -fill blue
				} else {
					$c itemconf  textcol[expr \$i]row[expr \$j] -fill black
				}
  			}
		}
		
		if {[expr $nbrow - 2] == "0"} {
		
		bind $c <KeyPress> ""
		$c delete efface


		bind $c <Return> ""
		#return du pave numerique
		bind $c <KP_Enter> ""

		if {$decim == 1} {
		geredecim $c $a
		return
		}
		geresuite $c $a
		return
		}
		set answer [tk_messageBox -message [mc "Maintenant l'addition !"] -type ok]
		$c delete efface
		incr nbrow
		for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
			# Derniere ligne de l'addition
			# ..............................................
    			$c create rect [expr $org0 + $i*$taillerect] [expr $org1 + $nbrow*$taillerect ]  [expr $org0 + ($i+1)*$taillerect]  [expr $org1 + ($nbrow+1)*$taillerect]  -width 2 -fill grey -tags rectcol[expr \$i]row[expr \$nbrow]

				# Pas de . dans la cellule
				$c create text [expr $org0 + $i*$taillerect + int($taillerect/2)] [expr $org1 + $nbrow*$taillerect + int($taillerect/2)] -tags textcol[expr \$i]row[expr \$nbrow] -font $sysFont(t) -text ""
  		}

		for {set j [expr $nbrow -1]} {$j >= 2} {incr j -1} {
			for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
				$c itemconf textcol[expr \$i]row[expr \$j +1] -text [$c itemcget textcol[expr \$i]row[expr \$j] -text]
			}
		}
		for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
			# Ligne ins�r�e pour les retenues
			# ..............................................
			$c itemconf rectcol[expr \$i]row2 -fill yellow
			$c itemconf textcol[expr \$i]row2 -text .
		}


		for {set i 4} {$i <= [expr $nbrow]} {incr i 1} {
			$c itemconf textcol0row[expr \$i] -fill blue -text +
		}
		incr nbrow


		# Ligne pour le total de l'addition
		# .......................................................
		for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
  			$c create rect [expr $org0 + $i*$taillerect] [expr $org1 + $nbrow*$taillerect ]  [expr $org0 + ($i+1)*$taillerect]  [expr $org1 + ($nbrow+1)*$taillerect]  -width 2 -fill grey -tags rectcol[expr \$i]row[expr \$nbrow]
			$c create text [expr $org0 + $i*$taillerect + int($taillerect/2)] [expr $org1 + $nbrow*$taillerect + int($taillerect/2)] -tags textcol[expr \$i]row[expr \$nbrow] -font $sysFont(t) -text ""
  			}
			$c create line [expr $org0] [expr $org1 + $nbrow*$taillerect ]  [expr $org0 + [expr $nbcol +1]*$taillerect]  [expr $org1 + $nbrow*$taillerect]  -width 4 -fill red -tags line
			$c itemconf rectcol[expr \$nbcol]row[expr \$nbrow] -fill white

			for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
  				for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
    					$c bind rectcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
    					$c bind textcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
  				}
			}
			set currow $nbrow
			set curcol $nbcol
			set activecol $nbcol

			$c bind rectcol[expr \$curcol]row[expr \$currow] <ButtonRelease-1> "changefocus $c $a"
			$c bind textcol[expr \$curcol]row[expr \$currow] <ButtonRelease-1> "changefocus $c $a"
			$c bind rectcol[expr \$curcol -1]row2 <ButtonRelease-1> "changefocus $c $a"
			$c bind textcol[expr \$curcol -1]row2 <ButtonRelease-1> "changefocus $c $a"
			for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
    				$c itemconf  textcol[expr \$i]row2 -fill blue -font $sysFont(s)
			}


			bind $c <Return> "verifope2 $c $a"
		        #return du pave numerique
		        bind $c <KP_Enter> "verifope2 $c $a"

			return
		} else {
			set nbrow [expr $nbrow +1]
			set currow $nbrow
			set retenue 0
			set ancretenue 0
			set decalage [expr $nbrow - 2]
			for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
    			$c create rect [expr $org0 + $i*$taillerect] [expr $org1 + $nbrow*$taillerect ]  [expr $org0 + ($i+1)*$taillerect]  [expr $org1 + ($nbrow+1)*$taillerect]  -width 2 -fill gray -tags rectcol[expr \$i]row[expr \$nbrow]
				$c create text [expr $org0 + $i*$taillerect + int($taillerect/2)] [expr $org1 + $nbrow*$taillerect + int($taillerect/2)] -tags textcol[expr \$i]row[expr \$nbrow] -font $sysFont(t) -text ""
  			}
		
		}
	}
for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
	for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
		$c itemconf  rectcol[expr \$i]row[expr \$j] -fill grey
		$c bind rectcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
		$c bind textcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
		$c itemconf textcol[expr \$i]row[expr \$j] -fill black
		
		if {[$c itemcget textcol[expr \$i]row[expr \$j] -text] == "X"} {
			$c itemconf  textcol[expr \$i]row[expr \$j] -fill blue
		} else {
			$c itemconf  textcol[expr \$i]row[expr \$j] -fill black
		}
	}
}

$c itemconf rectcol[expr \$actcol]row[expr \$currow] -fill white
$c bind rectcol[expr \$actcol]row[expr \$currow] <ButtonRelease-1> "changefocus $c $a"
$c bind textcol[expr \$actcol]row[expr \$currow] <ButtonRelease-1> "changefocus $c $a"
set curcol $actcol
set tmp1 0
set tmp2 0

	if {[$c itemcget textcol[expr \$activecol]row0 -text] != "" && [$c itemcget textcol[expr \$activecol]row0 -text] != ","} {
		set tmp1 [string map {. 0} [$c itemcget textcol[expr \$activecol]row0 -text]]
		$c itemconf textcol[expr \$activecol]row0 -fill blue
	}
	if {[$c itemcget textcol[expr \$curligne]row1 -text] != "" && [$c itemcget textcol[expr \$curligne]row1 -text] != ","} {
		set tmp2 [string map {. 0} [$c itemcget textcol[expr \$curligne]row1 -text]]
		$c itemconf textcol[expr \$curligne]row1 -fill blue
	}
	set tmp [expr ($tmp1 * $tmp2) + $retenue]
	set ancretenue $retenue
	set retenue 0
	if {[string length $tmp] > 1} {
		set retenue [string index $tmp 0]
		set resultat [string index $tmp 1]
	} else {
		set resultat $tmp
	}

}

proc verifope2 {c a} {
	global curcol currow nbcol nbrow activecol indfich resultf total partint decim listdata erreurscalcul2 erreursplacement user
	set result 0
	set averif 0
	set pas 1
	for {set i 2} {$i < [expr $nbrow]} {incr i 1} {
		if {[$c itemcget textcol[expr \$activecol]row$i -text] != "" && [$c itemcget textcol[expr \$activecol]row$i -text] != "," && [$c itemcget textcol[expr \$activecol]row$i -text] != "+"} {
		set result [expr $result + [string map {. 0} [$c itemcget textcol[expr \$activecol]row$i -text]]]
	}
}	

# si nombre entier
set averif [string map {. 0} [$c itemcget textcol[expr \$activecol - $pas]row2 -text]][string map {. 0} [$c itemcget textcol[expr \$activecol]row[expr \$nbrow] -text]]


# eviter le pb de conversion implicite en octale
set averif [string trimleft $averif 0]
if {$averif == ""} {set averif 0}
set result [string trimleft $result 0]
if {$result == ""} {set result 0}

	if {[expr $averif] != [expr $result]} {
		incr erreurscalcul2
		set answer [tk_messageBox -message [mc "Erreur de calcul"] -type ok] 
		return
	} else {
		set activecol [expr $activecol -1]
		for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
	  		for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
		 		$c itemconf  rectcol[expr \$i]row[expr \$j] -fill grey
				$c bind rectcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
				$c bind textcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
	  		}
		}

		if {$activecol < 0} {
			bind $c <Return> ""
			#return du pave numerique
			bind $c <KP_Enter> ""
			if {$decim == 1} {
				geredecim $c $a
				return
			}

			geresuite $c $a
		} else {
			$c itemconf rectcol[expr \$activecol]row[expr \$nbrow] -fill white
			$c bind rectcol[expr \$activecol]row[expr \$nbrow] <ButtonRelease-1> "changefocus $c $a"
			$c bind textcol[expr \$activecol]row[expr \$nbrow] <ButtonRelease-1> "changefocus $c $a"
			$c bind rectcol[expr \$activecol -1]row2 <ButtonRelease-1> "changefocus $c $a"
			$c bind textcol[expr \$activecol -1]row2 <ButtonRelease-1> "changefocus $c $a"
			set currow $nbrow
			set curcol $activecol
		}
	}

}

proc geredecim {c a} {
	global nbcol nbrow cmilieu sysFont
	set answer [tk_messageBox -message "Placement de la virgule" -type ok]

	# NSE : ajout de la consigne (decouper en 2)
	# .......................................................
	$c delete consigne
	$c create text $cmilieu 480 -text "Clique sur un chiffre du r�sultat pour placer la virgule \n et valide avec la touche Entr�e." -font $sysFont(messbas) -tag consigne

	for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
		$c bind rectcol[expr \$i]row[expr \$nbrow] <ButtonRelease-1> "placevirg $c $a"
		$c bind textcol[expr \$i]row[expr \$nbrow] <ButtonRelease-1> "placevirg $c $a"
	}
	bind $c <Return> "verifvirg $c $a"
	#return du pave numerique
	bind $c <KP_Enter> "verifvirg $c $a"
}

proc placevirg {c a} {
	global nbrow nbcol currow curcol taillerect org0 org1
	for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
		$c itemconf textcol[expr \$i]row[expr \$nbrow] -text [string index [$c itemcget textcol[expr \$i]row[expr \$nbrow] -text] 0]
	}
	set coord [$c coords current]
	set xc [expr int(([lindex $coord 0]- $org0)/$taillerect)]
	set yc [expr int(([lindex $coord 1]-$org1)/$taillerect)]
	$c itemconf textcol[expr \$xc]row[expr \$yc] -text "[$c itemcget textcol[expr \$xc]row[expr \$yc] -text],"
}

proc verifvirg {c a} {
	global partdec1 partdec2 erreurvirg nbrow nbcol
	set virg [expr $nbcol - ($partdec1 + $partdec2)]
	if {[string first , [$c itemcget textcol[expr \$virg]row[expr \$nbrow] -text]] != -1} {
		geresuite $c $a
		return
	} else {
		set answer [tk_messageBox -message [mc "Erreur de placement de la virgule"] -type ok]
		incr erreurvirg
	}
}

proc geresuite {c a} {
	global listdata sysColor indfich total resultf user erreursplacement erreurscalcul1 erreurvirg erreurscalcul2 cmilieu sysFont

	set xop [$c itemcget operation -text]
	set xop_nombre1 [lindex $xop 0]
	set xop_nombre2 [lindex $xop 2]

	$c itemconf operation -text "[$c itemcget operation -text] = [string map {. ,} $resultf]"

	# nse : Nouveau
	set scen [lindex $listdata 0]
	set op [$c itemcget operation -text]

	# Cherche si le resultata est un nombre decimal ou non
	if {$resultf == [expr int($resultf)]} {
		# Pas de decimales
		set nombre_decimal "Non"
	} else {
		# Nombre avec decimales
		set nombre_decimal "Oui"
	}

	# Besoin de faire l'addition ou nom
	if {$xop_nombre2 < 10} {
		set err_addition "Non"
	} else {
		set err_addition "Oui"
	}
	
	
	# Fenetre d'analyse des erreurs
	set str ""
	append str "\173" [mc "Op�ration effectu�e"] " : " [$c itemcget operation -text] "\175"
	enregistreeval [mc "Multiplications"] [lindex $listdata 0] $str $user
	set message ""

	if {$nombre_decimal == "Oui"} {
		if {$err_addition == "Oui"} {
			# Nombre d�cimal : affichage des erreurs de virgule
			append message "\173" [mc "Erreur(s) de placement"] " : " $erreursplacement "\175\040\173" [mc "Erreur(s) de calcul (multiplication)"] " : " $erreurscalcul1 "\175\040\173" [mc "Erreur(s) de calcul (addition)"] " : " $erreurscalcul2 "\175\040\173" [mc "Erreur(s) de virgule"] " : " $erreurvirg "\175"
			appendeval $message $user
			set message ""
			append message [mc "Op�ration termin�e"] "\n" \
				[mc "Erreur(s) de placement"] " : " $erreursplacement "\n" \
				[mc "Erreur(s) de calcul (multiplication)"] " : " $erreurscalcul1 "\n" \
				[mc "Erreur(s) de calcul (addition)"] " : " $erreurscalcul2 "\n" \
				[mc "Erreur(s) de virgule"] " : " $erreurvirg "\n" 
		} else {
			# Nombre decimal mais pas d'addition apr�s la multiplication
			append message "\173" [mc "Erreur(s) de placement"] " : " $erreursplacement "\175\040\173" [mc "Erreur(s) de calcul (multiplication)"] " : " $erreurscalcul1 "\175\040\173" [mc "Erreur(s) de virgule"] " : " $erreurvirg "\175"
			appendeval $message $user
			set message ""
			append message [mc "Op�ration termin�e"] "\n" \
				[mc "Erreur(s) de placement"] " : " $erreursplacement "\n" \
				[mc "Erreur(s) de calcul (multiplication)"] " : " $erreurscalcul1 "\n" \
				[mc "Erreur(s) de virgule"] " : " $erreurvirg "\n" 
		}
	} else {
		if {$err_addition == "Oui"} {
			# Nombre non decimal : pas d'affichage des erreurs li�es � la virgule
			append message "\173" [mc "Erreur(s) de placement"] " : " $erreursplacement "\175\040\173" [mc "Erreur(s) de calcul (multiplication)"] " : " $erreurscalcul1 "\175\040\173" [mc "Erreur(s) de calcul (addition)"] " : " $erreurscalcul2 "\175\040\173" "\175"
			appendeval $message $user
			set message ""
			append message [mc "Op�ration termin�e"] "\n" \
				[mc "Erreur(s) de placement"] " : " $erreursplacement "\n" \
				[mc "Erreur(s) de calcul (multiplication)"] " : " $erreurscalcul1 "\n" \
				[mc "Erreur(s) de calcul (addition)"] " : " $erreurscalcul2 "\n"
		} else {
			# Nombre non decima et pas d'addition
			append message "\173" [mc "Erreur(s) de placement"] " : " $erreursplacement "\175\040\173" [mc "Erreur(s) de calcul (multiplication)"] " : " $erreurscalcul1 "\175\040\173" "\175"
			appendeval $message $user
			set message ""
			append message [mc "Op�ration termin�e"] "\n" \
				[mc "Erreur(s) de placement"] " : " $erreursplacement "\n" \
				[mc "Erreur(s) de calcul (multiplication)"] " : " $erreurscalcul1 "\n"
		}
	}
	set answer [tk_messageBox -message $message -type ok]
	incr indfich

	# nse : Nouveau : affichage des pingoins bien ou mal
	set erreurstotal [expr $erreursplacement + $erreurscalcul1 + $erreurscalcul2 + $erreurvirg]



	# NSE : MAJ du tableau Historique pour l'operation
	# .......................................................
	# taille des cases
	set xtaillerect $sysFont(taillerect)

	set taillecol1 [expr ($xtaillerect * 4)]
	set taillecol234 [expr ($xtaillerect / 2 * 1.2)]
	set taillecol5 [expr ($xtaillerect * 1)]

	set taillehaut [expr $xtaillerect * 0.75]

	# point de d�part du tableau
	set xtaille [expr $taillecol1 + ($taillecol234 * 3)]
	set xorg0 [expr 120 - ($xtaille / 2)]
	# position du tableau depuis le haut de la fenetre
	set xorg1 180

	set z [expr $indfich -1]

	# NSE : MAj de l'operation
	# .................................................
	set x 0
	set xlarg $taillecol1
	set xhaut $taillehaut



	 
	$a delete histcol$x$z

	$a create rect [expr $xorg0 + $x*$xlarg] [expr $xorg1 + $z*$xhaut]  [expr $xorg0 + ($x+1)*$xlarg]  [expr $xorg1 + ($z+1)*$xhaut] -width 1 -fill $sysFont(couleur_col_text_op) -tag histcol$x$z
	
	if {$sysFont(text_op_avec_couleur) == "Non"} {
			set xfill black
	} else {
			if {$erreurstotal == 0} {set xfill green}
			if {$erreurstotal == 1} {set xfill yellow}
			if {$erreurstotal == 2} {set xfill orange}
			if {$erreurstotal >= 3} {set xfill red}
	}
	$a create text [expr $xorg0 + $x*$xlarg + int($xlarg/2)] [expr $xorg1 + $z*$xhaut + int($xhaut/2)] -tags textcol[expr \$x]row[expr \$z] -font $sysFont(tableau_hist) -text $op -fill $xfill


	# Colonne 1 � xnbcol - 2 : Nb erreurs
	set xlarg1 $xlarg
	set xlarg $taillecol234
	set xhaut $taillehaut
	set xorg00 [expr $xorg0 + $xlarg1 - $xlarg]

	for {set x 1} {$x <= 3} {incr x} {
		
		if {$x == 1} {set xval $erreursplacement}
		if {$x == 2} {set xval [expr $erreurscalcul1 + $erreurscalcul2]}
		if {$x == 3} {set xval $erreurvirg}

		if {$sysFont(erreur_op_avec_etoile) == "Non" && $sysFont(erreur_op_avec_chiffre) == "Non"} {
			if {$xval == 0} {set xtext " "; set xfill green}
			if {$xval == 1} {set xtext " "; set xfill yellow}
			if {$xval == 2} {set xtext " "; set xfill orange}
			if {$xval >= 3} {set xtext " "; set xfill red}
		} 
		if {$sysFont(erreur_op_avec_etoile) == "Oui"} {
			if {$xval == 0} {set xtext "***"; set xfill green}
			if {$xval == 1} {set xtext "**"; set xfill yellow}
			if {$xval == 2} {set xtext "*"; set xfill orange}
			if {$xval >= 3} {set xtext ""; set xfill red}
		}
		if {$sysFont(erreur_op_avec_chiffre) == "Oui"} {
			if {$xval == 0} {set xtext $xval; set xfill green}
			if {$xval == 1} {set xtext $xval; set xfill yellow}
			if {$xval == 2} {set xtext $xval; set xfill orange}
			if {$xval >= 3} {set xtext $xval; set xfill red}
		}

		if {$x == 1 || $x == 2} {
			# Colonnes Erreurs liees au placement et au calcul
			$a delete histcol$x$z
			$a create rect [expr $xorg00 + $x*$xlarg] [expr $xorg1 + $z*$xhaut]  [expr $xorg00 + ($x+1)*$xlarg]  [expr $xorg1 + ($z+1)*$xhaut] -width 1 -fill $xfill -tag histcol$x$z
			$a create text [expr $xorg00 + $x*$xlarg + int($xlarg/2)] [expr $xorg1 + $z*$xhaut + int($xhaut/2)] -tags textcol[expr \$x]row[expr \$z] -font $sysFont(tableau_hist) -text $xtext
		}
		if {$x == 3} {
			# Colonne Erreurs liees � la Virgule
			$a delete histcol$x$z
			if {$nombre_decimal == "Non"} {
					set xfill $sysFont(couleur_col_resultat_op)
					set xtext " "
			} else {
					# Utilisation des Couleurs definies plus haut
			}
			$a create rect [expr $xorg00 + $x*$xlarg] [expr $xorg1 + $z*$xhaut]  [expr $xorg00 + ($x+1)*$xlarg]  [expr $xorg1 + ($z+1)*$xhaut] -width 1 -fill $xfill -tag histcol$x$z
			$a create text [expr $xorg00 + $x*$xlarg + int($xlarg/2)] [expr $xorg1 + $z*$xhaut + int($xhaut/2)] -tags textcol[expr \$x]row[expr \$z] -font $sysFont(tableau_hist) -text $xtext
		}

	}


	if {$indfich < $total} {
		
		# NSE : MAJ du Pingoin
		# .......................................................
		catch {destroy .bframe.pneutre$indfich}
		
		if {$erreurstotal == 0} {
			button .bframe.pneutre$indfich -background $sysColor(color_pingoin_fond) -image bien -text [mc "$indfich"] -compound top -width 20 -height 45
		} else {
			button .bframe.pneutre$indfich -background $sysColor(color_pingoin_fond) -image mal -text [mc "$indfich"] -compound top -width 20 -height 45
		}
		grid .bframe.pneutre$indfich -column $indfich -padx 1 -row 0
		for {set ii [expr $indfich + 1]} {$ii <= 5} {incr ii} {
			# MAJ des autres pingoins
			#catch {destroy .bframe.pneutre$ii}
			#button .bframe.pneutre$ii -background $sysColor(color_mult_fond) -image neutre
			#grid .bframe.pneutre$ii -column $ii -padx 5 -row 0
		}
		


		# NSE : ajout de la consigne (decouper en 2)
		# .......................................................
		$c delete consigne
		$c create text $cmilieu 480 -text "Appuie sur le bouton pour continuer" -font $sysFont(messbas) -tag consigne	

		# NSE : Placement du bouton OK dans c
		# .......................................................
		button $c.ok -background $sysColor(color_mult_fond) -image ok1 -command "xplace $c $a"
		place $c.ok -x 420 -y 490 -anchor se


		# Quitte l'application grace au clavier (evite la souris pour cliquer sur le bouton OK)
		bind $c <Return> "xplace $c $a"
		#return du pave numerique$a
		bind $c <KP_Enter> "xplace $c $a"

		return

	} else {
		# NSE : ajout de la consigne (decouper en 2)
		# .......................................................
		$c delete consigne
		$c create text $cmilieu 480 -text "C'est fini !" -font $sysFont(messbas) -tag consigne
	
		button $c.ok -background $sysColor(color_mult_fond) -image ok1 -command exit
		place $c.ok -x 420 -y 490 -anchor se
	

		# NSE : MAJ du Pingoin
		# .......................................................
		catch {destroy .bframe.pneutre$indfich}

		if {$erreurstotal == 0} {
			button .bframe.pneutre$indfich -background $sysColor(color_pingoin_fond) -image bien -text [mc "$indfich"] -compound top -width 20 -height 45
		} else {
			button .bframe.pneutre$indfich -background $sysColor(color_pingoin_fond) -image mal -text [mc "$indfich"] -compound top -width 20 -height 45
		}
		grid .bframe.pneutre$indfich -column $indfich -padx 1 -row 0

		for {set ii [expr $indfich + 1]} {$ii <= 5} {incr ii} {
			# MAJ des autres pingoins
			#catch {destroy .bframe.pneutre$ii}
			#button .bframe.pneutre$ii -background $sysColor(color_mult_fond) -image neutre
			#grid .bframe.pneutre$ii -column $ii -padx 5 -row 0
		}	

		# Quitte l'application grace au clavier (evite la souris pour cliquer sur le bouton OK)
		bind $c <Return> exit
		#return du pave numerique$a
		bind $c <KP_Enter> exit	
	}
}
