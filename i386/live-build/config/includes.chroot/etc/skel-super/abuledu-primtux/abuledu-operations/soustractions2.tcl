############################################################################
# Copyright (C) 2004 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : soustractions2.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 01/11/2004
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: soustractions2.tcl,v 1.1 2004/11/14 15:38:27 david Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#soustractions2.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

global plateforme user listdata arg1 listgen indfich

package require Img

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
changehome


set c .frame.c
set a .frame.a


set arg1 [lindex $argv 0]




#interface
. configure -background $sysColor(color_sous2_fond)

frame .frame -width 640 -height 520 -background $sysColor(color_sous2_fond)
pack .frame -side top -fill both -expand yes

# Met la fen�tre en haut � gauche
wm geometry . +0+0


# NSE : nouveau : tableau pour les historiques
canvas $a -width 240 -height 500 -background $sysColor(color_hist_fond) -highlightbackground $sysColor(color_hist_fond)
pack $a -side left

# Zone pour l'operation
canvas $c -width 420 -height 500 -background $sysColor(color_sous2_fond) -highlightbackground $sysColor(color_sous2_fond)
pack $c -side left

set cmilieu [expr 420 / 2]


#ouverture du fichier Soustractions.conf
set f [open [file join $Home reglages soustractions.conf] "r"]
set listgen [gets $f]
close $f

image create photo bilan -file [file join sysdata bilan_60.gif]
image create photo bien -file [file join sysdata pbien_40.gif] 
image create photo pass -file [file join sysdata ppass_40.gif]
image create photo mal -file [file join sysdata pmal_40.gif]
image create photo neutre -file [file join sysdata pneutre_40.gif]

# NSE 200908 : pouce remplac� par pingoin ok
image create photo ok1 -file [file join sysdata ok2-50-rouge.gif]
image create photo ok0 -file [file join sysdata ok2-50-rouge.gif]



catch {destroy .aaframe}
frame .aaframe -width 440 -height 1 -background $sysColor(color_sous2_fond)
pack .aaframe -side top

catch {destroy .aframe}
frame .aframe -width 440 -height 1 -background $sysColor(color_sous2_fond)
pack .aframe -side top

catch {destroy .bframe}
frame .bframe -width 440 -height 100 -background $sysColor(color_sous2_fond)
pack .bframe -side left


# NSE : nouveau : Creation des pingoins neutres
set xlistdata [lindex $listgen $arg1]
set xdata [lindex $xlistdata 2]
set xtotal [llength $xdata]
set xscen [lindex $xlistdata 0]

for {set ii 1} {$ii <= $xtotal} {incr ii} {
    # MAJ des pingoins neutre
    catch {destroy .bframe.pneutre$ii}
    button .bframe.pneutre$ii -background $sysColor(color_sous2_fond) -image neutre -text [mc "$ii"] -compound top -width 20 -height 45
    grid .bframe.pneutre$ii -column $ii -padx 1 -row 0
}



# NSE : MAJ de Titres Historique 
# ...................................
$a create text 90 90 -text "Bilan" -font $sysFont(historique) -anchor w
$a create text 90 120 -text $xscen -font $sysFont(historique_scen) -tags xscenario -anchor w

# NSE : Ajout de l'image du serveur
# ...................................
set myimage1 [image create photo -file sysdata/bilan_80.png]
label $a.bilan -image $myimage1 -background $sysColor(color_hist_fond)
place $a.bilan -x 0 -y 60 -anchor nw

# NSE : MAJ de la L�gende du tableau 
# ...................................
$a create text 20 450 -anchor w -text "P = erreur de placement d'un chiffre" -font $sysFont(legende)
$a create text 20 470 -anchor w -text "C = erreur de calcul" -font $sysFont(legende)
$a create text 20 490 -anchor w -text "(V = erreur de placement de la virgule)" -font $sysFont(legende)

# NSE : Creation du tableau de Historique 
# ................................................
set xnbrow [expr $xtotal + 1]
set xnbcol 5

# taille des cases
set xtaillerect $sysFont(taillerect)

set taillecol1 [expr ($xtaillerect * 4)]
set taillecol234 [expr ($xtaillerect / 2 * 1.2)]
set taillecol5 [expr ($xtaillerect * 1)]

set taillehaut [expr $sysFont(taillerect) * 0.75]

# point de d�part du tableau
set xtaille [expr ($taillecol1) + ($taillecol234*3)]
set xorg0 [expr 120 - ($xtaille/2)]

# position du tableau depuis le haut de la fenetre
set xorg1 180

# NSE : Ligne 0 avec les titres des colonnes
# .................................................
set x 0
set z 0
set xlarg $taillecol234
set xhaut $taillehaut
set xorg00 [expr $xorg0 + $taillecol1 - ($taillecol234*1)]
set xorg11 [expr $xorg1 - $xhaut]

for {set x 1} {$x <= [expr $xnbcol - 2]} {incr x} {
    # Pour chaque colonne
    $a create rect [expr $xorg00 + $x*$xlarg] [expr $xorg11 + $z*$xhaut]  [expr $xorg00 + ($x+1)*$xlarg]  [expr $xorg11 + ($z+1)*$xhaut] -width 1 -fill grey -tag histcol$x

    if {$x == 1} {set xtext "P"}
    if {$x == 2} {set xtext "C"}
    if {$x == 3} {set xtext "V"}

    $a create text [expr $xorg00 + $x*$xlarg + int($xlarg/2)] [expr $xorg11 + $z*$xhaut + int($xhaut/2)] -tags textcol[expr \$x]row[expr \$z] -font $sysFont(tableau_hist) -text $xtext
}


# NSE : Parcours des Operations
# ..................................................
for {set z 0} {$z <= [expr $xtotal - 1]} {incr z} {

    set xop [lindex $xdata $z]
    set xtmp [lindex $xop 0]
    set xnbop [llength $xop]
    for {set w 1} {$w <= [expr $xnbop - 1]} {incr w} {
	set xtmp "$xtmp x [lindex $xop $w]"
    }
    # L'op�ration z
    set op $xtmp

    # Colonne 1 : Operation
    set x 0
    set xlarg $taillecol1
    set xhaut $taillehaut
    $a create rect [expr $xorg0 + $x*$xlarg] [expr $xorg1 + $z*$xhaut]  [expr $xorg0 + ($x+1)*$xlarg]  [expr $xorg1 + ($z+1)*$xhaut] -width 1 -fill $sysFont(couleur_col_text_op) -tag histcol$x$z

    # Colonne 1 � xnbcol - 2 : Nb erreurs
    set xlarg1 $xlarg
    set xlarg $taillecol234
    set xhaut $taillehaut
    set xorg00 [expr $xorg0 + $xlarg1 - $xlarg]

    for {set x 1} {$x <= [expr $xnbcol - 2]} {incr x} {
	# Pour chaque colonne
	$a create rect [expr $xorg00 + $x*$xlarg] [expr $xorg1 + $z*$xhaut]  [expr $xorg00 + ($x+1)*$xlarg]  [expr $xorg1 + ($z+1)*$xhaut] -width 1 -fill $sysFont(couleur_col_resultat_op) -tag histcol$x$z
    }
}


catch {destroy .cframe}
frame .cframe -width 100 -height 100 -background $sysColor(color_sous2_fond)
pack .cframe -side left

catch {destroy .dframe}
frame .dframe -width 200 -height 100 -background $sysColor(color_sous2_fond)
pack .dframe -side right

# Creation des boutons "Quitter, Echelle, ..."
button .dframe.but_quitter -command exit -image [image create photo -file [file join sysdata quitter_minus.gif]]
pack .dframe.but_quitter -side right

button .dframe.but_clear -command exit -image [image create photo -file [file join sysdata fgauche.gif]]
pack .dframe.but_clear -side right

button .dframe.but_echelle -command exit -image [image create photo -file [file join sysdata echelle.gif]]
pack .dframe.but_echelle -side right


set indfich 0


#proc�dure principale
proc xplace {c a} {
    global listdata arg1 listgen user basedir progaide mode data indfich total sysFont nbcol nbrow curcol currow plateforme taillerect org0 org1 donnee resultat decim partint partdec erreurscalcul erreursplacement partdec1 partdec2 cmilieu

    #recup de l'activit� dans listdata
    set listdata [lindex $listgen $arg1]
    set message ""
    append message [mc "Soustractions"] " " [lindex [lindex $listdata 0] 0]
    wm title . $message
    catch {destroy .bframe}
    frame .bframe -background #ffff80
    pack .bframe -side bottom

    set erreurscalcul 0
    set erreursplacement 0

    set mode [lindex $listdata 1]
    set data [lindex $listdata 2]
    set donnee [lindex $data $indfich]

    label .bframe.consigne -background #ffff80 -text [mc "Pose l'op�ration et appuie sur le bouton"]
    grid .bframe.consigne -column 0 -padx 10 -row 0
    button .bframe.ok -background #ffff80 -image ok1 -command "verifplace $c $a"
    grid .bframe.ok -column 1 -padx 10 -row 0

    set total [llength $data]
    $c delete all

    set decim 0
    set tmp 0
    set partint 0
    set partdec 0

    for {set i 0} {$i < [llength $donnee]} {incr i 1} {
	set l$i [string length [lindex $donnee $i]]
	if {[expr \$l$i] > $tmp} {set tmp [expr \$l$i]}
	if {[string first , [lindex $donnee $i]] != -1} {set decim 1}
    }


    set nbcol $tmp

    if {$decim == 1} {
	for {set i 0} {$i < [llength $donnee]} {incr i 1} {
	    set tmp [lindex $donnee $i]
	    set virg ,
	    if {[string first , $tmp] == -1} {
		set tmp $tmp$virg
	    }
	    if {$partint < [expr [string first , $tmp]]} {set partint [expr [string first , $tmp]]}
	    if {$partdec < [expr [string length $tmp] - [string first , $tmp] -1]} {set partdec [expr [string length $tmp] - [string first , $tmp]-1]}
	}
	set nbcol [expr $partint + $partdec +1]
    }

    set nbrow [expr [llength $donnee]]
    set currow 0
    set curcol $nbcol
    set taillerect 25
    set org0 [expr 320 - int((($nbcol +1)*$taillerect)/2)]
    set org1 50

    set tmp [lindex $donnee 0]
    for {set i 1} {$i < [llength $donnee]} {incr i 1} {
	set tmp "$tmp - [lindex $donnee $i]"
    }
    $c create text [expr 320] [expr 20] -text $tmp -font $sysFont(t) -tags operation
    #set resultat [expr [string map {, .} [lindex $donnee 0]] + [string map {, .} [lindex $donnee 1]]]
    set resultat [expr [string map {, .} $tmp]]


    for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
	for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
	    $c create rect [expr $org0 + $i*$taillerect] [expr $org1 + $j*$taillerect ]  [expr $org0 + ($i+1)*$taillerect]  [expr $org1 + ($j+1)*$taillerect]  -width 2 -fill gray -tags rectcol[expr \$i]row[expr \$j]
	    $c create text [expr $org0 + $i*$taillerect + int($taillerect/2)] [expr $org1 + $j*$taillerect + int($taillerect/2)] -tags textcol[expr \$i]row[expr \$j] -font $sysFont(t)

	}
    }
    for {set i 1} {$i <= [expr $nbcol]} {incr i 1} {
	for {set j 0} {$j <= [expr $nbrow -1]} {incr j 1} {
	    $c itemconf  textcol[expr \$i]row[expr \$j] -text .
	    $c bind rectcol[expr \$i]row[expr \$j] <ButtonRelease-1> "changefocus $c $a"
	    $c bind textcol[expr \$i]row[expr \$j] <ButtonRelease-1> "changefocus $c $a"

	}
    }

    for {set i 2} {$i <= [expr $nbcol]} {incr i 1} {
	$c create text [expr $org0 + $i*$taillerect + 4] [expr $org1 + int($taillerect) -5 ] -tags retcol[expr \$i]row0 -font $sysFont(s) -fill blue
    }

    for {set i 1} {$i < [expr $nbcol]} {incr i 1} {
	$c create text [expr $org0 + $i*$taillerect + int($taillerect/2)] [expr $org1 + 2*int($taillerect) -5 ] -tags retcol[expr \$i]row1 -font $sysFont(s) -fill blue
    }

    bind $c <KeyPress> "changenum $c $a %K"
    bind $c <Right> "changecell $c $a right 0 [expr $nbrow -1] 1 $nbcol"
    bind $c <Up> "changecell $c $a up 0 [expr $nbrow -1] 1 $nbcol"
    bind $c <Left> "changecell $c $a left 0 [expr $nbrow -1] 1 $nbcol"
    bind $c <Down> "changecell $c $a down 0 [expr $nbrow -1] 1 $nbcol"

    #if { $plateforme == "unix" }  {
    #bind $c <KP_Right> "changecell $c right 0 [expr $nbrow -1] 1 $nbcol"
    #bind $c <KP_Left> "changecell $c left 0 [expr $nbrow -1] 1 $nbcol"
    #bind $c "changecell $c down 0 [expr $nbrow -1] 1 $nbcol"
    #bind $c  <KP_Up> "changecell $c up 0 [expr $nbrow -1] 1 $nbcol"
    #}

    focus -force .frame.c
    for {set i 1} {$i < [expr $nbrow]} {incr i 1} {
	$c itemconf textcol0row$i -text -
    }
    $c create line [expr $org0] [expr $org1 + $nbrow*$taillerect ]  [expr $org0 + [expr $nbcol +1]*$taillerect]  [expr $org1 + $nbrow*$taillerect]  -width 4 -fill red -tags line
    $c itemconf rectcol[expr \$nbcol]row1 -fill white

}


#appel de la proc�dure principale
xplace $c $a

proc changeret {c} {
    global nbcol nbrow taillerect org0 org1 decim partint
    set coord [$c coords current]
    set xc [expr int(([lindex $coord 0]- $org0)/$taillerect)]
    set yc [expr int(([lindex $coord 1]-$org1)/$taillerect)]
    set tmp [$c itemcget retcol[expr \$xc]row[expr \$yc] -text]
    ### pas sur la virgule, la retenue
    if {$xc == [expr $partint +1]} {return}
    set pas 0
    if {$decim ==1 && ($xc == [expr $partint +2])} {set pas 1}

    if {$tmp == ""} {
	set result [string map {. 0} [$c itemcget retcol[expr \$xc -1 -$pas]row0 -text]][string map {. 0} [$c itemcget textcol[expr \$xc -1 -$pas]row0 -text]]
	if {$result == ""} {set result 0}
	set ope1 [expr $result]
	if {[string match {[1-9]} [$c itemcget textcol[expr \$xc -1 -$pas]row[expr \$yc] -text]]} {
	    $c itemconf retcol[expr \$xc]row[expr \$yc] -text 1
	    $c itemconf textcol[expr \$xc -1 -$pas]row[expr \$yc] -text [expr [$c itemcget textcol[expr \$xc -1 -$pas]row[expr \$yc] -text] - 1]
	}
	if {$result == "10"} {
	    $c itemconf retcol[expr \$xc]row[expr \$yc] -text 1
	    $c itemconf retcol[expr \$xc -1 -$pas]row[expr \$yc] -text ""
	    $c itemconf textcol[expr \$xc -1 -$pas]row[expr \$yc] -text "9"
	}
    } else {
	if {[$c itemcget retcol[expr \$xc - 1 - $pas]row[expr \$yc] -text] == "1"} {return}
	if {[string match {[0-9]} [$c itemcget textcol[expr \$xc -1 -$pas]row[expr \$yc] -text]]} {
	    $c itemconf retcol[expr \$xc]row[expr \$yc] -text ""
	    if {[$c itemcget textcol[expr \$xc -1 -$pas]row[expr \$yc] -text] != "9"} {
		$c itemconf textcol[expr \$xc -1 -$pas]row[expr \$yc] -text [expr [$c itemcget textcol[expr \$xc -1 -$pas]row[expr \$yc] -text] + 1]
	    } else {
		$c itemconf textcol[expr \$xc -1 -$pas]row[expr \$yc] -text "0"
		$c itemconf retcol[expr \$xc -1 -$pas]row[expr \$yc] -text "1"
	    }
	}
    }
}


proc changefocus {c a} {
    global nbcol nbrow curcol currow taillerect org0 org1
    for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
	for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
	    $c itemconf  rectcol[expr \$i]row[expr \$j] -fill grey
	}
    }
    set coord [$c coords current]
    set xc [expr int(([lindex $coord 0]- $org0)/$taillerect)]
    set yc [expr int(([lindex $coord 1]-$org1)/$taillerect)]
    $c itemconf rectcol[expr \$xc]row[expr \$yc] -fill white
    set curcol $xc
    set currow $yc
}

proc changecell {c a where rmin rmax cmin cmax} {
    global nbcol nbrow curcol currow taillerect
    switch $where {
        right { 
	    set tmp [expr $curcol +1]
	    if {$tmp <= $cmax} {
		set curcol $tmp
	    }
	}
	left { 
	    set tmp [expr $curcol -1]
	    if {$tmp >= $cmin} {
		set curcol $tmp
	    }
	}
	down { 
	    set tmp [expr $currow +1]
	    if {$tmp <= $rmax} {
		set currow $tmp
	    }
	}
	up { 
	    set tmp [expr $currow -1]
	    if {$tmp >= $rmin} {
		set currow $tmp
	    }
	}

    }
    for {set i 1} {$i <= [expr $nbcol]} {incr i 1} {
	for {set j 0} {$j <= [expr $nbrow -1]} {incr j 1} {
	    $c itemconf  rectcol[expr \$i]row[expr \$j] -fill grey
	}
    }
    $c itemconf rectcol[expr \$curcol]row[expr \$currow] -fill white

}

proc changenum {c a key} {
    global curcol currow
    focus -force .frame.c
    
    switch $key {
	BackSpace - Delete {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text .}
	0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9  {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text $key}
	KP_Insert {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "0"}
	KP_End {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "1"}
	KP_Down {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "2"}
	KP_Next {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "3"}
	KP_Left {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "4"}
	KP_Begin {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "5"}
	KP_Right {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "6"}
	KP_Home {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "7"}
	KP_Up {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "8"}
	KP_Prior {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text "9"}
	KP_Delete {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text ,}

	comma {$c itemconf textcol[expr \$curcol]row[expr \$currow] -text ,}
    }
}

proc verifplace {c a} {
    global nbcol donnee nbrow currow curcol sysFont activecol decim partint partdec erreursplacement
    for {set j 0} {$j < [expr $nbrow]} {incr j 1} {
	set nomb ""
	set saisie ""
	for {set i 1} {$i <= [expr $nbcol]} {incr i 1} {
	    set saisie [$c itemcget textcol[expr \$i]row[expr \$j] -text]
	    set nomb $nomb$saisie
	}

	set tmp [string trim $nomb .]
	if {[string first . $tmp] != -1} {
	    # pas de . au milieu du nombre
	    set message ""
	    append message [mc "Erreur au nombre"] " "  [expr $j + 1]
	    set answer [tk_messageBox -message $message -type ok] 
	    incr erreursplacement
	    return
	}

	if {$decim ==0} {
	    if {[string equal [string trimleft [lindex $donnee [expr $j]] 0] [string trimleft $nomb .]] != 1} {
		incr erreursplacement
		set message ""
		append message [mc "Erreur au nombre"] " "  [expr $j + 1]
		set answer [tk_messageBox -message $message -type ok] 
		return
	    }
	} else {
	    if {[string first , $nomb] == -1} {
		#si l'op�ration est d�cimale, si le nombre n'a pas de virgule, on force la virgule au bon endroit.
		set nomb [string replace $nomb [expr $partint] [expr $partint] ,]
	    }

	    set nomb [string map {. 0} $nomb]
	    set virg ,
	    set tmp [lindex $donnee [expr $j]]
	    if {[string first , $tmp] == -1} {set tmp $tmp$virg}
	    set entier [expr [string first , $tmp]]
	    if {$entier < $partint} { set tmp [string repeat 0 [expr $partint - $entier]]$tmp}
	    set decimal [expr [string length $tmp] - [string first , $tmp] -1]
	    if {$decimal < $partdec} { set tmp $tmp[string repeat 0 [expr $partdec - $decimal]]}
	    if {[string equal $nomb $tmp] != 1} {
		incr erreursplacement
		set message ""
		append message [mc "Erreur au nombre"] " "  [expr $j + 1]
		set answer [tk_messageBox -message $message -type ok] 
		return
	    } 
	}
    }

    .bframe.consigne configure -text [mc "Effectue chaque colonne et valide en appuyant sur Entr�e (Pour marquer les retenues, clique dans les cases)"]
    for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
	for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
	    $c itemconf  rectcol[expr \$i]row[expr \$j] -fill grey
	}
    }
    set curcol $nbcol
    set activecol $nbcol
    set currow $nbrow
    $c itemconf rectcol[expr \$curcol]row[expr \$currow] -fill white
    for {set i 1} {$i <= [expr $nbcol]} {incr i 1} {
	for {set j 0} {$j <= [expr $nbrow -1]} {incr j 1} {
	    $c bind rectcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
	    $c bind textcol[expr \$i]row[expr \$j] <ButtonRelease-1> ""
	}
    }


    catch {destroy .bframe.ok}
    bind $c <Return> "verifope $c $a"
    #return du pave numerique
    bind $c <KP_Enter> "verifope $c $a"

    set pas 0
    if {$decim == 1 && $activecol == [expr $partint + 2]} {
	set pas 1
    }
    for {set i 1} {$i <= [expr $nbcol]} {incr i 1} {
	$c bind rectcol[expr \$i]row0 <ButtonRelease-1> "changeret $c"
	$c bind textcol[expr \$i]row0 <ButtonRelease-1> "changeret $c"
    }


    bind $c <Right> ""
    bind $c <Up> ""
    bind $c <Left> ""
    bind $c <Down> ""
    $c bind rectcol[expr \$curcol]row[expr \$currow] <ButtonRelease-1> "changefocus $c $a"
    $c bind textcol[expr \$curcol]row[expr \$currow] <ButtonRelease-1> "changefocus $c"

    for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
	$c itemconf  textcol[expr \$i]row[expr $nbrow] -text .
    }

}

proc verifope {c a} {
    global curcol currow nbcol nbrow activecol indfich resultat total partint decim listdata erreurscalcul erreursplacement user
    set result 0
    set averif 0
    # cas du nombre entier, ou du nombre d�cimal quand on n'est pas sur la virgule
    if {$decim ==0 || ($activecol != [expr $partint + 1])} {
	#determination des chifres � traiter (ope1 et ope2)
	set tmp [string map {. 0} [$c itemcget retcol[expr \$activecol]row0 -text]][string map {. 0} [$c itemcget textcol[expr \$activecol]row0 -text]]
	if {$tmp == ""} {set tmp 0}
	set ope1 [expr $tmp]
	set retenue2 [$c itemcget retcol[expr \$activecol]row1 -text]
	if {$retenue2 == "" || $retenue2 == "."} {set retenue2 0}
	set tmp [string map {. 0 - 0} [$c itemcget textcol[expr \$activecol]row1 -text]]
	if {$tmp == ""} {set tmp 0}
	set ope2 [expr $tmp + $retenue2]
	set pas 0
	# si activecol est juste avant la virgule, pas = 1, pour reperer la retenue "basse" � v�rifier
	if {$decim == 1 && $activecol == [expr $partint + 2]} {
	    set pas 1
	}

	set result [expr $ope1 - $ope2]

	set averif [string map {. 0} [$c itemcget textcol[expr \$activecol]row2 -text]]
    }


    if {$decim == 1} {
	set pas 1
	if {$activecol == [expr $partint + 2]} {
	    set pas 2
	}
	#set averif [string map {. 0} [$c itemcget textcol[expr \$activecol - $pas]row2 -text]]
	#si on l'on doit traiter la virgule
	if {$activecol == [expr $partint +1]} {
	    set averif ,
	    set result [$c itemcget textcol[expr \$activecol]row2 -text]
	    if {$averif != $result} {
		incr erreurscalcul
		set answer [tk_messageBox -message [mc "Erreur de calcul"] -type ok] 
		return
	    } else {
		#substitution des valeurs par 0, pour rendre ok le test suivant de controle g�n�ral
		set result 0
		set averif 0
	    }
	}
    }


    set averif [string trimleft $averif 0]
    if {$averif == ""} {set averif 0}
    set result [string trimleft $result 0]
    if {$result == ""} {set result 0}

    if {[expr $averif] != [expr $result]} {
	incr erreurscalcul
	set answer [tk_messageBox -message [mc "Erreur de calcul"] -type ok] 
	return
    } else {
	set activecol [expr $activecol -1]
	for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
	    for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
		$c itemconf  rectcol[expr \$i]row[expr \$j] -fill grey
	    }
	}
	if {$activecol < 0} {
	    $c itemconf operation -text "[$c itemcget operation -text] = $resultat"
	    bind $c <Return> ""
	    #return du pave numerique
	    bind $c <KP_Enter> ""

	    set str ""
	    append str "\173" [mc "Op�ration effectu�e"] " : " [$c itemcget operation -text]  "\175"
	    enregistreeval Soustractions [lindex $listdata 0] $str $user
	    set message ""
	    append message "\173" [mc "Erreur(s) de placement"] " : " $erreursplacement "\175\040\173" \
		[mc "Erreur(s) de calcul"] " : " $erreurscalcul "\175"	
	    appendeval $message $user


	    set message ""
	    append message [mc "Op�ration termin�e"] "\n" [mc "Erreur(s) de placement"] " : " $erreursplacement "\n" \
		[mc "Erreur(s) de calcul"] " : " $erreurscalcul
	    set answer [tk_messageBox -message $message -type ok]
	    incr indfich
	    if {$indfich < $total} {
		.bframe.consigne configure -text [mc "Appuie sur le bouton pour continuer"]
		button .bframe.ok -background #ffff80 -image ok1 -command "xplace $c $a"
		grid .bframe.ok -column 1 -padx 10 -row 0

		return
	    } else {
		.bframe.consigne configure -text [mc "C'est fini !"]
	    }
	} else {
	    $c itemconf rectcol[expr \$activecol]row[expr \$nbrow] -fill white
	    #$c bind retcol[expr \$activecol +1]row0 <ButtonRelease-1> ""

	    #set pas 0
	    #if {$decim == 1 && $activecol == [expr $partint + 1]} {
	    #set pas 1
	    #}
	    #if {$decim == 1 && $activecol != [expr $partint + 1]} {
	    $c bind rectcol[expr \$activecol + 1 ]row0 <ButtonRelease-1> ""
	    $c bind textcol[expr \$activecol + 1 ]row0 <ButtonRelease-1> ""
	    #}


	    set currow $nbrow
	    set curcol $activecol
	}
    }

}
