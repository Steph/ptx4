#!/bin/sh
#aller.tcl
#\
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 David Lucardi <davidlucardi@aol.com>
#  Copyright (C) 2002 Andr� Connes <andre.connes@toulouse.iufm.fr>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : aller.tcl
#  Author  : David Lucardi <davidlucardi@aol.com>
#  Modifier:
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    $Id: aller.tcl,v 1.16 2007/01/07 06:53:11 david Exp $
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  David Lucardi
#  @copyright  Andr� Connes
# 
#***********************************************************************
global sysFont listappli listexo strtext basedir Homeconf plateforme env baseHome filuser
variable defautrep
variable demarre

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
source fonts.tcl
source path.tcl
source msg.tcl


init $plateforme
inithome
initlog $plateforme $ident
if {[lindex $argv 0] != ""} {set filuser [lindex $argv 0]}

set f [open [file join $baseHome reglages $filuser] "r"]
set demarre [gets $f]
set defautrep [gets $f]
set aller [gets $f]
close $f

if { [file exists [file join $Home textes $defautrep $demarre]] ==0 } {
set demarre "Au choix"
}

if { [file isdirectory [file join $Home textes $defautrep]] ==0 } {
set defautrep ""
}

set f [open [file join $baseHome reglages $filuser] "w"]
puts $f  $demarre
puts $f  $defautrep
puts $f $aller
close $f

wm geometry . +52+0
. configure -background #ffffff -width 640 -height 480

proc interface {} {
global sysFont strtext listexo iwish progaide basedir plateforme env Home baseHome prof abuledu user filuser
variable reperttext
variable demarre
variable defautrep
variable repertconf
variable langue
#############################
set f [open [file join $baseHome reglages $filuser] "r"]
set demarre [gets $f]
set defautrep [gets $f]
close $f
setlistexo $defautrep $demarre

switch [string range $defautrep 0 1] {
C_ {set groupe [mc {Communs}]}
S_ {set groupe [mc {Structures}]}
T_ {set groupe [mc {Textes}]}
D_ {set groupe [mc {Demos}]}
default {set groupe ""}
}
set rep [string range $defautrep 2 end]
wm title . "ALLER - [mc {Groupe}] : $groupe / [mc {Dossier}] : $rep / [mc {Texte}] : $demarre - [mc {Utilisateur}] : [string map {.log \040} [file tail $user]]"
# Creation du menu utilise comme barre de menu:
catch {destroy .menu}
catch {destroy .wwleft}
catch {destroy .wleft}
catch {destroy .wcenter}
catch {destroy .wright}
catch {destroy .wwright}

menu .menu -tearoff 0

# Creation du menu Fichier
menu .menu.fichier -tearoff 0
.menu add cascade -label [mc {Fichier}] -menu .menu.fichier
.menu.fichier add command -label [mc {Editeur}] -command "exec $iwish editeur.tcl 1 &"


.menu.fichier add cascade -label [mc {Gestion}] -menu .menu.fichier.gestion
menu .menu.fichier.gestion -tearoff 0 
.menu.fichier.gestion add command -label [mc {Utilisateurs}] -command "exec $iwish gestion.tcl 1 &"
.menu.fichier.gestion add command -label [mc {Groupes}] -command "exec $iwish groupe.tcl &"

.menu.fichier add command -label [mc {Imprimer le suivi}] -command "imprimesuivi"

.menu.fichier add sep
.menu.fichier add command -label [mc {Quitter  Ctrl-q}] -command exit

menu .menu.dossier -tearoff 0
.menu add cascade -label [mc {Dossier de textes}] -menu .menu.dossier
.menu.dossier add cascade -label [mc {Communs}] -menu .menu.dossier.commun
menu .menu.dossier.commun -tearoff 0 
catch {
foreach i [lsort [glob [file join $Home textes C_*]]] {
if {[file isdirectory $i]} {
.menu.dossier.commun add radio -label [string range [file tail $i] 2 end] -variable defautrep -value [file tail $i] -command "changereptext [file tail $i] \n interface"
}
}
}

.menu.dossier add cascade -label [mc {Textes}] -menu .menu.dossier.textes
menu .menu.dossier.textes -tearoff 0 
catch {
if {[file isdirectory $i]} {
foreach i [lsort [glob [file join $Home textes T_*]]] {
.menu.dossier.textes add radio -label [string range [file tail $i] 2 end] -variable defautrep -value [file tail $i] -command "changereptext [file tail $i] \n interface"
}
}
}

.menu.dossier add cascade -label [mc {Structures}] -menu .menu.dossier.structures
menu .menu.dossier.structures -tearoff 0 
catch {
if {[file isdirectory $i]} {
foreach i [lsort [glob [file join $Home textes S_*]]] {
.menu.dossier.structures add radio -label [string range [file tail $i] 2 end] -variable defautrep -value [file tail $i] -command "changereptext [file tail $i] \n interface"
}
}
}
.menu.dossier add cascade -label [mc {Demos}] -menu .menu.dossier.demos
menu .menu.dossier.demos -tearoff 0 
catch {
if {[file isdirectory $i]} {
foreach i [lsort [glob [file join $Home textes D_*]]] {
.menu.dossier.demos add radio -label [string range [file tail $i] 2 end] -variable defautrep -value [file tail $i] -command "changereptext [file tail $i] \n interface"
}
}
}


menu .menu.textes -tearoff 0
.menu add cascade -label [mc {Textes}] -menu .menu.textes

catch {
.menu.textes add radio -label [mc {Au choix}] -variable demarre -command "changetextedefaut"
foreach i [lsort [glob [file join $Home textes $defautrep *]]] {
if {[file isfile $i]} {
if {([string first .txt [file tail $i]] != 0) || ([string first .alr [file tail $i]] != 0)} {
.menu.textes add radio -label [file tail $i] -variable demarre -command "changetextedefaut"
}
}
}
}
menu .menu.option -tearoff 0
.menu add cascade -label [mc {Options}] -menu .menu.option

.menu.option add command -label [mc {Reglages}] -command "source reglages.tcl"


variable langue
set langue fr
menu .menu.option.repconf -tearoff 0 
#.menu.option add cascade -label [mc {Dossier de travail}] -menu .menu.option.repconf
#.menu.option.repconf add radio -label [mc {Commun}] -variable repertconf -value 1 -command "changeinterface"
#.menu.option.repconf add radio -label [mc {Individuel}] -variable repertconf -value 0 -command "changeinterface"

#if {$plateforme == "windows"} {
.menu add command -label [mc {Utilisateur}] -command "setwindowsusername"
#}

menu .menu.aide -tearoff 0
.menu add cascade -label [mc {Aide}] -menu .menu.aide



if {[file exists [file join $baseHome reglages lang.conf]] == 1} {
	set f [open [file join $baseHome reglages lang.conf] "r"]
  	gets $f langue
  	close $f
	}
	set fich "_index.htm"
	set fich $langue$fich
	set fichier [file join [pwd] aide $fich]
	  


if {[file exists $fichier] != 1} {
set fichier [file join [pwd] aide fr_index.htm]
}

.menu.aide add command -label [mc {Aide}] -command "exec $progaide file:///$fichier &"
.menu.aide add command -label [mc {A propos ...}] -command "source apropos.tcl"
. configure -menu .menu

#######################################################################"
. configure -background blue -width 640 -height 480
frame .wwleft -background blue -height 300 -width 120

frame .wleft -background blue -height 300 -width 120
#-width 270
pack .wwleft .wleft -side left
set listacti {{lanceappli closure.tcl} {lanceappli reconstitution.tcl} {lanceappli phrase.tcl} {lanceappli mots.tcl} {lanceappli fautes.tcl} {lanceappli espace.tcl} {lanceappli incomplet.tcl} {lance 1} {lance 2} {lanceappli flash.tcl} {lanceappli rapido.tcl} {lanceappli ponctuation1.tcl} {lanceappli ponctuation2.tcl} {lanceappli dictee.tcl}}

set listeval ""
catch {
set f [open [file join $user] "r" ]
while {![eof $f]} {
lappend listeval [gets $f]                          
}
close $f
}
set ext .gif
for {set i 0} {$i < 7} {incr i} {
if {[lindex [lindex $listexo $i] 4] == 1} {
button .wwleft.but$i -image [image create photo -file [file join sysdata [expr $i +1]$ext]] -background white -command "[lindex $listacti $i] [lindex [lindex $listexo $i] 3]" -relief ridge
pack .wwleft.but$i -side top -anchor w -pady 4 -padx 2
label .wleft.lab$i -text [lindex [lindex $listexo $i] 1] -background blue -wraplength 55m -width 20 -anchor w -font $sysFont(t) -pady 10 -justify left -foreground orange
pack .wleft.lab$i -side top -anchor w -pady 4 -padx 2
set frcolor white
for {set x [llength $listeval]} {$x>-1} {incr x -1} {
set it [lindex $listeval $x]
if {[lindex $it 5] == $i && [lindex $it 4] ==$repertconf && [lindex $it 1] == $demarre} {
.wleft.lab$i configure -text "[lindex [lindex $listexo $i] 1] : [lindex $it 3]%"
if {[lindex $it 3] < 50} {set frcolor red}
if {[lindex $it 3] >= 50 && [lindex $it 3] < 75} {set frcolor green}
if {[lindex $it 3] >= 75} {set frcolor yellow}
break
}
}
#bind .wleft.lab$i <1> "[lindex $listacti $i] [lindex [lindex $listexo $i] 3]"
.wleft.lab$i configure -foreground $frcolor
bind .wwleft.but$i <Any-Enter> ".wwleft.but$i configure -cursor hand1"
bind .wwleft.but$i <Any-Leave> ".wwleft.but$i configure -cursor left_ptr"
}
}



frame .wcenter -background blue -height 300
#-width 100
pack .wcenter -side left

frame .wright -background blue -height 300 -width 120
frame .wwright -background blue -height 300 -width 120

#-width 270
pack .wright .wwright -side left

for {set i 7} {$i < 14} {incr i} {
if {[lindex [lindex $listexo $i] 4] == 1} {
label .wright.lab$i -text [lindex [lindex $listexo $i] 1] -background blue -wraplength 55m -width 20 -anchor e -font $sysFont(t) -pady 10 -justify left -foreground orange
pack .wright.lab$i -side top -anchor e -pady 4 -padx 2

button .wwright.but$i -image [image create photo -file [file join sysdata [expr $i +1]$ext]] -background white -command "[lindex $listacti $i] [lindex [lindex $listexo $i] 3]" -relief ridge

pack .wwright.but$i -side top -anchor w -pady 4 -padx 2

set frcolor white
for {set x [llength $listeval]} {$x>-1} {incr x -1} {
set it [lindex $listeval $x]
if {[lindex $it 5] == $i && [lindex $it 4] ==$repertconf && [lindex $it 1] == $demarre} {
.wright.lab$i configure -text "[lindex [lindex $listexo $i] 1] : [lindex $it 3]%"
if {[lindex $it 3] < 50} {set frcolor red}
if {[lindex $it 3] >= 50 && [lindex $it 3] < 75} {set frcolor green}
if {[lindex $it 3] >= 75} {set frcolor yellow}
break
}
}
#bind .wright.lab$i <1> "[lindex $listacti $i] [lindex [lindex $listexo $i] 3]"
.wright.lab$i configure -foreground $frcolor
bind .wwright.but$i <Any-Enter> ".wwright.but$i configure -cursor hand1"
bind .wwright.but$i <Any-Leave> ".wwright.but$i configure -cursor left_ptr"

#bind .wright.lab$i <1> "[lindex $listacti $i] [lindex [lindex $listexo $i] 3]"
#bind .wright.lab$i <Any-Enter> ".wright.lab$i configure -foreground yellow"
#bind .wright.lab$i <Any-Leave> ".wright.lab$i configure -foreground orange"
}
}



set myimage [image create photo -file sysdata/background.gif]
label .wcenter.imagedisplayer -image $myimage -background blue -width 200
pack .wcenter.imagedisplayer -side top
label .wcenter.aide -text [mc {Premiere utilisation?}] -background blue -foreground white -font $sysFont(sb)
pack .wcenter.aide -side top
bind .wcenter.aide <1> "catch {destroy .wtux}; source compagnon.tcl; tux_presente"
bind .wcenter.aide <Any-Enter> ".wcenter.aide configure -cursor hand1"
bind .wcenter.aide <Any-Leave> ".wcenter.aide configure -cursor left_ptr"



#####################################################################"
}
#################
# Image de fond #
#################
############
# Bindings #
############
bind . <Control-q> {exit}






proc setlistexo {repertoire nom} {
global sysFont listappli listexo strtext Home Homeconf baseHome filuser
variable defautrep

set strtext ""

#set listexo {{closure Closure Complete 1 1} {reconstitution Reconstitution Complete 1 1} {phrase {Phrases melangees} {Clique sur une phrase et deplace-la en cliquant sur les fleches} 1 1} {mot {Mots melanges} {Remets les mots dans l'ordre sur les traits roses} 1 1} {faute {Texte a corriger} {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.} 1 1} {espace {Phrases sans espaces} {Clique pour separer les mots} 1 1} {incomplet {Phrases incompletes} {Complete} 1 1} {{Exercice supplementaire} {Exercice supplementaire 1} {Exercice 1} 1 0} {{Exercice supplementaire} {Exercice supplementaire 2} {Exercice 2} 1 0} {{Exercice supplementaire} {Exercice supplementaire 3} {Exercice 3} 1 0} {{Exercice supplementaire} {Exercice supplementaire 4} {Exercice 4} 1 0} {ponctuation1 Ponctuation1 {Complete avec le signe convenable} 1 1} {ponctuation2 Ponctuation2 {Clique pour r�tablir la ponctuation et complete avec le signe convenable} 1 1}}
#set listexo {{closure Closure Compl�te 1 1} {reconstitution Reconstitution Compl�te 1 1} {phrase {Phrases m�lang�es} {Clique sur une phrase et d�place-l� en cliquant sur les fl�ches} 1 1} {mot {Mots m�lang�s} {Remets les mots dans l'ordre sur les traits roses} 1 1} {faute {Texte � corriger} {Clique sur les mots mal �crits, r��cris-les et appuie sur entr�e.} 1 1} {espace {Phrases sans espaces} {Clique pour s�parer les mots} 1 1} {incomplet {Phrases incompl�tes} {Compl�te} 1 1} {{Exercice suppl�mentaire} {Exercice suppl�mentaire 1} {Exercice 1} 1 0} {{Exercice suppl�mentaire} {Exercice suppl�mentaire 2} {Exercice 2} 1 0} {{Exercice suppl�mentaire} {Exercice suppl�mentaire 3} {Exercice 3} 1 0} {{Exercice suppl�mentaire} {Exercice suppl�mentaire 4} {Exercice 4} 1 0} {ponctuation1 Ponctuation1 {Compl�te avec le signe convenable} 1 1} {ponctuation2 Ponctuation2 {Clique pour r�tablir la ponctuation et compl�te avec le signe convenable} 1 1}}
set listexo \173closure\040[mc {Closure}]\040[mc {Complete}]\0401\0401\175\040\173reconstitution\040[mc {Reconstitution}]\040[mc {Complete}]\0401\0401\175\040\173phrase\040\173[mc {Phrases melangees}]\175\040\173[mc {Clique sur une phrase et deplace-la en cliquant sur les fleches}]\175\0401\0401\175\040\173mot\040\173[mc {Mots melanges}]\175\040\173[mc {Remets les mots en ordre en les deplacant sur les traits roses.}]\175\0401\0401\175\040\173faute\040\173[mc {Texte a corriger}]\175\040\173[mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.}]\175\0401\0401\175\040\173espace\040\173[mc {Phrases sans espaces}]\175\040\173[mc {Clique pour separer les mots.}]\175\0401\0401\175\040\173incomplet\040\173[mc {Phrases incompletes}]\175\040\173[mc {Complete}]\175\0401\0401\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 1}]\175\040\173[mc {Exercice 1}]\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 2}]\175\040\173[mc {Exercice 2}]\175\0401\0400\175\040\173\173[mc {Flash}]\175\040\173[mc {Flash}]\175\040\173[mc {Clique sur le mot convenable}]\175\0401\0401\175\040\173\173[mc {Rapido}]\175\040\173[mc {Rapido}]\175\040\173[mc {Clique sur le mot convenable}]\175\0401\0401\175\040\173ponctuation1\040[mc {Ponctuation1}]\040\173[mc {Complete avec le signe de ponctuation convenable et appuie sur la touche entree pour valider.}]\175\0401\0401\175\040\173ponctuation2\040[mc {Ponctuation2}]\040\173[mc {Clique pour retablir la ponctuation et complete avec le signe convenable.}]\175\0401\0401\175

   if {$nom != "none" && $nom != "" && $nom !="Au choix"} {
   if {[catch {set f [open [file join $Home textes $repertoire $nom]]}] != 1} {                 

       while {![eof $f]} {

       set strtext $strtext[read $f 10000]
       }
   close $f
set ind  [string first "set listexo" $strtext]
if {$ind == -1} {
return
}
eval [string range $strtext $ind end]
} else {
set f [open [file join $baseHome reglages $filuser] "r"]
set tmp1 [gets $f]
set tmp2 [gets $f]
set aller [gets $f]
close $f

set f [open [file join $baseHome reglages $filuser] "w"]
puts $f "Au choix"
puts $f $defautrep
puts $f $aller
close $f
}
}
}


proc lanceappli {appli niveau} {
    global iwish filuser
    set appli [file join $appli]
    exec $iwish $appli $niveau $filuser &
    #destroy .
	exit
}

proc changeinterface {} {
global Home baseHome filuser
    variable demarre
    variable defautrep
    changehome
    set defautrep C_Commun
    set demarre "Au choix"
    catch {
    set demarre [file tail [lindex [lsort [glob -directory [file join $Home textes $defautrep] *.txt *.alr]] 0]]
    }
set f [open [file join $baseHome reglages $filuser] "r"]
set tmp1 [gets $f]
set tmp2 [gets $f]
set aller [gets $f]
close $f

set f [open [file join $baseHome reglages $filuser] "w"]
    puts $f $demarre
    puts $f $defautrep
    puts $f $aller
    close $f
    setlistexo $defautrep $demarre
    interface
}

proc lance {num i} {
global sysFont listexo
 switch $i {
   1 {lanceappli reconnaitre.tcl bisque$num}
   2 {lanceappli completer.tcl bisque$num}
}
}

setlistexo $defautrep $demarre
interface

proc setlang {lang} {
global env plateforme baseHome filuser
set env(LANG) $lang
set f [open [file join $baseHome reglages lang.conf] "w"]
puts $f $lang
close $f

::msgcat::mclocale $lang
::msgcat::mcload [file join [file dirname [info script]] msgs]
#############################################""
set f [open [file join $baseHome reglages $filuser] "r"]
set demarre [gets $f]
set defautrep [gets $f]
close $f
setlistexo $defautrep $demarre
######################################
interface
}

proc changereptext {what} {
global Home baseHome filuser
variable demarre

set demarre "Au choix"
catch {
set demarre [file tail [lindex [lsort [glob -directory [file join $Home textes $what] *.txt *.alr]] 0]]
}
set f [open [file join $baseHome reglages $filuser] "r"]
set tmp1 [gets $f]
set tmp2 [gets $f]
set aller [gets $f]
close $f

set f [open [file join $baseHome reglages $filuser] "w"]
puts $f $demarre
puts $f $what
puts $f $aller
close $f
setlistexo $what $demarre
interface
}

proc changetextedefaut {} {
global Home baseHome filuser
variable demarre
set f [open [file join $baseHome reglages $filuser] "r"]
set tmp1 [gets $f]
set tmp2 [gets $f]
set aller [gets $f]
close $f

set f [open [file join $baseHome reglages $filuser] "w"]
puts $f $demarre
puts $f $tmp2
puts $f $aller
close $f
setlistexo $tmp2 $demarre
interface
}

proc imprimesuivi {} {
global progaide LogHome user plateforme
variable demarre
set header "<head></head>"
if {$plateforme == "unix"} { set header "<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'></head>"}
if {$plateforme == "windows"} { set header "<head><meta http-equiv='Content-Type' content='text/html; charset=ISO 8859-1'></head>"}
set g [open [file join $LogHome tmp1.htm] "w" ] 
puts $g  $header
puts $g  "<html><body>"
puts $g  "<p align = center><B>ALLER - Fiche de [string map {.log \040} [file tail $user]] - [clock format [clock seconds] -format "%d/%m/%Y"]</B></p>"
puts $g  "<p><B>Texte : $demarre</B></p>"
puts $g  "<p><font color = blue>"
for {set i 0} {$i < 7} {incr i} {
set texte ""
catch {set texte [.wleft.lab$i cget -text]}
if {$texte != ""} {
puts $g  "$texte   -  "
}
}
puts $g  "</p>"
puts $g  "<p>"

for {set i 7} {$i < 14} {incr i} {
set texte ""
catch {set texte [.wright.lab$i cget -text]}
if {$texte != ""} {
puts $g  "$texte   - "
}
}
puts $g  "</p></font>"
puts $g  "</body></html>"

close $g
set fichier [string map {" " %20} [file join [pwd] $LogHome tmp1.htm]]
exec $progaide file:///$fichier &
}


