############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: associe6.tcl,v 1.4 2006/05/21 10:15:29 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
##########################################################################!/bin/sh
#associe6.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

###########################################################
#alear : tableau d'int pour le m�lange
#etiqarr :tableau pour recevoir les syllabes
#imgarr : tableau pour recevoir les images
#font2 : police de caract�re
#nb : nombre total d'item du jeu en cours
#listdata : donn�es du jeu, dans une liste
#bgl, bgn : couleurs utilis�es
# longlist1 : nombre de syllabes du mot
#categorie : variable pour la categorie
#user : variable pour le nom de l'utilisateur
#flag : d�tection de r�entrance dans le survol des objets, pour la gestion du son
#score calcul du pourcentage de r�ussite de l'�l�ve
#niveau : gestion du passage au niveau de difficult� sup�rieur 

#variables
source path.tcl
source msg.tcl
source eval.tcl
source fonts.tcl

global alear imgarr etiqarr soundarr bgn bgl nb font2 firstetiq listdata listeval categorie user flag niveau score repbasecat Home sound baseHome serie
set flag 0
set c .frame.c
set bgn #ffff80
set bgl #ff80c0
set base 0
set niveau 0
set score 0
set catedefaut ""
set font2 ""
set son 1
set nbreu 1
set arg [lindex $argv 0]
set serie [lindex $argv 1]

set ident $tcl_platform(user)
set plateforme $tcl_platform(platform)
initlog $plateforme $ident
inithome

#interface
. configure -background #ffff80 
frame .frame -width 640 -height 380 -background #ffff80
pack .frame -side top -fill both -expand yes
wm geometry . +0+0
canvas $c -width 640 -height 380 -background $bgn -highlightbackground #ffff80
pack $c
frame .bframe -background #ffff80
pack .bframe -side bottom
for {set i 1} {$i < 9} {incr i 1} {
  label .bframe.lab$i -background #ffff80 -width 4
  grid .bframe.lab$i -column [expr $i -1] -row 1 -sticky e
  }
label .bframe.lab22 -background #ffff80 -text [mc {Place la bonne etiquette sur la ligne rose.}]
grid .bframe.lab22 -column $i -padx 1 -row 1
button .bframe.b1 -image [image create photo imagavant -file [file join sysdata avant.gif] ] -background #ff80c0 -command "setniveauprec $c"
grid .bframe.b1 -column [expr $i + 1] -padx 1 -row 1 -sticky w

button .bframe.b2 -image [image create photo imagbut -file [file join sysdata again2.gif] ] -background #ff80c0
grid .bframe.b2 -column [expr $i + 2] -padx 1  -row 1 -sticky w
button .bframe.b3 -image [image create photo suitebut -file [file join sysdata suite.gif]] -background #ff80c0 -command "quitte"
grid .bframe.b3 -column [expr $i + 3] -padx 1  -row 1 -sticky w

#R�cup�ration donn�es g�n�rales dans associations.conf
catch {set f [open [file join $baseHome reglages associations.conf] "r"]
set catedefaut [gets $f]
set font2 [gets $f]
set son [gets $f]
set tmp [gets $f]
set tmp [gets $f]
set repbasecat [gets $f]
close $f}

set ext .cat
if {$catedefaut != "none" && $catedefaut != "" && $serie !="0"} {
set catedefaut [string map {.cat ""} $catedefaut]$serie$ext
}


switch $tcl_platform(platform) {
    unix {
    set taille 20
    }
    windows {
    set taille 14
    }
    }

if {$font2 == "none" || $font2 == ""} {
set font2 {Arial $taille bold}

} else {
set font2 \173$font2\175\040$taille\040bold
}

#d�tection possibilit� son (variable sound, et param�tre d'activation du son : son)
#if {[catch {package require snack}] || $son == 0} {
#set sound 0
#} else {
#set sound 1
#snack::sound s
#}

proc setniveauprec {c} {
global listeval categorie serie repbasecat listdata Home
if {$serie > 0} {
incr serie -1
set ext .cat
set file $categorie
for {set i 1} {$i <= [string length $file]} {incr i 1} {
if {[string match {[0-9]} [string index $file end]] == 1} {
set  file [string range $file 0 [expr [string length $file] -2] ]
}
}
if {$serie != 0} {
set file $file$serie$ext
} else {
set file $file$ext
}

set f [open [file join $Home categorie $repbasecat $file] "r"]
set listdata [gets $f]
close $f

set listeval \173[mc {Retrouver la fin des mots - }]\175\040$file
wm title . "[mc {Retrouver la fin des mots - }]$file"

init
place $c 0
}
}


proc initmain {c ind} {
global catedefaut listdata categorie Home font2 repbasecat getcat sound listeval iwish
	
	if {$catedefaut == "none" || $catedefaut == ""} {
	opencat
	catch {destroy .opencate}
	set ext .cat
	set file $getcat$ext
 	} else {
	set file $catedefaut
	}
wm title . "[mc {Retrouver la fin des mots - }]$file" 

	.bframe.b2 configure -command "recommence $c"
	#.bframe.b3 configure -command "suite $c"

if {[catch { set f [open [file join $Home categorie $repbasecat $file] "r" ] }] } {
 
      set answer [tk_messageBox -message [mc {Erreur de fichier.}] -type ok -icon info] 
	exec $iwish associations.tcl &
      exit
}

if {[catch {set listdata [gets $f]}]} {
     set answer [tk_messageBox -message [mc {Erreur de fichier.}] -type ok -icon info]
     close $f
	exec $iwish associations.tcl &
     exit
     } else {
     close $f
     }



set categorie [lindex [split [lindex [split $file /] end] .] 0]
set listeval \173[mc {Retrouver la fin des mots - }]\175\040$categorie

catch {
set ext ".dat"
set f [open [file join $Home categorie $repbasecat $categorie$ext] "r" ] 
set sson [lindex [lindex [gets $f] 2] 6]
set sound [expr $sound && $sson]
close $f
}

image create photo pbien -file [file join sysdata pbien.gif] 
image create photo ppass -file [file join sysdata ppass.gif]
image create photo pmal -file [file join sysdata pmal.gif]

init
place $c $ind
}

#proc�dures
###################################################################"""
proc init {} {
global listdata nb imgarr etiqarr firstetiq listeval categorie iwish

set listdata [string map {\* ""} $listdata]

if {[llength $listdata] <4} {
  set answer [tk_messageBox -message [mc {Pour ce jeu, il faut au moins 4 images.}] -type ok -icon info] 
	exec $iwish associations.tcl &
  exit
  }




for {set i [expr [llength $listdata] - 1]} {$i >= 0} {incr i -1} {
   if {[llength [lindex [lindex $listdata $i ] 3]] < 2} {
        set listdata [lreplace $listdata $i $i]
        }
}

# pas plus de huit associations retenues pour une cat�gorie
set nb int([llength $listdata]/4)
set nb [expr 4*$nb]
if {$nb > 31} {
set nb 32}

if {[llength $listdata] <4} {
set answer [tk_messageBox -message [mc {Pour ce jeu, les mots associes doivent compter au moins 2 syllabes}] -type ok -icon info] 
	exec $iwish associations.tcl &
exit
}

# melange des associations de la cat�gorie choisie
for {set i 1} {$i <= [llength $listdata]} {incr i 1} {
  set imgarr($i) [lindex [lindex $listdata [expr $i - 1] ] 0]
  set firstetiq($i) ""
  set etiqarr($i) ""
  set milieu [expr int(([llength [lindex [lindex $listdata [expr $i - 1] ] 3]] + 1) /2)]
     for {set k 0} {$k < [llength [lindex [lindex $listdata [expr $i - 1] ] 3]]} {incr k 1} {
       if {$k < $milieu} {
         set firstetiq($i) $firstetiq($i)[lindex [lindex [lindex $listdata [expr $i - 1] ] 3] $k]
         } else {
         set etiqarr($i) $etiqarr($i)[lindex [lindex [lindex $listdata [expr $i - 1] ] 3] $k]
     }
   }
}
# initialisation de listeval, pour la fiche bilan
#set tmp [mc {Retrouver la fin des mots - }]
#set listeval \173$tmp\175\040$categorie
}

#####################################################
proc recommence {c} {
global listeval user categorie score
set score 0
#enregistreval
# initialisation de listeval, pour la fiche bilan
#set tmp [mc {Retrouver la fin des mots - }]
#set listeval \173$tmp\175\040$categorie
#lappend listeval "4 \173[mc {Exercice recommence}]\175"
set listeval \173[mc {Retrouver la fin des mots - }]\175\040$categorie
place $c 0
}

proc melange {bas haut} {
global alear
set diff [expr $haut - $bas]
for {set i $bas} {$i < $haut} {incr i 1} {
  set alear($i) $i
  }
for {set i 1} {$i < 8} {incr i 1} {
  set t1 [expr int(rand()*$diff) + 1]
  set t2 [expr int(rand()*$diff) + 1]
  set temp $alear($t1)
  set alear($t1) $alear($t2)
  set alear($t2) $temp
  }
}


proc place {c bse} {
#base : pointeur sur l'indice de la planche de 4 images
global alear imgarr etiqarr soundarr bgn bgl nbreu base font2 sourcepos firstetiq nbessai listdata listeval categorie Home
set base $bse
set nbessai 0 
set font1 {Helvetica 12}

#on efface tout
$c delete all
#on place les images et les �tiquettes
if {$bse ==0} {
set nbreu 1
image create photo pneutre -file [file join sysdata pneutre.gif]
for {set i 1} {$i <= [expr int([llength $listdata]/4)]} {incr i 1} {
  .bframe.lab$i configure -image pneutre -width 30
  }
}


image create photo ok0 -file [file join sysdata ok0.gif] 
image create photo ok1 -file [file join sysdata ok1.gif]
$c create image 450 270 -image ok0 -tags verif
$c addtag highlight withtag verif

set imgpos {{90 70} {90 250} {350 70} {350 250}}
set sourcepos {{560 10} {560 110} {560 210} {560 310}}


melange 1 5
for {set i 1} {$i < 5} {incr i 1} {
  image create photo kimage$i -file [file join $Home images $imgarr([expr $i+ $base])] -width 130 -height 130
  set xpos [lindex [lindex $imgpos [expr $alear($i)-1]] 0]
  set ypos [lindex [lindex $imgpos [expr $alear($i)-1]] 1]
  $c create image $xpos $ypos -image kimage$i -tags img\040$i
  $c create text [expr $xpos - 40] [expr $ypos + 70] -text $firstetiq([expr $i + $base])  -anchor n -font $font2 -fill $bgl
  $c create text [expr $xpos - 40 + ([string length $firstetiq([expr $i + $base])] + 5)*10] [expr $ypos + 70] -text "______" -anchor n -tags cible$i -font $font2 -fill $bgl
  }

melange 1 5
for {set i 1} {$i < 5} {incr i 1} {
  $c create text [lindex [lindex $sourcepos [expr $alear($i) -1] ] 0] [lindex [lindex $sourcepos [expr $alear($i)- 1]] 1] -text $etiqarr([expr $i + $base]) -anchor n -tags source$i -font $font2 
  $c addtag drag withtag source$i
  }
}

#####################################
#appel des proc�dures principales
initmain $c $base

#gestion des �v�nements
$c bind highlight <Any-Enter> "pushEnter $c"
$c bind highlight <Any-Leave> "pushLeave $c"
$c bind verif <ButtonRelease-1> "verif $c"
$c bind drag <ButtonRelease-1> "itemStopDrag $c %x %y"
$c bind drag <1> "itemStartDrag $c %x %y"
$c bind drag <B1-Motion> "itemDrag $c %x %y"
#bind . <Destroy> "quitte"
if {$sound == 1} {
$c bind img <Any-Enter> "soundEnter $c"
$c bind img <Any-Leave> "soundLeave $c"
}


###############################################


proc soundEnter {c} {
global imgarr flag base repbasecat Home
if {$flag == 0 } {
#set strcible [string index [lindex [$c gettags current] 0] end]
set strcible [lindex [$c gettags current] 1]
    set ext .wav
    set son [lindex [split $imgarr([expr $strcible + $base]) .] 0] 
    enterstart [file join $Home sons $son$ext]
   }
}

proc soundLeave {c} {
enterstop
}


proc majbilan {} {
global listeval score etiqarr nbreu nbessai base firstetiq
   switch $nbessai {
                1 {.bframe.lab$nbreu configure -image pbien -width 30
                lappend listeval 1\040\173$firstetiq([expr $base + 1])$etiqarr([expr $base + 1])\040$firstetiq([expr $base + 2])$etiqarr([expr $base + 2])\040$firstetiq([expr $base + 3])$etiqarr([expr $base + 3])\040$firstetiq([expr $base + 4])$etiqarr([expr $base + 4])\175
                incr score 40
                }
                2 {.bframe.lab$nbreu configure -image ppass -width 30
                lappend listeval 2\040\173$firstetiq([expr $base + 1])$etiqarr([expr $base + 1])\040$firstetiq([expr $base + 2])$etiqarr([expr $base + 2])\040$firstetiq([expr $base + 3])$etiqarr([expr $base + 3])\040$firstetiq([expr $base + 4])$etiqarr([expr $base + 4])\175
                incr score 20
                }
                default {.bframe.lab$nbreu configure -image pmal -width 30
                lappend listeval 3\040\173$firstetiq([expr $base + 1])$etiqarr([expr $base + 1])\040$firstetiq([expr $base + 2])$etiqarr([expr $base + 2])\040$firstetiq([expr $base + 3])$etiqarr([expr $base + 3])\040$firstetiq([expr $base + 4])$etiqarr([expr $base + 4])\175
                }
            }

}
###########################################################"
proc pushEnter {c} {
$c itemconf highlight -image ok1
}

##########################################################
proc pushLeave {c} {
$c itemconf highlight -image ok0
}

#############################################"         
#gestion du jeu
proc itemStartDrag {c x y} {
    global lastX lastY sourcecoord flag
    set flag 1
    set sourcecoord [$c coords current]
    set lastX [$c canvasx $x]
    set lastY [$c canvasy $y]
    $c raise current
    }

proc itemStopDrag {c x y} {
    global lastX lastY sourcecoord nb listdata flag
    set flag 0
    #d�termination du tag de l'�tiquette que l'on a d�plac�, des coordonn�es de l'�tiquette, des coordonn�es de la cible
    set strcible cible
    set strsource [lindex [$c gettags current] 0]
    set ciblecoord [$c bbox [$c find withtag $strcible]]
    set coord [$c bbox current]
    if {[llength [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]]] > 2} {
    $c coords current [lindex $sourcecoord 0] [lindex $sourcecoord 1]
    return
    }
    
    foreach i [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]] {
         if {[lsearch -regexp [$c gettags $i] cible*] != -1} {
            $c coords current [$c coords $i]
            return
            } else {
		$c coords current [lindex $sourcecoord 0] [lindex $sourcecoord 1]
            }
    }

}

proc itemDrag {c x y} {
    global lastX lastY
    catch {$c delete withtag figure}
    set x [$c canvasx $x]
    set y [$c canvasy $y]
    $c move current [expr $x-$lastX] [expr $y-$lastY]
    set lastX $x
    set lastY $y
}



proc verif {c} {
global nbreu nb base sourcepos alear nbessai listeval etiqarr firstetiq niveau categorie score user listdata imgarr repbasecat Home serie iwish
variable repert

  set compt 0
  incr nbessai 
   # tests pour savoir si c'est ok
  for {set j 1} {$j <= 4} {incr j 1} {
     set ciblecoord [$c bbox [$c find withtag cible$j]]
     foreach i [$c find overlapping [lindex $ciblecoord 0] [lindex $ciblecoord 1] [lindex $ciblecoord 2] [lindex $ciblecoord 3]] {
       if {[lsearch [$c gettags $i] cible$j] == -1} {
          if {[lsearch [$c gettags $i] source$j] != -1} {
          incr compt
          
          } else {
          set numsource [string index [lindex [$c gettags $i] 0] end]
          $c coords $i [lindex [lindex $sourcepos [expr $alear($numsource) -1] ] 0] [lindex [lindex $sourcepos [expr $alear($numsource)-1]] 1]
          }
       }
     }
  }
#si les 4 images sont bien plac�es
  if {$compt==4} {
   image create photo figure -file [file join sysdata bien.gif]
   $c create image 450 170 -image figure -tags figure
   update
   majbilan
   if {$base < [expr $nb - 4]} {
       #si on n'a pas fini la s�rie
       incr base 4
       incr nbreu
       set nbessai 0
       update
       after 2000
       catch {$c delete withtag figure}
       place $c $base
       } else {
       #si tout est fini
       after 2000
       $c delete all
##########################################################"""""""
        #calcul du score
         set score [expr $score*10/$nb]
         set pourcent %
         $c create text 200 100 -text "[mc {Score : }] $score\040$pourcent" -font {Arial 24}
            #test pour savoir si on passe au niveau suivant et d�termination du niveau � faire
		lappend listeval \040bilan\040$serie\04016\040$repbasecat\040$repert\040$score
		enregistreval

            if {$score >= 75} { 
            update
            after 2000
            set file $categorie
            set niveau ""
            for {set i 1} {$i <= [string length $file]} {incr i 1} {
               if {[string match {[0-9]} [string index $file end]] == 1} {
                 set niveau [string index $file end]$niveau
                 set  file [string range $file 0 [expr [string length $file] -2] ]
                }
            }
            if {$niveau == ""} {
            set niveau 0
            }
            incr niveau
            set ext .cat
            set file $file$niveau$ext
            if {[catch {set f [open [file join $Home categorie $repbasecat $file] "r"]} ]} {
            $c create text 200 200 -text [mc {C'est fini!}] -font {Arial 24}
            } else {
            #si un autre niveau est requis
            set listdata [gets $f]
            close $f
            #enregistreval
            set categorie [lindex [split [lindex [split $file /] end] .] 0]
		wm title . "[mc {Retrouver la fin des mots - }]$file" 
		#lappend listeval "4 \{[mc {Retrouver la fin des mots - }] $categorie\}"
		for {set i 1} {$i < 9} {incr i 1} {
		.bframe.lab$i configure -image "" -width 1
		}

            #on r�initialise
#########################################################"
            init
######################################################
            set score 0
            # et on recommence
		incr serie
		set listeval \173[mc {Retrouver la fin des mots - }]\175\040$categorie
            place $c 0
            }
            } else {
            $c create text 200 200 -text [mc {C'est fini!}] -font {Arial 24}
            }
    }
    } else {
    image create photo figure -file [file join sysdata mal.gif]
    $c create image 450 200 -image figure -tags figure
   }
 }


proc suite {c} {
global catedefaut
enregistreval
set catedefaut ""
initmain $c 0
}
