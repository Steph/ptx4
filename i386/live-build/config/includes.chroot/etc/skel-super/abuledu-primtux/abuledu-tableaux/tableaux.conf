# $id$
# tableaux.tcl configuration file

#*************************************************************************
#  Copyright (C) 2002 André Connes <andre.connes@toulouse.iufm.fr>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : tableaux.tcl
#  Author  : André Connes <andre.connes@toulouse.iufm.fr>
#  Modifier:
#  Date    : 19/05/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version    $Id: tableaux.conf,v 1.15 2006/04/03 14:02:05 abuledu_andre Exp $
#  @author     André Connes
#  @modifier
#  @project    Le terrier
#  @copyright  André Connes
#
#***********************************************************************


set glob(version) "6.1"

set glob(bgcolor) #ffff80
set glob(width) 640 ;#800
set glob(height) 450 ;#600
set glob(org) 3 ;#translation de l'origine des coordonnées pour ajustement

set glob(attente) 4 ;# attente en seconde avant l'activité suivante

# ###################################################
# en général, ne pas modifier ci-dessous svp        #
# Cependant voir en fin de fichier le code pour     #
#     les répertoires des exercices (si nécessaire) #
# ###################################################

# gestion des répertoires Commun/Individuel,de la langue, des traces-élèves suivant l'OS
#   user=répertoire+identifiant-élève

# gestion des trace_dirs-élèves suivant l'OS
#   user=répertoire+identifiant-élève
  package require Img

if { $tcl_platform(platform) == "unix" } {
  set glob(platform) unix
  set glob(home_tableaux) [file join $env(HOME) leterrier tableaux]
  set glob(trace_dir) [file join $glob(home_tableaux) log] ;# on attend mieux !
  set glob(trace_user) $glob(trace_dir)/$tcl_platform(user)
  set glob(home_reglages) [file join $glob(home_tableaux) reglages]
  set glob(home_msgs) [file join $glob(home_tableaux) msgs]
  set glob(progaide) ./runbrowser
  set glob(wish) wish
} elseif { $tcl_platform(platform) == "windows" } {
  set glob(platform) windows
  set glob(home_tableaux) [file dir $argv0] ;# cad .
  set glob(trace_dir) [file join [file dir $argv0] tableaux log]
  set glob(trace_user) "$glob(trace_dir)/eleve" ;#forcer le nom (cf tableaux.tcl)
  set a ""
  catch {set a $env(USERNAME) }
  if { $a != "" } {
  # répertoire pour tous les utilisateurs
  #     catch { set glob(home_tableaux) [file join $env(ALLUSERSPROFILE) leterrier tableaux] }
  # ou répertoire pour l'utilisateur loggé
        catch { set glob(home_tableaux) [file join $env(USERPROFILE) leterrier tableaux] }
        set glob(trace_dir) [file join $glob(home_tableaux) log]
        set glob(trace_user) $glob(trace_dir)/$env(USERNAME)
        }
  set glob(home_msgs) [file join $glob(home_tableaux) msgs]
  set glob(home_reglages) [file join $glob(home_tableaux) reglages]
  set glob(progaide) shellexec.exe
  set glob(wish) wish
} else {
  set glob(platform) undef
  set glob(trace_dir) undef
  set glob(trace_user) undef
  set glob(home_tableaux) undef
}

#charger la liste des images
set all_directories [glob -type d *]
foreach tmp_didacticiel $all_directories {
  if { $tmp_didacticiel == "CVS"      } continue
  if { $tmp_didacticiel == "sysdata"  } continue
  if { $tmp_didacticiel == "mandrake" } continue
  if { $tmp_didacticiel == "debian"   } continue
  if { $tmp_didacticiel == "windows"  } continue
  if { $tmp_didacticiel == "reglages" } continue
  if { $tmp_didacticiel == "msgs"     } continue
  if { $tmp_didacticiel == "aides"    } continue
  if { $tmp_didacticiel == "tableaux" } continue
  if { $tmp_didacticiel == "Img1.2" } continue
  if { $tmp_didacticiel == "Img1.3" } continue
  lappend glob(didacticiels) $tmp_didacticiel
}

