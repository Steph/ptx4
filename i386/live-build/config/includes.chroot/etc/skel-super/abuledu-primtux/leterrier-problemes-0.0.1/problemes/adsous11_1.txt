set etapes 1
set niveaux {0 1 2 3 5}
::1
set niveau 2
set ope {{2 2} {2 4} {2 2} {2 4}}
set interope {{1 5 1} {1 8 1} {1 5 1} {1 8 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set ope3 [expr int(rand()*[lindex [lindex $ope 2] 1]) + [lindex [lindex $ope 2] 0]]
set ope4 [expr int(rand()*[lindex [lindex $ope 3] 1]) + [lindex [lindex $ope 3] 0]]
set volatil 0
set operations [list [list [expr $ope1]*[expr $ope2]+[expr $ope3]*[expr $ope4]] [list [expr $ope2]*[expr $ope1]+[expr $ope3]*[expr $ope4]] [list [expr $ope2]*[expr $ope1]+[expr $ope4]*[expr $ope3]] [list [expr $ope1]*[expr $ope2]+[expr $ope4]*[expr $ope3]] [list [expr $ope3]*[expr $ope4]+[expr $ope1]*[expr $ope2]] [list [expr $ope3]*[expr $ope4]+[expr $ope2]*[expr $ope1]] [list [expr $ope4]*[expr $ope3]+[expr $ope2]*[expr $ope1]] [list [expr $ope4]*[expr $ope3]+[expr $ope1]*[expr $ope2]] [list [expr $ope4*$ope3]+[expr $ope1*$ope2]] [list [expr $ope1*$ope2]+[expr $ope4*$ope3]]]
set enonce "Les fleurs.\nJ'ach�te $ope1 bouquets de $ope2 fleurs bleues et $ope3 bouquets de $ope4 fleurs rouges.\nCombien ai-je de fleurs?"
set cible {{5 4 {} source0} {5 4 {} source1}}
set intervalcible 60
set taillerect 45
set orgy 40
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 600
set source {fleurb.gif fleurr.gif}
set orient 0
set labelcible {{Fleurs bleues} {Fleurs rouges}}
set quadri 0
set reponse [list [list {1} [list {J'ai en tout} [expr ($ope1*$ope2) + ($ope3*$ope4)] {fleurs.}]]]
set ensembles [list [expr $ope1*$ope2] [expr $ope3*$ope4]]
set canvash 300
set c1height 160
set opnonautorise {}
::
