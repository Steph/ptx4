############################################################################
# Copyright (C) 2006 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : mathoeuf.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/08/2006
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: mathoeuf.tcl,v 1.3 2006/12/02 07:02:19 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/08/2006
#
#
#########################################################################
#!/bin/sh
#mathoeuf.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

global basedir Home plateforme baseHome
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source msg.tcl

initapp $plateforme
inithome
initlog $plateforme $ident
changehome

#wm resizable . 0 0
#on place la fenetre en haut � gauche
wm geometry . +0+0
. configure -background #ffff80

###########################################
proc recherchescorecouleur {} {
global listeval score frcolor serie seriemax Home
variable repert

	for {set x 0} {$x<60} {incr x} {
	set score($x) ""
	set frcolor($x) white
	set serie($x) 0
	set seriemax($x) 1
	}
	for {set x [llength $listeval]} {$x>-1} {incr x -1} {
		set it [lindex $listeval $x]
			if {[lindex [lindex $it end] 0] == "bilan"} {
			set bil [lindex $it end]

			if {$repert == [lindex $bil 3]} {
			set numexobil [lindex $bil 2]
			set catbil [lindex $it 1] 
#num exercice

			if {$score($numexobil) == ""} {

			set seriebil [lindex $bil 1]
			set scorebil [lindex $bil 4]
			

set file [lindex $it 0]
set f [open [file join $Home data $file] "r"]
gets $f listtmp
close $f
set seriemax($numexobil) [llength $listtmp]		



			set score($numexobil) "$scorebil %"
			set serie($numexobil) $seriebil
			if {$scorebil < 50} {set frcolor($numexobil) red}
			if {$scorebil >= 50 && $scorebil < 85} {set frcolor($numexobil) green}
			if {$scorebil  >= 85} {set frcolor($numexobil) yellow}
			}
			}
			}
	}
}



proc interface {} {

global Home basedir sysFont progaide plateforme listacti baseHome prof abuledu user listeval score frcolor serie iwish seriemax
variable repert
variable repertcat
variable demarre
variable police
variable langue

wm title . "[mc {Calcul reflechi}] - [mc {Utilisateur}] : [string map {.log \040} [file tail $user]]"

catch {destroy .menu}
catch {destroy .wleft}
catch {destroy .wcenter}
catch {destroy .wright}

menu .menu -tearoff 0

# Creation du menu Fichier
menu .menu.fichier -tearoff 0
.menu add cascade -label [mc {Fichier}] -menu .menu.fichier

if {($abuledu == 0) || ($repert==0) || ($prof==1)} {
.menu.fichier add cascade -label [mc {Editeur}] -menu .menu.fichier.editeur
} else {
.menu.fichier add cascade -label [mc {Editeur}] -state disabled -menu .menu.fichier.editeur
}

menu .menu.fichier.editeur -tearoff 0 
.menu.fichier.editeur add cascade -label [mc {Additions}] -menu .menu.fichier.editeur.additions
menu .menu.fichier.editeur.additions -tearoff 0 
.menu.fichier.editeur.additions add command -label "a + b" -command "exec $iwish editeur.tcl a+b.conf"
.menu.fichier.editeur.additions add command -label "aa + b" -command "exec $iwish editeur.tcl aa+b.conf"
.menu.fichier.editeur.additions add command -label "aa + bb" -command "exec $iwish editeur.tcl aa+bb.conf"
.menu.fichier.editeur.additions add command -label "aaa + b" -command "exec $iwish editeur.tcl aaa+b.conf"
.menu.fichier.editeur.additions add command -label "aaa + bb" -command "exec $iwish editeur.tcl aaa+bb.conf"
.menu.fichier.editeur.additions add command -label "aaa + bbb" -command "exec $iwish editeur.tcl aaa+bbb.conf"

.menu.fichier.editeur add cascade -label [mc {Soustractions}] -menu .menu.fichier.editeur.soustractions
menu .menu.fichier.editeur.soustractions -tearoff 0 
.menu.fichier.editeur.soustractions add command -label "a - b" -command "exec $iwish editeur.tcl a-b.conf"
.menu.fichier.editeur.soustractions add command -label "aa - b" -command "exec $iwish editeur.tcl aa-b.conf"
.menu.fichier.editeur.soustractions add command -label "aa - bb" -command "exec $iwish editeur.tcl aa-bb.conf"
.menu.fichier.editeur.soustractions add command -label "aaa - b" -command "exec $iwish editeur.tcl aaa-b.conf"
.menu.fichier.editeur.soustractions add command -label "aaa - bb" -command "exec $iwish editeur.tcl aaa-bb.conf"
.menu.fichier.editeur.soustractions add command -label "aaa - bbb" -command "exec $iwish editeur.tcl aaa-bbb.conf"

.menu.fichier.editeur add cascade -label [mc {Multiplications}] -menu .menu.fichier.editeur.multiplications
menu .menu.fichier.editeur.multiplications -tearoff 0 
.menu.fichier.editeur.multiplications add command -label "a x b" -command "exec $iwish editeur.tcl axb.conf"
.menu.fichier.editeur.multiplications add command -label "aa x b" -command "exec $iwish editeur.tcl aaxb.conf"
.menu.fichier.editeur.multiplications add command -label "aa x bb" -command "exec $iwish editeur.tcl aaxbb.conf"
.menu.fichier.editeur.multiplications add command -label "aaa x b" -command "exec $iwish editeur.tcl aaaxb.conf"
.menu.fichier.editeur.multiplications add command -label "aaa x bb" -command "exec $iwish editeur.tcl aaaxbb.conf"
.menu.fichier.editeur.multiplications add command -label "aaa x bbb" -command "exec $iwish editeur.tcl aaaxbbb.conf"

.menu.fichier.editeur add cascade -label [mc {Complements}] -menu .menu.fichier.editeur.complements
menu .menu.fichier.editeur.complements -tearoff 0 
.menu.fichier.editeur.complements add command -label "[mc {Complem. a}] 10" -command "exec $iwish editeur.tcl comp1.conf"
.menu.fichier.editeur.complements add command -label "[mc {Complem. a}] 20" -command "exec $iwish editeur.tcl comp2.conf"
.menu.fichier.editeur.complements add command -label "[mc {Complem. a}] 100" -command "exec $iwish editeur.tcl comp3.conf"
.menu.fichier.editeur.complements add command -label [mc {Complem. libre}] -command "exec $iwish editeur.tcl comp4.conf"


.menu.fichier.editeur add cascade -label [mc {Multiples}] -menu .menu.fichier.editeur.multiples
menu .menu.fichier.editeur.multiples -tearoff 0 
.menu.fichier.editeur.multiples add command -label "[mc {Multiples de}] 2" -command "exec $iwish editeur.tcl mult2.conf"
.menu.fichier.editeur.multiples add command -label "[mc {Multiples de}] 3" -command "exec $iwish editeur.tcl mult3.conf"
.menu.fichier.editeur.multiples add command -label "[mc {Multiples de}] 4" -command "exec $iwish editeur.tcl mult4.conf"
.menu.fichier.editeur.multiples add command -label "[mc {Multiples de}] 5" -command "exec $iwish editeur.tcl mult5.conf"
.menu.fichier.editeur.multiples add command -label "[mc {Multiples de}] 6" -command "exec $iwish editeur.tcl mult6.conf"
.menu.fichier.editeur.multiples add command -label "[mc {Multiples de}] 7" -command "exec $iwish editeur.tcl mult7.conf"
.menu.fichier.editeur.multiples add command -label "[mc {Multiples de}] 8" -command "exec $iwish editeur.tcl mult8.conf"
.menu.fichier.editeur.multiples add command -label "[mc {Multiples de}] 9" -command "exec $iwish editeur.tcl mult9.conf"

.menu.fichier.editeur add cascade -label [mc {Tables +}] -menu .menu.fichier.editeur.tables1
menu .menu.fichier.editeur.tables1 -tearoff 0 
.menu.fichier.editeur.tables1 add command -label "[mc {Table de}] 2" -command "exec $iwish editeur.tcl tab+2.conf"
.menu.fichier.editeur.tables1 add command -label "[mc {Table de}] 3" -command "exec $iwish editeur.tcl tab+3.conf"
.menu.fichier.editeur.tables1 add command -label "[mc {Table de}] 4" -command "exec $iwish editeur.tcl tab+4.conf"
.menu.fichier.editeur.tables1 add command -label "[mc {Table de}] 5" -command "exec $iwish editeur.tcl tab+5.conf"
.menu.fichier.editeur.tables1 add command -label "[mc {Table de}] 6" -command "exec $iwish editeur.tcl tab+6.conf"
.menu.fichier.editeur.tables1 add command -label "[mc {Table de}] 7" -command "exec $iwish editeur.tcl tab+7.conf"
.menu.fichier.editeur.tables1 add command -label "[mc {Table de}] 8" -command "exec $iwish editeur.tcl tab+8.conf"
.menu.fichier.editeur.tables1 add command -label "[mc {Table de}] 9" -command "exec $iwish editeur.tcl tab+9.conf"

.menu.fichier.editeur add cascade -label [mc {Tables x}] -menu .menu.fichier.editeur.tables2
menu .menu.fichier.editeur.tables2 -tearoff 0 
.menu.fichier.editeur.tables2 add command -label "[mc {Table de}] 2" -command "exec $iwish editeur.tcl tabx2.conf"
.menu.fichier.editeur.tables2 add command -label "[mc {Table de}] 3" -command "exec $iwish editeur.tcl tabx3.conf"
.menu.fichier.editeur.tables2 add command -label "[mc {Table de}] 4" -command "exec $iwish editeur.tcl tabx4.conf"
.menu.fichier.editeur.tables2 add command -label "[mc {Table de}] 5" -command "exec $iwish editeur.tcl tabx5.conf"
.menu.fichier.editeur.tables2 add command -label "[mc {Table de}] 6" -command "exec $iwish editeur.tcl tabx6.conf"
.menu.fichier.editeur.tables2 add command -label "[mc {Table de}] 7" -command "exec $iwish editeur.tcl tabx7.conf"
.menu.fichier.editeur.tables2 add command -label "[mc {Table de}] 8" -command "exec $iwish editeur.tcl tabx8.conf"
.menu.fichier.editeur.tables2 add command -label "[mc {Table de}] 9" -command "exec $iwish editeur.tcl tabx9.conf"

.menu.fichier add command -label [mc {Gestion}] -command "exec $iwish gestion.tcl"
.menu.fichier add command -label [mc {Imprimer le suivi}] -command "imprimesuivi"

.menu.fichier add sep
.menu.fichier add command -label [mc {Quitter  Ctrl-Q}] -command exit





.menu add cascade -label [mc {Activites}] -menu .menu.activite
menu .menu.activite -tearoff 0

.menu.activite add cascade -label [mc {Additions}] -menu .menu.activite.additions
menu .menu.activite.additions -tearoff 0 
.menu.activite.additions add command -label "a + b" -command "exec $iwish oeuf.tcl a+b.conf 0 0 &;exit"
.menu.activite.additions add command -label "aa + b" -command "exec $iwish oeuf.tcl aa+b.conf 0 1 &;exit"
.menu.activite.additions add command -label "aa + bb" -command "exec $iwish oeuf.tcl aa+bb.conf 0 2 &;exit"
.menu.activite.additions add command -label "aaa + b" -command "exec $iwish oeuf.tcl aaa+b.conf 0 3 &;exit"
.menu.activite.additions add command -label "aaa + bb" -command "exec $iwish oeuf.tcl aaa+bb.conf 0 4 &;exit"
.menu.activite.additions add command -label "aaa + bbb" -command "exec $iwish oeuf.tcl aaa+bbb.conf 0 5 &;exit"

.menu.activite add cascade -label [mc {Soustractions}] -menu .menu.activite.soustractions
menu .menu.activite.soustractions -tearoff 0 
.menu.activite.soustractions add command -label "a - b" -command "exec $iwish oeuf.tcl a-b.conf 0 6 &;exit"
.menu.activite.soustractions add command -label "aa - b" -command "exec $iwish oeuf.tcl aa-b.conf 0 7 &;exit"
.menu.activite.soustractions add command -label "aa - bb" -command "exec $iwish oeuf.tcl aa-bb.conf 0 8 &;exit"
.menu.activite.soustractions add command -label "aaa - b" -command "exec $iwish oeuf.tcl aaa-b.conf 0 9 &;exit"
.menu.activite.soustractions add command -label "aaa - bb" -command "exec $iwish oeuf.tcl aaa-bb.conf 0 10 &;exit"
.menu.activite.soustractions add command -label "aaa - bbb" -command "exec $iwish oeuf.tcl aaa-bbb.conf 0 11 &;exit"

.menu.activite add cascade -label [mc {Multiplications}] -menu .menu.activite.multiplications
menu .menu.activite.multiplications -tearoff 0 
.menu.activite.multiplications add command -label "a x b" -command "exec $iwish oeuf.tcl axb.conf 0 12 &;exit"
.menu.activite.multiplications add command -label "aa x b" -command "exec $iwish oeuf.tcl aaxb.conf 0 13 &;exit"
.menu.activite.multiplications add command -label "aa x bb" -command "exec $iwish oeuf.tcl aaxbb.conf 0 14 &;exit"
.menu.activite.multiplications add command -label "aaa x b" -command "exec $iwish oeuf.tcl aaaxb.conf 0 15 &;exit"
.menu.activite.multiplications add command -label "aaa x bb" -command "exec $iwish oeuf.tcl aaaxbb.conf 0 16 &;exit"
.menu.activite.multiplications add command -label "aaa x bbb" -command "exec $iwish oeuf.tcl aaaxbbb.conf 0 17 &;exit"

.menu.activite add cascade -label [mc {Complements}] -menu .menu.activite.complements
menu .menu.activite.complements -tearoff 0 
.menu.activite.complements add command -label "[mc {Complem. a}] 10" -command "exec $iwish oeuf.tcl comp1.conf 0 18 &;exit"
.menu.activite.complements add command -label "[mc {Complem. a}] 20" -command "exec $iwish oeuf.tcl comp2.conf 0 19 &;exit"
.menu.activite.complements add command -label "[mc {Complem. a}] 100" -command "exec $iwish oeuf.tcl comp3.conf 0 20 &;exit"
.menu.activite.complements add command -label [mc {Complem. libre}] -command "exec $iwish oeuf.tcl comp4.conf 0 21 &;exit"

.menu.activite add cascade -label [mc {Multiples}] -menu .menu.activite.multiples
menu .menu.activite.multiples -tearoff 0 
.menu.activite.multiples add command -label "[mc {Multiples de}] 2" -command "exec $iwish oeuf.tcl mult2.conf 0 22 &;exit"
.menu.activite.multiples add command -label "[mc {Multiples de}] 3" -command "exec $iwish oeuf.tcl mult3.conf 0 23 &;exit"
.menu.activite.multiples add command -label "[mc {Multiples de}] 4" -command "exec $iwish oeuf.tcl mult4.conf 0 24 &;exit"
.menu.activite.multiples add command -label "[mc {Multiples de}] 5" -command "exec $iwish oeuf.tcl mult5.conf 0 25 &;exit"
.menu.activite.multiples add command -label "[mc {Multiples de}] 6" -command "exec $iwish oeuf.tcl mult6.conf 0 26 &;exit"
.menu.activite.multiples add command -label "[mc {Multiples de}] 7" -command "exec $iwish oeuf.tcl mult7.conf 0 27 &;exit"
.menu.activite.multiples add command -label "[mc {Multiples de}] 8" -command "exec $iwish oeuf.tcl mult8.conf 0 28 &;exit"
.menu.activite.multiples add command -label "[mc {Multiples de}] 9" -command "exec $iwish oeuf.tcl mult9.conf 0 29 &;exit"

.menu.activite add cascade -label [mc {Tables +}] -menu .menu.activite.tables1
menu .menu.activite.tables1 -tearoff 0 
.menu.activite.tables1 add command -label "[mc {Table de}] 2" -command "exec $iwish oeuf.tcl tab+2.conf 0 20 &;exit"
.menu.activite.tables1 add command -label "[mc {Table de}] 3" -command "exec $iwish oeuf.tcl tab+3.conf 0 31 &;exit"
.menu.activite.tables1 add command -label "[mc {Table de}] 4" -command "exec $iwish oeuf.tcl tab+4.conf 0 32 &;exit"
.menu.activite.tables1 add command -label "[mc {Table de}] 5" -command "exec $iwish oeuf.tcl tab+5.conf 0 33 &;exit"
.menu.activite.tables1 add command -label "[mc {Table de}] 6" -command "exec $iwish oeuf.tcl tab+6.conf 0 34 &;exit"
.menu.activite.tables1 add command -label "[mc {Table de}] 7" -command "exec $iwish oeuf.tcl tab+7.conf 0 35 &;exit"
.menu.activite.tables1 add command -label "[mc {Table de}] 8" -command "exec $iwish oeuf.tcl tab+8.conf 0 36 &;exit"
.menu.activite.tables1 add command -label "[mc {Table de}] 9" -command "exec $iwish oeuf.tcl tab+9.conf 0 37 &;exit"

.menu.activite add cascade -label [mc {Tables x}] -menu .menu.activite.tables2
menu .menu.activite.tables2 -tearoff 0 
.menu.activite.tables2 add command -label "[mc {Table de}] 2" -command "exec $iwish oeuf.tcl tabx2.conf 0 38 &;exit"
.menu.activite.tables2 add command -label "[mc {Table de}] 3" -command "exec $iwish oeuf.tcl tabx3.conf 0 39 &;exit"
.menu.activite.tables2 add command -label "[mc {Table de}] 4" -command "exec $iwish oeuf.tcl tabx4.conf 0 40 &;exit"
.menu.activite.tables2 add command -label "[mc {Table de}] 5" -command "exec $iwish oeuf.tcl tabx5.conf 0 41 &;exit"
.menu.activite.tables2 add command -label "[mc {Table de}] 6" -command "exec $iwish oeuf.tcl tabx6.conf 0 42 &;exit"
.menu.activite.tables2 add command -label "[mc {Table de}] 7" -command "exec $iwish oeuf.tcl tabx7.conf 0 43 &;exit"
.menu.activite.tables2 add command -label "[mc {Table de}] 8" -command "exec $iwish oeuf.tcl tabx8.conf 0 44 &;exit"
.menu.activite.tables2 add command -label "[mc {Table de}] 9" -command "exec $iwish oeuf.tcl tabx9.conf 0 45 &;exit"

menu .menu.option -tearoff 0
.menu add cascade -label [mc {Options}] -menu .menu.option



set ext .msg
menu .menu.option.lang -tearoff 0 
.menu.option add cascade -label "[mc {Langue}]" -menu .menu.option.lang

foreach i [glob [file join  $basedir msgs *$ext]] {
set lang [string map {.msg ""} [file tail $i]]
.menu.option.lang add radio -label $lang -variable langue -command "setlang $lang"
}


###########################

menu .menu.option.repwork -tearoff 0 
.menu.option add cascade -label [mc {Repertoire de travail}] -menu .menu.option.repwork 
.menu.option.repwork add radio -label [mc {Individuel}] -variable repert -value 0 -command "changeinterface"
.menu.option.repwork add radio -label [mc {Commun}] -variable repert -value 1 -command "changeinterface"



.menu add command -label [mc {Utilisateur}] -command "setwindowsusername"

menu .menu.aide -tearoff 0
.menu add cascade -label [mc {Aide}] -menu .menu.aide
if {[file exists [file join $baseHome lang.conf]] == 1} {
	set f [open [file join $baseHome lang.conf] "r"]
  	gets $f langue
  	close $f
	}
	set fich "_index.htm"
	set fich $langue$fich
	set fichier [file join [pwd] aide $fich]
	  


if {[file exists $fichier] != 1} {
set fichier [file join [pwd] aide fr_index.htm]
}
  


.menu.aide add command -label [mc {Aide}] -command "exec $progaide file:$fichier &"
.menu.aide add command -label [mc {A propos ...}] -command "source apropos.tcl"

. configure -menu .menu
###############################################################"
set listeval ""
catch {
set f [open [file join $user] "r" ]
while {![eof $f]} {
lappend listeval [gets $f]                          
}
close $f
}
recherchescorecouleur
#######################################################################"
. configure -background blue
frame .wleft -background blue -height 300 -width 200
pack .wleft -side left
label .wleft.a1 -text [mc {Additions}] -background #ff9f80 -font $sysFont(s)
grid .wleft.a1 -row 0 -column 0 -sticky news
label .wleft.a2 -text [mc {Soustractions}] -background #ffff80 -font $sysFont(s)
grid .wleft.a2 -row 0 -column 1 -sticky news
label .wleft.a3 -text [mc {Multiplications}] -background #ff9f80 -font $sysFont(s)
grid .wleft.a3 -row 0 -column 2 -sticky news
label .wleft.a4 -text "[mc {Complement}] (+)" -background #ffff80 -font $sysFont(s)
grid .wleft.a4 -row 0 -column 3 -sticky news
label .wleft.a5 -text [mc {Multiples}] -background #ff9f80 -font $sysFont(s)
grid .wleft.a5 -row 0 -column 4 -sticky news
label .wleft.a6 -text [mc {Tables +}] -background #ffff80 -font $sysFont(s)
grid .wleft.a6 -row 0 -column 5 -sticky news
label .wleft.a7 -text [mc {Tables x}] -background #ff9f80 -font $sysFont(s)
grid .wleft.a7 -row 0 -column 6 -sticky news


label .wleft.b11 -foreground $frcolor(0) -font $sysFont(t) -bg grey -width 9
label .wleft.ba11 -foreground $frcolor(0) -font $sysFont(s) -bg blue
grid .wleft.b11 -row 1 -column 0 -padx 3
grid .wleft.ba11 -row 2 -column 0 -padx 3
set ltext ""
if {$score(0) != ""} {set ltext "([expr $serie(0) +1]/$seriemax(0))"}
.wleft.b11 configure -text "a + b" 
.wleft.ba11 configure -text $ltext 
bind .wleft.b11 <1> "exec $iwish oeuf.tcl a+b.conf $serie(0) 0 &;exit"
bind .wleft.b11 <Any-Enter> ".wleft.b11 configure -cursor hand1"
bind .wleft.b11 <Any-Leave> ".wleft.b11 configure -cursor left_ptr"


label .wleft.b12 -foreground $frcolor(1) -font $sysFont(t) -bg grey -width 9
label .wleft.ba12 -foreground $frcolor(1) -font $sysFont(s) -bg blue
grid .wleft.b12 -row 3 -column 0 -padx 3
grid .wleft.ba12 -row 4 -column 0 -padx 3
set ltext ""
if {$score(1) != ""} {set ltext "([expr $serie(1) +1]/$seriemax(1))"}
.wleft.b12 configure -text "aa + b" 
.wleft.ba12 configure -text $ltext
bind .wleft.b12 <1> "exec $iwish oeuf.tcl aa+b.conf $serie(1) 1 &;exit"
bind .wleft.b12 <Any-Enter> ".wleft.b12 configure -cursor hand1"
bind .wleft.b12 <Any-Leave> ".wleft.b12 configure -cursor left_ptr"

label .wleft.b13 -foreground $frcolor(2) -font $sysFont(t) -bg grey -width 9
label .wleft.ba13 -foreground $frcolor(2) -font $sysFont(s) -bg blue
grid .wleft.ba13 -row 6 -column 0 -padx 3
grid .wleft.b13 -row 5 -column 0 -padx 3
set ltext ""
if {$score(2) != ""} {set ltext "([expr $serie(2) +1]/$seriemax(2))"}
.wleft.b13 configure -text "aa + bb"
.wleft.ba13 configure -text $ltext 
bind .wleft.b13 <1> "exec $iwish oeuf.tcl aa+bb.conf $serie(2) 2 &;exit"
bind .wleft.b13 <Any-Enter> ".wleft.b13 configure -cursor hand1"
bind .wleft.b13 <Any-Leave> ".wleft.b13 configure -cursor left_ptr"


label .wleft.b14 -foreground $frcolor(3) -font $sysFont(t) -bg grey -width 9
label .wleft.ba14 -foreground $frcolor(3) -font $sysFont(s) -bg blue
grid .wleft.b14 -row 7 -column 0 -padx 3
grid .wleft.ba14 -row 8 -column 0 -padx 3
set ltext ""
if {$score(3) != ""} {set ltext "([expr $serie(3) +1]/$seriemax(3))"}
.wleft.b14 configure -text "aaa + b"
.wleft.ba14 configure -text $ltext 
bind .wleft.b14 <1> "exec $iwish oeuf.tcl aaa+b.conf $serie(3) 3 &;exit"
bind .wleft.b14 <Any-Enter> ".wleft.b14 configure -cursor hand1"
bind .wleft.b14 <Any-Leave> ".wleft.b14 configure -cursor left_ptr"

label .wleft.b15 -foreground $frcolor(4) -font $sysFont(t) -bg grey -width 9
label .wleft.ba15 -foreground $frcolor(4) -font $sysFont(s) -bg blue
grid .wleft.b15 -row 9 -column 0 -padx 3
grid .wleft.ba15 -row 10 -column 0 -padx 3
set ltext ""
if {$score(4) != ""} {set ltext "([expr $serie(4) +1]/$seriemax(4))"}
.wleft.b15 configure -text "aaa + bb"
.wleft.ba15 configure -text $ltext 
bind .wleft.b15 <1> "exec $iwish oeuf.tcl aaa+bb.conf $serie(4) 4 &;exit"
bind .wleft.b15 <Any-Enter> ".wleft.b15 configure -cursor hand1"
bind .wleft.b15 <Any-Leave> ".wleft.b15 configure -cursor left_ptr"

label .wleft.b16 -foreground $frcolor(5) -font $sysFont(t) -bg grey -width 9
label .wleft.ba16 -foreground $frcolor(5) -font $sysFont(s) -bg blue
grid .wleft.b16 -row 11 -column 0 -padx 3
grid .wleft.ba16 -row 12 -column 0 -padx 3
set ltext ""
if {$score(5) != ""} {set ltext "([expr $serie(5) +1]/$seriemax(5))"}
.wleft.b16 configure -text "aaa + bbb"
.wleft.ba16 configure -text $ltext 
bind .wleft.b16 <1> "exec $iwish oeuf.tcl aaa+bbb.conf $serie(5) 5 &;exit"
bind .wleft.b16 <Any-Enter> ".wleft.b16 configure -cursor hand1"
bind .wleft.b16 <Any-Leave> ".wleft.b16 configure -cursor left_ptr"

label .wleft.b21 -foreground $frcolor(6) -font $sysFont(t) -bg grey -width 9
label .wleft.ba21 -foreground $frcolor(6) -font $sysFont(s) -bg blue
grid .wleft.b21 -row 1 -column 1 -padx 3
grid .wleft.ba21 -row 2 -column 1 -padx 3
set ltext ""
if {$score(6) != ""} {set ltext "([expr $serie(6) +1]/$seriemax(6))"}
.wleft.b21 configure -text "a - b"
.wleft.ba21 configure -text $ltext 
bind .wleft.b21 <1> "exec $iwish oeuf.tcl a-b.conf $serie(6) 6 &;exit"
bind .wleft.b21 <Any-Enter> ".wleft.b21 configure -cursor hand1"
bind .wleft.b21 <Any-Leave> ".wleft.b21 configure -cursor left_ptr"


label .wleft.b22 -foreground $frcolor(7) -font $sysFont(t) -bg grey -width 9
label .wleft.ba22 -foreground $frcolor(7) -font $sysFont(s) -bg blue
grid .wleft.b22 -row 3 -column 1 -padx 3
grid .wleft.ba22 -row 4 -column 1 -padx 3
set ltext ""
if {$score(7) != ""} {set ltext "([expr $serie(7) +1]/$seriemax(7))"}
.wleft.b22 configure -text "aa - b"
.wleft.ba22 configure -text $ltext 
bind .wleft.b22 <1> "exec $iwish oeuf.tcl aa-b.conf $serie(7) 7 &;exit"
bind .wleft.b22 <Any-Enter> ".wleft.b22 configure -cursor hand1"
bind .wleft.b22 <Any-Leave> ".wleft.b22 configure -cursor left_ptr"

label .wleft.b23 -foreground $frcolor(8) -font $sysFont(t) -bg grey -width 9
label .wleft.ba23 -foreground $frcolor(8) -font $sysFont(s) -bg blue
grid .wleft.b23 -row 5 -column 1 -padx 3
grid .wleft.ba23 -row 6 -column 1 -padx 3
set ltext ""
if {$score(8) != ""} {set ltext "([expr $serie(8) +1]/$seriemax(8))"}
.wleft.b23 configure -text "aa - bb"
.wleft.ba23 configure -text $ltext 
bind .wleft.b23 <1> "exec $iwish oeuf.tcl aa-bb.conf $serie(8) 8 &;exit"
bind .wleft.b23 <Any-Enter> ".wleft.b23 configure -cursor hand1"
bind .wleft.b23 <Any-Leave> ".wleft.b23 configure -cursor left_ptr"

label .wleft.b24 -foreground $frcolor(9) -font $sysFont(t) -bg grey -width 9
label .wleft.ba24 -foreground $frcolor(9) -font $sysFont(s) -bg blue
grid .wleft.b24 -row 7 -column 1 -padx 3
grid .wleft.ba24 -row 8 -column 1 -padx 3
set ltext ""
if {$score(9) != ""} {set ltext "([expr $serie(9) +1]/$seriemax(9))"}
.wleft.b24 configure -text "aaa - b"
.wleft.ba24 configure -text $ltext 
bind .wleft.b24 <1> "exec $iwish oeuf.tcl aaa-b.conf $serie(9) 9 &;exit"
bind .wleft.b24 <Any-Enter> ".wleft.b24 configure -cursor hand1"
bind .wleft.b24 <Any-Leave> ".wleft.b24 configure -cursor left_ptr"

label .wleft.b25 -foreground $frcolor(10) -font $sysFont(t) -bg grey -width 9
label .wleft.ba25 -foreground $frcolor(10) -font $sysFont(s) -bg blue
grid .wleft.b25 -row 9 -column 1 -padx 3
grid .wleft.ba25 -row 10 -column 1 -padx 3
set ltext ""
if {$score(10) != ""} {set ltext "([expr $serie(10) +1]/$seriemax(10))"}
.wleft.b25 configure -text "aaa - bb"
.wleft.ba25 configure -text $ltext 
bind .wleft.b25 <1> "exec $iwish oeuf.tcl aaa-bb.conf $serie(10) 10 &;exit"
bind .wleft.b25 <Any-Enter> ".wleft.b25 configure -cursor hand1"
bind .wleft.b25 <Any-Leave> ".wleft.b25 configure -cursor left_ptr"

label .wleft.b26 -foreground $frcolor(11) -font $sysFont(t) -bg grey -width 9
label .wleft.ba26 -foreground $frcolor(11) -font $sysFont(s) -bg blue
grid .wleft.b26 -row 11 -column 1 -padx 3
grid .wleft.ba26 -row 12 -column 1 -padx 3
set ltext ""
if {$score(11) != ""} {set ltext "([expr $serie(11) +1]/$seriemax(11))"}
.wleft.b26 configure -text "aaa - bbb"
.wleft.ba26 configure -text $ltext 
bind .wleft.b26 <1> "exec $iwish oeuf.tcl aaa-bbb.conf $serie(11) 11 &;exit"
bind .wleft.b26 <Any-Enter> ".wleft.b26 configure -cursor hand1"
bind .wleft.b26 <Any-Leave> ".wleft.b26 configure -cursor left_ptr"

label .wleft.b31 -foreground $frcolor(12) -font $sysFont(t) -bg grey -width 9
label .wleft.ba31 -foreground $frcolor(12) -font $sysFont(s) -bg blue
grid .wleft.b31 -row 1 -column 2 -padx 3
grid .wleft.ba31 -row 2 -column 2 -padx 3
set ltext ""
if {$score(12) != ""} {set ltext "([expr $serie(12) +1]/$seriemax(12))"}
.wleft.b31 configure -text "a x b"
.wleft.ba31 configure -text $ltext 
bind .wleft.b31 <1> "exec $iwish oeuf.tcl axb.conf $serie(12) 12 &;exit"
bind .wleft.b31 <Any-Enter> ".wleft.b31 configure -cursor hand1"
bind .wleft.b31 <Any-Leave> ".wleft.b31 configure -cursor left_ptr"

label .wleft.b32 -foreground $frcolor(13) -font $sysFont(t) -bg grey -width 9
label .wleft.ba32 -foreground $frcolor(13) -font $sysFont(s) -bg blue
grid .wleft.b32 -row 3 -column 2 -padx 3
grid .wleft.ba32 -row 4 -column 2 -padx 3
set ltext ""
if {$score(13) != ""} {set ltext "([expr $serie(13) +1]/$seriemax(13))"}
.wleft.b32 configure -text "aa x b"
.wleft.ba32 configure -text $ltext 
bind .wleft.b32 <1> "exec $iwish oeuf.tcl aaxb.conf $serie(13) 13 &;exit"
bind .wleft.b32 <Any-Enter> ".wleft.b32 configure -cursor hand1"
bind .wleft.b32 <Any-Leave> ".wleft.b32 configure -cursor left_ptr"

label .wleft.b33 -foreground $frcolor(14) -font $sysFont(t) -bg grey -width 9
label .wleft.ba33 -foreground $frcolor(14) -font $sysFont(s) -bg blue
grid .wleft.b33 -row 5 -column 2 -padx 3
grid .wleft.ba33 -row 6 -column 2 -padx 3
set ltext ""
if {$score(14) != ""} {set ltext "([expr $serie(14) +1]/$seriemax(14))"}
.wleft.b33 configure -text "aa x bb"
.wleft.ba33 configure -text $ltext 
bind .wleft.b33 <1> "exec $iwish oeuf.tcl aaxbb.conf $serie(14) 14 &;exit"
bind .wleft.b33 <Any-Enter> ".wleft.b33 configure -cursor hand1"
bind .wleft.b33 <Any-Leave> ".wleft.b33 configure -cursor left_ptr"

label .wleft.b34 -foreground $frcolor(15) -font $sysFont(t) -bg grey -width 9
label .wleft.ba34 -foreground $frcolor(15) -font $sysFont(s) -bg blue
grid .wleft.b34 -row 7 -column 2 -padx 3
grid .wleft.ba34 -row 8 -column 2 -padx 3
set ltext ""
if {$score(15) != ""} {set ltext "([expr $serie(15) +1]/$seriemax(15))"}
.wleft.b34 configure -text "aaa x b"
.wleft.ba34 configure -text $ltext 
bind .wleft.b34 <1> "exec $iwish oeuf.tcl aaaxb.conf $serie(15) 15 &;exit"
bind .wleft.b34 <Any-Enter> ".wleft.b34 configure -cursor hand1"
bind .wleft.b34 <Any-Leave> ".wleft.b34 configure -cursor left_ptr"

label .wleft.b35 -foreground $frcolor(16) -font $sysFont(t) -bg grey -width 9
label .wleft.ba35 -foreground $frcolor(16) -font $sysFont(s) -bg blue
grid .wleft.b35 -row 9 -column 2 -padx 3
grid .wleft.ba35 -row 10 -column 2 -padx 3
set ltext ""
if {$score(16) != ""} {set ltext "([expr $serie(16) +1]/$seriemax(16))"}
.wleft.b35 configure -text "aaa x bb"
.wleft.ba35 configure -text $ltext 
bind .wleft.b35 <1> "exec $iwish oeuf.tcl aaaxbb.conf $serie(16) 16 &;exit"
bind .wleft.b35 <Any-Enter> ".wleft.b35 configure -cursor hand1"
bind .wleft.b35 <Any-Leave> ".wleft.b35 configure -cursor left_ptr"

label .wleft.b36 -foreground $frcolor(17) -font $sysFont(t) -bg grey -width 9
label .wleft.ba36 -foreground $frcolor(17) -font $sysFont(s) -bg blue
grid .wleft.b36 -row 11 -column 2 -padx 3
grid .wleft.ba36 -row 12 -column 2 -padx 3
set ltext ""
if {$score(17) != ""} {set ltext "([expr $serie(17) +1]/$seriemax(17))"}
.wleft.b36 configure -text "aaa x bbb"
.wleft.ba36 configure -text $ltext 
bind .wleft.b36 <1> "exec $iwish oeuf.tcl aaaxbbb.conf $serie(17) 17 &;exit"
bind .wleft.b36 <Any-Enter> ".wleft.b36 configure -cursor hand1"
bind .wleft.b36 <Any-Leave> ".wleft.b36 configure -cursor left_ptr"


label .wleft.b41 -foreground $frcolor(18) -font $sysFont(t) -bg grey -width 12
label .wleft.ba41 -foreground $frcolor(18) -font $sysFont(s) -bg blue
grid .wleft.b41 -row 1 -column 3 -padx 3
grid .wleft.ba41 -row 2 -column 3 -padx 3
set ltext ""
if {$score(18) != ""} {set ltext "([expr $serie(18) +1]/$seriemax(18))"}
.wleft.b41 configure -text "[mc {Compl. a}] 10"
.wleft.ba41 configure -text $ltext 
bind .wleft.b41 <1> "exec $iwish oeuf.tcl comp1.conf $serie(18) 18 &;exit"
bind .wleft.b41 <Any-Enter> ".wleft.b41 configure -cursor hand1"
bind .wleft.b41 <Any-Leave> ".wleft.b41 configure -cursor left_ptr"

label .wleft.b42 -foreground $frcolor(19) -font $sysFont(t) -bg grey -width 12
label .wleft.ba42 -foreground $frcolor(19) -font $sysFont(s) -bg blue
grid .wleft.b42 -row 3 -column 3 -padx 3
grid .wleft.ba42 -row 4 -column 3 -padx 3
set ltext ""
if {$score(19) != ""} {set ltext "([expr $serie(19) +1]/$seriemax(19))"}
.wleft.b42 configure -text "[mc {Compl. a}] 20"
.wleft.ba42 configure -text $ltext 
bind .wleft.b42 <1> "exec $iwish oeuf.tcl comp2.conf $serie(19) 19 &;exit"
bind .wleft.b42 <Any-Enter> ".wleft.b42 configure -cursor hand1"
bind .wleft.b42 <Any-Leave> ".wleft.b42 configure -cursor left_ptr"

label .wleft.b43 -foreground $frcolor(20) -font $sysFont(t) -bg grey -width 12
label .wleft.ba43 -foreground $frcolor(20) -font $sysFont(s) -bg blue
grid .wleft.b43 -row 5 -column 3 -padx 3
grid .wleft.ba43 -row 6 -column 3 -padx 3
set ltext ""
if {$score(20) != ""} {set ltext "([expr $serie(20) +1]/$seriemax(20))"}
.wleft.b43 configure -text "[mc {Compl. a}] 100"
.wleft.ba43 configure -text $ltext 
bind .wleft.b43 <1> "exec $iwish oeuf.tcl comp3.conf $serie(10) 20 &;exit"
bind .wleft.b43 <Any-Enter> ".wleft.b43 configure -cursor hand1"
bind .wleft.b43 <Any-Leave> ".wleft.b43 configure -cursor left_ptr"

label .wleft.b44 -foreground $frcolor(21) -font $sysFont(t) -bg grey -width 12
label .wleft.ba44 -foreground $frcolor(21) -font $sysFont(s) -bg blue
grid .wleft.b44 -row 7 -column 3 -padx 3
grid .wleft.ba44 -row 8 -column 3 -padx 3
set ltext ""
if {$score(21) != ""} {set ltext "([expr $serie(21) +1]/$seriemax(21))"}
.wleft.b44 configure -text [mc {Compl. libre}]
.wleft.ba44 configure -text $ltext 
bind .wleft.b44 <1> "exec $iwish oeuf.tcl comp4.conf $serie(21) 21 &;exit"
bind .wleft.b44 <Any-Enter> ".wleft.b44 configure -cursor hand1"
bind .wleft.b44 <Any-Leave> ".wleft.b44 configure -cursor left_ptr"

label .wleft.b51 -foreground $frcolor(22) -font $sysFont(t) -bg grey -width 9
label .wleft.ba51 -foreground $frcolor(22) -font $sysFont(s) -bg blue
grid .wleft.b51 -row 1 -column 4 -padx 3
grid .wleft.ba51 -row 2 -column 4 -padx 3
set ltext ""
if {$score(22) != ""} {set ltext "([expr $serie(22) +1]/$seriemax(22))"}
.wleft.b51 configure -text "[mc {Mult. de}] 2"
.wleft.ba51 configure -text $ltext 
bind .wleft.b51 <1> "exec $iwish oeuf.tcl mult2.conf $serie(22) 22 &;exit"
bind .wleft.b51 <Any-Enter> ".wleft.b51 configure -cursor hand1"
bind .wleft.b51 <Any-Leave> ".wleft.b51 configure -cursor left_ptr"

label .wleft.b52 -foreground $frcolor(23) -font $sysFont(t) -bg grey -width 9
label .wleft.ba52 -foreground $frcolor(23) -font $sysFont(s) -bg blue
grid .wleft.b52 -row 3 -column 4 -padx 3
grid .wleft.ba52 -row 4 -column 4 -padx 3
set ltext ""
if {$score(23) != ""} {set ltext "([expr $serie(23) +1]/$seriemax(23))"}
.wleft.b52 configure -text "[mc {Mult. de}] 3"
.wleft.ba52 configure -text $ltext 
bind .wleft.b52 <1> "exec $iwish oeuf.tcl mult3.conf $serie(23) 23 &;exit"
bind .wleft.b52 <Any-Enter> ".wleft.b52 configure -cursor hand1"
bind .wleft.b52 <Any-Leave> ".wleft.b52 configure -cursor left_ptr"

label .wleft.b53 -foreground $frcolor(24) -font $sysFont(t) -bg grey -width 9
label .wleft.ba53 -foreground $frcolor(24) -font $sysFont(s) -bg blue
grid .wleft.b53 -row 5 -column 4 -padx 3
grid .wleft.ba53 -row 6 -column 4 -padx 3
set ltext ""
if {$score(24) != ""} {set ltext "([expr $serie(24) +1]/$seriemax(24))"}
.wleft.b53 configure -text "[mc {Mult. de}] 4"
.wleft.ba53 configure -text $ltext 
bind .wleft.b53 <1> "exec $iwish oeuf.tcl mult4.conf $serie(24) 24 &;exit"
bind .wleft.b53 <Any-Enter> ".wleft.b53 configure -cursor hand1"
bind .wleft.b53 <Any-Leave> ".wleft.b53 configure -cursor left_ptr"

label .wleft.b54 -foreground $frcolor(25) -font $sysFont(t) -bg grey -width 9
label .wleft.ba54 -foreground $frcolor(25) -font $sysFont(s) -bg blue
grid .wleft.b54 -row 7 -column 4 -padx 3
grid .wleft.ba54 -row 8 -column 4 -padx 3
set ltext ""
if {$score(25) != ""} {set ltext "([expr $serie(25) +1]/$seriemax(25))"}
.wleft.b54 configure -text "[mc {Mult. de}] 5"
.wleft.ba54 configure -text $ltext 
bind .wleft.b54 <1> "exec $iwish oeuf.tcl mult5.conf $serie(25) 25 &;exit"
bind .wleft.b54 <Any-Enter> ".wleft.b54 configure -cursor hand1"
bind .wleft.b54 <Any-Leave> ".wleft.b54 configure -cursor left_ptr"

label .wleft.b55 -foreground $frcolor(26) -font $sysFont(t) -bg grey -width 9
label .wleft.ba55 -foreground $frcolor(26) -font $sysFont(s) -bg blue
grid .wleft.b55 -row 9 -column 4 -padx 3
grid .wleft.ba55 -row 10 -column 4 -padx 3
set ltext ""
if {$score(26) != ""} {set ltext "([expr $serie(26) +1]/$seriemax(26))"}
.wleft.b55 configure -text "[mc {Mult. de}] 6"
.wleft.ba55 configure -text $ltext 
bind .wleft.b55 <1> "exec $iwish oeuf.tcl mult6.conf $serie(26) 26 &;exit"
bind .wleft.b55 <Any-Enter> ".wleft.b55 configure -cursor hand1"
bind .wleft.b55 <Any-Leave> ".wleft.b55 configure -cursor left_ptr"

label .wleft.b56 -foreground $frcolor(27) -font $sysFont(t) -bg grey -width 9
label .wleft.ba56 -foreground $frcolor(27) -font $sysFont(s) -bg blue
grid .wleft.b56 -row 11 -column 4 -padx 3
grid .wleft.ba56 -row 12 -column 4 -padx 3
set ltext ""
if {$score(27) != ""} {set ltext "([expr $serie(27) +1]/$seriemax(27))"}
.wleft.b56 configure -text "[mc {Mult. de}] 7"
.wleft.ba56 configure -text $ltext 
bind .wleft.b56 <1> "exec $iwish oeuf.tcl mult7.conf $serie(27) 27 &;exit"
bind .wleft.b56 <Any-Enter> ".wleft.b56 configure -cursor hand1"
bind .wleft.b56 <Any-Leave> ".wleft.b56 configure -cursor left_ptr"

label .wleft.b57 -foreground $frcolor(28) -font $sysFont(t) -bg grey -width 9
label .wleft.ba57 -foreground $frcolor(28) -font $sysFont(s) -bg blue
grid .wleft.b57 -row 13 -column 4 -padx 3
grid .wleft.ba57 -row 14 -column 4 -padx 3
set ltext ""
if {$score(28) != ""} {set ltext "([expr $serie(28) +1]/$seriemax(28))"}
.wleft.b57 configure -text "[mc {Mult. de}] 8"
.wleft.ba57 configure -text $ltext 
bind .wleft.b57 <1> "exec $iwish oeuf.tcl mult8.conf $serie(28) 28 &;exit"
bind .wleft.b57 <Any-Enter> ".wleft.b57 configure -cursor hand1"
bind .wleft.b57 <Any-Leave> ".wleft.b57 configure -cursor left_ptr"

label .wleft.b58 -foreground $frcolor(29) -font $sysFont(t) -bg grey -width 9
label .wleft.ba58 -foreground $frcolor(29) -font $sysFont(s) -bg blue
grid .wleft.b58 -row 15 -column 4 -padx 3
grid .wleft.ba58 -row 16 -column 4 -padx 3
set ltext ""
if {$score(29) != ""} {set ltext "([expr $serie(29) +1]/$seriemax(29))"}
.wleft.b58 configure -text "[mc {Mult. de}] 9"
.wleft.ba58 configure -text $ltext 
bind .wleft.b58 <1> "exec $iwish oeuf.tcl mult9.conf $serie(29) 29 &;exit"
bind .wleft.b58 <Any-Enter> ".wleft.b58 configure -cursor hand1"
bind .wleft.b58 <Any-Leave> ".wleft.b58 configure -cursor left_ptr"

label .wleft.b59 -foreground $frcolor(30) -font $sysFont(t) -bg grey -width 11
label .wleft.ba59 -foreground $frcolor(30) -font $sysFont(s) -bg blue
grid .wleft.b59 -row 1 -column 5 -padx 3
grid .wleft.ba59 -row 2 -column 5 -padx 3
set ltext ""
if {$score(30) != ""} {set ltext "([expr $serie(30) +1]/$seriemax(30))"}
.wleft.b59 configure -text "[mc {Table de}] 2"
.wleft.ba59 configure -text $ltext 
bind .wleft.b59 <1> "exec $iwish oeuf.tcl tab+2.conf $serie(30) 30 &;exit"
bind .wleft.b59 <Any-Enter> ".wleft.b59 configure -cursor hand1"
bind .wleft.b59 <Any-Leave> ".wleft.b59 configure -cursor left_ptr"

label .wleft.b60 -foreground $frcolor(31) -font $sysFont(t) -bg grey -width 11
label .wleft.ba60 -foreground $frcolor(31) -font $sysFont(s) -bg blue
grid .wleft.b60 -row 3 -column 5 -padx 3
grid .wleft.ba60 -row 4 -column 5 -padx 3
set ltext ""
if {$score(31) != ""} {set ltext "([expr $serie(31) +1]/$seriemax(31))"}
.wleft.b60 configure -text "[mc {Table de}] 3"
.wleft.ba60 configure -text $ltext 
bind .wleft.b60 <1> "exec $iwish oeuf.tcl tab+3.conf $serie(31) 31 &;exit"
bind .wleft.b60 <Any-Enter> ".wleft.b60 configure -cursor hand1"
bind .wleft.b60 <Any-Leave> ".wleft.b60 configure -cursor left_ptr"

label .wleft.b61 -foreground $frcolor(32) -font $sysFont(t) -bg grey -width 11
label .wleft.ba61 -foreground $frcolor(32) -font $sysFont(s) -bg blue
grid .wleft.b61 -row 5 -column 5 -padx 3
grid .wleft.ba61 -row 6 -column 5 -padx 3
set ltext ""
if {$score(32) != ""} {set ltext "([expr $serie(32) +1]/$seriemax(32))"}
.wleft.b61 configure -text "[mc {Table de}] 4"
.wleft.ba61 configure -text $ltext 
bind .wleft.b61 <1> "exec $iwish oeuf.tcl tab+4.conf $serie(32) 32 &;exit"
bind .wleft.b61 <Any-Enter> ".wleft.b61 configure -cursor hand1"
bind .wleft.b61 <Any-Leave> ".wleft.b61 configure -cursor left_ptr"

label .wleft.b62 -foreground $frcolor(33) -font $sysFont(t) -bg grey -width 11
label .wleft.ba62 -foreground $frcolor(33) -font $sysFont(s) -bg blue
grid .wleft.b62 -row 7 -column 5 -padx 3
grid .wleft.ba62 -row 8 -column 5 -padx 3
set ltext ""
if {$score(33) != ""} {set ltext "([expr $serie(33) +1]/$seriemax(33))"}
.wleft.b62 configure -text "[mc {Table de}] 5"
.wleft.ba62 configure -text $ltext 
bind .wleft.b62 <1> "exec $iwish oeuf.tcl tab+5.conf $serie(33) 33 &;exit"
bind .wleft.b62 <Any-Enter> ".wleft.b62 configure -cursor hand1"
bind .wleft.b62 <Any-Leave> ".wleft.b62 configure -cursor left_ptr"

label .wleft.b63 -foreground $frcolor(34) -font $sysFont(t) -bg grey -width 11
label .wleft.ba63 -foreground $frcolor(34) -font $sysFont(s) -bg blue
grid .wleft.b63 -row 9 -column 5 -padx 3
grid .wleft.ba63 -row 10 -column 5 -padx 3
set ltext ""
if {$score(34) != ""} {set ltext "([expr $serie(34) +1]/$seriemax(34))"}
.wleft.b63 configure -text "[mc {Table de}] 6"
.wleft.ba63 configure -text $ltext 
bind .wleft.b63 <1> "exec $iwish oeuf.tcl tab+6.conf $serie(34) 34 &;exit"
bind .wleft.b63 <Any-Enter> ".wleft.b63 configure -cursor hand1"
bind .wleft.b63 <Any-Leave> ".wleft.b63 configure -cursor left_ptr"

label .wleft.b64 -foreground $frcolor(35) -font $sysFont(t) -bg grey -width 11
label .wleft.ba64 -foreground $frcolor(35) -font $sysFont(s) -bg blue
grid .wleft.b64 -row 11 -column 5 -padx 3
grid .wleft.ba64 -row 12 -column 5 -padx 3
set ltext ""
if {$score(35) != ""} {set ltext "([expr $serie(35) +1]/$seriemax(35))"}
.wleft.b64 configure -text "[mc {Table de}] 7"
.wleft.ba64 configure -text $ltext 
bind .wleft.b64 <1> "exec $iwish oeuf.tcl tab+7.conf $serie(35) 35 &;exit"
bind .wleft.b64 <Any-Enter> ".wleft.b64 configure -cursor hand1"
bind .wleft.b64 <Any-Leave> ".wleft.b64 configure -cursor left_ptr"

label .wleft.b65 -foreground $frcolor(36) -font $sysFont(t) -bg grey -width 11
label .wleft.ba65 -foreground $frcolor(36) -font $sysFont(s) -bg blue
grid .wleft.b65 -row 13 -column 5 -padx 3
grid .wleft.ba65 -row 14 -column 5 -padx 3
set ltext ""
if {$score(36) != ""} {set ltext "([expr $serie(36) +1]/$seriemax(36))"}
.wleft.b65 configure -text "[mc {Table de}] 8"
.wleft.ba65 configure -text $ltext 
bind .wleft.b65 <1> "exec $iwish oeuf.tcl tab+8.conf $serie(36) 36 &;exit"
bind .wleft.b65 <Any-Enter> ".wleft.b65 configure -cursor hand1"
bind .wleft.b65 <Any-Leave> ".wleft.b65 configure -cursor left_ptr"

label .wleft.b66 -foreground $frcolor(37) -font $sysFont(t) -bg grey -width 11
label .wleft.ba66 -foreground $frcolor(37) -font $sysFont(s) -bg blue
grid .wleft.b66 -row 15 -column 5 -padx 3
grid .wleft.ba66 -row 16 -column 5 -padx 3
set ltext ""
if {$score(37) != ""} {set ltext "([expr $serie(37) +1]/$seriemax(37))"}
.wleft.b66 configure -text "[mc {Table de}] 9"
.wleft.ba66 configure -text $ltext 
bind .wleft.b66 <1> "exec $iwish oeuf.tcl tab+9.conf $serie(37) 37 &;exit"
bind .wleft.b66 <Any-Enter> ".wleft.b66 configure -cursor hand1"
bind .wleft.b66 <Any-Leave> ".wleft.b66 configure -cursor left_ptr"

label .wleft.b67 -foreground $frcolor(38) -font $sysFont(t) -bg grey -width 11
label .wleft.ba67 -foreground $frcolor(38) -font $sysFont(s) -bg blue
grid .wleft.b67 -row 1 -column 6 -padx 3
grid .wleft.ba67 -row 2 -column 6 -padx 3
set ltext ""
if {$score(38) != ""} {set ltext "([expr $serie(38) +1]/$seriemax(38))"}
.wleft.b67 configure -text "[mc {Table de}] 2"
.wleft.ba67 configure -text $ltext 
bind .wleft.b67 <1> "exec $iwish oeuf.tcl tabx2.conf $serie(38) 38 &;exit"
bind .wleft.b67 <Any-Enter> ".wleft.b67 configure -cursor hand1"
bind .wleft.b67 <Any-Leave> ".wleft.b67 configure -cursor left_ptr"

label .wleft.b68 -foreground $frcolor(39) -font $sysFont(t) -bg grey -width 11
label .wleft.ba68 -foreground $frcolor(39) -font $sysFont(s) -bg blue
grid .wleft.b68 -row 3 -column 6 -padx 3
grid .wleft.ba68 -row 4 -column 6 -padx 3
set ltext ""
if {$score(39) != ""} {set ltext "([expr $serie(39) +1]/$seriemax(39))"}
.wleft.b68 configure -text "[mc {Table de}] 3"
.wleft.ba68 configure -text $ltext 
bind .wleft.b68 <1> "exec $iwish oeuf.tcl tabx3.conf $serie(39) 39 &;exit"
bind .wleft.b68 <Any-Enter> ".wleft.b68 configure -cursor hand1"
bind .wleft.b68 <Any-Leave> ".wleft.b68 configure -cursor left_ptr"

label .wleft.b69 -foreground $frcolor(40) -font $sysFont(t) -bg grey -width 11
label .wleft.ba69 -foreground $frcolor(40) -font $sysFont(s) -bg blue
grid .wleft.b69 -row 5 -column 6 -padx 3
grid .wleft.ba69 -row 6 -column 6 -padx 3
set ltext ""
if {$score(40) != ""} {set ltext "([expr $serie(40) +1]/$seriemax(40))"}
.wleft.b69 configure -text "[mc {Table de}] 4"
.wleft.ba69 configure -text $ltext 
bind .wleft.b69 <1> "exec $iwish oeuf.tcl tabx4.conf $serie(40) 40 &;exit"
bind .wleft.b69 <Any-Enter> ".wleft.b69 configure -cursor hand1"
bind .wleft.b69 <Any-Leave> ".wleft.b69 configure -cursor left_ptr"

label .wleft.b70 -foreground $frcolor(41) -font $sysFont(t) -bg grey -width 11
label .wleft.ba70 -foreground $frcolor(41) -font $sysFont(s) -bg blue
grid .wleft.b70 -row 7 -column 6 -padx 3
grid .wleft.ba70 -row 8 -column 6 -padx 3
set ltext ""
if {$score(41) != ""} {set ltext "([expr $serie(41) +1]/$seriemax(41))"}
.wleft.b70 configure -text "[mc {Table de}] 5"
.wleft.ba70 configure -text $ltext 
bind .wleft.b70 <1> "exec $iwish oeuf.tcl tabx5.conf $serie(41) 41 &;exit"
bind .wleft.b70 <Any-Enter> ".wleft.b70 configure -cursor hand1"
bind .wleft.b70 <Any-Leave> ".wleft.b70 configure -cursor left_ptr"

label .wleft.b71 -foreground $frcolor(42) -font $sysFont(t) -bg grey -width 11
label .wleft.ba71 -foreground $frcolor(42) -font $sysFont(s) -bg blue
grid .wleft.b71 -row 9 -column 6 -padx 3
grid .wleft.ba71 -row 10 -column 6 -padx 3
set ltext ""
if {$score(42) != ""} {set ltext "([expr $serie(42) +1]/$seriemax(42))"}
.wleft.b71 configure -text "[mc {Table de}] 6"
.wleft.ba71 configure -text $ltext 
bind .wleft.b71 <1> "exec $iwish oeuf.tcl tabx6.conf $serie(42) 42 &;exit"
bind .wleft.b71 <Any-Enter> ".wleft.b71 configure -cursor hand1"
bind .wleft.b71 <Any-Leave> ".wleft.b71 configure -cursor left_ptr"

label .wleft.b72 -foreground $frcolor(43) -font $sysFont(t) -bg grey -width 11
label .wleft.ba72 -foreground $frcolor(43) -font $sysFont(s) -bg blue
grid .wleft.b72 -row 11 -column 6 -padx 3
grid .wleft.ba72 -row 12 -column 6 -padx 3
set ltext ""
if {$score(43) != ""} {set ltext "([expr $serie(43) +1]/$seriemax(43))"}
.wleft.b72 configure -text "[mc {Table de}] 7"
.wleft.ba72 configure -text $ltext 
bind .wleft.b72 <1> "exec $iwish oeuf.tcl tabx7.conf $serie(43) 43 &;exit"
bind .wleft.b72 <Any-Enter> ".wleft.b72 configure -cursor hand1"
bind .wleft.b72 <Any-Leave> ".wleft.b72 configure -cursor left_ptr"

label .wleft.b73 -foreground $frcolor(44) -font $sysFont(t) -bg grey -width 11
label .wleft.ba73 -foreground $frcolor(44) -font $sysFont(s) -bg blue
grid .wleft.b73 -row 13 -column 6 -padx 3
grid .wleft.ba73 -row 14 -column 6 -padx 3
set ltext ""
if {$score(44) != ""} {set ltext "([expr $serie(44) +1]/$seriemax(44))"}
.wleft.b73 configure -text "[mc {Table de}] 8"
.wleft.ba73 configure -text $ltext 
bind .wleft.b73 <1> "exec $iwish oeuf.tcl tabx8.conf $serie(44) 44 &;exit"
bind .wleft.b73 <Any-Enter> ".wleft.b73 configure -cursor hand1"
bind .wleft.b73 <Any-Leave> ".wleft.b73 configure -cursor left_ptr"

label .wleft.b74 -foreground $frcolor(45) -font $sysFont(t) -bg grey -width 11
label .wleft.ba74 -foreground $frcolor(45) -font $sysFont(s) -bg blue
grid .wleft.b74 -row 15 -column 6 -padx 3
grid .wleft.ba74 -row 16 -column 6 -padx 3
set ltext ""
if {$score(45) != ""} {set ltext "([expr $serie(45) +1]/$seriemax(45))"}
.wleft.b74 configure -text "[mc {Table de}] 9"
.wleft.ba74 configure -text $ltext 
bind .wleft.b74 <1> "exec $iwish oeuf.tcl tabx9.conf $serie(45) 45 &;exit"
bind .wleft.b74 <Any-Enter> ".wleft.b74 configure -cursor hand1"
bind .wleft.b74 <Any-Leave> ".wleft.b74 configure -cursor left_ptr"

}



############
# Bindings #
############
bind . <Control-q> {exit}


proc changeinterface {} {
global baseHome
variable repert
set f [open [file join $baseHome reglages repert.conf] "w"]
puts $f $repert
close $f

changehome
interface
}



interface

proc setlang {lang} {
global env plateforme baseHome
set env(LANG) $lang
set f [open [file join $baseHome lang.conf] "w"]
puts $f $lang
close $f

::msgcat::mclocale $lang
::msgcat::mcload [file join [file dirname [info script]] msgs]
interface
}




proc imprimesuivi {} {
global progaide LogHome user score serie seriemax
set listacti {a+b aa+b aa+bb aaa+b aaa+bb aaa+bbb a-b aa-b aa-bb aaa-b aaa-bb aaa-bbb axb aaxb aaaxb aaaxbb aaaxbbb Compl.10 Compl.20 Compl.100 Compl.lib. Mult2 Mult3 Mult4 Mult5 Mult6 Mult7 Mult8 Mult9 Ta.+2 Ta.+3 Ta.+4 Ta.+5 Ta.+6 Ta.+7 Ta.+8 Ta.+9 Ta.x2 Ta.x3 Ta.x4 Ta.x5 Ta.x6 Ta.x7 Ta.x8 Ta.x9}
set g [open [file join $LogHome tmp1.htm] "w" ] 
puts $g  "<html><body>"
puts $g  "<p align = center><B>[mc {Fiche de}] [string map {.log \040} [file tail $user]]</B></p>"


for {set i 0} {$i < [llength $listacti]} {incr i} {
set texte [lindex $listacti $i]
if {$score($i) !=""} {set texte "[mc {Fichier}] : $texte - [mc {serie}] [expr $serie($i)+ 1]/$seriemax($i) : $score($i)"


puts $g  "<p>$texte</p>"}
}

close $g
set fichier [file join [pwd] $LogHome tmp1.htm]
exec $progaide $fichier &
}

