############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editeur.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: editcalapa.tcl,v 1.1.1.1 2004/04/16 11:45:44 erics Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

##################################"sourcing
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source utils.tcl
source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
changehome

set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"

####################### Creation des menus
#menu .menu -tearoff 0
#menu .menu.fichier -tearoff 0
#.menu add cascade -label [mc {Activite}] -menu .menu.fichier
#.menu.fichier add command -label [mc {Calapa}] -command "interface calapa"
#. configure -menu .menu
#######################################################################"
#variables
#decor$i pour l'habitat 
# pdecor$i pour l'animal (0 ou 1), index� sur l'habitat
#variable sel1 pour les habitats s�lectionn�s
#variable anim$i (animation ou pas 0 ou 1)
#animal0 ou 1 pour les brouilleurs

global arg
set arg [lindex $argv 0]

. configure -background blue
frame .geneframe -background blue -height 300 -width 400
pack .geneframe -side left


#################################################g�n�ration interface
proc interface { what} {
global sysFont listanimaux listhabitat arg compteligne

if {$arg == "calapa"} {
set compteligne 3
} else {
set compteligne 4
}
set f [open [file join  $what.ani] "r"]
set listanimaux [gets $f]
close $f

set f [open [file join  $what.hab] "r"]
set listhabitat [gets $f]
close $f

catch {destroy .geneframe}

catch {destroy .leftframe}

frame .leftframe -background white -height 400 -width 100 
pack .leftframe -side left -anchor n

frame .geneframe -background white -height 300 -width 200
pack .geneframe -side left -anchor n
label .leftframe.lignevert1 -bg red -text "" -font {Arial 1}
pack .leftframe.lignevert1 -side right -expand 1 -fill y

######################leftframe.frame1
frame .leftframe.frame1 -background white -width 100 
pack .leftframe.frame1 -side top 

label .leftframe.frame1.lab1 -foreground red -bg white -text [mc $arg] -font $sysFont(l)
pack .leftframe.frame1.lab1 -side top -fill x -pady 5

label .leftframe.frame1.lab2 -bg blue -foreground white -text [mc {Scenario}] -font $sysFont(l)
pack .leftframe.frame1.lab2 -side top -fill x -expand 1 -pady 5

listbox .leftframe.frame1.listsce -yscrollcommand ".leftframe.frame1.scrollpage set" -width 15 -height 6
scrollbar .leftframe.frame1.scrollpage -command ".leftframe.frame1.listsce yview" -width 7
pack .leftframe.frame1.listsce .leftframe.frame1.scrollpage -side left -fill y -expand 1 -pady 10
bind .leftframe.frame1.listsce <ButtonRelease-1> "changelistsce %x %y"

############################leftframe.frame2
frame .leftframe.frame2 -background white -width 100 
pack .leftframe.frame2 -side top 
button .leftframe.frame2.but0 -text [mc {Nouveau}] -command "additem" -activebackground white
pack .leftframe.frame2.but0 -pady 5 -side top
button .leftframe.frame2.but1 -text [mc {Supprimer}] -command "delitem" -activebackground white
pack .leftframe.frame2.but1 -pady 5 -side top


########################leftframe.frame3
frame .leftframe.frame3 -background white -width 100 
pack .leftframe.frame3 -side top 
label .leftframe.frame3.lab0 -foreground white -bg blue -text [mc {    Type    }] -font $sysFont(l)
pack .leftframe.frame3.lab0 -pady 5 -side top -fill x -expand 1
if {$arg == "calapa"} {
radiobutton .leftframe.frame3.sel1 -text [mc {Tri}] -variable sel1 -relief flat -value 0 -background white -activebackground white 
pack .leftframe.frame3.sel1 -side top -anchor w
}
radiobutton .leftframe.frame3.sel2 -text [mc {Marquage}] -variable sel1 -relief flat -value 1 -background white -activebackground white 
pack .leftframe.frame3.sel2 -side top -anchor w
radiobutton .leftframe.frame3.sel3 -text [mc {Enumeration}] -variable sel1 -relief flat -value 2 -background white -activebackground white 
pack .leftframe.frame3.sel3 -side top -anchor w

button .leftframe.frame3.but0 -text [mc {ok}] -command "fin" -activebackground white
pack .leftframe.frame3.but0 -pady 5 -side bottom


variable ciel
set ciel 1

set tmp 5
for {set i 0} {$i <= $compteligne} {incr i 1} {
  label .geneframe.labll$i -bg red -text "" -font {Arial 1}
  grid .geneframe.labll$i -column 0 -row $tmp -columnspan 6 -sticky "w e"
  incr tmp 

}

#############colonne habitat
  label .geneframe.lab0_0 -bg white -text [mc {Habitat}] -font $sysFont(l)
  grid .geneframe.lab0_0 -column 0 -row 0 -sticky "w e"
  label .geneframe.labl1 -bg red -text "" -font {Arial 1}
  grid .geneframe.labl1 -column 0 -row 1 -columnspan 6 -sticky "w e"


  checkbutton .geneframe.chk0 -text [mc {Ciel}] -variable ciel -relief flat -state disabled -bg white -activebackground white
  grid .geneframe.chk0 -column 0 -row 2 -sticky "n s w e"
  label .geneframe.labl2 -bg red -text "" -font {Arial 1}
  grid .geneframe.labl2 -column 0 -row 3 -columnspan 6 -sticky "w e"

set tmp 4
for {set i 0} {$i < $compteligne} {incr i 1} {
  #checkbutton .geneframe.chk[expr $i +1] -image [image create photo -file [file join images m[lindex $listhabitat $i]]]  -indicatoron 0 -variable decor$i -relief flat -bg grey50 -command "changedecor $i"
  if {$arg == "calapa"} {
  checkbutton .geneframe.chk[expr $i +1] -text [format [mc {Ensemble %1$s}] $i] -variable decor$i -relief flat -bg white -command "changedecor $i" -activebackground white
  } else {
  checkbutton .geneframe.chk[expr $i +1] -text [format [mc {%1$s elements}] [expr $i + 2]] -variable decor$i -relief flat -bg white -command "changedecor $i" -activebackground white
  }
  grid .geneframe.chk[expr $i +1] -column 0 -row $tmp -sticky "n s w e"
  incr tmp 2
}



###############colonne Animaux
  label .geneframe.lab1_0 -bg #78F8FF -text [mc {Animaux}] -font $sysFont(l)
  grid .geneframe.lab1_0 -column 1 -row 0 -columnspan 2 -sticky "w e"

  checkbutton .geneframe.b1_1 -image [image create photo -file [file join images [lindex [lindex $listanimaux 0] 1]]] -indicatoron 0 -bg #78F8FF -variable animal1
  grid .geneframe.b1_1 -column 1 -row 2 -sticky "n s w e"
  checkbutton .geneframe.b1_2 -image [image create photo -file [file join images [lindex [lindex $listanimaux 0] 0]]] -indicatoron 0 -bg #78F8FF -variable animal0
  grid .geneframe.b1_2 -column 2 -row 2 -sticky "n s w e"

set tmp 4
for {set i 0} {$i < $compteligne} {incr i 1} {
  checkbutton .geneframe.rb$i -image [image create photo -file [file join images [lindex [lindex $listanimaux [expr $i + 1]] 0]]] -indicatoron 0 -bg #78F8FF -variable pdecor$i -width 45 -height 45 -command "changepdecor $i"
  grid .geneframe.rb$i -column 1 -row $tmp  -sticky "w e"
#incr tmp
  checkbutton .geneframe.rbb$i -image [image create photo -file [file join images [lindex [lindex $listanimaux [expr $i + 1]] 1]]] -indicatoron 0 -bg #78F8FF -variable ppdecor$i -width 45 -height 45  -command "changepdecor $i"
  grid .geneframe.rbb$i -column 2 -row $tmp  -sticky "w e"
incr tmp 2
}

################colonne nombre
  label .geneframe.lab3_0 -bg white -text [mc {Nombre (Min Max)}] -font $sysFont(l)


  grid .geneframe.lab3_0 -column 3 -row 0 -sticky "w e" -columnspan 2
  scale .geneframe.scalebrouilleur -orient horizontal -length 120 -from 3 -to 10 -tickinterval 7 -borderwidth 0 -bg white -activebackground white -highlightcolor white -highlightbackground white -font $sysFont(s)
  grid .geneframe.scalebrouilleur -column 3 -row 2 -sticky "w e" -columnspan 2
  .geneframe.scalebrouilleur set 3
  #.geneframe.scale4 configure

if {$arg == "calapa"} {
set tmp 4
for {set i 0} {$i < $compteligne} {incr i 1} {
  scale .geneframe.scale$i -orient horizontal -length 120 -from 1 -to 18 -tickinterval 17 -borderwidth 0 -bg white -activebackground white -highlightcolor white -highlightbackground white -font $sysFont(s)
  grid .geneframe.scale$i -column 3 -row $tmp -sticky "w e"
  bind .geneframe.scale$i <ButtonRelease-1> "changescalemin $i"
  scale .geneframe.scale$i$i -orient horizontal -length 120 -from 1 -to 18 -tickinterval 17 -borderwidth 0 -bg white -activebackground white -highlightcolor white -highlightbackground white -font $sysFont(s)
  grid .geneframe.scale$i$i -column 4 -row $tmp -sticky "w e"
  bind .geneframe.scale$i$i <ButtonRelease-1> "changescalemax $i" 
  incr tmp 2
}
} else {
set tmp 4
for {set i 0} {$i < $compteligne} {incr i 1} {
  scale .geneframe.scale$i -orient horizontal -length 120 -from [expr $i + 2] -to [expr ($i + 2)*4] -tickinterval [expr $i + 2] -resolution [expr $i + 2] -borderwidth 0 -bg white -activebackground white -highlightcolor white -highlightbackground white -font $sysFont(s)
  grid .geneframe.scale$i -column 3 -row $tmp -sticky "w e"
  bind .geneframe.scale$i <ButtonRelease-1> "changescalemin $i"
  scale .geneframe.scale$i$i -orient horizontal -length 120 -from [expr $i + 2] -to [expr ($i + 2)*4] -tickinterval [expr $i + 2] -resolution [expr $i + 2] -borderwidth 0 -bg white -activebackground white -highlightcolor white -highlightbackground white -font $sysFont(s)
  grid .geneframe.scale$i$i -column 4 -row $tmp -sticky "w e"
  bind .geneframe.scale$i$i <ButtonRelease-1> "changescalemax $i" 
  incr tmp 2
}

}
##############colonne animation
  label .geneframe.lab5_0 -bg #78F8FF -text [mc {Animation}] -font $sysFont(l)
  grid .geneframe.lab5_0 -column 5 -row 0 -sticky "w e"

  checkbutton .geneframe.chk01 -variable anim4 -relief flat -bg #78F8FF -activebackground #78F8FF
  grid .geneframe.chk01 -column 5 -row 2 -sticky "n s w e"
set tmp 4
for {set i 0} {$i < $compteligne} {incr i 1} {
  checkbutton .geneframe.chkk$i -variable anim$i -relief flat -bg #78F8FF -activebackground #78F8FF
  grid .geneframe.chkk$i -column 5 -row $tmp -sticky "n s w e"
incr tmp 2
}
 charge 0
}


####################################################################"""
proc charge {index} {
global listdata activelist indexpage totalpage arg compteligne Home
variable sel1
variable anim4 
set ext .conf
for {set i 0} {$i <= 1} {incr i 1} {
variable animal$i
set animal$i 0
}
for {set i 0} {$i < $compteligne} {incr i 1} {
.geneframe.scale$i set 1
.geneframe.scale$i$i set 1
variable pdecor$i
variable ppdecor$i
variable decor$i
set decor$i 0
set pdecor$i 0
set ppdecor$i 0
}
for {set i 0} {$i <= $compteligne} {incr i 1} {
variable anim$i
set anim$i 0
}

set f [open [file join $Home reglages $arg$ext] "r"]
set listdata [gets $f]
close $f


set totalpage [llength $listdata] 
set indexpage $index
	.leftframe.frame1.listsce delete 0 end
	#.leftframe.frame3.listsce2 delete 0 end
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.leftframe.frame1.listsce insert end [lindex [lindex [lindex $listdata $k] 0] 0]
	#.leftframe.frame3.listsce2 insert end [lindex [lindex [lindex $listdata $k] 0] 0]
	}
set activelist [lindex $listdata $indexpage]
.leftframe.frame1.listsce selection set $indexpage
	set sel1 [lindex [lindex $activelist 0] 1]
		for {set i 2} {$i < [llength $activelist]} {incr i 1} {
		set k [lindex [lindex $activelist $i] 1]
		set decor$k 1
		switch [lindex [lindex $activelist $i] 0] {
		0 { set pdecor$k 0
		set ppdecor$k 0}
		 
		1 { set pdecor$k 1
		set ppdecor$k 0}
		
		2 { set pdecor$k 0
		set ppdecor$k 1
		}

		3 { set pdecor$k 1
		set ppdecor$k 1
		}

 		}
		#set pdecor$k [lindex [lindex $activelist $i] 0]
		.geneframe.scale$k set [lindex [lindex $activelist $i] 3]
		.geneframe.scale$k$k set [lindex [lindex $activelist $i] 4]
		set anim$k [lindex [lindex $activelist $i] 2]
	}

foreach item [lindex $activelist 1] {
set animal[lindex $item 0] 1
set anim4 [lindex $item 1] 
.geneframe.scalebrouilleur set [lindex $item 2]
}
#.geneframe.scalebrouilleur set 10
}


######################################################
proc changepdecor {m} {
global compteligne
for {set k 0} {$k < $compteligne} {incr k 1} {
variable decor$k
variable pdecor$k
variable ppdecor$k
}

if {[expr \$pdecor$m] ==0 && [expr \$ppdecor$m] ==0} {
	set decor$m 0
	} else {
	set decor$m 1
	verifieclasse $m
	}

}

proc changedecor {i} {
global arg compteligne
for {set k 0} {$k < $compteligne} {incr k 1} {
variable decor$k
variable pdecor$k
variable ppdecor$k
}
verifieclasse $i
if {[expr \$decor$i] !=0 && [expr \$pdecor$i] ==0 && [expr \$ppdecor$i] ==0} {
set pdecor$i 1
}
if {[expr \$decor$i] ==0} {
set pdecor$i 0
set ppdecor$i 0
}

}

proc verifieclasse {i} {
global arg compteligne
for {set k 0} {$k < $compteligne} {incr k 1} {
variable decor$k
variable pdecor$k
variable ppdecor$k
}
set tmp 0
for {set k 0} {$k < $compteligne} {incr k 1} {
if {[expr \$decor$k] ==1} {
incr tmp
}
}
if {$tmp > 2 && [expr \$decor$i] == 1} {
set pdecor$i 0
set ppdecor$i 0
set decor$i 0
tk_messageBox -message [mc {Seulement 2 habitats autorises}] -type ok -title [mc $arg]
} 
}
###################################################################################"
proc enregistre_sce {} {
global indexpage listdata activelist totalpage arg compteligne Home
set ext .conf
variable sel1
variable anim4 

for {set i 0} {$i <= 1} {incr i 1} {
variable animal$i
}
for {set i 0} {$i <= $compteligne} {incr i 1} {
# compteligne was 2
variable pdecor$i
variable ppdecor$i
variable decor$i
}
for {set i 0} {$i <= $compteligne} {incr i 1} {
variable anim$i
}

set activelist \173[.leftframe.frame1.listsce get $indexpage]\040$sel1\175
set tmp1 ""
set tmp2 ""
if {$animal0==1} {
set tmp1 \1730\040$anim4\040[.geneframe.scalebrouilleur get]\175
}
if {$animal1==1} {
set tmp2 \1731\040$anim4\040[.geneframe.scalebrouilleur get]\175
}
lappend activelist $tmp1\040$tmp2

if {$decor0==1} {
lappend activelist [expr $pdecor0 + 2*$ppdecor0]\0400\040$anim0\040[.geneframe.scale0 get]\040[.geneframe.scale00 get]
}

if {$decor1==1} {
lappend activelist [expr $pdecor1 + 2*$ppdecor1]\0401\040$anim1\040[.geneframe.scale1 get]\040[.geneframe.scale11 get]
}

if {$decor2==1} {
lappend activelist [expr $pdecor2 + 2*$ppdecor2]\0402\040$anim2\040[.geneframe.scale2 get]\040[.geneframe.scale22 get]
}

if {$compteligne == 4} {
if {$decor3==1} {
lappend activelist [expr $pdecor3 + 2*$ppdecor3]\0403\040$anim3\040[.geneframe.scale3 get]\040[.geneframe.scale33 get]
}

}

set listdata [lreplace $listdata $indexpage $indexpage $activelist]
set f [open [file join $Home reglages $arg$ext] "w"]
puts $f $listdata
close $f
}

###################################################################
proc changescalemin {min} {
if {[.geneframe.scale$min get] > [.geneframe.scale$min$min get] } { 
.geneframe.scale$min$min set [.geneframe.scale$min get] 
}
}

proc changescalemax {max} {
if {[.geneframe.scale$max$max get] < [.geneframe.scale$max get] } { 
.geneframe.scale$max set [.geneframe.scale$max$max get] 
}
}

proc changelistsce {x y} {
enregistre_sce
set ind [.leftframe.frame1.listsce index @$x,$y]
charge $ind
}

proc fin {} {
enregistre_sce
destroy .
}

#######################################################################
proc delitem {} {
global listdata indexpage totalpage arg Home
set ext .conf
	if {$totalpage < 2} {
	tk_messageBox -message [mc {Impossible de supprimer la fiche}] -type ok -title [mc $arg]
	return
	}
set response [tk_messageBox -message [format [mc {Voulez-vous vraiment supprimer la fiche %1$s ?}] [lindex [lindex [lindex $listdata $indexpage] 0] 0]] -type yesno -title [mc $arg]]
	if {$response == "yes"} {
	set totalpage [expr $totalpage - 1]
		set listdata [lreplace $listdata $indexpage $indexpage]
     		if {$indexpage > 0} {
     		set indexpage [expr $indexpage - 1]						
		} 
	set f [open [file join $Home reglages $arg$ext] "w"]
	puts $f $listdata
	close $f
	charge $indexpage

	} 

}


proc additem {} {
global listdata indexpage totalpage
enregistre_sce
catch {destroy .nomobj}
toplevel .nomobj -background grey -width 250 -height 100
wm geometry .nomobj +50+50
frame .nomobj.frame -background grey -width 250 -height 100
pack .nomobj.frame -side top
label .nomobj.frame.labobj -font {Helvetica 10} -text [mc {Nom du scenario :}] -background grey
pack .nomobj.frame.labobj -side top 
entry .nomobj.frame.entobj -font {Helvetica 10} -width 10
pack .nomobj.frame.entobj -side top 
button .nomobj.frame.ok -background gray75 -text [mc {Ok}] -command "verifnomobj"
pack .nomobj.frame.ok -side top -pady 10
}

proc verifnomobj {} {
global nom listdata indexpage totalpage arg Home
set ext .conf
set nom [join [.nomobj.frame.entobj get] ""]
if {$nom !=""} {
set indexpage $totalpage
incr totalpage
lappend listdata \173$nom\0400\175
set f [open [file join $Home reglages $arg$ext] "w"]
puts $f $listdata
close $f
charge $indexpage
}
catch {destroy .nomobj}
}

#proc changelistsce2 {x y} {
#.leftframe.frame3.choix configure -text [.leftframe.frame3.listsce2 get @$x,$y]
#}


interface $arg
