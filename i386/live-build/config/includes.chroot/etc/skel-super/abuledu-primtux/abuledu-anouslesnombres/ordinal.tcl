############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : ordinal.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 08/02/2003
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: ordinal.tcl,v 1.1.1.1 2004/04/16 11:45:50 erics Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#ordinal.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}


set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source utils.tcl
source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
changehome

set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"


proc placeboutons {c ind} {
global nbcat typeexo indwagonens nbcat user
catch {destroy .bframe}
frame .bframe -background #ffff80
pack .bframe -side bottom
label .bframe.tete -background #ffff80 -image neutre
grid .bframe.tete -column 0 -padx 10 -row 0

button .bframe.butpoub -image [image create photo -file [file join images poubelle.gif] ] -background #ff80c0 -command "effacetout $c"
grid .bframe.butpoub -column 4  -padx 5 -row 0 -sticky w

switch $ind {
	0 {
	#initialisation du jeu (appel g�n�ral)
		if {$typeexo == 0} {
		label .bframe.consigne -background #ffff80 -text [mc {Place le wagon sur les rails}]
		grid .bframe.consigne -column 1 -padx 10 -row 0
		affichemodele $c
		} else {
		label .bframe.consigne -background #ffff80 -text [mc {Observe puis appuie sur le pouce}]
		grid .bframe.consigne -column 1 -padx 10 -row 0
		button .bframe.butok -image [image create photo -file [file join sysdata ok1.gif] ] -background #ff80c0 -command "placeboutons $c 1"
		grid .bframe.butok -column 3  -padx 5 -row 0 -sticky w
		}
	}
	1 {
		label .bframe.consigne -background #ffff80 -text [mc {Place le wagon sur les rails}]
		grid .bframe.consigne -column 1 -padx 10 -row 0
		affichemodele $c
		$c create image 330 120 -image oeiln -tags "voir"
		$c bind voir <Any-Enter> "voirEnter $c"
		$c bind voir <Any-Leave> "voirLeave $c"
		foreach item [$c find withtag modele] {
		$c itemconfigure $item -state hidden
		}

	}

}

}

#pour revoir le mod�le
proc voirEnter {c} {
global evalrevoir
foreach item [$c find withtag modele] {
$c itemconfigure $item -state normal
}
foreach item [$c find withtag plac�] {
$c itemconfigure $item -state hidden
}

incr evalrevoir
$c itemconf voir -image oeils
}

proc voirLeave {c} {
foreach item [$c find withtag modele] {
$c itemconfigure $item -state hidden
}
foreach item [$c find withtag plac�] {
$c itemconfigure $item -state normal
}

$c itemconf voir -image oeiln
}

global listdata arg1 arg2 cat listgen

#listgen : liste cat�gorie (barque, calapa, etc ...)
#listdata : liste pour le sc�nario en cours
#arg1 : type de activit� (calapa, barque, etc)
#arg2 : num�ro de l'activit� dans le sc�nario 
set c .frame.c
set arg1 [lindex $argv 0]
set arg2 [lindex $argv 1]
set arg3 [lindex $argv 2]
set arg4 [lindex $argv 3]
#interface
. configure -background #ffff80 
frame .frame -width 640 -height 420 -background #ffff80
pack .frame -side top -fill both -expand yes
wm geometry . +0+0
canvas $c -width 640 -height 400 -background #ffff80 -highlightbackground #ffff80
pack $c

#ouverture du fichier calapa.conf ou barque.conf ...
set ext .conf
set f [open [file join $Home reglages $arg1$ext] "r"]
set listgen [gets $f]
close $f

image create photo bien -file [file join sysdata pbien.gif] 
image create photo pass -file [file join sysdata ppass.gif]
image create photo mal -file [file join sysdata pmal.gif]
image create photo neutre -file [file join sysdata pneutre.gif]
image create photo rails -file [file join images railpetit.gif]
image create photo locog -file [file join images locog.gif]
image create photo locod -file [file join images locod.gif]
image create photo oeiln -file [file join sysdata oeil.gif]
image create photo oeils -file [file join sysdata oeil2.gif]


#procedures
####################################################""
proc recommence {c} {
$c bind clic <ButtonRelease-1> ""
$c bind drag <ButtonRelease-1> ""
$c bind drag <ButtonPress-1> ""
$c bind drag <B1-Motion> ""
place $c
}

proc affichemodele {c} {
global listewagon
if {[llength $listewagon] !=0} {
set ext .gif
set ind [expr int(rand()*[llength $listewagon])]
set tmp [lindex $listewagon $ind]
$c create image 330 190 -image [image create photo comwagon$tmp -file [file join images wagons$tmp$ext]] -tags "drag cib$tmp"
set listewagon [lreplace $listewagon $ind $ind]
}
}
###########################################################################

#proc�dure principale
proc place {c} {
global cate nbens listdata arg1 arg2  listgen  listewagon typeexo indplace user tabcoulwagon posinverse evalrevoir nbessai copielistewagon
#listewagon : liste interm�diaire pour le melange des wagons
#nbens : tableau du nombre total de wagons par ensemble (nbens(i))
#cate : tableau des ensembles cat(1), cat(2) par classe de wagons
#typeexo commandefenetre accrocheauto correctioncommande dejap borne : param�tres pour les exos (voir todo.txt)

set tabcoulwagon {0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15}

for {set i 0} {$i <= [llength $tabcoulwagon]} {incr i 1} {
	set t1 [expr int(rand()*[llength $tabcoulwagon])]
	set t2 [expr int(rand()*[llength $tabcoulwagon])]
	set tmp [lindex $tabcoulwagon $t1]
	set tabcoulwagon [lreplace $tabcoulwagon $t1 $t1 [lindex $tabcoulwagon $t2]]
	set tabcoulwagon [lreplace $tabcoulwagon $t2 $t2 $tmp]
	}


set listdata [lindex $listgen $arg2]
wm title . "[mc $arg1] [lindex [lindex $listdata 0] 0]"
set typeexo [lindex [lindex $listdata 0] 1]
set posinverse [lindex [lindex $listdata 0] 2]


set cate [lindex $listdata 1]

set evalrevoir 0
set indplace 0
set nbessai 0
set listewagon ""
set nbens 0

set ext .gif
#wagons
	for {set i 0} {$i <= 7} {incr i 1} {
	image create photo wagons$i -file [file join images wagons$i$ext]
	}

	set intervalle [expr [lindex $cate 1] - [lindex $cate 0] + 1] 
	set nbens [expr int(rand()*$intervalle) + [lindex $cate 0]]

##############################################################################################"
		for {set i 1} {$i <= $nbens} {incr i 1} {
		lappend listewagon [lindex $tabcoulwagon $i]
		}
	
################################################################################################"
#on efface et on place les images, apr�s avoir touill�
	for {set i 0} {$i < [llength $listewagon]} {incr i 1} {
	set t1 [expr int(rand()*[llength $listewagon])]
	set t2 [expr int(rand()*[llength $listewagon])]
	set tmp [lindex $listewagon $t1]
	set listewagon [lreplace $listewagon $t1 $t1 [lindex $listewagon $t2]]
	set listewagon [lreplace $listewagon $t2 $t2 $tmp]
	}

$c delete all

# la gare, la gare-r�sultat en gris, les cadres blancs
$c create rect 0 0 650 50 -fill #78F8FF -tags gare
$c create rect 0 250 650 300 -fill grey -tags resultat
#$c create rect 200 130 620 [expr 130 + $nbcat*40] -fill white

#les wagons du train modele
	for {set i 1} {$i <= [llength $listewagon]} {incr i 1} {
		image create photo wagons[lindex $listewagon [expr $i -1]]$i -file [file join images wagons[lindex $listewagon [expr $i -1]]$ext]
		set ypos [expr int([expr $i - 1]/30)*30 + 27]
		set xpos [expr fmod([expr $i -1],30)*30 +65]
		$c create image $xpos $ypos -image wagons[lindex $listewagon [expr $i -1]]$i -tags modele
		if {$posinverse == 0} {
		$c create image [expr 40 + 38*$i] 285 -image rails
		$c create text [expr 40 + 38*$i] 285 -text $i -tags "uid[lindex $listewagon [expr $i -1]] textcible" -fill red
		} else {
		$c create image [expr 600 - 38*$i] 285 -image rails
		$c create text [expr 600 - 38*$i] 285 -text $i -tags "uid[lindex $listewagon [expr $i -1]] textcible" -fill red

		}			
	}
	if {$posinverse == 0} {
	$c create image 32 280 -image locog 
	} else {
	$c create image 605 278 -image locod
	}
	$c create image 30 25 -image locog
$c bind drag <ButtonRelease-1> "itemStopDrag $c %x %y"
$c bind drag <ButtonPress-1> "itemStartDrag $c %x %y"
$c bind drag <B1-Motion> "itemDrag $c %x %y"


set copielistewagon $listewagon

placeboutons $c 0

set str "\173[mc {Nombre de wagons a placer :}] $nbens\175"
#set str [append str "\173Nombre de wagons � placer : $nbens\175]
#set str [append str "\173Nombre total de wagons : [llength $listewagon]\175\040"]
enregistreeval $arg1 [lindex [lindex $listdata 0] 0] $str $user 
}


#appel de la proc�dure principale
place $c



#a la fin de l'exercice
proc geresuite {c} {
global arg2 arg3 arg4 listgen user nbplace typeexo evalrevoir nbessai Home
catch {destroy .bframe}
frame .bframe -background #ffff80
pack .bframe -side bottom
label .bframe.tete -background #ffff80 -image bien
grid .bframe.tete -column 0 -padx 10 -row 0
label .bframe.consigne -background #ffff80 -text [mc {Bravo, c'est gagne!}]
grid .bframe.consigne -column 1 -padx 30 -row 0
if {$arg2 < [expr [llength $listgen] -1] && $arg3 == 0} {
button .bframe.suite -image [image create photo -file [file join sysdata suite.gif] ] -background #ff80c0 -command "suite $c"
grid .bframe.suite -column 2  -padx 20 -row 0 -sticky w

}

set f [open [file join $Home reglages parcours.conf] "r"]
set listparcours [gets $f]
close $f

if {$arg4 < [expr [llength $listparcours] -1] && $arg3 == 1} {
button .bframe.suite -image [image create photo -file [file join sysdata suite.gif] ] -background #ff80c0 -command "suiteparcours $c $arg4"
grid .bframe.suite -column 2  -padx 20 -row 0 -sticky w
}
button .bframe.recommencer -image [image create photo -file [file join sysdata again2.gif] ] -background #ff80c0 -command "recommence $c"
grid .bframe.recommencer -column 3  -padx 20 -row 0 -sticky w

set str [append str "\173[mc {Nombre d'essais :}] [expr $nbessai +1]\175\040"]
if {$typeexo == 1} {
set str [append str "\173[mc {Nombre d'aides :}] $evalrevoir\175\040"]
}
appendeval $str $user 
label .bframe.reussite -background #ffff80 -text "[mc {Nombre d'essais :}] [expr $nbessai + 1]"
grid .bframe.reussite -column 4 -padx 30 -row 0

}


#gestion du jeu, accrochage manuel
proc itemStartDrag {c x y} {
    global lastX lastY sourcecoord
    set sourcecoord [$c coords current]
    set lastX [$c canvasx $x]
    set lastY [$c canvasy $y]
    $c raise current
    .bframe.tete configure -image neutre
    }

proc itemStopDrag {c x y} {
    global lastX lastY sourcecoord listewagon indplace nbens
    set idcible [string range [lindex [$c gettags current] [lsearch -regexp [$c gettags current] cib*]] 3 end]
    set coord [$c bbox current]
    foreach i [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]] {
		if {[lsearch [$c gettags $i] uid$idcible] != -1} {
		$c coords current [lindex [$c coords $i] 0] [lindex [$c coords $i] 1]
		incr indplace
		$c dtag current cib$idcible
		$c addtag plac� withtag current
		$c dtag current drag
		foreach i [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]] {
		if {[lsearch [$c gettags $i] textcible] != -1} {
		$c dtag $i textcible
		}
		}
		#.bframe.tete configure -image bien
			if {$indplace == $nbens} {
			geresuite $c
			return
			}
		affichemodele $c
		return
		} elseif { [lsearch [$c gettags $i] textcible] != -1} {
		#.bframe.tete configure -image mal
		$c coords current [lindex [$c coords $i] 0] [lindex [$c coords $i] 1]
		$c dtag current drag
		$c addtag plac� withtag current
		foreach i [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]] {
		if {[lsearch [$c gettags $i] textcible] != -1} {
		$c dtag $i textcible
		}
		}
		affichemodele $c
		return
            } else {
		$c coords current [lindex $sourcecoord 0] [lindex $sourcecoord 1]
		}
    }
}

proc itemDrag {c x y} {
    global lastX lastY
    set x [$c canvasx $x]
    set y [$c canvasy $y]
    $c move current [expr $x-$lastX] [expr $y-$lastY]
    set lastX $x
    set lastY $y
}

proc effacetout {c} {
global indplace listewagon copielistewagon nbessai
 incr nbessai
$c delete [$c find withtag drag]
foreach item [$c find withtag plac�] {
$c delete $item
}

set indplace 0

set listewagon $copielistewagon
affichemodele $c
}