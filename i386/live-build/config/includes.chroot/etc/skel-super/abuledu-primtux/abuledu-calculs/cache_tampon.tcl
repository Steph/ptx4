#!/bin/sh
#cache_tampon.tcl
#\
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Date    : 01/02/2005
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version 0.1
#  @author     Jean-Louis Sendral. 
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  ##############################################################
source bande.conf ; ##calculs.conf ???
source msg.tcl
source path.tcl
######################Recceuil des parametres dans le fichier .ban ##########################
## A deux ou contre ordi, qui ? l'un est le connect� l'autre ? li ? col ? zone d�part (pas),
## limite des pi�ges(nbre , espacement) � mettre dans des toplevel
###2 �leves : le premier choisit le nombre et dit le chaud tiede froid il peut �tre l'ordinateur. Le second devine
proc fais_grille { } {
global init tab pas
 for {set y 0} {$y <=  $init(nbrow) } {incr y} {
		 for {set x 0} {$x < $init(nbcol) } {incr x} {
		 set ne_x  [expr $x * $init(wlen) ] ; set ne_y [expr $y * $init(hlen) ]
		set tab(rect${y}_${x})	[ .frame_middle.can create rectangle  $ne_x $ne_y   [expr $ne_x + $init(wlen)]  [expr $ne_y  +$init(hlen) ]  -tags rect${y}_${x} -outline red]
						 }
 }
}

### Pour connaitre les deux �leves, qui choisit/contr�le et donc qui devine ?
proc init_qui_deb_cible { } {
  global nom_autre premier cible nom_elev w2 argv max_n
proc retiens_nom { } {
global nom_autre  w2
set  nom_autre  [${w2}.nom_autre get ]

if { $nom_autre == {}} {
${w2}.nom_autre  delete 0 end
focus ${w2}.nom_autre
} else {
set nom_autre  $nom_autre ; retiens_prem
if { $nom_autre != "ordinateur" } {${w2}.nom_autre configure -state disabled}

}
}
proc retiens_cible { } {
global nom_elev nom_autre  choix_prem cible w2 premier max_n
if { [ ${w2}.choix_pas  get] == {}  || [ regexp {(^[0-9]+$)} [ ${w2}.choix_pas  get]  ]  != 1 || [${w2}.choix_pas  get]  > [expr $max_n -1 ] }  { 
 set cib [ ${w2}.choix_pas  get]
 tk_messageBox -message "[format [mc {La cible %1$s n'est pas conforme}] $cib ]" -title "[mc {Message Cible}]" -icon info -type ok
 ${w2}.choix_pas delete 0 end ;  focus -force  ${w2}.choix_pas }  else  {
set cible [ ${w2}.choix_pas  get]
destroy ${w2}
}
##set retour [ list $nom_autre $premier $pas $choix_prem  ]
##return $retour
 ##destroy ${w2}
}


proc retiens_premier {} {
global nom_elev nom_autre w2 choix_prem cible premier max_n
set premier $choix_prem
label ${w2}.pas  -text "[format [mc {%1$s doit choisir la cible (<= %2$s):}] $choix_prem $max_n]" -font {arial 10} 
place ${w2}.pas -x 25 -y 200
entry ${w2}.choix_pas -width 3 -justify center
place ${w2}.choix_pas -x 240 -y 220
focus -force  ${w2}.choix_pas 
${w2}.elev configure -state disabled
${w2}.autre configure -state disabled
##set pas ""
###set pas [ ${w2}.choix_pas  get]
bind ${w2}.choix_pas <Return> "retiens_cible"
bind ${w2}.choix_pas <KP_Enter> "retiens_cible"
##destroy ${w2}
}
proc retiens_prem {} {
global nom_elev nom_autre w2 choix_prem premier cible max_n
label ${w2}.text_prem -text "[format [mc {Qui choisit la cible et contr�le...:  %1$s ou %2$s ?}] $nom_elev $nom_autre ]" -font {arial 10} 
place ${w2}.text_prem -x 15 -y 130 
radiobutton ${w2}.elev -text  "[mc {choisir}] $nom_elev" -variable choix_prem  -value $nom_elev -foreground red -command  "set premier $nom_elev ;  retiens_premier"
radiobutton ${w2}.autre -text  "[mc {choisir}] $nom_autre" -variable choix_prem  -value $nom_autre -foreground  red -command "set premier $nom_autre ;  retiens_premier"
place ${w2}.elev -x  60     -y 150
place ${w2}.autre  -x  210    -y 150
if { $nom_autre == "ordi" } {
${w2}.elev invoke 
}


if { $nom_autre == "ordinateur" } {
${w2}.autre invoke ; set cible [expr int( rand()*$max_n)] ; destroy ${w2}
}
}
 catch {destroy .top2 }
set w2 [toplevel .top2]
wm geometry $w2 360x250+0+0 
wm resizable $w2 no no
wm title $w2 "[mc {Choix de l'autre eleve,  du premier, de la cible}]"
  label ${w2}.text_autre -text "[mc {Nom de l'autre �l�ve :Vous pouvez saisir ordinateur pour  chercher seul la cible}]\n\
		  [mc {ou saisir ordi pour aider l'ordinateur � la chercher.Penser � valider toutes les r�ponses par Entr�e}]" -font {arial 10}
place ${w2}.text_autre -x 10 -y 10 
entry ${w2}.nom_autre -width 20 -font {arial 10} -justify center
place ${w2}.nom_autre -x 100 -y 100 
focus ${w2}.nom_autre
bind ${w2}.nom_autre <Return> "retiens_nom"
bind ${w2}.nom_autre <KP_Enter> "retiens_nom "
##pour les exemples sous icones
if { [lindex $argv 1] == 1 } {
set nom_autre "ordinateur" ; ${w2}.nom_autre insert end $nom_autre ; retiens_nom  ; return
}
if { [lindex $argv 1] == 2 } {
set nom_autre "ordi" ; ${w2}.nom_autre insert end $nom_autre ; retiens_nom  ;\
set cible [expr int( rand()*$max_n)] ; ${w2}.choix_pas insert end $cible ; retiens_cible    ;  return
}



 ##grab ${w2}
raise  ${w2} .

  }
##fin init-deb-cible
  ##set nom_elev "lolo" 
 ##init_qui_deb_cible
 ####verifie por frod et lance les op�rations
 proc retiens_froid {} {
 global chaud tiede froid premier nom_autre flag_rec cible limite_inf limite_sup max_n 
 global  flag_f  flag_t flag_c flag_r en_avant init diametre liste_chaleurs argv
 if { $premier != "ordinateur" } {  set froid [.frame_up.n_froid  get] }
 if { ![regexp {^(\d|10)$}  $froid ]  || $froid <=  $tiede     }  {
 tk_messageBox -message "[format [mc {La valeur %1$s pour froid ne convient pas.car <= %2$s ou non conforme}] $froid $tiede]" -title "[mc {Message froid}]" -icon info -type ok 
	.frame_up.n_froid delete 0 end ; focus -force .frame_up.n_froid ; return
 } else {
 .frame_up.n_froid configure -state disabled
		 if { $flag_rec == 1 &&  $premier != "ordinateur"} {
 ##redemander cible � premier si pas ordi 
		 set cible [expr  int(${max_n}*rand())  ]
		 
		tk_messageBox -message "[format [mc {%1$s Voici la nouvelle cible %2$s}] $premier $cible ]" -title "[mc {Message nouvelle cible}]" -icon info -type ok 
 		 }
	if { [lindex $argv 1] == 2 } {	 
		tk_messageBox -message "[format [mc {%1$s Voici la nouvelle cible %2$s}] $premier  $cible ]" -title "[mc {Message nouvelle cible}]" -icon info -type ok  
		} 
 ###toujours limiter cible 
 .frame_up.consigne  configure -text "[string range $premier 0 9] [mc {a choisi une cible ?? et va contr�ler les choix de}] [string range $nom_autre 0 9].\n\
[string range $nom_autre 0 9] [mc {doit trouver cette cible en essayant de s'en approcher}].\n[string range $premier 0 9] [mc {lui dira si c'est froid(rouge)\
 ou ti�de(orange) ou chaud(vert)en tapant la lettre qui convient}] :\n\
 [format [mc {c : chaud (<= %1$s)  t : -ti�de (<=%2$s )  f : -froid : f(<= %3$s) -rien : r(> %3$s)}] $chaud $tiede $froid ]" 
 set liste_chaleurs [list "r"  "f"  "t"  "c" ]
 set diametre [expr $init(coeff_diam) * $froid] ; set has_lim_inf [expr int(${diametre}*rand() )  ]
 set limite_inf [expr $cible - $has_lim_inf ] ; set limite_sup [expr $limite_inf + $diametre]
 if { $limite_inf <=0 }  { set limite_inf 1} ; if { $limite_sup >=  $max_n }  {  set limite_sup [expr $max_n -1]  }
 tk_messageBox -message "$nom_autre voici les limites de recherche :\n entre $limite_inf et $limite_sup " -title "[mc {Message cible}]" -icon info -type ok 
 .frame_middle.right.decision configure -state normal
 focus -force  .frame_middle.right.decision
 if { $nom_autre == "ordi"} {
 set flag_f 0 ; set flag_t 0 ; set flag_c 0 ; set flag_r 0 ;set en_avant 1
 set essai [expr int([expr $limite_inf +  $limite_sup ]/2)] ;## autre 1er essai ? hasard ou ennd�but ou en fin ?
##set essai $limite_inf
tk_messageBox -message "Premier essai de ordi\n  $essai " -title "ordi cheche!!" -icon info -type ok 
  .frame_up.n_froid configure -state disabled
 .frame_middle.right.decision insert end $essai
  focus -force .frame_middle.right.decision
   .frame_middle.right.decision configure -state disabled
 ou_suis_je
  return
 }
 .frame_middle.right.decision configure -state normal
 focus -force .frame_middle.right.decision
 }
 
 .frame_up.n_froid configure -state disabled
## .frame_down.message configure -text "Consignes :\n $nom_autre cherche la cible\n $premier dit\n chaud($chaud) tiede($tiede) froid ($froid) rien\n" 
.frame_middle.right.decision configure -state normal
 focus -force .frame_middle.right.decision
 
  return
 }
 
 
########Programme principal####################
####################################### 
##tab tab des cases tab_n celui du serpent premier (choisit cible et contr�le) nom_autre celui qui devine
 proc main {} {
  global init tcl_platform  cible  tab tab_n premier nom_autre nom_elev choix_prem w2 tab_chaleur n_essais font_mess1 font_case font_case_jeu 
  global liste_essais flag_rec max_n progaide chaud tiede froid 
if { $tcl_platform(platform) == "unix" } {
set nom_elev $tcl_platform(user)
set nom_classe [lindex [exec  id -G -n ] 1]
set font_mess1 {helvetica 8 bold} ;  set font_case_jeu {helvetica 15 bold}
set font_case {helvetica 14 bold}
set font_mess2  {helvetica 10 bold}
set font_mess3  {helvetica 11 bold}
} else {
 set nom_elev eleve
 set nom_classe classe
 set font_mess1  {helvetica 10 bold} 
 set font_mess2  {helvetica 10 bold} 
 set font_mess3  {helvetica 11 bold}
  set font_case_jeu {helvetica 22  bold} ; set font_case {helvetica 22 bold}
}
set nom_autre ""
initlog $tcl_platform(platform) $nom_elev
### $nom_autre $premier $pas $choix_prem]nom_elev nom_autre  choix_prem pas w2 premier
 init_qui_deb_cible ; catch {tkwait window ${w2}}
set premier $premier 
set  tab_chaleur(c)  {chaud  green} ; set  tab_chaleur(t)  {ti�de orange}
set  tab_chaleur(f)  {froid red} ; set  tab_chaleur(r)  {rien blue}
if { $premier == $nom_elev } { set nom_autre $nom_autre } else { set  nom_autre $nom_elev }
if { $nom_autre == "ordinateur"  }  {
 set premier ordinateur ; set nom_autre $nom_elev
 } 

set n_essais 0 ; set liste_essais {} ; set flag_rec 0
set largeur $init(width) ; set hauteur  $init(height) 
 . configure -width $largeur -height $hauteur
 ###-width 600   -height 460
wm title . "[mc {Calculs sur  bande. Cache Tampon (Cf Ermel Cycle2)}]"
wm geometry  . +0+0  
wm resizable . 1 1
catch {destroy .frame_up   .frame_middle .frame_down}
set init(wlen)   [ expr [expr $largeur -  225]  /  $init(nbcol) ] 
set init(hlen)  [ expr [expr $hauteur - 140]  /  [expr $init(nbrow)  + 1]] 
#if { $init(wlen)   <=  $init(hlen) }  { set  init(hlen)  $init(wlen) } else {  set init(wlen) $init(hlen) }
#set init(wlen) [expr $init(wlen) + 1 ] ; ###pour adapter taille quadr

if { [expr $init(nbrow)  % 2] !=0 } {set mult [expr [expr $init(nbrow) / 2] +1] ; set reste [expr $init(nbrow) / 2] }  else  {
							set mult [expr $init(nbrow) / 2]  ;  set reste [expr [expr $init(nbrow) / 2]  - 1] }
set max_n [  expr [ expr $mult * $init(nbcol) ] +  $reste  ]

frame .frame_up  -borderwidth 1 -height 90 -width $largeur -background blue
set haut_middle [expr $hauteur - 90 - 50]
 frame .frame_middle -width $largeur -height $haut_middle
 frame .frame_down  -height 50 -width $largeur -background bisque1 
 frame .frame_middle.left -width [expr 100 +10] -height $haut_middle  
 frame .frame_middle.right -width [expr 100 +15] -height $haut_middle  
 canvas .frame_middle.can  -height $haut_middle -width [expr $largeur -100 - 100 -25] -background orange 
##label  .frame_up.pres -text "$premier a choisi une cible ?? et va contr�ler les choix de $nom_autre" -font {helvetica 10} -background bisque
##label  .frame_up.pres1 -text "$nom_autre doit trouver cette cible en essayant e s'en approcher."  -font {helvetica 10}
##label  .frame_up.pres2 -text "$premier lui dira si c'est froid() ou ti�de(orange) ou chaud(rouge)" -font {helvetica 10} -background bisque1
set message "[string range $premier 0 9] a choisi une cible ?? et va contr�ler les choix de $nom_autre.\n\
[string range $nom_autre 0 9] doit trouver cette cible en essayant de s'en approcher.\n[string range $premier 0 9] lui dira si c'est froid(rouge)\
 ou ti�de(orange) ou chaud(vert)\n en tapant la lettre qui convient :\nc : chaud (<=?)  t : -ti�de (<=?)  f : -froid : f(<=?) r :  rien (>?)"
message  .frame_up.consigne -text $message -foreground red  -aspect 500 -relief sunken -font $font_mess2
label .frame_up.chaud -text "[mc {�cart pour chaud :}]" -foreground green4 -background bisque -font $font_mess3
label .frame_up.tiede -text "[mc {�cart pour ti�de  :}]" -foreground orange  -background  bisque  -font $font_mess3
label .frame_up.froid -text "[mc {�cart pour froid   :}]" -foreground red -font $font_mess3  -background bisque  
entry .frame_up.n_chaud -width 3 -font {helvetica 12 bold} -justify center 
entry .frame_up.n_tiede -width 3 -font {helvetica 12 bold} -justify center 
entry .frame_up.n_froid -width 3 -font {helvetica 12 bold} -justify center 
if { $premier == "ordinateur"  }  {
 .frame_up.n_chaud insert end 2 ; set chaud 2
.frame_up.n_tiede insert end 4 ; set tiede 4
.frame_up.n_froid insert end 6 ; set froid 6
.frame_up.n_chaud configure -state disabled
.frame_up.n_tiede  configure -state disabled
.frame_up.n_froid  configure -state disabled
}
button .frame_up.quit -text "[mc {Quitter}]" -command exit
button .frame_up.rec -text "[mc {Recommencer}]" -command recommencer
 .frame_up.rec configure -state disabled
 button .frame_up.aide -text [mc {Aide}]
label .frame_middle.left.nom_cible  -text "[mc {D�cision de}] $premier : " -background bisque -font $font_mess1
label .frame_middle.left.t_decision -text "[mc {C' est :}]" -font $font_mess1
entry  .frame_middle.left.decision -width 2  -justify center -font {helvetica 20 bold} -bd 4 -highlightcolor red -highlightthickness 3 -bg white
##entry  .frame_middle.left.liste_decision -width 15 -justify center 
###pour debug ordi
###.frame_middle.left.decision insert end $cible
listbox .frame_middle.left.liste_decision -background #c0c0c0 -height 13 -width 15 -font $font_mess1 -yscrollcommand " .frame_middle.left.scroll1 set"
scrollbar .frame_middle.left.scroll1 -command ".frame_middle.left.liste_decision yview"

label .frame_middle.right.nom_cible  -text "[mc {Essai de}]\n$nom_autre : " -background bisque -font {helvetica 10 bold} -justify center
entry  .frame_middle.right.decision -width 3 -justify center -font {helvetica 20 bold} -bg white -bd 4 -highlightcolor red -highlightthickness 3 
label .frame_middle.right.t_rep -text "[mc {R�ponse de }]\n$premier : " -background bisque -font $font_mess1
entry  .frame_middle.right.rep  -width 13 -justify center
label .frame_middle.right.t_rep1 -text "[mc {R�ponse corrig�e par l'ordinateur!:}]" -background bisque -font $font_mess1 -justify center
listbox .frame_middle.right.rep1  -background #c0c0c0 -height 13 -width 15 -font $font_mess2 -yscrollcommand " .frame_middle.right.scroll1 set"
scrollbar .frame_middle.right.scroll1 -command ".frame_middle.right.rep1 yview"
message  .frame_down.bilan -text "[mc {Bilan}]" -font {helvetica 12}  -fg red  -aspect 800 -justify center
## pack  .frame_up  .frame_middle .frame_down -side top -fill both
place .frame_up -x 0 -y 0 ; place  .frame_middle  -x 0 -y 90   ; place .frame_down -x 0 -y [expr $haut_middle  + 90]
place .frame_up.chaud -x 0 -y 5 
place .frame_up.n_chaud -x 130 -y 5
place .frame_up.tiede -x 0 -y 30 
place .frame_up.n_tiede -x 130 -y 30
place .frame_up.froid -x 0 -y 55
place .frame_up.n_froid -x 130 -y 55
##pack   .frame_up.consigne 
place  .frame_up.consigne  -x  170   -y 0
place .frame_up.quit  -x [expr $largeur -90]  -y 0
place .frame_up.rec  -x [expr $largeur -120]  -y 30
place .frame_up.aide -x [expr $largeur - 80] -y 65
	    switch $tcl_platform(platform) {
		"unix" {
	bind .frame_up.aide <1>  "exec $progaide  [pwd]/aide/calculs_sur_bandes.html & "
			}
		"windows" {
	    bind .frame_up.aide <1>  "exec $progaide  ${basedir}/aide/calculs_sur_bandes.html & "
		
			}
				}
place   .frame_down.bilan -x  200 -y 0
##pack  .frame_middle.left .frame_middle.can .frame_middle.right -side left
place  .frame_middle.left -x 0 -y 0 ; place .frame_middle.can -x [expr 100 +10] -y 0 ; place .frame_middle.right -x  [expr $largeur -100 -15 ]  -y 0
###pack  .frame_middle.left.nom_cible .frame_middle.left.t_decision   .frame_middle.left.decision  
 place .frame_middle.left.nom_cible -x 5 -y 0
 place .frame_middle.left.t_decision  -x 25 -y 35
 place .frame_middle.left.decision   -x 25  -y 53
 place .frame_middle.left.liste_decision -x 0 -y 100
 
 if { $tcl_platform(platform) == "unix" } {
 place  .frame_middle.left.scroll1 -x 92 -y 100  -height 245 } {
  place  .frame_middle.left.scroll1 -x 92 -y 100  -height 250
 }
 
 place .frame_middle.right.nom_cible -x 5  -y 0 ; place .frame_middle.right.decision -x 5 -y 40
## place .frame_middle.right.t_rep -x 15 -y 53  ; ## place .frame_middle.right.rep -x 5 -y 90
 place .frame_middle.right.t_rep1 -x 0 -y  90 ; place .frame_middle.right.rep1 -x 5 -y 130
if { $tcl_platform(platform) == "unix" } {
 place .frame_middle.right.scroll1 -x 100 -y 130  -height 250 } else {
							
place .frame_middle.right.scroll1 -x 100 -y 130  -height 250
}
fais_grille
for {set y 0  } {$y < $init(nbrow) } {incr y } {
		set reste [expr $y % 4] ; set quot [expr $y / 4]  ; set m [expr $y / 2] ; set k 0 ; set k_deux 0
		 for {set x 0} {$x < $init(nbcol) } {incr x} {
			switch $reste  { 
				 0 { 
				
				  set i [expr [ expr $m * $init(nbcol) ] + $m + $k ]
				 set ne_x  [expr $x * $init(wlen) ] ; set ne_y [expr $y * $init(hlen) ] 
				set tab_n(text_$i)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2 ]    -text $i -font  $font_case  -fill black -tags tag_${i}  ]    
				
				.frame_middle.can  itemconfigure	 $tab(rect${y}_${x})   -fill bisque1 
				 incr k
				  if { $x == [expr $init(nbcol) -1] } {
				  incr i
					set tab_n(text_$i)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2  +$init(hlen)]    -text $i -font $font_case -fill black   -tags tag_${i}  ]   
								incr y 
				
				.frame_middle.can  itemconfigure	 $tab(rect${y}_${x})   -fill bisque1
				set k 0 
				}
				  }
				 
				 2 { 			 
				 set j  [expr [ expr $m * $init(nbcol) ] + $m  +$init(nbcol) - 1 -  $k_deux ]
				   set ne_x  [expr $x * $init(wlen) ] ; set ne_y [expr $y * $init(hlen) ] 
				set tab_n(text_$j)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2 ]    -text $j -font $font_case -fill black -tags tag_${j}  ]    
				 .frame_middle.can  itemconfigure	 $tab(rect${y}_${x})   -fill bisque1
				 incr k_deux
				 if { $x == 0 } {
				  incr j
				set tab_n(text_$j)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2  +$init(hlen)]    -text $j -font $font_case -fill black -tags tag_${j}  ]   
				set z [expr $y +1] 
				
				.frame_middle.can  itemconfigure	 $tab(rect${z}_${x})  -fill bisque1
				set k_deux 1 
				}
				 
				 
				 }
				 default {}
				 }
		 
		 
				 }
 }
	
if {$premier != "ordinateur" } {
 focus -force .frame_up.n_chaud 
 bind .frame_up.n_chaud <Return> "retiens_chaud"
 bind .frame_up.n_chaud <KP_Enter> "retiens_chaud"
 bind .frame_up.n_tiede <Return> "retiens_tiede"
 bind .frame_up.n_tiede <KP_Enter> "retiens_tiede"
 bind .frame_up.n_froid <Return> "retiens_froid"
 bind .frame_up.n_froid <KP_Enter> "retiens_froid"
 } else {  retiens_froid
  ##focus -force .frame_middle.right.decision
 }
 bind .frame_middle.right.decision <Return> "ou_suis_je"
  bind .frame_middle.right.decision <KP_Enter> "ou_suis_je"  
  .frame_middle.left.decision configure -state disabled
bind .frame_middle.left.decision <Return> "valide_pos"
bind .frame_middle.left.decision <KP_Enter> "valide_pos"  
.frame_up.n_tiede configure -state disabled 
  .frame_up.n_froid configure -state disabled
##.frame_middle.right.decision  configure -state disabled
 .frame_middle.right.rep configure -state disabled
##  focus -force  .frame_middle.right.decision
   } ;####fin programme principal
 main
 proc recommencer {} {
 global nom_autre  premier cible essai liste_essais n_essais tab_n flag_rec font_case font_mess1 max_n
 catch { .frame_middle.can delete tags img }
 foreach i $liste_essais {
  .frame_middle.can  itemconfigure $tab_n(text_$i)  -text $i -font $font_case -fill black
	  }
 if { $premier != "ordinateur" } {
 init_wid_ch 
  init_wid_bind
.frame_up.n_chaud  delete 0 end ;  .frame_up.n_tiede delete 0 end
  .frame_up.n_froid  delete 0 end 
  focus -force .frame_up.n_chaud
 } 
  set  liste_essais {}  ;  set n_essais 0 ; set flag_rec 1
  .frame_down.bilan configure -text  "Bilans" -font $font_mess1
  .frame_middle.right.decision delete 0 end 
.frame_middle.left.decision  delete 0 end ; .frame_middle.right.rep delete 0 end 
 .frame_middle.left.liste_decision delete 0 end ; .frame_middle.right.rep1   delete 0 end 
if { $premier == "ordinateur"  } {   set cible  [expr int($max_n*rand() )]  ; retiens_froid  }
   }
 proc init_wid_ch { } {
 .frame_up.n_chaud configure -state normal
  .frame_up.n_tiede configure -state normal
  .frame_up.n_froid configure -state normal
.frame_middle.right.decision configure -state normal
  .frame_middle.right.rep configure -state normal
  .frame_up.n_chaud delete 0 end ; .frame_up.n_tiede delete 0 end ; .frame_up.n_froid delete 0 end
  .frame_middle.right.decision   delete 0 end     ;  .frame_middle.right.rep delete 0 end 
    .frame_middle.left.decision  delete 0 end
  .frame_up.n_tiede configure -state disabled
  .frame_up.n_froid configure -state disabled
.frame_middle.right.decision  configure -state disabled
 .frame_middle.right.rep configure -state disabled
  .frame_middle.left.decision  configure -state disabled
 }
 proc  init_wid_bind { } {
  bind .frame_up.n_chaud <Return> "retiens_chaud"
   bind .frame_up.n_tiede <Return>  "retiens_tiede"
   bind .frame_up.n_froid <Return> "retiens_froid"
 }
 proc retiens_chaud {}  {
 global chaud
 set chaud  [.frame_up.n_chaud  get]
 if { ![regexp {^[0-8]$}  $chaud]  }  {
 	tk_messageBox -message "[format [mc {La valeur %1$s pour chaud ne convient pas. car >= 8 ou non conforme}] $chaud]" -title "[mc {Message chaud}]" -icon info -type ok 
	.frame_up.n_chaud delete 0 end
 } else { 
  .frame_up.n_chaud configure -state disabled
 .frame_up.n_tiede configure -state normal
 focus -force .frame_up.n_tiede ; bind .frame_up.n_chaud <Return> "" 
 ## retiens_tiede
  return
 }
 }
 
 proc retiens_tiede {} {
 global chaud tiede nom_autre

 set tiede [.frame_up.n_tiede  get]
 if { ![regexp {^\d$}  $tiede]  || $tiede <=  $chaud  || [expr  ![string compare  $nom_autre  "ordi"]   &&  [expr   $tiede  > [expr 3 * $chaud] ] ] }  {
tk_messageBox -message "[format [mc {La valeur %1$s pour ti�de ne convient pas.car <= %2$s ou non conforme.ou pour ordi ti�de trop grand!!}] $tiede $chaud ]" -title "[mc {Message tiede}]" -icon info -type ok 
	.frame_up.n_tiede delete 0 end
 }  else {
  .frame_up.n_tiede configure -state disabled
  .frame_up.n_froid configure -state normal
focus -force .frame_up.n_froid
 bind .frame_up.n_tiede <Return> "" 
 ## retiens_froid 
 return
 }
 }
 
 proc ou_suis_je {} {
  global nom_autre  premier cible essai liste_essais n_essais font_case_jeu max_n basedir
 set essai [ .frame_middle.right.decision get]
  if { ![regexp {^\d+$} $essai ]  || $essai >=  $max_n } {
  .frame_middle.right.decision delete 0 end
  } else {
##  .frame_middle.right.decision delete 0 end
if { $premier == "ordinateur" } { valide_pos ; return}
.frame_middle.left.decision configure -state normal
 focus -force .frame_middle.left.decision 
.frame_middle.right.decision configure -state disabled
  }
  }
  proc valide_pos {} {
  global nom_autre  premier cible chaud tiede froid  tab_n essai chaleur tab_chaleur n_essais liste_essais  font_case_jeu font_mess1
  global limite_inf  limite_sup tab_essais flag_f flag_t flag_c basedir
 if { $premier == "ordinateur" } {
		 set rep "c" }  else  {
 set rep  [.frame_middle.left.decision get ] }
  if { ![regexp {(^[ctfr]$)} $rep ] } {
	 .frame_middle.left.decision delete 0 end
	 focus -force .frame_middle.left.decision
		 }  else  { 
		if { [expr int(rand()*3) ]  ==  1  &&  $premier != "ordinateur" }  {	 
		set sur [ tk_messageBox -message "[format [mc {%1$s : Est tu s�r(e) de ta r�ponse %2$s}] $premier $rep ]" -title "[mc {Degr� de conviction ?}]" -icon info -type yesno ]
	if { $sur == "no" } { 
	tk_messageBox -message "[mc {Tu peux modifier ta r�ponse}] $rep. " -title "[mc {Test conviction}]" -icon info -type ok 
	.frame_middle.left.decision delete 0 end 
	focus .frame_middle.left.decision  ; return
	}
	}				   
  set ecart [expr abs([expr $cible - $essai])] ; set liste_essais [linsert $liste_essais end $essai]
        if { $ecart == 0 } {
	tk_messageBox -message "$nom_autre [mc {c'est BIEN Tu as atteint la cible en}] [expr $n_essais +1] [mc {essai(s)}]." -title "[mc {Message justesse cible}]" -icon info -type ok
	set chaleur "c" ;  bell ; bell ; clignote   $tab_n(text_$essai)  2 
	  .frame_middle.can  itemconfigure $tab_n(text_$essai)   -font {helvetica 34 bold} 
.frame_middle.can create image  200 100     -image [image create photo -file [file join  $basedir images bien.png]]  -tags img	  
	  
	  
.frame_down.bilan configure -text  "[format [mc {Bilans:Cible %1$s atteinte par %2$s en}] $cible $nom_autre] [expr $n_essais + 1] [mc {essai(s)}].\n [mc {liste des essais :}] $liste_essais" \
		-font $font_mess1
	.frame_up.rec configure -state normal
	.frame_middle.right.rep delete 0 end
	.frame_middle.right.rep insert end "$cible [mc {est  BON}]"
	.frame_middle.right.rep1 insert end "$cible [mc {est  BON}]"
	  return
	}
  
  
	if { $ecart <= $chaud } {
			set chaleur "c" } elseif { $ecart <= $tiede } {
								set chaleur "t" } elseif { $ecart <= $froid } {
											 set chaleur "f"} elseif  { $ecart > $froid} {
											set chaleur "r" }
											
	if { $premier == "ordinateur" } {set rep $chaleur }										
	if { $rep != $chaleur } {
	tk_messageBox -message "[format [mc {%1$s ta proposition %2$s est fausse.C'est %3$s qui est juste}] $premier $rep $chaleur ]" -title "[mc {Message justesse chaleur}]" -icon info -type ok
	}

	 .frame_middle.can  itemconfigure $tab_n(text_$essai)  -fill [lindex $tab_chaleur($chaleur) 1] -font $font_case_jeu 
##	 clignote   $tab_n(text_$essai)  2
if { $premier != "ordinateur" } {
	.frame_middle.left.liste_decision insert end  "$essai $rep [mc {puis}] [lindex $tab_chaleur($chaleur) 0]" 
	 .frame_middle.left.decision delete 0 end
	}
	
	.frame_middle.right.rep  delete 0 end
	 .frame_middle.right.rep insert end  "$essai [mc {est}] [lindex $tab_chaleur($chaleur) 0]" 
	.frame_middle.right.rep1	insert end  "$essai [mc {est}] [lindex $tab_chaleur($chaleur) 0]"
        .frame_middle.right.decision configure -state normal
	 .frame_middle.right.decision delete 0 end
	  incr n_essais
	  if { $nom_autre == "ordi" && $n_essais  == 1}  {
## essai fait passer au 2ieme
##	  incr n_essais
	  set tab_essais(1) [list $essai $chaleur]
	  .frame_middle.right.decision insert end [ordi_cherche  $limite_inf  $limite_sup $chaleur $essai]
	  }
	 if { $nom_autre == "ordi" && $n_essais > 1}  { 
	  .frame_middle.right.decision configure -state normal
	  set tab_essais($n_essais) [list $essai $chaleur]
	 .frame_middle.right.decision insert end [ordi_cherche  $limite_inf  $limite_sup $chaleur $essai]
##	 .frame_middle.right.decision configure -state disabled
					 }   
				 
	 focus -force .frame_middle.right.decision 
	  .frame_middle.left.decision configure -state disabled
	  
	 ou_suis_je ; return
	}			  
	}	  
##Ordi cheche la cible (remplace ou suis je)
#############################
proc ordi_cherche {lim_inf lim_sup chal ess} {
global n_essais liste_essais max_n tab_essais chaud tiede froid flag_f flag_t flag_c  flag_r en_avant init diametre liste_chaleurs
global limite_inf limite_sup
##set tab_essais(1) [ list $ess $chal ]
## debug.frame_middle.right.rep1	insert end "$tab_essais($n_essais)  $liste_chaleurs "
		if { $n_essais == 1} {
		switch $chal {
				"r" { return [expr $ess + $froid] }
				"f" { return [expr $ess + $froid -$tiede] }
				"t" { return [expr $ess + $tiede -$chaud] }
				"c" { return [expr $ess + 1] }
		}
		
		}

		if  { $n_essais > 1} { 
					if { $chal == "r" && $ess >= $limite_sup } {
					set essai [ expr $ess - [expr $diametre - 2 * $froid] ]
					return $essai  
					}
					if { $chal == "r" && $ess  <= $limite_inf} {
					set essai [ expr $ess + [expr $diametre - 2 * $froid] ]
					return $essai  } 
					}
					
					set ch_prec  [lindex  $tab_essais([expr $n_essais - 1]) 1] 
					set ess_prec  [lindex  $tab_essais([expr $n_essais - 1])  0] 
					if { $ess >= $max_n } {  return [expr $max_n - 1] }
					if { $ess <= 0 } {  return 1}
					
			switch  $en_avant   {
			"1"  {
			if { $chal == "r" }  {
					  if  { [lsearch $liste_chaleurs $chal]  <=  [lsearch $liste_chaleurs $ch_prec ] }   { 
						  set en_avant 0
						     set essai [expr $ess_prec - 1]  ; return $essai
						     }  else {
						      set essai [expr $ess + 1]
						       return $essai
						     		 }
						}														
			
					 if { $chal == "f"  } {
					if { [lsearch $liste_chaleurs $chal]  <  [lsearch $liste_chaleurs $ch_prec ] }  { ;##de t ou c vers f
						  set en_avant 0
						     set essai [expr $ess_prec - 1]  ; return $essai
						     }  else  {
						      set essai [expr $ess + [expr $froid - $tiede]]
						       return $essai
						     		 }
								 }
					if { $chal == "t"  } {	
					
					if  { [lsearch $liste_chaleurs $chal]  <  [lsearch $liste_chaleurs $ch_prec ] }  { ;##de c vers t
						  set en_avant 0
						     set essai [expr $ess_prec - 1]  ; return $essai
						     } else {
						      set essai [expr $ess + [expr $tiede - $chaud]]
						       return $essai
						     		 }	
					
						}
					
					if { $chal == "c"  } { ; ##on avance ds chaud
					set essai [expr $ess + 1]
					return $essai
						}					
						
					}
		
			"0"  {
				if { $chal == "r" }  {
					set essai [expr $ess - 1]
					return $essai								
						}
				if { $chal ==	"f" }  {
					if { [lsearch $liste_chaleurs $chal]  <  [lsearch $liste_chaleurs $ch_prec ] } { ;##de c vers t
						  set en_avant 1
						     set essai [expr $ess_prec + [ expr [expr $tiede - $froid] ] ] ; return $essai
						     } else {
						      set essai [expr $ess - [expr $froid -$tiede ]]
						       return $essai
						     		 }																										
							}
				if { $chal ==	"t" }  {
					if { [lsearch $liste_chaleurs $chal]  <  [lsearch $liste_chaleurs $ch_prec ] } { ;##de c vers t
						  set en_avant 1
						     set essai [expr $ess_prec + [ expr [expr $chaud - $tiede]  ] ] ; return $essai
						} else {
						set essai [expr $ess -  [expr $tiede - $chaud] ]
						return $essai	
						}
					
						}
				if { $chal ==	"c"  } {
					set essai [expr $ess - 1]
					return $essai	
						
						}
			
			
			
			}
}		
}	

	
proc clignote {  qui nclic} {
global tab_n chaud tiede froid tab_chaleur chaleur font_case
 for  {set cl 1} {$cl <= $nclic} {incr cl} {
after 200
.frame_middle.can itemconfigure $qui   -font $font_case  -fill yellow 
 update
after 200
 .frame_middle.can itemconfigure  $qui -font $font_case   -fill [lindex $tab_chaleur($chaleur) 1] 
update
}

}



