#!/bin/sh
#pluie.tcl   ESSAI DE RECONSTRUCTION D'UN Logiciel IPT (auteur ?)
#\
exec wish "$0" ${1+"$@"}
###########################################################################
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier: 
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean-Louis Sendral
#  @modifier
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
set basedir [file dir $argv0]
cd $basedir
 source calculs.conf
source i18n

source msg.tcl
source fonts.tcl
 source path.tcl
#bind . <F1> "showaide { }"
set basedir [pwd] ; cd $basedir
 set basedir [pwd]
set fich [lindex $argv 0 ]
set DOSSIER_EXOS1  [lindex $argv 5]
##cd  $DOSSIER_EXOS1
set f [open [file join $DOSSIER_EXOS1 $fich] "r"  ]
set listdata [gets $f] ;#init de la liste des couples.
#set fich $f
#"set fich [file tail $f]
close $f
proc vieux_fichier {expr} {
set e1 [regsub -all  {\/}  $expr ":" var]
set e2 [regsub -all  {\*} $var "x" var1 ]
set e3 [regsub -all  {\.} $var1 "," var2 ]
return $var2
}
 set listcouples [vieux_fichier [lindex $listdata 1]]

if { $listcouples  == {} } {
 tk_messageBox -message [mc {mess_list3}] -title "[mc {Message mauvaise liste}]" -icon error -type ok
exit  }
foreach el $listcouples {
 if { ![ regexp {^[0-9]*\,?[0-9]*$} $el ]  } {
 tk_messageBox -message [mc {mess_list4} ] -title "[mc {Message mauvaise liste}]" -icon error -type ok
 exit
 }
 }
 
 global sysFont
##source msg.tcl
##source fonts.tcl
##source path.tcl
 set bgcolor orange
set bgl #ff80c0
wm geometry . +0+0
. configure -background $bgcolor
##wm resizable . 0 0
wm  title . "[mc {calculs approch�s}]-Sous GPL"
set date [clock format [clock seconds] -format "%H%M%D"]

frame .frame -width 640 -height 640 -background $bgcolor


#bframe pour les calculs (bframe.res)
frame .bframe -background $bgcolor -relief raised -bd 3 -height 6
pack .bframe -side bottom -fill both
label .bframe.lab_consigne -background $bgcolor -text [mc {mess_calc_appr}]
grid .bframe.lab_consigne -padx 20 -column 1 -row 1
button .bframe.but_calcul -text [mc {lancer_calc}] \
-command ".bframe.but_calcul configure -state disable ; calcul"
grid .bframe.but_calcul -column 3 -row 1 -padx 10
entry .bframe.res -width 18 -justify center -textvariable rep  -font {Arial 20}
grid .bframe.res -column 2 -row 1 ; 
#essai de bind control� par return, mais ne sert � rien pour l'instant"
#bind .bframe.res <Keypress-Return>  [set rep1 [.bframe.res get]] ;#[set rep1 $rep]

#mframe pour le nom et le score et les tests de deboggage !!
frame .mframe -background $bgcolor -relief raised -bd 3
pack .mframe -side bottom -fill both 
#entry .mframe.ent_nom -width 20
#grid .mframe.ent_nom -column 1 -row 1 -sticky e
#label .mframe.lab_nom  -text "est   ton nom " -background #ffff80  -anchor e
#grid .mframe.lab_nom -column 2 -row 1
set nom_elev  [lindex $argv 1 ] ;#init nom �l�ve et nom clase
set nom_classe [lindex $argv 2 ]
initlog  $tcl_platform(platform) $nom_elev
label .mframe.lab_nom  -text "[mc {bonjour}] $nom_elev" \
         -background $bgcolor -anchor e
  #AC# column 1 par 5
  grid .mframe.lab_nom -column 1 -row 1
#cd $basedir
proc reviens_oper {expr} { 
 set e1 [regsub -all  {\,}  $expr "." var]
set e2 [regsub -all  {\:}  $var "\/" var1]
 set e3 [regsub -all  {x}  $var1 "*" var2]
 return $var2
 }
set oper [lindex  $listdata 0]
set n_lig [lindex  $listdata 2]
#set n_col [lindex  $listdata 3]
set n_col 1 
if { $n_lig > 17 } {
set n_lig 17
}
proc traite_expr { expr} {
if { [string last + $expr] != -1  || [string last - $expr] != -1  \
 || [string last * $expr] != -1  || [string last \\ $expr] != -1 } {
return "\(${expr}\)"
} else {
return $expr
}
}
proc traite_expr1 {expr} {
 if { [string last "\(" $expr ] == -1  } {
 return $expr
    } else {
	return [expr $expr]
	}
}

#hframe pour la tomb�e des expressions.  par grid
frame .hframe -background $bgcolor
pack .hframe -side bottom -fill both
set j 1
for {set i 1} {$i <= $n_lig} {incr i 1} {
           label .hframe.lab_bilan$i$j -text "_________"  -background $bgcolor -font {Arial 20}
        grid .hframe.lab_bilan$i$j -column $j -row $i
    }

label .hframe.rep -text "____________\n____________" -font {Arial 10}  -background $bgcolor ; place .hframe.rep  -x 0 -y 200
#grid .hframe.rep -column [expr $n_col + 4] -row 6 ;# essai pour les bad calculs
message .hframe.rep_bon -text "                   \n                 " -background $bgcolor 
grid .hframe.rep_bon -column 3 -row 1 -padx 20 
set icone_b	[image create photo -file [file join  $basedir images bien.png ]   ]
##label .hframe.bien -image $icone_b -background $bgcolor
set icone_m	[image create photo -file [file join  $basedir images mal.png]   ]
#"set listdata {+ {1 2 5 3 14 5 7 8 4 5 1 5 14 5 23 5 4 15 8 9 0 3 4 5  78 1}}
#init des couples de nombre en jeu, de l'op�ration, des couples au d�part
#des bons calculs et des mauvais

 set oper [ vieux_fichier [lindex $listdata 0]]
set ndepart [expr int($n_col / 2) ]
#set ndepart 1; #< nbre colonnes -1  
##set listcouples [lindex $listdata 1]
set uu [llength $listcouples]
set nbrecouples [expr $uu /2]
    set nx [traite_expr [lindex [lrange $listcouples 0 1] 0] ]
    set ny [ traite_expr [lindex [lrange $listcouples 0 1] 1]]
    set ii [expr ($i /2) + 1]
.hframe.lab_bilan11  configure -text "${nx}${oper}${ny}"  -background $bgcolor -font {Arial 20}
if {[reviens_oper [lindex  $argv 4]] == {} || [reviens_oper [lindex  $argv 4] ] >=  100000 } {
set approx 5
   } else {
set approx [reviens_oper [lindex $argv 4]]
}
if { ![regexp {^[0-9]+\.?[0-9]*$} $approx ] } {
set approx  5  } else {
set approx  $approx
}
set listcouples [lrange $listcouples 2 end]
set nbrecouplesrestant [expr $nbrecouples - 1 ]
set indexcol 2
#attention � ne pas d�passer $depart"
set score 0 ; set echec 0 ; set reussite 0 ; set mrep 0 ; set bonneliste {} ; set echecliste {} ;
set tempo [lindex $argv 3] ; set drapeau 1 ; set drapeau_bon 0 ; set bon_lig 0   ; set troptard {}
if { ![regexp {^[0-9]+$} $tempo ] } {
set tempo 1000  } 

#"destroy .hframe  #" de foreach
proc memorise {} {
global input
set input [.bframe.res get]
.bframe.res delete 0 end
}
proc solution { op b_lis t_lis e_lis app nom fich date } {
global env tcl_platform w2
catch {destroy .top2 }
set w2 [toplevel .top2]
wm geometry $w2 +250+0
wm title $w2 [mc {bil_bon_calc_app}]
###grab ${w2}
text ${w2}.text -yscrollcommand "${w2}.scroll set" -setgrid true -width 60 -height 15 \
 -wrap word -background black -font   {Helvetica  12}
scrollbar ${w2}.scroll -command "${w2}.text yview"
pack ${w2}.scroll -side right -fill y
pack ${w2}.text -expand yes -fill both
${w2}.text tag configure green -foreground green
${w2}.text tag configure red -foreground red
${w2}.text tag configure yellow -foreground yellow
${w2}.text tag configure normal -foreground black
${w2}.text tag configure white1 -foreground white    -font  {helvetica 16}
${w2}.text tag configure white -foreground white -underline yes
proc marge {} {
global w2
${w2}.text insert end  "   "
}
  ${w2}.text insert end  "   le $date, exercice : $fich\n"     red
${w2}.text insert end   "[mc {resum}] $nom  :\n"    white1
marge ; ${w2}.text insert end  "[mc {bon_calc_app}] $app [mc {pres}] :\n"   white
foreach cal $b_lis {
##${w2}.text insert end "  Ton calcul : [lindex $cal 0] $op [lindex $cal 1] donne [lindex $cal 2] \
##On a bien \([lindex $cal 0] $op [lindex $cal 1]\) \- [lindex $cal 2] < $app\n" green

 if {  [expr 1.0 * [reviens_oper "[lindex $cal 0] $op [lindex $cal 1]" ] ] > [reviens_oper [lindex $cal 2]] } {
 set sup  [expr 1.0 * [reviens_oper "[lindex $cal 0] $op [lindex $cal 1]" ]] ; set inf [reviens_oper [lindex $cal 2] ]
 } else {
set inf  [expr 1.0 * [reviens_oper "[lindex $cal 0] $op [lindex $cal 1]" ]] ; set sup [reviens_oper [lindex $cal 2] ]
}
marge
${w2}.text insert end "[format [mc {Ton calcul %1$s %2$s %3$s donne %4$s . On a bien (%6$s  - \
%7$s )<= %5$s}]  [lindex $cal 0] $op [lindex $cal 1] [lindex $cal 2] $app $sup $inf ]\n" green
}
${w2}.text insert end "\n"
marge ; ${w2}.text insert end  "[mc {mauv_calc_app}] $app [mc {pres}] :\n"   white
foreach cal $e_lis {
 if { [ expr [reviens_oper "[lindex $cal 0] $op [lindex $cal 1]" ]] >  [reviens_oper [lindex  $cal 2]]} {
 set sup [expr [reviens_oper "[lindex $cal 0] $op [lindex $cal 1]" ]] ; set inf [reviens_oper [lindex $cal 2] ]
 } else {
set inf [expr [reviens_oper "[lindex $cal 0] $op [lindex $cal 1]" ] ]; set sup [reviens_oper [lindex $cal 2] ]
}
marge ; ${w2}.text insert end "[mc {ton_idee}] [lindex $cal 2] [mc {proche}] ([lindex $cal 0] $op [lindex $cal 1]\)\n \
		Or, ${sup} - ${inf} [mc {n_pas}] <= � $app\n" red
##### \([lindex $cal 0] $op [lindex $cal 1]\) \- [lindex $cal 2]  [mc {n_pas}] < � $app\n\n" red
}
${w2}.text insert end "\n"
marge ; ${w2}.text insert end  "[mc {trop_tard}]\n"   white
foreach cal $t_lis {
${w2}.text insert end "  $cal\n" yellow
}
 if  {  $tcl_platform(platform)  == "unix"  }  {
###after 1000
set rep [tk_messageBox  -message "[mc {imprim}]" -type yesno -icon question  -default no -title Imprimer? ]
 if {  $rep  == "yes"  }  {
### set  t    [ ${w2}.text  get  0.0  end  ]
 exec  lpr   <<  [ ${w2}.text  get  0.1  end  ]
 ###  $b_lis  $e_lis
 ### ${w2}.text  get  0 end  ]
 }
 }
raise ${w2} .frame
}
proc non_doublons { liste} {
if { $liste == {} } {
return $liste
} else {
if { [lsearch [lrange $liste 1 end] [lindex $liste 0]] == -1 } {
    return [ list [lindex $liste 0] [ non_doublons [lrange $liste 1 end]] ]
   } else {
     return [non_doublons [lrange $liste 1 end]]
    }   
}
}




bind .bframe.res <Return> "memorise"
bind .bframe.res <KP_Enter> "memorise"
set input 100000

##############################################################################################
# programme principal avec usage pseudo recursif. R�le de after pas tr�s clair encore
# A l'arr�t : travail sauv� ds un fichier qui doit exister
##############################################################################################
proc calcul {} {
global fich tempo nom_elev n_col n_lig listcouples nbrecouples score echec avreu reussite  approx  bgcolor  \
mrep nbrecouplesrestant indexcol oper input bonneliste echecliste date drapeau bon_lig bon_col drapeau_bon   troptard
global icone_m icone_b
focus -force .bframe 
catch { destroy .hframe.bien .hframe.mal }
#"update
        if  { ( $echec == $nbrecouples ) || \
( $reussite >= $nbrecouples ) || \
( [expr $echec + $reussite] >= $nbrecouples )} {
#focus -force .mframe.ent_bilan
##$nom_elev, [mc {reussite}] $reussite calcul(s)." 
label .mframe.lab_bilan  -text \
"[format [mc {%1$s Tu as reussi � %2$s calcul(s)}] $nom_elev $reussite]" -background $bgcolor  -anchor e
    grid .mframe.lab_bilan -column 5 -row 1 -padx 10
    set mrep_approx [list $approx $echecliste]
 button .bframe.sol -text [mc {R�sum� du travail ?}] -foreground yellow \
-command "solution $oper {$bonneliste} {$troptard}  {$echecliste} $approx $nom_elev [file tail $fich]  $date"
 grid .bframe.sol -column 4 -row 1
button .bframe.bis -text [mc {Recommencer ? }] -foreground blue  -command "recommencer "
grid .bframe.bis -column 5 -row 1
    sauver $date $nom_elev $fich $oper $bonneliste $troptard  $mrep_approx
  .bframe.but_calcul configure -text [mc {quitter}] -state active -command { exit }
return 1
        }
          .hframe.lab_bilan${n_lig}1 configure -text "_________" -background $bgcolor -font {Arial 20}

        if  { ($nbrecouplesrestant != 0 ) && ($indexcol <= $n_col && $drapeau )  } {
            set acal [ lrange $listcouples 0 1 ]
            set listcouples [lrange $listcouples 2 end ]
            set nx [traite_expr [lindex $acal 0]] ; set ny [traite_expr [lindex $acal 1] ]
		if { ${bon_lig} != 1 && $drapeau_bon == 1 } {
.hframe.lab_bilan1${indexcol}  configure -text "$nx$oper$ny"  -background $bgcolor -font {Arial 20}
} else {
.hframe.lab_bilan1${indexcol}  configure -text "$nx$oper$ny"  -background $bgcolor -font {Arial 20}
}
           set indexcol 1  
            set nbrecouplesrestant [expr $nbrecouplesrestant - 1]
		set drapeau 0
} ;
if { $drapeau_bon == 1} {
if { ${bon_lig} !=1 } {
.hframe.lab_bilan${bon_lig}${bon_col}  configure -text "_________" -background $bgcolor -font {Arial 20}
#set drapeau_bon 0
#} else {
#.hframe.lab_bilan${bon_lig}${bon_col}  configure -text "BON" -background $bgcolor -font {Arial 20}
set drapeau_bon 0
}
}

set avreu $reussite ; avancer  ;
after $tempo  calcul     
 }
### gestion des calculs de l'avan��e...
proc avancer {} {
global rep1 n_lig n_col bon_lig bon_col drapeau_bon listcouples nbrecouplesrestant oper approx avreu reussite echec mrep score bonneliste echecliste \
troptard  indexcol input bgcolor icone_m icone_b
focus  .bframe.res ; set drapeau_bon 0
set rep1 $input
### [.bframe.res get]
if  {![ regexp {^0?[0-9]+\,?[0-9]*$} $rep1 ] == 1 || [ regexp {(^0[8-9]+$)} $rep1   ] } {
       set input 100000  ;    set rep1 {}
}

###if {[regexp {(\(?\d*[+|\-|*|\/]{0,1}\d*\)?)[+|\-|*|\/](\(?\d*[+|\-|*|\/]{0,1}\d*\)?)} $rep1 tout prem deux]  } {
###            .bframe.res delete 0 end ; set rep1 {}
##}
# receuil de la r�ponse de l'�l�ve. La dur�e de la frappe d�pend de after 1500
# pour chaque case jusqu'� la ligne n_lig-1 faire :
for  {set col 1 } {$col <= $n_col } {incr col } {
    for  {set lig [expr $n_lig - 1]} {$lig > 0 } {incr lig -1} {
        set val [.hframe.lab_bilan${lig}${col} cget -text] ;# ce qu'il y a dans la case!
if { [ regexp {^([0-9]*\,?[0-9]*)[\+|\-|x|\:]([0-9]*\,?[0-9]*)$} $val tout prem deux ] } {		

            set nx  [traite_expr $prem ]
            set ny  [ traite_expr $deux ] ; set inter [expr  [expr 1.0 * [reviens_oper $val ]]]
##[expr $nx $oper $ny]
           if { $input == {}  } {
#pour pouvoir comparer
set input 100000 ; .hframe.rep configure -text "____________\n____________"  
		}		
set evaluation [ expr abs([expr $inter - [reviens_oper $input ] ]) ]
              if  { $evaluation <= $approx } { ; #bonne r�ponse.
           set drapeau_bon 1 ;
##	    .bframe.res delete 0 end  ;# on efface la bonne r�ponse de l'input.



            incr score ; incr reussite ; #" .mframe.bil insert end $reussite
set bonneliste [linsert  $bonneliste end [list $prem $deux $rep1 ]]
##set bonneliste [ linsert  $bonneliste end $tout]
set bon_lig $lig ; set bon_col $col
.hframe.rep configure -text "____________\n____________"
 if { $bon_lig == 1 } {
.hframe.lab_bilan2${col}  configure -text "[mc {exact}] � $approx [mc {pres}]" -background #ffffff -font {Arial 20}
clignote "[mc {exact}]  $approx [mc {pres}]" 2   ${col}  2
.hframe.lab_bilan${lig}${col}  configure -text "_________" -background $bgcolor -font {Arial 20} -foreground black
} else {
.hframe.lab_bilan${lig}${col}  configure -text "[mc {exact}]  $approx [mc {pres}]" -background #ffffff -font {Arial 20}
clignote "[mc {exact}]  $approx [mc {pres}]" ${lig}   ${col}  2
.hframe.lab_bilan${lig}${col}  configure -text "_________" -background $bgcolor -font {Arial 20} -foreground black
}
label .hframe.bien -image $icone_b -background $bgcolor
grid .hframe.bien -row ${lig}  -column 3 -rowspan 3

if  { ($nbrecouplesrestant != 0) && ([string compare .hframe.lab_bilan1${col} "_________" ] != 0 ) } {
    set acal [ lrange $listcouples 0 1 ]
    set listcouples [lrange $listcouples 2 end ]
    set nx1 [traite_expr [lindex $acal 0]] ; set ny1 [traite_expr  [lindex $acal 1] ]
    set nbrecouplesrestant [expr $nbrecouplesrestant - 1]
    .hframe.lab_bilan1${col} configure -text "${nx1}${oper}${ny1}" -background $bgcolor -font {Arial 20}
}
 set input 100000
  break
#".hframe.lab_bilan${lig}${col}  configure -text "___ " -background $bgcolor -font {Arial 14}
} else {
 #mauvaise r�ponse, on fait descendre! l'expression.
    set k [expr $lig + 1]
.hframe.lab_bilan${k}${col} configure -text "${val}" -background $bgcolor -font {Arial 20}
.hframe.lab_bilan${lig}${col}  configure -text "_________" -background $bgcolor -font {Arial 20}
if { $input == 100000} {
#fausse mauvaise r�ponse (100000)
.hframe.rep configure -text "____________\n____________" 
}  else {
label .hframe.mal -image $icone_m -background $bgcolor
grid .hframe.mal -row ${lig} -column 3 -rowspan 3
.hframe.rep configure -text [mc {mauv_approx}] -font {Arial 10} -background  red
   # on efface la mauvaise r�ponse de l'input !.
   
set echecliste [ linsert $echecliste end [list $prem $deux $rep1]]
 ##set echecliste [ linsert $echecliste end $tout ]
set rep1 100000 ; incr mrep
}
	}
}
}
}
#il reste la derni�re ligne : les trop tard !


for  {set t 1} {$t <= $n_col} {incr t} {
    set val [.hframe.lab_bilan${n_lig}${t} cget -text]
	if { [ regexp {^([0-9]*\,?[0-9]*)[\+|\-|x|\:]([0-9]*\,?[0-9]*)$} $val tout prem deux ] } {	
 
            .hframe.lab_bilan${n_lig}${t} configure -text [mc {trop_tard1}] -background #ffffff -font {Arial 20}
        	    #"clignote "Perdu" $n_lig $t 10 ;
            incr echec
#			label .hframe.mal -image $icone_m -background $bgcolor
#			grid .hframe.mal -row [expr ${n_lig} - 2] -column 3 -rowspan 3
#.hframe.lab_bilan${n_lig}${t} configure -text [mc {Perdu}] -background #ffffff -font {Arial 20}
    set nx  $prem
    set ny  $deux
if { $input != 100000 } {

 set troptard [ linsert $troptard end $tout ]
} else {

 set troptard  [ linsert $troptard end $tout]
##[list $prem $deux]
}
     if  { $nbrecouplesrestant != 0  } {   ;#&& ($t != $indexcol)
        set acal [ lrange $listcouples 0 1 ]
        set listcouples [lrange $listcouples 2 end ]
        set nx [traite_expr [lindex $acal 0] ] ; set ny [traite_expr [lindex $acal 1]]
        .hframe.lab_bilan1${t}  configure -text "$nx${oper}$ny"  -background $bgcolor -font {Arial 20}
        set nbrecouplesrestant [expr $nbrecouplesrestant - 1]
     }
}

}
#".mframe.bil insert 0 [list "$reussite u" $echec] ;# pour tester
# si le nbe de reussites est inchang� c'est une mauvaise r�ponse sinon bonne r�ponse.
#essai de mentionner un mauvais calcul : non concluant !
if  { $avreu == $reussite && [reviens_oper $input ] != 100000 } {
#     set mrep [expr $mrep +1 ] ;
##       incr mrep 
 set input 100000
 ##      .hframe.rep configure -text "${mrep} [mc {mauv_approx}](s)" -font {Arial 10} -background red
          } else  {
    .hframe.rep configure -text "____________\n____________"
}
return 1
}
proc clignote {texte lig col  nclic} { ; #after supprim� car semble pos� des pbs de // avec l'autre after
#"   if  { $nclic ==1 } {.hframe.lab_bilan${lig}${col}  configure -text "______"  -background #ffffff -font {Arial 10} ; return 1}
global bgcolor
for  {set cl 1} {$cl <= $nclic} {incr cl} {

after 200
.hframe.lab_bilan${lig}${col}  configure -text "$texte"  -background $bgcolor -font {Arial 20} -foreground green
update
after 200
.hframe.lab_bilan${lig}${col}  configure -text "$texte"  -background $bgcolor -font {Arial 20} -foreground red
update
update
#"  after 50  "clignote $texte $lig $col [expr $nclic - 1 ]" ;
#".hframe.lab_bilan${lig}${col}  configure -text "___________________3
##2"  -background #ffffff -font {Arial 20}
}
}

# sauver les r�sultats
proc sauver {dat nom fic oper listereussite troptard mrep_approx } { ; # � s�curiser
global savingFile nom_classe TRACEDIR LogHome  user choix_lieu approx DOSSIER_EXOS1 basedir
set TRACEDIR $LogHome
cd $TRACEDIR
#set types {
#    {"Cat�gories"		{.txt}	}
#}
#catch {set file [tk_getSaveFile -filetypes $types]}
#set f [open [file join $file] "a"]
#set travail  "date : $dat nom : $nom fichier : $fic $oper bon : $listereussite echec : $listechec  mauvais cal : $badcal"
set basefile [file tail $fic]
set ou [lrange [ split $DOSSIER_EXOS1 "/" ]  end-1 end]
if { [lsearch -exact $ou "C:" ]  >= 0 } {
set ou [ lreplace $ou  [lsearch -exact $ou "C:" ] [lsearch -exact $ou "C:"  ] "C" ]
}
##"$dat&$nom&{[file tail ${fic}] de $ou}&$oper&{$liste_rep}&$listetard&$listechec&un"
set savingFile [file join $basedir data  ${nom_classe}.bil ]
set travail  "$dat&$nom&{[file tail ${fic}] de $ou} &$oper&{$listereussite}&$troptard&$mrep_approx&ap"
catch {set f [open [file join $savingFile] "a"]}
puts $f $travail
close $f
#set savingFile ${nom_classe}.bil
#  catch {set f [open [file join $savingFile] "a"]}
#  set travail  ":$dat:$nom:${basefile} de $choix_lieu :$oper:$listereussite:$troptard:$mrep_approx:ap"
#puts $f $travail
#close $f
set ou [lindex [ split $DOSSIER_EXOS1 "/" ]  end]
catch {set flog  [open [file join $user] "a+"]}
set log "$dat&$nom&{[file tail ${fic}] de $ou }&$oper&$listereussite&$troptard&$mrep_approx&ap"
puts  $flog  $log
close $flog
###exec   /usr/bin/leterrier_log  --message=$log   --logfile=$user
}
 proc recommencer {} {
 global basedir DOSSIER_EXOS1 argv  iwish nom_elev nom_classe tempo approx
 set exo [lindex $argv 0 ]  ; destroy .
 exec  $iwish  [file join $basedir pluie_app1.tcl ] [file join $DOSSIER_EXOS1 $exo ] $nom_elev $nom_classe $tempo $approx $DOSSIER_EXOS1 &
}








