###################################################################################################"
proc stretchpolygon {c x y} {
global envparam lcord cible tid idd
set lcord [$c coords enveloppe]
set imgcor [$c bbox cible]
set id [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] uident*]]
switch $envparam {
      0 {
                 zoomepolygon1 $c $id $imgcor 0 $x
        }

      1 {
                 zoomepolygon2 $c $id $imgcor 0 $y
        }

     2 {
               zoomepolygon1 $c $id $imgcor 1 $x
       }

      3 {
                 zoomepolygon2 $c $id $imgcor 1 $y
        }

                 }
}


#####################################################################
 
proc zoomepolygon1 {c id imgcor cote x} {
global tid startx lcord
set fact [expr ([lindex $lcord 2] - [lindex $lcord 0])/([lindex $imgcor 2] - [lindex $imgcor 0])]

$c scale $id [lindex [$c bbox $id] 0] [lindex [$c bbox $id] 1] $fact 1
$c move $id [expr $x -[lindex $imgcor 0] - $cote*([lindex $lcord 2] - [lindex $lcord 0])] 0
redessineenveloppe $c $id
}


proc zoomepolygon2 {c id imgcor cote y} {
global tid startx lcord
set fact [expr ([lindex $lcord 3] - [lindex $lcord 1])/([lindex $imgcor 3] - [lindex $imgcor 1])]
$c scale $id [lindex [$c bbox $id] 0] [lindex [$c bbox $id] 1] 1 $fact
 
$c move $id 0 [expr $y -[lindex $imgcor 1] - $cote*([lindex $lcord 3] - [lindex $lcord 1])]

redessineenveloppe $c $id
}


#############################################################################################""
proc createpolygon {c fichier arrcoul linecoul smoth identif} {

global tid canvw canvh tabson tabobj tabevent
$c dtag cible
$c focus ""
creetool canvas $c
catch {$c delete enveloppe}
set sm $smoth

if {$identif == "new"} {
set id [getid]
set f [open [file join polygon $fichier] "r"]
set it [gets $f]
close $f
set arrcoul "white"
} elseif {$identif == "renew"} {
set id [getid]
set it $fichier
} else {
set id $identif
set it $fichier
}
$c create polygon $it -fill $arrcoul -tags "drag $id typepolygon" -outline $linecoul -smooth $sm

if {$identif == "new"} {
set x [expr int($canvw/2) - ([lindex [$c bbox $id] 2] - [lindex [$c bbox $id] 0])]
set y [expr int($canvh/2) - ([lindex [$c bbox $id] 3] - [lindex [$c bbox $id] 1])]
$c move $id $x $y
}
set tid($id) "\173$fichier\175 $id"
set tabobj($id) $id   
set tabevent($id) {enter {} leave {} click {}}  
set tabson($id) ""

#createenveloppe $c cible
}

proc fondtrans1 {c trans} {
if {$trans ==0} {
$c itemconfigure cible -fill ""
}
}

proc changefond1 {c coul} {

variable ftrans2
set ftrans2 0
$c itemconfigure cible -fill $coul
}


proc changetrait1 {c coul} {
$c itemconfigure cible -outline $coul
}

##########################################################################################
#############################################################################################""
proc createpolygon2 {c fichier arrcoul linecoul smoth identif} {

global tid canvw canvh tabson tabobj

$c dtag cible
$c focus ""
creetool canvas $c
catch {$c delete enveloppe}

set it $fichier
set sm $smoth

if {$identif == "new" || $identif == "renew"} {
set id [getid]
set arrcoul "white"
} else {
set id $identif
}
$c create polygon $it -fill $arrcoul -tags "drag $id typepolygon" -outline $linecoul -smooth $sm

if {$identif == "new"} {
#set x [expr int($canvw/2) - ([lindex [$c bbox $id] 2] - [lindex [$c bbox $id] 0])]
#set y [expr int($canvh/2) - ([lindex [$c bbox $id] 3] - [lindex [$c bbox $id] 1])]
#$c move $id $x $y
}
set tid($id) "\173$fichier\175 $id"
set tabobj($id) $id   

#createenveloppe $c cible
}

proc fondtrans1 {c trans} {
if {$trans ==0} {
$c itemconfigure cible -fill ""
}
}
