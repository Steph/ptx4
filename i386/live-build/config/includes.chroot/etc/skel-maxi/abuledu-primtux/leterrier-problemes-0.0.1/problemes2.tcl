#!/bin/sh
#problemes.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2007 David Lucardi <davidlucardi@aol.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier:
#  Date    : 01/06/2007
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  David Lucardi 01/06/2007
# 
#  *************************************************************************

source utils.tcl

proc verifdessin {} {
global cible ensembles flagok log
	for {set k 0} {$k < [llength $cible]} {incr k 1} {
		for {set i 0} {$i < [expr [lindex [lindex $cible $k] 1]]} {incr i 1} {
				for {set j 0} {$j < [expr [lindex [lindex $cible $k] 0]]} {incr j 1} {
				set coord [.frame.c bbox [.frame.c find withtag rect$k$i$j]]
					foreach z [.frame.c find overlapping [expr [lindex $coord 0] + ([lindex $coord 2] - [lindex $coord 0])/2] [expr [lindex $coord 1] + ([lindex $coord 3] - [lindex $coord 1])/2] [expr [lindex $coord 0] + ([lindex $coord 2] - [lindex $coord 0])/2] [expr [lindex $coord 1] + ([lindex $coord 3] - [lindex $coord 1])/2]] {
						#liste les tags des objets
						if {[lsearch [.frame.c gettags $z] current] == -1} {lappend listagoverlap$k [.frame.c gettags $z]}
					}
				
				}
		}
	set source [lindex [lindex $cible $k] 3]
	set count1 [llength [lsearch -all -regexp [expr \$listagoverlap$k] $source]]
	set count2 [llength [lsearch -all -regexp [expr \$listagoverlap$k] source*]]
		if {$count1 != [lindex $ensembles $k] || $count1 != $count2} {
		.frame.c itemconfigure cible$k -fill red
		lappend log [concat Dessin : erreur sur l'ensemble  n� [expr $k +1].]
		#($count1 �l�ments)
		set flagok 0
		} else {
		.frame.c itemconfigure cible$k -fill yellow
				}
	}
if {$flagok == 1} {lappend log "Le dessin est juste."}
}

proc montredessin {} {
global reponse sysFont
.framebottom0.verif configure -text "V�rifier" -command "verif"
.frame.c itemconfigure canvas -state normal
button .framebottom0.corrige -command "corrige" -text "Corriger" -font $sysFont(tb)
pack .framebottom0.corrige -side left -padx 60 -pady 10
catch {
.framebottom.ope1 configure -state disabled
for {set z 0} {$z < [llength $reponse]} {incr z} {
set repons [lindex $reponse $z]
for {set i 0} {$i < [llength [lindex $repons 1]]} {incr i} {
.framebottom$z.reponse$i configure -state disabled
}
}
}
}

proc corrige {} {
global reponse sysFont
.frame.c itemconfigure canvas -state hidden
.framebottom0.verif configure -command "montredessin" -text "Continuer" -font $sysFont(tb)
catch {
destroy button .framebottom0.corrige
.framebottom.ope1 configure -state normal
for {set z 0} {$z < [llength $reponse]} {incr z} {
set repons [lindex $reponse $z]
for {set i 0} {$i < [llength [lindex $repons 1]]} {incr i} {
.framebottom$z.reponse$i configure -state normal
}
}
}
}

##################################################################################
proc tracesource {orgsourcey orgsourcexorig taillerect intervalcible} {
global source
set ind 0
set decal 0
	foreach item $source {
		set orgy [expr $orgsourcey + $decal + $intervalcible]
		for {set i 0} {$i < [lindex $item 2]} {incr i 1} {
		for {set j 0} {$j < [lindex $item 3]} {incr j 1} {
		.frame.c create image [expr $orgsourcexorig + $i*$taillerect] [expr $orgy + + $j*$taillerect] -image [image create photo -file [file join sysdata [lindex $item 0]]] -tags "drag source0 canvas"
		incr ind
		if {$ind == [lindex $item 1]} {break}
		}
		if {$ind == [lindex $item 1]} {break}
		}
		set ind 0
		set decal [expr $decal + [lindex $item 3]*$taillerect]
	}
}
#############################################################################
proc itemStopDrag {x y} {
global lastX lastY sourcecoord source okdessinniv4
set listagoverlap ""
set listagcurrent [.frame.c gettags current]
set coord [.frame.c bbox current]
########################renvoie une liste de tous les objets sous le pointeur de la souris
	foreach i [.frame.c find overlapping [expr [lindex $coord 0] + ([lindex $coord 2] - [lindex $coord 0])/2] [expr [lindex $coord 1] + ([lindex $coord 3] - [lindex $coord 1])/2] [expr [lindex $coord 0] + ([lindex $coord 2] - [lindex $coord 0])/2] [expr [lindex $coord 1] + ([lindex $coord 3] - [lindex $coord 1])/2]] {
		#liste les tags des objets
		if {[lsearch [.frame.c gettags $i] current] == -1} {lappend listagoverlap [.frame.c gettags $i]}

	}
set listagoverlap [join $listagoverlap]
##########################placer un objet 
	if {[lsearch -regexp $listagoverlap rect] != -1 && [lsearch -regexp $listagoverlap place] == -1} {
	set ciblecoord [.frame.c bbox [lindex $listagoverlap [lindex [lsearch -all -regexp $listagoverlap rect] 0]]]
		set flg 0
		foreach i [.frame.c find overlapping [expr [lindex $ciblecoord 0] + ([lindex $ciblecoord 2] - [lindex $ciblecoord 0])/2] [expr [lindex $ciblecoord 1] + ([lindex $ciblecoord 3] - [lindex $ciblecoord 1])/2] [expr [lindex $ciblecoord 0] + ([lindex $ciblecoord 2] - [lindex $ciblecoord 0])/2] [expr [lindex $ciblecoord 1] + ([lindex $ciblecoord 3] - [lindex $ciblecoord 1])/2]] {
		if {[lsearch [.frame.c gettags $i] place] != -1} {set flg 1}
		}
	if {$flg == 0} {
	set okdessinniv4 1
	.frame.c coords current [expr [lindex $ciblecoord 0] + ([lindex $ciblecoord 2] - [lindex $ciblecoord 0])/2] [expr [lindex $ciblecoord 1] + ([lindex $ciblecoord 3] - [lindex $ciblecoord 1])/2]
		############si l'objet source n'�tait pas d�j� plac�, ajouter le tag plac�                
		if {[lsearch $listagcurrent place] == -1} {
		.frame.c addtag place withtag current
		}
	} else {
		.frame.c coords current [expr [lindex $sourcecoord 0] + ([lindex $sourcecoord 2] - [lindex $sourcecoord 0])/2] [expr [lindex $sourcecoord 1] + ([lindex $sourcecoord 3] - [lindex $sourcecoord 1])/2]          
	}
	} elseif {[lsearch -regexp $listagoverlap rect] == -1 && [lsearch $listagcurrent place] != -1} {
	############on enl�ve le tag plac�
	.frame.c dtag current place
	} else {
	#############dans tous les autres cas, on renvoie � la position initiale
	.frame.c coords current [expr [lindex $sourcecoord 0] + ([lindex $sourcecoord 2] - [lindex $sourcecoord 0])/2] [expr [lindex $sourcecoord 1] + ([lindex $sourcecoord 3] - [lindex $sourcecoord 1])/2]          
	}
}

###########################################################################"

proc main {} {
global source cible ensembles sysFont niveau operations reponse arg page etapes niv enonce user Home
#wm resizable . 0 0
wm geometry . +0+0

. configure -bg grey
catch {
destroy .frametop
destroy .frame
destroy .framebottom
}
frame .frametop -width 800 -bg grey
pack .frametop -side top -fill both -expand yes
frame .frame -width 800 -bg grey
pack .frame -side top -fill both -expand yes
frame .framebottom -width 800 -bg grey
pack .framebottom -side top -fill both -expand yes


set f [open [file join $Home problemes $arg] "r"]
set tmp [gets $f]
eval $tmp
	while {$tmp != "::$page"} {
	set tmp [gets $f]
	}

	while {$tmp != "::"} {
	set tmp [gets $f]
	if {$tmp != "::"} {eval $tmp}
	}
close $f
catch {
destroy .frametop.enonce
}

label .frametop.enonce -bg grey -justify left -font $sysFont(tb) -text $enonce
pack .frametop.enonce -expand true -side left -anchor w
set texte [string map {\n \040} $enonce]
bind .frametop.enonce <ButtonRelease-1> "speaktexte \173$texte\175"

if {$niv != "none"} {set niveau $niv}
	if {$niveau <= 4} {
	catch {
	destroy .frame.c
	}
	canvas .frame.c -width 800 -height $canvash -bg grey
	pack .frame.c -expand true
	.frame.c create text $orgsourcexorig 30 -font $sysFont(tb) -text "D�place les images" -tags "canvas sp1"
	.frame.c bind sp1 <ButtonRelease-1> "speaktexte \173D�place les images\175"
	.frame.c bind drag <ButtonRelease-1> "itemStopDrag %x %y"
	.frame.c bind drag <1> "itemStartDrag %x %y"
	.frame.c bind drag <B1-Motion> "itemDrag %x %y"
	}

	for {set z 0} {$z < [llength $reponse]} {incr z} {
	catch {
	destroy .framebottom$z
	}
	}
	for {set z 0} {$z < [llength $reponse]} {incr z} {
	frame .framebottom$z -width 800 -bg grey
	pack .framebottom$z -side top -fill both -expand yes
	}

	if {$niveau >= 2} {
	label .framebottom.consigne1 -bg grey -justify left -font $sysFont(tb) -text "Ecris l'operation :"
	pack .framebottom.consigne1 -expand true -side top -anchor w -pady 10
	bind .framebottom.consigne1 <ButtonRelease-1> "speaktexte \173Ecris l'op�ration\175"
	entry .framebottom.ope1 -justify center -font $sysFont(tb) 
	pack .framebottom.ope1 -expand true -side top -anchor w -pady 10
	}

	if {$niveau >= 1} {
	label .framebottom.consigne2 -bg grey -justify left -font $sysFont(tb) -text "Ecris la reponse :"
	pack .framebottom.consigne2 -expand true -side top -anchor w
	bind .framebottom.consigne2 <ButtonRelease-1> "speaktexte \173Ecris la r�ponse\175"
		for {set z 0} {$z < [llength $reponse]} {incr z} {
		set repons [lindex $reponse $z]
		set compt 0
		set ind [lindex [lindex $repons 0] $compt]
			for {set i 0} {$i < [llength [lindex $repons 1]]} {incr i} {
 				if {$i != $ind } {
				label .framebottom$z.reponse$i -bg grey -justify left -font $sysFont(tb) -text [lindex  [lindex $repons 1] $i]
				bind .framebottom$z.reponse$i <ButtonRelease-1> "speaktexte \173[lindex  [lindex $repons 1] $i]\175"
				} else {
				entry .framebottom$z.reponse$i -justify center -font $sysFont(tb) -width 4
				incr compt
				set ind [lindex [lindex $repons 0] $compt]
				}
			pack .framebottom$z.reponse$i -side left -anchor w -padx 2
			}
		}
	}
if {$niveau <=4} {
tracecibleline $intervalcible $taillerect $orgxorig $orgy $orient $labelcible $quadri $volatil
#trac� des sources
tracesource $orgsourcey $orgsourcexorig $taillerect $intervalcible
}
if {$niveau != "3"} {
button .framebottom0.verif -command "verif" -text "V�rifier" -font $sysFont(tb)
pack .framebottom0.verif -side left -padx 60 -pady 10
} else {
button .framebottom0.verif -command "montredessin" -text "Continuer" -font $sysFont(tb)
pack .framebottom0.verif -side left -padx 60 -pady 10
.frame.c itemconfigure canvas -state hidden
}
if {$niveau >=4} {
for {set z [expr [llength $opnonautorise] -1]} {$z >= 0} {incr z -1} {
set operations [lreplace $operations [lindex  $opnonautorise $z] [lindex  $opnonautorise $z]]
}
}
wm title . "Probl�mes - $arg - Niveau : $niveau - Utilisateur : [string map {.log \040} [file tail $user]]"
}
############################################################################
global arg page niv parcours indparcours log nbverif okdessinniv4
set indparcours 0
set okdessinniv4 0
set log ""
set nbverif 0
set arg [lindex $argv 0]
set niv [lindex $argv 1]
set parcours [lindex $argv 2]
set page 1
main
