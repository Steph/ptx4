#!/bin/sh
#closure.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************
global sysFont nbreu essais auto couleur aide longchamp xcol ycol listevariable categorie startdirect user Homeconf repertoire Home initrep baseHome lecture_mot lecture_mot_cache tablecture_mot tablecture_mot_cache

source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl

proc cancelkey {A} {
set A ""
#focus .
}

#variables
#nbreu : nombre d'items effectues
#essais : total des essais effectues
#auto : flag de detection du mode de fonctionnement 
#couleur : couleur associ�e � l'exercice
#aide : pr�cise � quel moment doit intervenir l'aide
#longchamp : pr�cise si les champs sont de longueur variable

set nbreu 0
set essais 0
set auto 0
set couleur blue
set aide 2
set longchamp 1
set xcol 0
set ycol 0
set listevariable 1
set categorie ""
set startdirect 1
set filuser [lindex $argv 1]
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
initlog $plateforme $ident
inithome

#interface
. configure -background black -width 640 -height 480
wm geometry . +52+0


frame .menu -height 40
pack .menu -side bottom -fill both

button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main .text"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right


tux_commence

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both

#recup�ration des options de reglages
catch {
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set aller [gets $f]
set aller_ponctu1 [lindex $aller 11]
close $f
set aide [lindex $aller_ponctu1 0]
set longchamp [lindex $aller_ponctu1 1]
set listevariable [lindex $aller_ponctu1 2]
#set categorie [lindex $aller_ponctu1 3]
set startdirect [lindex $aller_ponctu1 4]
set lecture_mot [lindex $aller_ponctu1 5]
set lecture_mot_cache [lindex $aller_ponctu1 6]
}
set initrep [file join $Home textes $repertoire]

#chargement du texte avec d�tection du mode
set auto [charge .text [file join $Home textes $repertoire $categorie]]

#focus .text
#.text configure -state disabled

bind .text <ButtonRelease-1> "lire"
bind .text <Any-Enter> ".text configure -cursor target"
bind .text <Any-Leave> ".text configure -cursor left_ptr"

bind . <KeyPress> "cancelkey %A"

if {$auto == 1} {
set lecture_mot_cache $tablecture_mot_cache(ponctuation1)
}


wm title . "[mc {Exercice}] $categorie - [lindex [lindex $listexo 11] 1]"
label .menu.titre -text "[lindex [lindex $listexo 11] 1] - [mc {Observe}]" -justify center
pack .menu.titre -side left -fill both -expand 1


proc main {t} {
#liste principale de phrases contenant les mots sans ponctuation, liste de mots � cacher
#listessai : tableau pour tenir � jour les essais sur chaque mot
#texte : le texte initial
#longmot : longueur maximale des champs de texte

global sysFont plist listemotscaches listessai texte nbmotscaches auto longchamp longmot iwish aide user lecture_mot_cache filuser
set nbmotscaches 0

#catch {destroy .menu.b1}
#button .menu.b1 -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"
#pack .menu.b1 -side right
#.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "exec $iwish aller.tcl $filuser & ;exit"
.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"

bind .text <ButtonRelease-1> ""
bind .text <Any-Enter> ""
bind .text <Any-Leave> ""

bind . <KeyPress> ""

$t configure -state normal
set what [mc {Complete avec le signe de ponctuation convenable. Appuie sur la touche entree pour valider.}]
set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"

tux_exo $what

    pauto $t
    
    $t configure -state disabled -selectbackground white -selectforeground black

    if {$lecture_mot_cache == "0" } {catch {destroy .menu.bb1}}

}

proc pauto {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches aide longchamp longmot listeaide listexo iwish filuser
set nbmotscaches 0
set listemots {}
set listemotscaches {}
    catch {destroy .menu.titre}
    catch {destroy .menu.lab}
    label .menu.lab -text [lindex [lindex $listexo 11] 2] -font $sysFont(m) -justify center
#[mc {Complete avec le signe de ponctuation convenable et appuie sur la touche entree pour valider.}] -font $sysFont(m)
    pack .menu.lab

#On rep�re tous les signes de ponctuation et on les marque
    set cur 1.0
   set tmp 1.0
    while 1 {
    set cur [$t search -regexp -count length {[.!?,;:()]} $cur end]
            if {$cur == ""} {
	      break
            }
   if {[$t index $tmp ]!=[$t index $cur ] && [$t get $cur] != "..."} {
    $t mark set cur$nbmotscaches [$t index $cur]
    incr nbmotscaches
    lappend listemotscaches [$t get $cur]
    } else {
    set listemotscaches [lreplace $listemotscaches end end [lindex $listemotscaches end][$t get $cur]]
    }
    set tmp "$cur + 1c"
    set cur "$cur + 1c"
    }

 set longmot 0
        foreach mot $listemotscaches {
            if {$longmot < [string length $mot]} {
            set longmot  [string length $mot]
            }
        }

    if {$nbmotscaches == 0} {
    set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
    exec $iwish aller.tcl $filuser &
    exit
    }


#on remplace les signes de ponctuation et on les marques avec le tag ent$i
    for {set i 0} {$i < [llength $listemotscaches]} {incr i 1} {           
    $t delete cur$i "cur$i + [string length [lindex $listemotscaches $i]]c"
    if {$longchamp == 0} {
    entry $t.ent$i -font $sysFont(l) -width [expr [string length [lindex $listemotscaches $i]] +1] -bg yellow
    } else {
    entry $t.ent$i -font $sysFont(l) -width [expr $longmot + 1] -bg yellow
    }
    
    $t window create cur$i -window $t.ent$i
    bind $t.ent$i <Return> "verif $i $t"
    bind $t.ent$i <KeyPress> "tux_continue_ponctuation"

    set listessai($i) 0
    }
}


#############################################################################"
####################################################################""
proc verif {i t} {
global sysFont listemotscaches listessai nbreu essais aide listeaide listevariable disabledfore
incr essais
    if {[lindex $listemotscaches $i] == [$t.ent$i get]} {
    incr nbreu
    if {$listessai($i)== 0} {tux_reussi} else {tux_continue_bien}
    catch {destroy .menu.lab}
    label .menu.lab -text [format [mc {%1$s signe(s) trouve(s) sur %2$s.}] $nbreu [llength $listemotscaches] ]
    pack .menu.lab

    bind $t.ent$i <Return> {}
    $t.ent$i configure -state disabled -$disabledfore blue
    testefin $nbreu $essais
    } else {
    incr listessai($i)
    if {$listessai($i) == 1} {tux_echoue1} else {tux_echoue2}
    $t.ent$i delete 0 end
    }
affichecouleur $i $t
}

proc testefin {nbreu essais} {
global sysFont listemotscaches user categorie nbmotscaches
    if {$nbreu >= [llength $listemotscaches]} {
    catch {destroy .menu.lab}
    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s) pour %2$s signe(s).}] $essais $nbreu]
    set str1 [mc {Exercice Ponctuation 1}]
    #enregistreval $str1 $categorie $str2 $user
set score [expr ($nbreu*100)/($nbmotscaches +($essais- $nbreu))]
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}

    label .menu.lab -text $str0$str2
    pack .menu.lab
    bell
        }
}


proc affichecouleur {ind t} {
global sysFont listessai disabledback
switch $listessai($ind) {
    0 { $t.ent$ind configure -$disabledback yellow -bg yellow}
    1 { $t.ent$ind configure -$disabledback green -bg green}
    default { $t.ent$ind configure -$disabledback red -bg red}
    }
}

if {$startdirect == 0 } {
main .text
}

proc fin {} {
global sysFont categorie user essais nbmotscaches nbreu listexo iwish filuser repertoire lecture_mot_cache
variable repertconf

#set score [expr ($nbreu*100)/$essais]
set score [expr ($nbreu*100)/($nbmotscaches +($essais- $nbreu))]

    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s) pour %2$s signe(s) sur %3$s.}] $essais $nbreu $nbmotscaches]
    set str1 [mc {Exercice Ponctuation 1}]

switch $lecture_mot_cache {
1 { set lectmotcacheconf [mc {Le texte peut �tre entendu.}]}
0 { set lectmotcacheconf [mc {Le texte ne peut pas �tre entendu.}]}
}

set exoconf [mc {Parametres :}]
set exoconf "$exoconf  Son : $lectmotcacheconf"

    enregistreval $str1\040[lindex [lindex $listexo 11] 1] \173$categorie\175 $str2 $score $repertconf 11 $user $exoconf $repertoire
exec $iwish aller.tcl $filuser &
exit
}

proc boucle {} {
global  categorie startdirect essais nbreu listexo auto listemotscaches nbmotscaches user
    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s) pour %2$s signe(s) sur %3$s.}] $essais $nbreu $nbmotscaches]
    set str1 [mc {Exercice Ponctuation 1}]
    enregistreval $str1 \173$categorie\175 $str2 $user
set essais 0
set nbreu 0
set listexo ""
set listemotscaches ""
set nbmotscaches 0
set categorie "Au choix"
.text configure -state normal
set auto [charge .text $categorie]
.text configure -state disabled
focus .text
.menu.b1 configure -text [mc {Commencer}] -command "main .text"
catch {destroy .menu.suiv}
if {$startdirect == 0 } {
main .text
}
}

