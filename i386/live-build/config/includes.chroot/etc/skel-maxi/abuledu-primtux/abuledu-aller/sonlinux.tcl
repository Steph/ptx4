#########################
#proceures pour le son
proc soundcap {} {
global sound sysFont
if {$sound == 1} {
label .frame2.menu7 -text [mc {Associer un son}]
place .frame2.menu7 -x 300 -y 10

radiobutton .frame2.menu8 -text [mc {Pour la categorie}] -background #ffff80 -value 0 -variable optson -font $sysFont(s)
place .frame2.menu8 -x 300 -y 60

radiobutton .frame2.menu9 -text [mc {Pour l'image}] -background #ffff80 -value 1 -variable optson -font $sysFont(s)
place .frame2.menu9 -x 300 -y 110


radiobutton .frame2.menu10 -text [mc {Pour le mot principal}] -background #ffff80 -value 2 -variable optson -font $sysFont(s)
place .frame2.menu10 -x 300 -y 160

.frame2.menu9 select

snack::createIcons
button .frame2.b1 -bitmap snackPlay -command Play
button .frame2.b2 -bitmap snackPause -command Pause
button .frame2.b3 -bitmap snackStop -command Stop
button .frame2.b4 -bitmap snackRecord -command Record -fg red

place .frame2.b1 -x 290 -y 210
place .frame2.b2 -x 315 -y 210
place .frame2.b3 -x 340 -y 210
place .frame2.b4 -x 365 -y 210

}
}

proc Record {} {
global currentimg repbasecat Home categorie
variable optson
.frame2.menu8 configure -state disabled
.frame2.menu9 configure -state disabled
.frame2.menu10 configure -state disabled

if {$currentimg !="" && $currentimg !="blank.gif"} {
   set ext .wav
   set son [lindex [split $currentimg .] 0] 
    set pref s_	 
    #s stop
switch $optson {
0 {set son $categorie}
1 {set son $pref[lindex [split $currentimg .] 0]}
2 {set son [lindex [split $currentimg .] 0]}
}
      catch {file delete [file join $Home sons $son$ext]}

   s configure -file [file join $Home sons $son$ext] -rate 8000

#############################
s stop
##
update
##

s record
set ::op r
update
##
   .frame2.b1 configure -relief raised
   .frame2.b4 configure -relief groove
    } else {
    set answer [tk_messageBox -message [mc {Aucune image selectionnee. Ouvrez une categorie ou choisissez d'abord une image}] -type ok -icon info] 
.frame2.menu8 configure -state normal
.frame2.menu9 configure -state normal
.frame2.menu10 configure -state normal

  }

}

proc Play {} {
global currentimg repbasecat Home categorie
variable optson

if {$currentimg !="" && $currentimg !="blank.gif"} {
    set ext .wav
    set pref s_	 
    s stop
    set ::op s
   update

switch $optson {
0 {set son $categorie}
1 {set son $pref[lindex [split $currentimg .] 0]}
2 {set son [lindex [split $currentimg .] 0]}
}
  if {[file exists [file join $Home sons $son$ext]] == 1} {
.frame2.menu8 configure -state disabled
.frame2.menu9 configure -state disabled
.frame2.menu10 configure -state disabled

    catch {
	s configure -file [file join $Home sons $son$ext]
	update
	s play -command Stop
	set ::op p
	update
#set file [file join $Home sons $son$ext]
#exec play $file
      
    .frame2.b1 configure -relief groove
    .frame2.b4 configure -relief raised
    .frame2.b2 configure -relief raised
    }
    }
    } else {
    set answer [tk_messageBox -message [mc {Aucune image selectionnee. Ouvrez une categorie ou choisissez d'abord une image}] -type ok -icon info] 
.frame2.menu8 configure -state normal
.frame2.menu9 configure -state normal
.frame2.menu10 configure -state normal

    }
}

proc Stop {} {

catch {
#s stop
set ::op s
update
.frame2.menu8 configure -state normal
.frame2.menu9 configure -state normal
.frame2.menu10 configure -state normal

.frame2.b1 configure -relief raised
.frame2.b4 configure -relief raised
.frame2.b2 configure -relief raised
}
}

proc Pause {} {
 s pause
update
  if {$::op != "s"} {
     if {[.frame2.b2 cget -relief] == "raised"} {
	 .frame2.b2 configure -relief groove
       } else {
	 .frame2.b2 configure -relief raised
       }
  }
}

proc enterstart {file} {
global Home
    #s stop
#update
    #catch {
    #  s configure -file $file
	
    #  s play
      ##set ::op p
    #  } 
#set cur [pwd]

#set file [string map {./ /} $file]
#set file "$cur$file"
	
exec play $file
}

proc enterstop {} {
#s stop
#update
}


global sound

if {[catch {package require snack}]} {
set sound 0
} else {
set sound 1
snack::sound s
}

