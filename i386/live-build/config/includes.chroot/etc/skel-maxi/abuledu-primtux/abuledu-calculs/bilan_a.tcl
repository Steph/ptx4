#!/bin/sh
#bilan.tcl
#\
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier:
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean-Louis Sendral
#  @modifier
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
set basedir [file dir $argv0]
cd $basedir
source calculs.conf
source i18n
source path.tcl
set plateforme $tcl_platform(platform) ; set ident $tcl_platform(user)
init $plateforme ; initlog $plateforme $ident
wm geometry . +0+0
wm title .  "bilan eleves calcul arbre"

. configure  -height 480 -width 640 -background #808080
set font {Arial 10}



proc capturenoms {} {
global nom_elev global file f  TRACEDIR_ARBRE LogHome
set TRACEDIR_ARBRE $LogHome
cd  $TRACEDIR_ARBRE
#set nom_elev [ .frame1.nom_elev get ]
set types {
    {"Catégories"		{.ps}	}
}
catch {set file [tk_getOpenFile -filetypes $types]}
if { [ catch { set f [open [file join $file] "r" ] } ] } {
set answer [tk_messageBox -message "Erreur de fichier" -type ok -icon info]
exit
}
#set liste_noms [exec  cut -d ":" -f3 $file ]
#set liste_noms {}
#while {[gets $f ligne] >=0} {
 #set liste_noms [linsert $liste_noms 0 [lindex [split $ligne ":"] 2]]
##}
  exec gv $file
}
capturenoms
 destroy .

