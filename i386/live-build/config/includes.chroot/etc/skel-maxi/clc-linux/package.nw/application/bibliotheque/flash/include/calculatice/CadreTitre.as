﻿class CadreTitre {
	private var objExo:CalcExo;
	private var conteneur:MovieClip;
	private var titre:TextField;
	private var consigne:TextField;
	private var illustration:MovieClip;
	
	public function CadreTitre(obj) {
		this.objExo=obj;
		this.conteneur = objExo.rootMovie.createEmptyMovieClip("TitreobjExo_conteneur", objExo.rootMovie.getNextHighestDepth());
		//on ajoute le logo IA59
		var logo=this.conteneur.attachMovie("logoIA59","logoIA59",this.conteneur.getNextHighestDepth());
		logo._x=735-logo._width-5;
		logo._y=450-logo._height-5;
	}
	public function SetTitre(texte) {
		var d = this.conteneur.getNextHighestDepth();
		if(this.titre==undefined){
			this.titre = this.conteneur.createTextField("titre", d, 0, 0, 0, 0);
			this.titre.embedFonts=true;
			this.titre.antiAliasType = "advanced";
		}
		this.titre.text = texte;
		var fmt:TextFormat = new TextFormat();
		fmt.align = "center";
		fmt.size = 24;
		fmt.font = "emb_tahoma";
		fmt.bold = true;
		this.titre.setTextFormat(fmt);
		this.titre.autoSize = true;
		this.titre._x = (750-this.titre._width)/2;
		this.titre._y = 50;
	}
	public function SetConsigne(texte) {
		var d = this.conteneur.getNextHighestDepth();
		if(this.consigne==undefined){
			this.consigne = this.conteneur.createTextField("consigne", d, 0, 0, 500, 0);
			this.consigne.embedFonts=true;
			this.consigne.antiAliasType = "advanced";
		}
		this.consigne.multiline=true;
		this.consigne.wordWrap=true;
		this.consigne.text = texte;
		var fmt:TextFormat = new TextFormat();
		fmt.align = "center";
		fmt.size = 16;
		fmt.font = "emb_tahoma";
		fmt.bold = true;
		this.consigne.setTextFormat(fmt);
		this.consigne.autoSize=true;
		this.consigne._x = (750-this.consigne._width)/2;
		this.consigne._y = this.titre._y+this.titre._height+20;
		this.Show();
	}
	
	public function AddIllustration(nom:String){
		this.illustration.removeMovieClip();
		var d = this.conteneur.getNextHighestDepth();
		this.illustration=this.conteneur.attachMovie(nom,"illustration_exo",d);
		this.illustration._x=(750-this.illustration._width)/2;
		this.illustration._y=this.consigne._y+this.consigne._height+20;
		return this.illustration;
	}
	
	public function Hide(){
		this.conteneur._visible=false;
	}
	public function Show(){
		this.conteneur._visible=true;
	}
}
