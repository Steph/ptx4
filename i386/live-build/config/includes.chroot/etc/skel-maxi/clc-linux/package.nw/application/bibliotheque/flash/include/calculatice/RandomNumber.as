﻿/***********************************************************************************************************************************
Action Script 2.0 class
@ Discription : Class for Genrating random numbers without repetation from a selected range
@ param : nTotal : Total random numbers to be genrated in number
@ param : nRangeMax : Largest number to be genrated random numbers to be genrated in number
@ param : nRangeMin : Smallest number to be genrated random numbers to be genrated if not provided considered "0" in number
@ param : bRepeat : Bolean value for Numbers to be genrated can be repeated or not (true if can be repeated) default TRUE
@ param : nMultipleOf : Number represent should be Multiple Of Number
@ param : bMultipleOf : Boolean if true no should be Multiple and false if Number should not be multiple of nMultipleOf
@ return : function genrate returns array of random Numbers
@ example to be written in fla
@**********************************************************************
@    import RandomNumber;
@    var nNumber:RandomNumber = new RandomNumber();
@    nA = nNumber.generate(3,20,10,false,4,false);
@    //Above line means that you need 3 random number from 10 to 20 should be non repeated and should not be divisible of 4
@**********************************************************************
@ Last modified:    12/10/2005
@ Flash version:    7.0
@ Author        Ajay Pal Singh
@ Version        1.0.1
 **********************************************************************************************************************************/
class RandomNumber {
	private var nTotal:Number;
	private var nRangeMax:Number;
	private var nRangeMin:Number;
	private var nRange:Number;
	private var bRepeat:Boolean;
	private var nMultipleOf:Number;
	private var bMultipleOf:Boolean;
	function RandomNumber(nTotalEx:Number, nRangeMaxEx:Number, nRangeMinEx:Number, bRepeatEx:Boolean, nMultipleOfEx:Number, bMultipleOfEx:Boolean) {
		nTotal = nTotalEx;
		nRangeMax = nRangeMaxEx;
		nRangeMin = (nRangeMinEx == undefined) ? 0 : nRangeMinEx;
		//If undefined set to 0
		bRepeat = (bRepeatEx == undefined) ? true : bRepeatEx;
		//If undefined set to true
		trace("nMultipleOfEx"+nMultipleOfEx);
		nMultipleOf = (nMultipleOfEx == undefined) ? 1 : nMultipleOfEx;
		//If undefined set to 1
		bMultipleOf = (bMultipleOfEx == undefined) ? true : bMultipleOfEx;
		//If undefined set to true
		nRange = nRangeMax-nRangeMin;
		if(nRange<nTotal-1){
			//au cas où la fourchette n'est pas suffisante 
			//on autorise les doublons
			bRepeat=true;
			trace("Range = "+nRange+" It's less than total of nbrs to be genrated");
		}
		if (nRangeMax == undefined) {
			trace("There is no range specified");
		}
		if (nTotal == undefined) {
			trace("No Parameter is passed. Class need at least two parameters first for total Nos to be generated and second for Maximum Range");
		}
	}
	public function generate():Array {
		var n:Number;
		//n m & i are used as loop counters only
		var m:Number;
		var i:Number;
		var nX:Number;
		var aNx:Array = new Array();
		for (n=0; n<nTotal; n++) {
			nX = Math.round(Math.random()*nRange)+nRangeMin;
			aNx[n] = nX;
			if (!bRepeat) {
				for (m=0; m<n; m++) {
					if (aNx[n] == aNx[m]) {
						n--;
						break;
					}
				}
			}
			
			if (nX % nMultipleOf != 0)
			{
				if (bMultipleOf)
					n--;
					
			}
			else if (nX % nMultipleOf == 0)
			{
				if (!bMultipleOf)
					n--;
					
			}
			/*if (bMultipleOf) {
				if (aNx[n] % nMultipleOf != 0) {
					n--;
				}
			} 
			else {
				if ((aNx[n] % nMultipleOf) == 0) {
					n--;
				}
			}*/
		}
		return aNx;
	}
	/**public function clear() {
		nRange = (nRange == undefined) ? 0 : nRange;
		for (var i = 0; i<nRange; i++) {
			aNx.pop[nRange-i];
		}
	}**/
}
