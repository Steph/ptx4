﻿class Signe {
	public static var MOINS : String;
	public static var DIVISE : String;
	public static var FOIS : String;
	public static var PLUS : String;
	
	public static function Init(){
		MOINS = unescape("%E2%80%93");
		DIVISE = ":";
		FOIS = unescape("%C3%97");
		PLUS="+";
	}
	//remplace le point par une virgule, retourne un string
	public static function numToString(num){
		return String(num).split(".").join(",");
	}

	//remplace une virgule par un point retourne un nombre
	public static function stringToNum(str){
		return Number(str.split(",").join("."));
	}
	
	
}