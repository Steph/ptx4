WriterRotationTool-1.0.0.oxt
Copyright (c) 2010 Thibault Vataire

L'extension WriterRotationTool permet la rotation d'une image 
dans un document texte. L'image est transformée en une forme 
de dessin afin de permettre sa rotation.
Cette extension fonctionne à la fois dans : 
 * les documents Writer, 
 * les documents maîtres, 
 * les formulaire XML, 
 * le document HTML, 
 * les rapports de base de données, 
 * les les formulaires de base de données.

