#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PicoSvoxOOo\n"
"Report-Msgid-Bugs-To: lirecouleur@arkaline.fr\n"
"POT-Creation-Date: 2013-12-10 21:34+0100\n"
"PO-Revision-Date: 2013-12-10 21:34+0100\n"
"Last-Translator: Marie Brungard <lirecouleur@arkaline.fr>\n"
"Language-Team: français <LL@li.org>\n"
"Language: français\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Langue de lecture :"
msgstr "Langue de lecture :"

msgid "Vitesse :"
msgstr "Vitesse :"

msgid "Surligner le texte lu"
msgstr "Surligner le texte lu"

msgid "Activer les raccourcis clavier"
msgstr "Activer les raccourcis clavier"

msgid "Activer la lecture en cours de frappe"
msgstr "Activer la lecture en cours de frappe"

msgid "Valider"
msgstr "Valider"

msgid "Annuler"
msgstr "Annuler"

msgid "mot"
msgstr "mot"

msgid "phrase"
msgstr "phrase"

msgid "paragraphe"
msgstr "paragraphe"

msgid "suivant"
msgstr "suivant"

msgid "retour"
msgstr "retour"

msgid "debut_texte"
msgstr "Vous êtes au début du texte"

msgid "fin_texte"
msgstr "Vous êtes à la fin du texte"

msgid "repeter"
msgstr "répéter"
