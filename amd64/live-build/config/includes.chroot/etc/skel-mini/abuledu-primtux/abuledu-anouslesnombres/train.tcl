############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : train.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 08/02/2003
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: train.tcl,v 1.1.1.1 2004/04/16 11:45:55 erics Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#calapa.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source utils.tcl
source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
changehome


set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"


proc placeboutons {c ind} {
global accrocheauto nbcat commandefenetre typeexo correctioncommande indwagonens indfenetre nbcat user
catch {destroy .bframe}
frame .bframe -background #ffff80
pack .bframe -side bottom
label .bframe.tete -background #ffff80 -image neutre
grid .bframe.tete -column 0 -padx 10 -row 0
catch {destroy $c.poubelle}


switch $ind {
	0 {
	#initialisation du jeu (appel g�n�ral)
	if {$typeexo == 0} {

	label .bframe.consigne -background #ffff80 -text [mc {Commande le bon nombre de wagons, puis appuie sur le bouton}]
	grid .bframe.consigne -column 1 -padx 10 -row 0
		if {$accrocheauto == 1} {
		button .bframe.butok -image [image create photo -file [file join sysdata ok1.gif] ] -background #ff80c0 -command "accrochewagon $c"
		grid .bframe.butok -column 3  -padx 5 -row 0 -sticky w
		} else {
		button .bframe.butok -image [image create photo -file [file join sysdata ok1.gif] ] -background #ff80c0 -command "placeboutons $c 2"
		grid .bframe.butok -column 3  -padx 5 -row 0 -sticky w
		}
	if {$correctioncommande == 1} {
	button .bframe.poubelle -image [image create photo -file [file join images poubelle.gif] ] -background #ff80c0 -command "corrigecommande $c"
	grid .bframe.poubelle -column 4  -padx 5 -row 0 -sticky w
	}
	} else {
	label .bframe.consigne -background #ffff80 -text [mc {Commande le bon nombre de wagons, appuie sur entree.}]
	grid .bframe.consigne -column 1 -padx 10 -row 0
	}
	}

	1 {
	label .bframe.consigne -background #ffff80 -text [mc {Appuie sur le bouton pour accrocher les wagons ou corrige}]
	grid .bframe.consigne -column 1 -padx 10 -row 0
	button .bframe.butok -image [image create photo -file [file join sysdata ok1.gif] ] -background #ff80c0 -command "accrochewagon $c"
	grid .bframe.butok -column 3  -padx 5 -row 0 -sticky w
	if {$correctioncommande == 1} {
	button .bframe.poubelle -image [image create photo -file [file join images poubelle.gif] ] -background #ff80c0 -command "corrigecommande $c"
	grid .bframe.poubelle -column 4  -padx 5 -row 0 -sticky w
	}
	}

	2 {
	#accrochage manuel des wagons
	label .bframe.consigne -background #ffff80 -text [mc {Accroche les wagons}]
	grid .bframe.consigne -column 1 -padx 10 -row 0
	button .bframe.butok -image [image create photo -file [file join sysdata ok1.gif] ] -background #ff80c0 -command "controlewagon $c"
	$c dtag all clic
	grid .bframe.butok -column 3  -padx 5 -row 0 -sticky w
		for {set i 1} {$i <= $nbcat} {incr i 1} {
		$c bind dragens($i) <ButtonRelease-1> "itemStopDrag $c %x %y"
		$c bind dragens($i) <ButtonPress-1> "itemStartDrag $c %x %y"
		$c bind dragens($i) <B1-Motion> "itemDrag $c %x %y"
		}
	}

	3 {   .bframe.tete configure -image bien
		# a l'issue de l'accrochage manuel des wagons
		if {$commandefenetre == 1} {
		label .bframe.consigne -background #ffff80 -text [mc {Commande les fenetres}]
		grid .bframe.consigne -column 1 -padx 10 -row 0
		gerefenetre $c
		button .bframe.butok -image [image create photo -file [file join sysdata ok1.gif] ] -background #ff80c0 -command "accrochefenetre $c"
		grid .bframe.butok -column 3  -padx 5 -row 0 -sticky w
		} else {
		geresuite $c
		}
	}
	
	4 {
	for {set i 1} {$i <= $nbcat} {incr i 1} {
	set str [append str "\173[format [mc {Nombre de wagons serie %1$s :}] $i] $indwagonens($i)\175\040"]
	}
	set str [append str "\173[mc {Nombre de fenetres :}] $indfenetre\175\040"]
	appendeval $str $user 
	
	label .bframe.consigne -background #ffff80 -text [mc {C'est perdu!}]
	grid .bframe.consigne -column 1 -padx 10 -row 0
	button .bframe.recommencer -image [image create photo -file [file join sysdata again2.gif] ] -background #ff80c0 -command "recommence $c"
	grid .bframe.recommencer -column 3  -padx 20 -row 0 -sticky w
	.bframe.tete configure -image mal
	}

	5 {
	label .bframe.consigne -background #ffff80 -text [mc {Appuie sur le bouton pour placer les fenetres}]
	grid .bframe.consigne -column 1 -padx 10 -row 0
	button .bframe.butok -image [image create photo -file [file join sysdata ok1.gif] ] -background #ff80c0 -command "accrochefenetre $c"
	grid .bframe.butok -column 3  -padx 5 -row 0 -sticky w
	}

}
}


#pour les compl�ments � collection, reste � g�rer pr�s des bornes
proc dejaplace {c} {
global dejap borne nbcat nbens indwagonens commandefenetre cat Home copieindwagonens
	for {set i 1} {$i <= $nbcat} {incr i 1} {
		if {$dejap($i) == 1} {
		set nbtmp [expr int(rand()*$nbens($i)) + 1]
		for {set k 1} {$k <= $nbtmp} {incr k 1} {
		set tmp wagon[lindex $cat($i) 0]$commandefenetre
		set ypos [expr $i*40 + 90]
		$c create image [expr 220 + [expr \$indwagonens($i)*20]] [expr $ypos + 20] -image $tmp -tags "dragens($i) drag" 
		incr indwagonens($i)
		incr copieindwagonens($i)
		}
		}
	}
}

global listdata arg1 arg2 gettext cat nbcat listgen

#listgen : liste cat�gorie (barque, calapa, etc ...)
#listdata : liste pour le sc�nario en cours
#arg1 : type de activit� (calapa, barque, etc)
#arg2 : num�ro de l'activit� dans le sc�nario 
set c .frame.c
set arg1 [lindex $argv 0]
set arg2 [lindex $argv 1]
set arg3 [lindex $argv 2]
set arg4 [lindex $argv 3]

#interface
. configure -background #ffff80 
frame .frame -width 640 -height 420 -background #ffff80
pack .frame -side top -fill both -expand yes
wm geometry . +0+0
canvas $c -width 640 -height 400 -background #ffff80 -highlightbackground #ffff80
pack $c

#ouverture du fichier calapa.conf ou barque.conf ...
set ext .conf
set f [open [file join $Home reglages $arg1$ext] "r"]
set listgen [gets $f]
close $f

image create photo bien -file [file join sysdata pbien.gif] 
image create photo pass -file [file join sysdata ppass.gif]
image create photo mal -file [file join sysdata pmal.gif]
image create photo neutre -file [file join sysdata pneutre.gif]
image create photo rails -file [file join images rails.gif]

#procedures
####################################################""
proc recommence {c} {
global nbcat
$c bind clic <ButtonRelease-1> ""
for {set i 1} {$i <= $nbcat} {incr i 1} {
$c bind dragens($i) <ButtonRelease-1> ""
$c bind dragens($i) <ButtonPress-1> ""
$c bind dragens($i) <B1-Motion> ""
}
place $c
}

###########################################################################

#proc�dure principale
proc place {c} {
global cat nbens serieencours nbcat listdata arg1 arg2 gettext listgen indfenetre indwagonens copieindwagonens listewagon typeexo commandefenetre accrocheauto correctioncommande dejap borne comptetypeexo1 indplace user
#listewagon : liste interm�diaire pour le melange des wagons
#nbens : tableau du nombre total de wagons par ensemble (nbens(i))
#cat : tableau des ensembles cat(1), cat(2) par classe de wagons
#indfenetre : nombre total de fenetres command�es (pour la limite � 20)
#indwagonens : nombre total de wagons command�s (pour la limite � 20)
#typeexo commandefenetre accrocheauto correctioncommande dejap borne : param�tres pour les exos (voir todo.txt)

set listdata [lindex $listgen $arg2]
wm title . "[mc $arg1] [lindex [lindex $listdata 0] 0]"
#wm title . "$arg1 $listdata"
set comptetypeexo1 0
set indfenetre 0
set nbcat [expr [llength $listdata] - 1]

set typeexo [lindex [lindex $listdata 0] 1]
set correctioncommande [lindex [lindex $listdata 0] 2]
set commandefenetre [lindex [lindex $listdata 0] 3]
set accrocheauto [lindex [lindex $listdata 0] 4]

for {set i 1} {$i <= $nbcat} {incr i 1} {
set cat($i) [lindex $listdata $i]
}

set indplace 0
set listewagon ""
for {set i 0} {$i <= 5} {incr i 1} {
set nbens($i) 0
set indwagonens($i) 0
set copieindwagonens($i) 0

}
set serieencours ""
set ext .gif
#wagons
	for {set i 1} {$i <= $nbcat} {incr i 1} {
	set intervalle [expr [lindex $cat($i) 2] - [lindex $cat($i) 1] + 1] 
	set nbens($i) [expr int(rand()*$intervalle) + [lindex $cat($i) 1]]
	set dejap($i) [lindex $cat($i) 3]
	set borne($i) [lindex $cat($i) 4]
	image create photo wagon[lindex $cat($i) 0]$commandefenetre -file [file join images wagon[lindex $cat($i) 0]$commandefenetre$ext]
	}

	for {set j 1} {$j <= $nbcat} {incr j 1} {
		for {set i 1} {$i <= $nbens($j)} {incr i 1} {
		lappend listewagon wagon[lindex $cat($j) 0]$commandefenetre$ext\040ens($j)

		}
	}

#on efface et on place les images, apr�s avoir touill�
	for {set i 1} {$i <= [llength $listewagon]} {incr i 1} {
	set t1 [expr int(rand()*[llength $listewagon])]
	set t2 [expr int(rand()*[llength $listewagon])]
	set tmp [lindex $listewagon $t1]
	set listewagon [lreplace $listewagon $t1 $t1 [lindex $listewagon $t2]]
	set listewagon [lreplace $listewagon $t2 $t2 $tmp]
	}

$c delete all


# la gare, la gare-r�sultat en gris, les cadres blancs
$c create rect 0 0 650 50 -fill #78F8FF -tags gare
$c create rect 0 50 650 100 -fill grey -tags resultat
$c create rect 200 130 620 [expr 130 + $nbcat*40] -fill white
$c create image 310 85 -image rails 

#les wagons du train modele
	for {set i 1} {$i <= [llength $listewagon]} {incr i 1} {
		image create photo [lindex [lindex $listewagon [expr $i -1]] 0]$i -file [file join images [lindex [lindex $listewagon [expr $i -1]] 0]]
		set ypos [expr int([expr $i - 1]/30)*20 + 25]
		set xpos [expr fmod([expr $i -1],30)*20 +25]
		#les wagons sont taggu�s avec  uidens(i) pour l'appartenance � ens(i)
		$c create image $xpos $ypos -image [lindex [lindex $listewagon [expr $i -1]] 0]$i -tags "uid[lindex [lindex $listewagon [expr $i -1]] 1] modele$i" 
				
	}
#Les wagonsmodeles pour la commande
	for {set i 1} {$i <= $nbcat} {incr i 1} {
	$c create image 20 [expr 100 + $i*40] -image [image create photo comwagon$i -file [file join images wagon[lindex $cat($i) 0]$commandefenetre$ext]] -tags "uidens($i) clic commande"
	catch {destroy $c.entryuidens($i)}
	catch {destroy $c.entryfenetre}

	entry $c.entryuidens($i) -width 8 -relief sunken -font {Arial 14}
	$c create window 60 [expr 100 + $i*40] -window $c.entryuidens($i) -anchor nw
	if {$typeexo == 0} {
      bind $c.entryuidens($i) <Return> "commandeglobale $c wagon $i"
	#quand on clique sur les wagons commande
	$c bind clic <ButtonRelease-1> "commande $c"
	} else {
	bind $c.entryuidens($i) <Return> "commandedifferee $c wagon $i"
	#bind $c.entryuidens($i) <Return> "commandeglobale $c $id $i"
	}
	}
placeboutons $c 0
#on place les wagons pour le type d'exercice compl�ment
dejaplace $c
set str ""
for {set i 1} {$i <= $nbcat} {incr i 1} {
set str [append str "\173[format [mc {Nombre de wagons serie %1$s :}] $i] $nbens($i)\175\040\173[format [mc {Nombre de wagons serie %1$s deja places :}] $i] $indwagonens($i)\175\040"]
}
set str [append str "\173[mc {Nombre total de wagons :}] [llength $listewagon]\175\040"]
enregistreeval $arg1 [lindex [lindex $listdata 0] 0] $str $user 
}


#appel de la proc�dure principale
place $c



#a la fin de l'exercice
proc geresuite {c} {
global arg2 arg3 arg4 listgen nbcat indwagonens user indfenetre nbcat Home
catch {destroy .bframe}
frame .bframe -background #ffff80
pack .bframe -side bottom
label .bframe.tete -background #ffff80 -image bien
grid .bframe.tete -column 0 -padx 10 -row 0
label .bframe.consigne -background #ffff80 -text [mc {Bravo, c'est gagne!}]
grid .bframe.consigne -column 1 -padx 30 -row 0
if {$arg2 < [expr [llength $listgen] -1] && $arg3 == 0} {
button .bframe.suite -image [image create photo -file [file join sysdata suite.gif] ] -background #ff80c0 -command "suite $c"
grid .bframe.suite -column 2  -padx 20 -row 0 -sticky w

}

set f [open [file join $Home reglages parcours.conf] "r"]
set listparcours [gets $f]
close $f

if {$arg4 < [expr [llength $listparcours] -1] && $arg3 == 1} {
button .bframe.suite -image [image create photo -file [file join sysdata suite.gif] ] -background #ff80c0 -command "suiteparcours $c $arg4"
grid .bframe.suite -column 2  -padx 20 -row 0 -sticky w
}

button .bframe.recommencer -image [image create photo -file [file join sysdata again2.gif] ] -background #ff80c0 -command "recommence $c"
grid .bframe.recommencer -column 3  -padx 20 -row 0 -sticky w
for {set i 1} {$i <= $nbcat} {incr i 1} {
set str [append str "\173[format [mc {Nombre de wagons serie %1$s :}] $i] $indwagonens($i)\175\040"]
}
set str [append str "\173[mc {Nombre de fenetres :}] $indfenetre\175\040"]
appendeval $str $user 

}

#commande des wagons ou des fenetres par clic sur les mod�les
proc commande {c} {
global indfenetre indwagonens serieencours typeexo
set tmp [$c itemcget current -image]
set lcoord [$c bbox current]
set id [string range [lindex [$c gettags current] [lsearch -regexp [$c gettags current] uid*]] 3 end]
if {$id == "fenetre"} {
	if {$indfenetre < 30} {
	$c create image [expr 210 + $indfenetre*12] 380 -image [image create photo fenetre -file [file join images fenetre.gif]] -tags "uidfenetreplace" 
	incr indfenetre
	$c.entryfenetre delete 0 end

	$c.entryfenetre configure -state disabled
	}
} else {

	#on gere serieencours
	if { $serieencours!= ""} {
		if {$id != $serieencours} {
		#if {$typeexo !=0} {
		$c dtag uid$serieencours clic
		#}
		set serieencours $id
		}
	} else {
	set serieencours $id
	}
	$c.entryuid$serieencours delete 0 end

	$c.entryuid$serieencours configure -state disabled

	if {[expr \$indwagon$id] < 20} {
	$c create image [expr 220 + [expr \$indwagon$id*20]] [expr [lindex $lcoord 1] + 20] -image $tmp -tags "drag$id drag solution" 
	catch {destroy $c.poubelle}
	button $c.poubelle -image [image create photo -file [file join sysdata poubelle.gif] ] -background #ff80c0 -command "corrigeclic $c"
	$c create window 160 [lindex $lcoord 1] -window $c.poubelle -anchor nw
	incr indwagon$id
	}
}
}


#pour placer auto les fenetres
proc accrochefenetre {c} {
global listewagon indwagonens 
set indfenetre 0
$c dtag all uidfenetre
$c dtag all clic
$c.entryfenetre configure -state disabled
	for {set i 1} {$i <= [llength $listewagon]} {incr i 1} {
	set item [$c find withtag modele$i]
	set id [string range [lindex [$c gettags $item] [lsearch -regexp [$c gettags $item] uid*]] 3 end]
	set tmplist [$c find withtag "uidfenetreplace"]
	set lpos [$c bbox $item]
		if {[llength $tmplist] !=0} {
		set idfenetre [lindex $tmplist 0]
		$c coords $idfenetre [expr [lindex $lpos 0] + 15] [expr [lindex $lpos 1] + 60]
		incr indfenetre
		$c dtag $idfenetre uidfenetreplace
		} else {
		break
		}
	}
if {$indfenetre == [expr [llength $listewagon]] && [llength [$c find withtag uidfenetreplace]] == 0} {
geresuite $c
} else {
placeboutons $c 4
}
}

#pour placer auto les wagons, accrochage automatique
proc accrochewagon {c} {
global listewagon indplace
	for {set i 1} {$i <= [llength $listewagon]} {incr i 1} {
	set item [$c find withtag modele$i]
	set id [string range [lindex [$c gettags $item] [lsearch -regexp [$c gettags $item] uid*]] 3 end]
	set tmplist [$c find withtag "drag$id"]
	set lpos [$c bbox $item]
		if {[llength $tmplist] !=0} {
		set iddrag [lindex $tmplist 0]
		$c coords $iddrag [expr [lindex $lpos 0] + 15] [expr [lindex $lpos 1] + 64]
		incr indplace
		$c dtag $iddrag drag$id
		$c dtag $iddrag drag
		} else {
		break
		}

	}
if {$indplace == [expr [llength $listewagon]] && [llength [$c find withtag drag]] == 0} {
placeboutons $c 3
} else {
placeboutons $c 4
}
}

##################"modif : controle validit�
#commande des wagons ou des fenetres par ecriture des nombres
proc commandeglobale {c id i} {
global indfenetre indwagonens serieencours nbcat typeexo cat commandefenetre listewagon

	if {$id == "fenetre"} {
		if {$indfenetre < 30} {
			if {[catch {set nbtmp [expr [$c.entryfenetre get]]}] == 0} {
				if {$nbtmp <= 30 && $nbtmp >=0} {
					for {set k 1} {$k <= $nbtmp} {incr k 1} {
					$c create image [expr 210 + $indfenetre*12] 380 -image [image create photo fenetre -file [file join images fenetre.gif]] -tags "uidfenetreplace" 
					incr indfenetre
					bind $c.entryfenetre <Return> ""
					}
				placeboutons $c 5
				} else {
				#tk_messageBox -message "Nombre non valide ou trop grand" -type ok -title Train
				$c.entryfenetre delete 0 end
				}
			} else {
			#tk_messageBox -message [mc {Nombre non valide ou trop grand}] -type ok -title [mc {Train}]
			$c.entryfenetre delete 0 end
			}

		}
	} else {
	set tmp wagon[lindex $cat($i) 0]$commandefenetre
	set ypos [expr $i*40 + 90]
		if {[expr \$indwagonens($i)] < 20} {
			if {[catch {set nbtmp [expr [$c.entryuidens($i) get]]}] == 0} {
				if {[expr $nbtmp + $indwagonens($i)] <= 20 && $nbtmp >=0} {
				$c dtag uid$serieencours clic
				$c dtag uidens\($i\) clic
				#if {$typeexo !=0} {
				$c.entryuidens\($i\) configure -state disabled
				bind $c.entryuidens\($i\) <Return> ""
				#}
		
					for {set k 1} {$k <= $nbtmp} {incr k 1} {
					$c create image [expr 220 + [expr \$indwagonens($i)*20]] [expr $ypos + 20] -image $tmp -tags "dragens($i) drag solution" 
					incr indwagonens($i)
					}
					if {$typeexo == 0} {
					catch {destroy $c.poubelle}
					button $c.poubelle -image [image create photo -file [file join sysdata poubelle.gif] ] -background #ff80c0 -command "corrigeclicentree $c $i"
					$c create window 160 [expr $i*40 + 90] -window $c.poubelle -anchor nw
					}
				} else {
				#tk_messageBox -message "Nombre non valide ou trop grand � la ligne $i" -type ok -title Train
				$c.entryuidens\($i\) delete 0 end
				}
			} else {
			#tk_messageBox -message "Nombre non valide ou trop grand � la ligne $i" -type ok -title Train
			$c.entryuidens\($i\) delete 0 end
			}

		}
	}
}

#gestion du jeu, accrochage manuel
proc itemStartDrag {c x y} {
    global lastX lastY sourcecoord
    set flag 1
    set sourcecoord [$c coords current]
    set lastX [$c canvasx $x]
    set lastY [$c canvasy $y]
    $c raise current
    }

proc itemStopDrag {c x y} {
    global lastX lastY sourcecoord listewagon indplace

    set idcible [string range [lindex [$c gettags current] [lsearch -regexp [$c gettags current] drag*]] 4 end]

    set coord [$c bbox current]

    foreach i [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]] {
         if {[lsearch [$c gettags $i] resultat] != -1} {
		if {[lindex [lindex $listewagon $indplace] 1]== $idcible} {
            $c coords current [expr $indplace*20 + 30] [expr [lindex [$c coords $i] 1] + 30] 
		incr indplace
		$c dtag current drag$idcible
		$c dtag current drag
		$c addtag solution withtag current

		if {$indplace == [expr [llength $listewagon]] && [llength [$c find withtag drag]] == 0} {
		placeboutons $c 3
		}
		return
		}
		} else {
		$c coords current [lindex $sourcecoord 0] [lindex $sourcecoord 1]
            }
    }

}

proc controlewagon {c} {
global listewagon indplace nbcat
	if {$indplace == [expr [llength $listewagon]] && [llength [$c find withtag drag]] == 0} {
	placeboutons $c 3
	} else {
	for {set k 1} {$k <= $nbcat} {incr k 1} {
	$c dtag all dragens($k)
	}
	placeboutons $c 4
	}
}

proc itemDrag {c x y} {
    global lastX lastY
    set x [$c canvasx $x]
    set y [$c canvasy $y]
    $c move current [expr $x-$lastX] [expr $y-$lastY]
    set lastX $x
    set lastY $y
}

#pour les commandes avec ecriture usuelle ou complexe
proc commandedifferee {c id i} {
global comptetypeexo1 nbcat typeexo accrocheauto indwagonens
	if {[catch {set nbtmp [expr [$c.entryuidens($i) get]]}] == 1} {
	tk_messageBox -message [mc {Nombre non valide ou trop grand}] -type ok -title [mc {Train}]
	$c.entryuidens\($i\) delete 0 end
	return
	} 
	if {[expr $nbtmp + $indwagonens($i)] > 20 || $nbtmp < 0} {
	tk_messageBox -message [mc {Nombre non valide ou trop grand}] -type ok -title [mc {Train}]
	$c.entryuidens\($i\) delete 0 end
	return
	}
$c dtag uidens\($i\) clic
$c.entryuidens\($i\) configure -state disabled
bind $c.entryuidens\($i\) <Return> ""

incr comptetypeexo1
	if {$comptetypeexo1 == $nbcat && $typeexo ==1} {
		for {set k 1} {$k <= $nbcat} {incr k 1} {
		commandeglobale $c  $id $k
		}
	if {$accrocheauto ==1} {
	placeboutons $c 1
	} else {
	placeboutons $c 2
	}
	}
}


#les fenetres
proc gerefenetre {c} {
	$c create image 20 380 -image [image create photo fenetre2 -file [file join images fenetre2.gif]] -tags "uidfenetre clic" 
	catch {destroy $c.entryfenetre}
	entry $c.entryfenetre -width 8 -relief sunken -font {Arial 14}
	#1 : valeurs bidons
      bind $c.entryfenetre <Return> "commandeglobale $c fenetre 1"

	$c create window 60 370 -window $c.entryfenetre -anchor nw -tags commandefenetre
	$c create rect 200 360 620 400 -fill white
}

proc corrigecommande {c} {
global nbcat indwagonens serieencours comptetypeexo1 correctioncommande copieindwagonens typeexo
.bframe.poubelle configure -state disabled
foreach item [$c find withtag solution] {
$c delete $item
}
set comptetypeexo1 0
$c addtag clic withtag commande
set indwagonens(0) $copieindwagonens(0)
set serieencours ""
for {set k 1} {$k <= $nbcat} {incr k 1} {
set indwagonens($k) $copieindwagonens($k)
$c.entryuidens($k) configure -state normal
	if {$typeexo == 0} {
      bind $c.entryuidens($k) <Return> "commandeglobale $c wagon $k"
	} else {
	bind $c.entryuidens($k) <Return> "commandedifferee $c wagon $k"
	}

$c.entryuidens($k) delete 0 end
}
set correctioncommande 0
for {set i 1} {$i <= $nbcat} {incr i 1} {
$c bind dragens($i) <ButtonRelease-1> ""
$c bind dragens($i) <ButtonPress-1> ""
$c bind dragens($i) <B1-Motion> ""
}

placeboutons $c 0
}


proc corrigeclicentree {c i} {
global nbcat indwagonens serieencours copieindwagonens 
$c.entryuidens\($i\) configure -state normal
$c.entryuidens\($i\) delete 0 end
bind $c.entryuidens($i) <Return> "commandeglobale $c wagon $i"
		foreach item [$c find withtag dragens($i)] {
			if {[lsearch [$c gettags $item] solution] != -1} {
			$c delete $item
			}
		}
	set indwagonens($i) [expr \$copieindwagonens($i)]
	
}

proc corrigeclic {c} {
global nbcat indwagonens serieencours copieindwagonens 
	if { $serieencours!= ""} {
		foreach item [$c find withtag drag$serieencours] {
			if {[lsearch [$c gettags $item] solution] != -1} {
			$c delete $item
			}
		}
	set indwagon$serieencours [expr \$copieindwagon$serieencours]
	}
}
