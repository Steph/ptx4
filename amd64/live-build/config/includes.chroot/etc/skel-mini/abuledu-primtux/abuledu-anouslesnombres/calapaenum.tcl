############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : calapa.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 08/02/2003
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: calapaenum.tcl,v 1.1.1.1 2004/04/16 11:45:40 erics Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#calapa.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

global erreurselection erreurhabitat serieincomplete
set erreurselection 0
set erreurhabitat 0
set serieincomplete 0

#gestion du jeu et des déplacements
proc itemStartDrag {c x y} {
    global lastX lastY sourcecoord flag
    set sourcecoord [$c coords current]
    set lastX [$c canvasx $x]
    set lastY [$c canvasy $y]
    $c raise current
    .bframe.tete configure -image neutre
    }

proc highlight {c image gamma} {
$image configure -gamma $gamma
$c itemconfigure current -image $image
}


proc itemStopDrag {c x y} {
global indens listdata gettext serieencours erreurselection
	if {[lindex [lindex $listdata 0] 1] == "2"} {
		foreach imag [image names] {
		$imag configure -gamma 1.0
		}
	}

set tmp [$c itemcget current -image]
set id [lindex [$c gettags current] [lsearch -regexp [$c gettags current] uid*]]
highlight $c $tmp 2.5
	if {$id== "uidens(0)" || [lsearch [$c gettags current] compte] != -1 || ($serieencours!= "" && [string range $id 3 end] != $serieencours)} {
	bell
	$c dtag uid$serieencours compte
		if {[lindex [lindex $listdata 0] 1] == "1"} {
			foreach imag [image names] {
			$imag configure -gamma 1.0
			}
		}
	highlight $c $tmp 0.01
	set ind$serieencours 0
	set serieencours ""
	incr erreurselection
      .bframe.tete configure -image mal
	return
	}
#highlight $c $tmp 1.0
$c addtag compte withtag current
set serieencours [string range $id 3 end]
incr ind$serieencours
return
}

proc itemDrag {c x y} {
}

proc verif {c lieu} {
global nbens indens listdata gettext serieencours nbcat erreurselection erreurhabitat serieincomplete user reussite
	foreach imag [image names] {
	$imag configure -gamma 1.0
	}
#$c create text 100 100 -text "$serieencours $lieu $compteens1 $compteens2 $comptetri"
if {$serieencours != $lieu } {
bell
$c dtag uid$serieencours compte
set ind$serieencours 0
set serieencours ""
.bframe.tete configure -image mal
incr erreurhabitat
return
}

if {$serieencours !=""} {
	#$c create text 100 200 -text "[expr \$ind$serieencours] [expr \$nb$serieencours] ind$serieencours nb$serieencours"

	if {[expr \$ind$serieencours] != [expr \$nb$serieencours]} {
	bell
	$c dtag uid$serieencours compte
	set ind$serieencours 0
	set serieencours ""
	incr serieincomplete
	.bframe.tete configure -image mal
	} else {
	$c dtag uid$serieencours drag
	if {$nbcat == 1} {
	set lpos { {180 220} {230 220} {280 220} {330 220} {380 220} {430 220} {180 270} {230 270} {280 270} {330 270} {380 270} {430 270} {180 320} {230 320} {280 320} {330 320} {380 320} {430 320} }
	} else {
	switch $serieencours {
	ens(1) {
	set lpos { {30 220} {80 220} {130 220} {180 220} {230 220} {280 220} {30 270} {80 270} {130 270} {180 270} {230 270} {280 270} {30 320} {80 320} {130 320} {180 320} {230 320} {280 320} }
	}
	ens(2) {
	set lpos { {350 220} {400 220} {450 220} {500 220} {550 220} {600 220} {350 270} {400 270} {450 270} {500 270} {550 270} {600 270} {350 320} {400 320} {450 320} {500 320} {550 320} {600 320} }
	}
	}
	}
	set compt 0
		foreach item [$c find withtag uid$serieencours] {
		$c coords $item [lindex $lpos $compt]
		$c dtag $item compte
		$c dtag $item drag
		$c dtag $item anim1
		incr compt
	.bframe.tete configure -image bien

		}
	set serieencours ""
	if {$indens(1) == $nbens(1) && $indens(2) == $nbens(2)} {
	$c dtag all drag
	.bframe.tete configure -image bien

	appendeval "\173[mc {Nombre d'erreurs de selection :}]$erreurselection\175\040\173[mc {Nombre d'erreurs d'habitats :}]$erreurhabitat\175\040\173[mc {Erreurs sur series incompletes :}]$serieincomplete\175" $user
	set reussite [expr $erreurselection + $erreurhabitat +$serieincomplete]
	geresuite $c
	#$c dtag all anim1
	}
	}
}
}


