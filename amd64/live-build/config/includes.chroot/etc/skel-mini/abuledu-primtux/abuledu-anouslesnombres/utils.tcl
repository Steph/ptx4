############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: utils.tcl,v 1.1.1.1 2004/04/16 11:45:55 erics Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#utils.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

#sur bouton suite
proc suite {c} {
global arg2 Home
incr arg2
recommence $c
}


proc suiteparcours {c indexpar} {
global Home
incr indexpar
set f [open [file join $Home reglages parcours.conf] "r"]
	set listparcours [gets $f]
	close $f
	set activite [lindex [lindex $listparcours $indexpar] 0]
	set ext .conf
	set f [open [file join $Home reglages [lindex $activite 0]$ext] "r"]
	set listactivite [gets $f]
	close $f
	set compt 0
		foreach item $listactivite {
		if {[string first [lindex [lindex $listparcours $indexpar] 1] $item] != -1} {
		break}
		incr compt
		}
		if {$compt >= [llength $listactivite ]} {
		tk_messageBox -message "Le sc�nario [lindex $listparcours $indexpar] n'existe pas" -type ok -title "A nous les nombres"
		return
		}

		if {[lindex $activite 0] == "calapa"} {
		#appli.tcl activit� index_dans_le_sc�nario flag_parcours index_dans_le_parcours
		exec wish place1.tcl calapa $compt 1 $indexpar &
		}
		if {[lindex $activite 0] == "constellation"} {
		exec wish place1.tcl constellation $compt 1 $indexpar &
		}
		if {[lindex $activite 0] == "nombre"} {
		exec wish place1.tcl nombre $compt 1 $indexpar &
		}
		if {[lindex $activite 0] == "barque"} {
		exec wish place1.tcl barque $compt 1 $indexpar &
		}
		if {[lindex $activite 0] == "train"} {
		exec wish train.tcl train $compt 1 $indexpar &
		}
		if {[lindex $activite 0] == "ordinal"} {
		exec wish ordinal.tcl ordinal $compt 1 $indexpar &
		}
		if {[lindex $activite 0] == "voir"} {
		exec wish quevoir.tcl voir $compt 1 $indexpar &
		}

exit
}
