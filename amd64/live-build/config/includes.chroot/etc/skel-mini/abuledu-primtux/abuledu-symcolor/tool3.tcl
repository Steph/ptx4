#!/bin/sh
#tool2.tcl
#Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  :
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version $Id: tool3.tcl,v 1.1.1.1 2005/12/27 13:08:45 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################


#######################################################################
proc interface {c cc} {
global plateforme progaide basedir Home canvw canvh bgn bgl
frame .tframe
pack .tframe -side left -fill both -expand yes

set widthf 200
if {[winfo screenwidth .] == "800"} {
set widthf 140
}
frame .frame -width [expr [winfo screenwidth .]] -height [expr [winfo screenheight .]]
pack .frame -side top -fill both -expand yes

wm geometry . +0+0
wm title . [mc {Symcolor}]


set canvh [expr ([winfo screenheight .] -$widthf)/2]
set canvw [expr ([winfo screenwidth .] -$widthf)/2]

canvas $c -width $canvw -height $canvh 
pack $c -side top
canvas $cc -width $canvw -height $canvh
pack $cc -side bottom


frame .bframe 
pack .bframe -side bottom

#frame .waction -width [expr $canvw] -height 32
#pack .waction -side left -anchor w -fill both

}

####################################################################

proc creetool {itm c} {
global canvw canvh taillefont tabpage tid msound tabobj iwish progaide basedir nompage




set widthc "100"
set padwidthc 5
if {[winfo screenwidth .] == "800"} {
set widthc 80
set padwidthc 1
}

set ind [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] uident*]]
catch {destroy .tframe.waction}
frame .tframe.waction 
pack .tframe.waction -side top -fill both
set ftrans2 0



	foreach coul {Black DarkGreen Red Green Blue Yellow Cyan Magenta White Brown DarkSeaGreen DarkViolet} {
	button .tframe.waction.c$coul -image [image create photo -file [file join sysdata [string tolower $coul].gif]] -background $coul -activebackground $coul -width $widthc -command "changefond1 $c $coul"
	pack .tframe.waction.c$coul -side top -fill x -pady $padwidthc
	}
label .tframe.waction.binette -image [image create photo -file [file join sysdata pneutre.gif]]
pack .tframe.waction.binette -side left -fill y -pady $padwidthc
button .tframe.waction.ok -image [image create photo -file [file join sysdata ok.gif]] -activebackground grey -borderwidth 1 -command "verif"
pack .tframe.waction.ok -side right -fill y -pady $padwidthc	
}


#############################################################################






proc forme {c nom} {
      if {$nom !=""} {
	createpolygon $c $nom.pol Blue Black 0 new 
	}
}





proc setcolor {type c} {
	global tid
    	set color [tk_chooseColor -title [mc {Choisir une couleur}]]

    if {$color != ""} {
	switch $type {
		canvas {$c configure -background $color}
		coultext {$c itemconfigure cible -fill $color}
		coulfond {changefond $c $color}
		coultour {changetrait $c $color}
		coulfond1 {changefond1 $c $color}
		coultrait1 {changetrait1 $c $color}
		bouton2 {changetextebouton2 $c $color}
	}
    }
}



proc setlang {lang} {
global env plateforme Home c
set env(LANG) $lang
set f [open [file join $Home lang.conf] "w"]
puts $f $lang
close $f

::msgcat::mclocale $lang
::msgcat::mcload [file join [file dirname [info script]] msgs]
tk_messageBox -message [mc {Les changements prendront effets au redemarrage de l'application.}] -type ok -title "Symcolor"
}


proc quitte {c} {
set answer [tk_messageBox -message [mc {Enregistrer les modifications?}] -type yesno -icon info -title "Symcolor"]
if {$answer == "yes"} {
sauve $c old
}
destroy .
}

proc verif {} {
global nbessai categorie user fautes
set flag 0
catch {.frame.cc delete enveloppe}
	foreach it [.frame.c find all] {
		if {[lsearch -regexp [.frame.c gettags $it] uident*] !=-1} {
		set ident [lindex [.frame.c gettags $it] [lsearch -regexp [.frame.c gettags $it] uident*]]
		set typ [lindex [.frame.c gettags $it] [lsearch -regexp [.frame.c gettags $it] type*]]
			if {$typ == "typepolygon"} {
			set arrfill [string tolower [.frame.c itemcget $it -fill]]
			set arrfillcc [string tolower [.frame.cc itemcget $it -fill]]
				if {$arrfill != $arrfillcc} {
				.frame.cc itemconfigure $it -fill white
				incr flag
				incr fautes
				}		
			}
		}
	}
incr nbessai
	if {$flag ==  0} {
	.tframe.waction.binette configure -image [image create photo -file [file join sysdata pbien.gif]]
	tk_messageBox -message [format [mc {Reussi en %1$s essai(s).}] $nbessai]
	.tframe.waction.ok configure -image [image create photo -file [file join sysdata suite.gif]] -command exit
	.frame.cc configure -state disabled
	bind .frame.cc <ButtonPress-1> ""
        enregistreval $categorie $nbessai $fautes $user
	} else {
	.tframe.waction.binette configure -image [image create photo -file [file join sysdata pmal.gif]]
	tk_messageBox -message [format [mc {Il y a %1$s erreur(s).}] $flag]
	}
}
interface $c $cc

