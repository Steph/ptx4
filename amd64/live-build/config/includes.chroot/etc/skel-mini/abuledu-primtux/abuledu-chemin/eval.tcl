#!/bin/sh
#eval.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#  Copyright (C) 2003 Andr� Connes <andre.connes@toulouse.iufm.fr>
#  Copyright (C) 2003 David Lucardi <davidlucardi@aol.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $Id: eval.tcl,v 1.4 2006/03/27 13:13:16 abuledu_andre Exp $
#  Author  : davidlucardi@aol.com
#  Modifier:
#  Date    :
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     David Lucardi
#  @modifier
#  @project    Le terrier
#  @copyright  Eric Seigne
#
#  *************************************************************************

proc enregistreval {titre categorie quoi user} {

  global tcl_platform

  set listeval \173$titre\175\040$categorie\040\173$quoi\175
  if { $tcl_platform(platform) == "unix" } {
#   exec /usr/bin/leterrier_log --message=$listeval --logfile=$user
    set f [open [file join $user.log] "a+"]
    puts $f $listeval
    close $f
  } elseif { $tcl_platform(platform) == "windows" } {
    #wm title . $user
    set f [open [file join $user.log] "a+"]
    puts $f $listeval
    close $f
  } else {
  }
}

################################################

# ##
# sauver trace-eleve et parcours
# ##

proc sauver_trace_parcours {} {
  global glob categorie

  ## trace
  # utilisateur/classe/date/duree/didacticiel/difficulte/version
  set eleve $glob(trace_user)
  set titre "chemin-$glob(version) $glob(trace_fname) $glob(dir_exos)"
  set nc [expr int([llength $glob(trace)]/2)]
  set heure_fin [clock seconds]
  set duree [expr $heure_fin-$glob(heure_debut)]
  set date_heure [clock format [clock seconds] -format "%A %d %B %Y %H:%M"]
  set quoi "classe \{$date_heure\} durée=$duree erreurs/cases=$glob(nberreurs)/$nc"

  enregistreval $titre $categorie $quoi $eleve

}


