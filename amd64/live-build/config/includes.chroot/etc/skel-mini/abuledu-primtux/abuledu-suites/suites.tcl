#!/bin/sh
# suites.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 David Lucardi
# Algorithme principal de :Yann LANGLAIS (http://ilay.org/yann)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : suites.tcl
#  Author  : David Lucardi
#  Modifier:
#  Date    : 15/12/2005
#  Licence : GNU/GPL Version 2 ou plus
#
#  @version    $Id: suites.tcl,v 0.1 2004/12/15 
#  @author     David Lucardi
#  @modifier
#  @project    Le terrier
#  @copyright  David Lucardi
#
################################################################################ 
proc xy_cell {x y} {
global nbcol
return [expr $x*$nbcol +$y]
}

proc cell_xy {cell} {
global nbcol orig taillecell
return "[expr ($cell % $nbcol)*$taillecell + [lindex $orig 0] + $taillecell/2 ] [expr ($cell / $nbcol)*$taillecell + [lindex $orig 1] + $taillecell/2 ]"
}
#######################################################
####################################################""
proc compute_alea {} {
global hsol tab_sol tabcell

		#set indh [expr $hsol - 2]
		#set ind [expr $tabcell($tab_sol($indh))]

	        
	
		set indh [expr $hsol + 2]
		set ind [expr $tabcell($tab_sol($indh))]

		
	     
 

return $ind
}


####################################################""
proc compute_next {ind sens cell} {
global pas typesuite array_pas

	switch $typesuite {
 		0 {
			switch $sens {
				"up" {
					set ind [expr $ind + $pas]
					}
				"down" {
					set ind [expr $ind - $pas]
					}
			}
   		}
 		
		1 {
			switch $sens {
				"up" {
				set ind [expr $ind * $pas]
				}
				"down" {
				set ind [expr $ind / $pas]
				}
			}
   		}

		2 {
			switch $sens {
				"up" {
				set ind [expr $ind + $array_pas($cell)]
				}
				"down" {
				set ind [expr $ind - $array_pas($cell)]
				}
			}
   		}

 		3 {
			switch $sens {
				"up" {
				set ind [expr $ind + $pas]
				}
				"down" {
				set ind [expr $ind - $pas]
				}
			}
   		}

 		4 {
			switch $sens {
				"up" {
				set ind [expr $ind + $pas]
				}
				"down" {
				set ind [expr $ind - $pas]
				}
			}
   		}
	}
return $ind
}

#####################################################################
proc compute_cell_out {nbrow nbcol} {
global tabcell maxparcours
set tmp 0
set tmp2 0

if {$maxparcours == 0} {
  for {set i 0} {$i < [expr $nbrow]} {incr i 1} {
    for {set j 0} {$j < [expr $nbcol]} {incr j 1} {
       if {$i ==0 || $j ==0 || $i==[expr $nbcol-1] || $j==[expr $nbrow-1]} {
	 if {[expr $tmp] < [expr $tabcell([xy_cell $i $j])]} { 
	 set tmp $tabcell([xy_cell $i $j])
	 set tmp2 [xy_cell $i $j]
	 }
       }
    }
  } 
} else {
  for {set i 0} {$i < [expr $nbrow]} {incr i 1} {
    for {set j 0} {$j < [expr $nbcol]} {incr j 1} {
 	if {[expr $tmp] < [expr $tabcell([xy_cell $i $j])]} { 
	set tmp $tabcell([xy_cell $i $j])
	set tmp2 [xy_cell $i $j]
	}
    }
  }	
 }
return $tmp2
}

###############################################################"
proc rand_border_cell {nbrow nbcol} {

    set i [expr int(rand()*4)]
    switch $i {
    0 {
        set j 0
        set i [expr int(rand()*$nbrow)]
	  }
    1 {
        set j [expr $nbcol - 1]
        set i [expr int(rand()*$nbrow)]
	  }
    2 {
        set i 0
        set j [expr int(rand()*$nbcol)]
	  }
    3 {
        set i [expr $nbrow - 1]
        set j [expr int(rand()*$nbcol)]
        }
    }

    return [xy_cell $i $j]
}

###############################################################"""""
proc scanne_cell {in i} {
global nbrow nbcol
set xrow [expr $in/$nbcol]
set ycol [expr $in%$nbcol]

if  {$i == 0} {incr ycol -1}
if  {$i == 1} {incr xrow -1}
if  {$i == 2} {incr ycol}
if  {$i == 3} {incr xrow}
  if {$xrow >=0 && $xrow < $nbrow && $ycol >=0 && $ycol<$nbcol} {
  return [xy_cell $xrow $ycol]
  } else {
  return -1
  }
}
###########################################################################
proc cell_next {in indsuite} {
global tabcell array_pas
set h  0
set history([incr h])  $in
set pp  0
	set tabcell($in) $indsuite
	set indsuite [compute_next $indsuite up $in]
	
	.frame.c itemconfigure [.frame.c find withtag text$in] -text $tabcell($in)
    while {$pp == 0} {
#tk_messageBox -message "indsuite : $indsuite in : $in pas : $array_pas($in)"
        while {$pp == 0} {
        .frame.c itemconfigure [.frame.c find withtag rect$in] -fill pink
            set n 0
            for {set i 0} {$i <= 3} {incr i} {
		set cell [scanne_cell $in $i]
			if {$cell != -1} {
				if {$tabcell($cell) == -1} {
					set dir($n) "$i $cell"
					incr n			
				} 
			}
			
		}

	if {$n==0} {break}

		if {$n == 1} {
		set i 0 
            } else {      
		set i  [expr int(rand()*$n)] 
		}
	set pcell [lindex [lindex [array get dir $i] 1] 1]
	set i [lindex [lindex [array get dir $i] 1] 0]
	set in $pcell
        set history([incr h]) $in
	set tabcell($in) $indsuite
	set indsuite [compute_next $indsuite up $in]
#tk_messageBox -message "indsuite : $indsuite in : $in pas : $array_pas($in)"
	.frame.c itemconfigure [.frame.c find withtag text$in] -text $tabcell($in)

      } 

        if {[expr $h -1] == 0} {break}
        set indsuite [compute_next $indsuite down $in]
	set in $history([incr h -1])
	  

    }
}

###################################################
proc verifcelltext {cell cur_cell ind1} {
global nberreur lhistory typesuite tabcell hsol reverse plafond plancher mess1 mess2 nombrexact
variable flag
set exprverif 0

catch {set exprverif [expr [.frame.c.text1 get 0.0 end]]}
	if {$typesuite == 2} {

		if {$reverse == 0} {
			if {[expr $exprverif < $ind1] == 1 && [expr $exprverif > $tabcell($cur_cell)] == 1} { 
			if {$nombrexact == 1 } {
				if { [expr $exprverif == $tabcell($cell)] ==1 } {
				set flag 1
				set ind1 $exprverif
				} else {
					if { [expr $exprverif - $tabcell($cell)] < 0} {
					set mess "C'est plus."
					if {$plancher < $exprverif} {
					set plancher $exprverif
					set mess1 "Le nombre est plus grand que $plancher."
					}
					} else {
					set mess "C'est moins."
					if {$plafond > $exprverif || $plafond == -1} {
					set plafond $exprverif
					set mess2 "Le nombre est plus petit que $plafond."
					}
					}
				tux_devinette $mess $mess1 $mess2
				#tk_messageBox -message "C'est $mess."
				.frame.c.text1 delete 0.0 end
				#.framebottom.history configure -state normal
				#.framebottom.history delete 0.0 end
				#.framebottom.history insert end " $mess1 \n $mess2"
				#.framebottom.history configure -state disabled
				return
				}
			
			} else {
			set flag 1
			set ind1 $exprverif
			}
			}
		}
		if {$reverse == 1} {
			if {[expr $exprverif > $ind1] == 1 && [expr $exprverif < $tabcell($cur_cell)] == 1} {
			if {$nombrexact == 1 } {
				if { [expr $exprverif == $tabcell($cell)] ==1 } {		
				set flag 1
				set ind1 $exprverif
				} else {
					if { [expr $exprverif - $tabcell($cell)] < 0} {
					set mess "C'est plus."
					if {$plancher < $exprverif} {
					set plancher $exprverif
					set mess1 "Le nombre est plus grand que $plancher."
					}
					} else {
					set mess "C'est moins."
					if {$plafond > $exprverif || $plafond == -1} {
					set plafond $exprverif
					set mess2 "Le nombre est plus petit que $plafond."
					}
					}
				tux_devinette $mess $mess1 $mess2
				#tk_messageBox -message "c'est $mess"
				.frame.c.text1 delete 0.0 end
				#.framebottom.history configure -state normal
				#.framebottom.history delete 0.0 end
				#.framebottom.history insert end " $mess1 \n $mess2"
				#.framebottom.history configure -state disabled
				return
				}

			} else {
			set flag 1
			set ind1 $exprverif
			}
			}
		}
	} else {
	if {$exprverif == $ind1} {set flag 1}
	}

	if {$flag == 1} {
	lappend lhistory "-$ind1"
	bind .frame.c.text1 <Return> ""
	bind .frame.c.text1 <KP_Enter> ""
	incr hsol
	.frame.c itemconfigure text$cell -text $ind1
	.framebottom.binette configure -image [image create photo -file [file join sysdata pbien.gif]]
	tux_reussi
	} else {
	.frame.c.text1 delete 0.0 end
	flipcur wrong $cell
	incr nberreur
	.framebottom.binette configure -image [image create photo -file [file join sysdata pmal.gif]]
	tux_echoue1
		if {$nberreur == 3} {
		#gele_ecran
		#.framebottom.recommence configure -state normal
		#tk_messageBox -message [mc {Trop d'erreurs, il faut recommencer!}]
		#set flag 1
		}
	}

}

#############################################""
proc verifcellkey {cell cur_cell sens} {
global tab_sol tabcell typesuite hsol mess1 mess2 plafond plancher
variable flag
set flag 0

set plafond -1
set plancher -1
set mess1 ""
set mess2 ""


set ind1 $tabcell($cur_cell)
set ind2 $tabcell($cell)

if {$typesuite != 2} {
set ind2 [compute_next $ind2 $sens $cell]
if {[expr $ind1 == $ind2] != 1} {
flipcur2 out $cell
return 0
}
text .frame.c.text1 -height 1 -width 5
.frame.c create window [cell_xy $cell] -window .frame.c.text1
bind .frame.c.text1 <Return> "verifcelltext $cell $cur_cell $tabcell($cell)"
bind .frame.c.text1 <KP_Enter> "verifcelltext $cell $cur_cell $tabcell($cell)"
focus .frame.c.text1
tkwait variable flag
catch {destroy .frame.c.text1}
focus .frame.c 
} else {
set ind2 [compute_alea]
#tk_messageBox -message "tabcell : $tabcell($tab_sol([expr $hsol +1 ])) ind2 : $tabcell($cell)"
if { [expr $tabcell($tab_sol([expr $hsol +1 ])) == $tabcell($cell)] !=1 } {
flipcur2 out $cell
return 0
}
############################################""
text .frame.c.text1 -height 1 -width 5
.frame.c create window [cell_xy $cell] -window .frame.c.text1
bind .frame.c.text1 <Return> "verifcelltext $cell $cur_cell $ind2"
bind .frame.c.text1 <KP_Enter> "verifcelltext $cell $cur_cell $ind2"
focus .frame.c.text1
tkwait variable flag
catch {destroy .frame.c.text1}
focus .frame.c 
}
 }

########################################################
proc verif_cell {cell cur_cell} {
global tabcell out cellvisited lhistory hsol

set ind1 $tabcell($cur_cell)
set ind2 $tabcell($cell)

set ind2 [compute_next $ind2 down $cur_cell]

set ind3 $tabcell($cell)
set ind3 [compute_next $ind3 up $cell]

	if {[expr $ind1 == $ind2] == 1} {
	lappend lhistory "-$tabcell($cell)"
	incr hsol
	}
	if {[expr $ind1 == $ind3] == 1 && $cellvisited($cell) == 1} {
	set lhistory [lreplace $lhistory end end]
	incr hsol -1
	}

	if {[expr $ind1 == $ind2] == 1 || ([expr $ind1 == $ind3] == 1 && $cellvisited($cell) == 1)} {
		if {$cell==$out} {
		return 2
		} else {
		return 1
		}
	} else {
	return 0
	}
}
#################################################################""
proc verif_cell_decomp {cell} {
global tabcell out pas lhistory cellvisited
set ind4 [expr [.frame.c itemcget text$cell -text]]
	if {[expr $ind4 == $pas] != 1} {
	set lhistory [lreplace $lhistory end end]
	return 0
	}

	if {$cell==$out} {
	return 2
	} else {
	if {$cellvisited($cell) != 1} {lappend lhistory "$pas"}
	return 1
	}
}

proc verif_cell_decomp_solve {cell} {
global out
	if {$cell==$out} {
	return 2
	} else {
	return 1
	}
}

proc verif_cell_laby {cell cur_cell} {
global tabcell out cellvisited lhistory hsol

set ind1 $tabcell($cur_cell)
set ind2 $tabcell($cell)
set ind4 [expr [.frame.c itemcget text$cell -text]]
if {[expr $ind4 == $ind2] != 1} {
return 0
}

set ind2 [compute_next $ind2 down $cur_cell]


set ind3 $tabcell($cell)
set ind3 [compute_next $ind3 up $cell]


	if {[expr $ind1 == $ind2] == 1} {
	lappend lhistory "-$tabcell($cell)"
	incr hsol
	}
	if {[expr $ind1 == $ind3] == 1 && $cellvisited($cell) == 1} {
	set lhistory [lreplace $lhistory end end]
	incr hsol -1
	}

	if {[expr $ind1 == $ind2] == 1 || ([expr $ind1 == $ind3] == 1 && $cellvisited($cell) == 1)} {
		if {$cell==$out} {
		return 2
		} else {
		return 1
		}
	} else {
	return 0
	}
}

#################################################################""
proc verif_cell_laby_reverse {cell cur_cell} {
global tabcell out cellvisited lhistory hsol

set ind1 $tabcell($cur_cell)
set ind2 $tabcell($cell)
set ind4 [expr [.frame.c itemcget text$cell -text]]
if {[expr $ind4 == $ind2] != 1} {
return 0
}
set ind2 [compute_next $ind2 down $cur_cell]

set ind3 $tabcell($cell)
set ind3 [compute_next $ind3 up $cell]

if {[expr $ind1 == $ind2] == 1 && $cellvisited($cell) == 1} {
set lhistory [lreplace $lhistory end end]
incr hsol -1
}
if {[expr $ind1 == $ind3] == 1 } {
lappend lhistory "-$tabcell($cell)"
incr hsol
}

	if {([expr $ind1 == $ind2] == 1 && $cellvisited($cell) == 1) || [expr $ind1 == $ind3] == 1} {
	
		if {$cell==$out} {
		return 2
		} else {
		return 1
		}
	} else {
	return 0
	}
}


###########################################################
proc verif_reverse {cell cur_cell} {
global tabcell out cellvisited lhistory hsol


set ind1 $tabcell($cur_cell)
set ind2 $tabcell($cell)
set ind2 [compute_next $ind2 down $cur_cell]

set ind3 $tabcell($cell)
set ind3 [compute_next $ind3 up $cell]

if {[expr $ind1 == $ind2] == 1 && $cellvisited($cell) == 1} {
set lhistory [lreplace $lhistory end end]
incr hsol -1
}
if {[expr $ind1 == $ind3] == 1 } {
lappend lhistory "-$tabcell($cell)"
incr hsol
}

	if {([expr $ind1 == $ind2] == 1 && $cellvisited($cell) == 1) || [expr $ind1 == $ind3] == 1} {
	
		if {$cell==$out} {
		return 2
		} else {
		return 1
		}
	} else {
	return 0
	}
}
#########################################################"
proc flipcur {how what} {

set color1 [.frame.c itemcget [.frame.c find withtag rect$what] -fill]
	switch $how {
		"out" {
		set color2 red
		tux_sorti
		}
		"wrong" {
		set color2 black
		tux_echoue1
		}
		"right" {
		set color2 white
		}
	}

	for  {set i 1} {$i <= 3} {incr i} {
	after 100
	.frame.c itemconfigure [.frame.c find withtag rect$what] -fill $color2
	update
	after 100
	.frame.c itemconfigure [.frame.c find withtag rect$what] -fill $color1
	update
	}
}
################################################################"""
proc flipcur2 {how what} {
set color1 [.frame.c itemcget [.frame.c find withtag rect$what] -fill]
	switch $how {
		"out" {
		set color2 red
		tux_mauvaisedir
		}
		"wrong" {
		set color2 black
		tux_echoue1
		}
		"right" {
		set color2 white
		}
	}

	for  {set i 1} {$i <= 3} {incr i} {
	after 100
	.frame.c itemconfigure [.frame.c find withtag rect$what] -fill $color2
	update
	after 100
	.frame.c itemconfigure [.frame.c find withtag rect$what] -fill $color1
	update
	}
}

##############################################################"
proc gele_ecran {} {
global plateforme
bind .frame.c <Right> ""
bind .frame.c <Left> ""
bind .frame.c <Up> ""
bind .frame.c <Down> ""
	if { $plateforme == "unix" }  {
	bind .frame.c <KP_Right> ""
	bind .frame.c <KP_Left> ""
	bind .frame.c <KP_Down> "m"
	bind .frame.c <KP_Up> ""
	}

}
###################################################"
proc move_cur {dir} {
global color1 trace reverse cellvisited cur_cell lhistory nberreur nomf user listdata niveau hsol next_cell typesuite
.framebottom.binette configure -image [image create photo -file [file join sysdata pneutre.gif]]
	if {$trace == 1} {set color1 blue}
set next_cell [scanne_cell $cur_cell $dir]

	if {$next_cell == -1} {
	flipcur out $cur_cell
	return
	}

	if {[.frame.c itemcget text$next_cell -text] == "?" } {
		tux_consigne_question
		if {$reverse==0} {
		set v_cell [verifcellkey $next_cell $cur_cell down]
		} else {
		set v_cell [verifcellkey $next_cell $cur_cell up]
		}
		if {$v_cell == 0} {return}
	} else {
		if {$reverse==0} {
		if {$typesuite != 3 && $typesuite != 4} {set v_cell [verif_cell $next_cell $cur_cell]}
		if {$typesuite == 3} {set v_cell [verif_cell_laby $next_cell $cur_cell]}
		if {$typesuite == 4} {set v_cell [verif_cell_decomp $next_cell]}

		} else {
		if {$typesuite != 3 && $typesuite != 4} {set v_cell [verif_cell_laby_reverse $next_cell $cur_cell]}
		if {$typesuite == 3} {set v_cell [verif_cell_laby_reverse $next_cell $cur_cell]}
		if {$typesuite == 4} {set v_cell [verif_cell_decomp $next_cell]}
		}
	}

	if {$v_cell == 0} {
	flipcur wrong $next_cell
	incr nberreur
	.framebottom.binette configure -image [image create photo -file [file join sysdata pmal.gif]]
		if {$nberreur == 3} {
		#gele_ecran
		#.framebottom.recommence configure -state normal
		#tk_messageBox -message [mc {Trop d'erreurs, il faut recommencer!}]
		}
	return
	}
.frame.c itemconfigure [.frame.c find withtag rect$cur_cell] -fill $color1
set color1 [.frame.c itemcget [.frame.c find withtag rect$next_cell] -fill]
.frame.c itemconfigure [.frame.c find withtag rect$next_cell] -fill cyan
if {$typesuite != 4} {
.framebottom.history configure -state normal
.framebottom.history delete 0.0 end
.framebottom.history insert end $lhistory
.framebottom.history see end
.framebottom.history configure -state disabled
}
set cur_cell $next_cell
set cellvisited($cur_cell) 1

	if {$v_cell == 2} {
		for  {set i 1} {$i <= 2} {incr i} {
		flipcur right $cur_cell
		}
	gele_ecran
	.framebottom.recommence configure -state normal
	.framebottom.history configure -state normal
	set total [llength $lhistory]
	set score [expr int((($total-1.5*$nberreur)*100)/$total)]
	if {$score <0} {set score 0}
		if {$score >= 75} {
		tux_content $score
		}
		if {$score >= 50 && $score < 75} {
		tux_moyen $score
		}
		if {$score < 50} {
		tux_triste $score
		}

		if {[teste_suite] == 1} {
			if {$nberreur <4} {
			.framebottom.suite configure -state normal
			}
		}
	.framebottom.binette configure -image [image create photo -file [file join sysdata pbien.gif]]
	set activite [lindex [lindex $listdata $niveau] 0]
	enregistreval $nomf $activite $nberreur $score $user
	return
	}
tux_continue_bien $nberreur [llength $lhistory]

}
#########################################################################""
proc teste_suite {} {
global listdata niveau

if {[expr [llength $listdata]-1] > $niveau} {
return 1
}
return 0
}
############################################"
proc ouvre {what} {
global nomf Home_data niveau user 
set ext .conf
    if {$what != ""} {
        if  {[string match -nocase *.conf $what]==0} {
        set nomf $what$ext
	  } else {
	  set nomf $what
	  }
	  set niveau 0
	wm title . "[mc {Le bon chemin}] - $what - [mc {Utilisateur}] : [string map {.log \040} [file tail $user]]"
      #main
	#majmenu
    }
}
##############################################################################"
proc majmenu {} {
global niveau Home_data nomf
catch {.wmenu.menu.activite delete 0 end}
set f [open [file join $Home_data $nomf] "r"]
 set listdata [gets $f]
 close $f

for {set k 0} {$k < [llength $listdata]} {incr k 1} {
.wmenu.menu.activite add command -label [lindex [lindex $listdata $k] 0] -command "set niveau $k ; main"
}
}

###########################################################################"
proc cache_nombre {} {
global tab_sol freq
set ind 0
if {$freq == 0} {
set interv 4
} elseif {$freq == 1} {
set interv 3
} 


set cellkey ""

set long [expr [llength [array names tab_sol]] -1]
if {$freq != 2} {
if {$long > 5} {lappend cellkey [expr int(rand()*2) + $interv]}
	for {set i 5} {$i <$long} {incr i $interv} {
	set cell [expr int(rand()*$interv) + 1 +$i]
		if {$cell < $long} {
		lappend cellkey $cell
		}
	}
} else {
	for {set i 1} {$i <$long} {incr i} {
	lappend cellkey $i
	}
}
foreach item $cellkey {
.frame.c itemconfigure text$tab_sol($item) -text "?"
}
}


###########################################################################"
proc laby_nombre {in out} {
global tab_sol tabcell freqlaby aplusb amoinsb aplusbplusc pas
set cellkey ""
set cellk ""
set listope ""
if {$aplusb ==1} {lappend listope plus}
if {$amoinsb ==1} {lappend listope moins}
if {$aplusbplusc ==1} {lappend listope plusplus}
if {$listope == ""} {lappend listope plus}

set long [expr [llength [array names tab_sol]] -1]
set len [expr [llength [array names tabcell]] -1]

if {$freqlaby == 0} {
set interv 4
} elseif {$freqlaby == 1} {
set interv 3
} 

if {$freqlaby != 2} {
if {$len > 5} {lappend cellk [expr int(rand()*2) + $interv]}
	for {set i 5} {$i <$len} {incr i $interv} {
	set cell [expr int(rand()*$interv) + 1 +$i]
		if {$cell < $len} {
		lappend cellk $cell
		}
	}
} else {
	for {set i 1} {$i <$len} {incr i} {
	lappend cellk $i
	}
}

for {set i 1} {$i <$long} {incr i} {
lappend cellkey $i
}

	for {set i 0} {$i < $len} {incr i} {
	if {[lsearch $cellk $i] != -1} {
		if {$i != $in && $i != $out} {
		set result $tabcell($i)
		substinombrelabyw $listope $i $result
		}
	}
	}
	for {set i 0} {$i < $len} {incr i} {
		if {[lsearch $cellkey $i] != -1 } {
		set result $tabcell($tab_sol($i))
		substinombrelabyr $listope $i $result $cellk
		#if {[lsearch $cellk $i] != -1} {.frame.c itemconfigure text$tab_sol($i) -text "$nb1+$nb2"} else {.frame.c itemconfigure text$tab_sol($i) -text [expr $nb1+$nb2]}
		} 		
	}

}
###########################################################################"""""
proc substinombrelabyw {listope i result} {
global pas
set ind [lindex $listope [expr int(rand()*[llength $listope])]]
	switch $ind {

		plus {
		set nb1 [expr $result - $pas]
		set param [expr int(rand()*2)]
		if {$param == 0} {set param -1}
		set nb2 [expr $pas]
		if {[expr $nb1 + $param +$nb2] != $result} {set expre "[expr $nb1 + $param]+$nb2"} else {set expre "[expr $nb1]+$nb2"}
		}

		moins {
		set nb1 [expr $result + $pas]
		set param [expr int(rand()*2)]
		if {$param == 0} {set param -1}
		set nb2 [expr $pas]
		if {[expr $nb1 + $param -$nb2] != $result} {set expre "[expr $nb1 + $param]-$nb2"} else {set expre "[expr $nb1]-$nb2"}
		}

		plusplus {
		set nb1 [expr int(rand()*$result) +1]
		set param [expr int(rand()*2)]
		if {$param == 0} {set param -1}
		set nb2 [expr int(rand()*($result - $nb1))]
		set nb3 [expr $result- $nb1 - $nb2 +1]
		if {[expr $nb1 + $param +$nb2 + $nb3 + $param] != $result} {set expre "[expr $nb1 + $param]+$nb2+[expr $nb3 + $param]"} else {set expre "[expr $nb1 + $param]+$nb2+[expr $nb3]"}
		}

	}

.frame.c itemconfigure text$i -text $expre
}
###########################################################################"

###########################################################################"""""
proc substinombrelabyr {listope i result cellk} {
global pas tab_sol
set ind [lindex $listope [expr int(rand()*[llength $listope])]]
	switch $ind {

		plus {
		set nb1 [expr $result - $pas]
		set nb2 [expr $pas]
		set expre "$nb1+$nb2" 
		}

		moins {
		set nb1 [expr $result + $pas]
		set nb2 [expr $pas]
		set expre "$nb1-$nb2"
		}

		plusplus {
		set nb1 [expr int(rand()*$result)]
		set nb2 [expr int(rand()*($result - $nb1))]
		set nb3 [expr $result - $nb1 - $nb2]
		set expre "$nb1+$nb2+$nb3"
		}

	}

	if {[lsearch $cellk $i] != -1} {.frame.c itemconfigure text$tab_sol($i) -text $expre} else {.frame.c itemconfigure text$tab_sol($i) -text [expr $expre]}
}
###########################################################################"

###########################################################################"""""
proc substinombredecompw {listope i} {
global pas
set ind [lindex $listope [expr int(rand()*[llength $listope])]]
	switch $ind {

		plus {
		set nb1 [expr int(rand()*$pas) +1]
		set param [expr int(rand()*2)]
		if {$param == 0} {set param -1}
		set nb2 [expr $pas - $nb1]
		if {[expr $nb1 + $param +$nb2] != $pas} {set expre "[expr $nb1 + $param]+$nb2"} else {set expre "[expr $nb1]+$nb2"}
		}

		moins {
		set nb1 [expr int(rand()*$pas) + 1 + $pas]
		set param [expr int(rand()*2)]
		if {$param == 0} {set param -1}
		set nb2 [expr $nb1 - $pas]
		if {[expr $nb1 + $param -$nb2] != $pas} {set expre "[expr $nb1 + $param]-$nb2"} else {set expre "[expr $nb1]-$nb2"}
		}

		plusplus {
		set nb1 [expr int(rand()*$pas) +1]
		set param [expr int(rand()*2)]
		if {$param == 0} {set param -1}
		set nb2 [expr int(rand()*($pas - $nb1))]
		set nb3 [expr $pas - $nb1 - $nb2 +1]
		if {[expr $nb1 + $param +$nb2 + $nb3 + $param] != $pas} {set expre "[expr $nb1 + $param]+$nb2+[expr $nb3 + $param]"} else {set expre "[expr $nb1 + $param]+$nb2+[expr $nb3]"}
		}

	}

.frame.c itemconfigure text$i -text $expre
}
###########################################################################"

###########################################################################"""""
proc substinombredecompr {listope i} {
global pas tab_sol
set ind [lindex $listope [expr int(rand()*[llength $listope])]]

	switch $ind {

		plus {
		set nb1 [expr int(rand()*$pas) +1]
		set nb2 [expr $pas - $nb1]
		set expre "$nb1+$nb2" 
		}

		moins {
		set nb1 [expr int(rand()*$pas) + 1]
		set nb2 [expr $nb1 + $pas]
		set expre "$nb2-$nb1"
		}

		plusplus {
		set nb1 [expr int(rand()*$pas)]
		set nb2 [expr int(rand()*($pas - $nb1))]
		set nb3 [expr $pas - $nb1 - $nb2]
		set expre "$nb1+$nb2+$nb3"
		}

	}

.frame.c itemconfigure text$tab_sol($i) -text $expre
}
###########################################################################"

proc decomp_nombre {in out} {
global tab_sol tabcell aplusb amoinsb aplusbplusc pas
set cellkey ""
set listope ""
if {$aplusb ==1} {lappend listope plus}
if {$amoinsb ==1} {lappend listope moins}
if {$aplusbplusc ==1} {lappend listope plusplus}
if {$listope == ""} {lappend listope plus}
set long [expr [llength [array names tab_sol]] -1]
set len [expr [llength [array names tabcell]] -1]

for {set i 1} {$i <$long} {incr i} {
lappend cellkey $i
}

	for {set i 0} {$i < $len} {incr i} {
		if {$i != $in && $i != $out} {
		substinombredecompw $listope $i
		} else {
		.frame.c itemconfigure text$i -text $pas
		}
	}

	for {set i 0} {$i < $len} {incr i} {
		if {[lsearch $cellkey $i] != -1 } {
		substinombredecompr $listope $i
		} 		
	}

}
###########################################################################"""""
proc interface {} {
global Home basedir plateforme Home_data baseHome progaide nomf


catch {destroy .framebottom}
catch {destroy .wmenu}
catch {destroy .frame}
wm geometry . +0+0


frame .wmenu
pack .wmenu -side top -fill both -expand yes

   menu .wmenu.menu -tearoff 0
    menu .wmenu.menu.fichier -tearoff 0
    .wmenu.menu add cascade -label [mc {Fichier}] -menu .wmenu.menu.fichier
#    .wmenu.menu.fichier add cascade -label [mc {Ouvrir}] -menu .wmenu.menu.fichier.ouvrir
#menu .wmenu.menu.fichier.ouvrir -tearoff 0
#set ext .conf
#    	foreach i [lsort [glob [file join  $Home_data *$ext]]] {
#    	set fich [string map {.conf ""} [file tail $i]]
#    .wmenu.menu.fichier.ouvrir add command -label $fich -command "ouvre $fich"
#    }
	.wmenu.menu.fichier add cascade -label [mc {Editeur}] -menu .wmenu.menu.fichier.editeur
menu .wmenu.menu.fichier.editeur -tearoff 0
	set ext .conf
    	foreach i [lsort [glob [file join  $Home_data *$ext]]] {
    	set fich [string map {.conf ""} [file tail $i]]
    .wmenu.menu.fichier.editeur add command -label $fich -command "exec wish editeur.tcl $fich &"
    }

##########################################################
#.wmenu.menu.fichier add cascade -label [mc {Gestion}] -menu .wmenu.menu.fichier.gestion
#menu .wmenu.menu.fichier.gestion -tearoff 0 
#.wmenu.menu.fichier.gestion add command -label [mc {Utilisateurs}] -command "exec wish gestion.tcl 1 &"

#########################################################
    .wmenu.menu.fichier add sep
    .wmenu.menu.fichier add command -label [mc {Quitter}] -command "destroy ."

    #menu .wmenu.menu.activite -tearoff 0
    #.wmenu.menu add cascade -label [mc {Activites}] -menu .wmenu.menu.activite
    #menu .wmenu.menu.activite.m1 -tearoff 0

    menu .wmenu.menu.options -tearoff 0
    .wmenu.menu add cascade -label [mc {Options}] -menu .wmenu.menu.options
    set ext .msg
      menu .wmenu.menu.options.lang -tearoff 0 
      .wmenu.menu.options add cascade -label "[mc {Langue}]" -menu .wmenu.menu.options.lang
      foreach i [glob [file join  $basedir msgs *$ext]] {
      set langue [string map {.msg ""} [file tail $i]]
      .wmenu.menu.options.lang add radio -label $langue -variable langue -command "setlang $langue"
      }
####################################
menu .wmenu.menu.options.repconf -tearoff 0 
  .wmenu.menu.options add cascade -label [mc {Dossier de travail}] -menu .wmenu.menu.options.repconf
    .wmenu.menu.options.repconf add radio -label [mc {Commun}] -variable repertconf -value 1 -command "changehome; interface"
    .wmenu.menu.options.repconf add radio -label [mc {Individuel}] -variable repertconf -value 0 -command "changehome; interface"

if {$plateforme == "windows"} {
.wmenu.menu add command -label [mc {Utilisateur}] -command "setwindowsusername"
}

menu .wmenu.menu.aide -tearoff 0
  .wmenu.menu add cascade -label [mc {?}] -menu .wmenu.menu.aide

	if {[file exists [file join $baseHome reglages lang.conf]] == 1} {
	set f [open [file join $baseHome reglages lang.conf] "r"]
  	gets $f lang
  	close $f
	set fich "_index.htm"
	set fich $lang$fich
	set aideurl /usr/share/doc/abuledu-suites
	set fichier [file join [pwd] $aideurl $fich]
	} else {
	
	set aideurl /usr/share/doc/abuledu-suites
	set fichier [file join [pwd] $aideurl fr_index.htm]
	}
  


if {[file exists $fichier] != 1} {
set fichier [file join [pwd] aide fr_index.htm]
}
  .wmenu.menu.aide add command -label [mc {Aide}] -command "exec $progaide file:$fichier &"
  .wmenu.menu.aide add command -label [mc {A propos ...}] -command "source apropos.tcl"

. configure -menu .wmenu.menu

menu_page1
}


proc menu_page1 {} {
catch {destroy .framebottom}
catch {destroy .frame}
frame .frame -bg blue
pack .frame -side top -fill both -expand yes
button .frame.c1 -image [image create photo -file [file join sysdata cycle1.png]] -command "menu_page2 1"
grid .frame.c1 -row 0 -column 0 -padx 40 -pady 10
label .frame.l1 -text "Cycle 1"
grid .frame.l1 -row 1 -column 0 -padx 40 -pady 10
button .frame.c2 -image [image create photo -file [file join sysdata cycle2.png]] -command "menu_page2 2"
grid .frame.c2 -row 0 -column 1 -padx 40 -pady 10
label .frame.l2 -text "Cycle 2"
grid .frame.l2 -row 1 -column 1 -padx 40 -pady 10
button .frame.c3 -image [image create photo -file [file join sysdata cycle3.png]] -command "menu_page2 3"
grid .frame.c3 -row 0 -column 2 -padx 40 -pady 10
label .frame.l3 -text "Cycle 3"
grid .frame.l3 -row 1 -column 2 -padx 40 -pady 10
}

proc menu_page2 {what} {
global Home_data
catch {destroy .framebottom}
catch {destroy .frame}
frame .frame -bg blue
pack .frame -side top -fill both -expand yes
label .frame.titre -text "Cycle $what" -background #ff9f80 
grid .frame.titre -row 0 -column 0 -columnspan 3 -sticky news

set ext .conf
set pref cycle$what
set row 1
set col 1
    	foreach i [lsort [glob [file join  $Home_data $pref*$ext]]] {
    	set fich [string range [string map {.conf ""} [file tail $i]] 7 end]
	button .frame.b$row$col -text $fich -command "menu_page3 [file tail $i]" -width 40 -fg brown -bg white
	grid .frame.b$row$col -row $row -column $col -padx 10 -pady 10
	incr row
		if {$row == 8} {
		set row 1
		incr col
		}
    	}
set row 8
button .frame.quit -image [image create photo -file [file join sysdata quitte.gif]] -command "menu_page1" -fg brown -bg white
grid .frame.quit -row $row -column $col -sticky es
}

proc menu_page3 {what} {
global Home_data listdata niveau nomf
ouvre $what
set f [open [file join $Home_data $nomf] "r"]
 set listdata [gets $f]
 close $f

catch {destroy .framebottom}
catch {destroy .frame}

frame .frame -bg blue
pack .frame -side top -fill both -expand yes

label .frame.titre -text [string map {_ \040-\040 .conf \040} $what] -background #ff9f80 
grid .frame.titre -row 0 -column 0 -columnspan 3 -sticky news

set row 1
set col 1
set tmp 0
    	foreach i $listdata {
	button .frame.b$row$col -text [lindex $i 0] -command "set niveau $tmp ; main" -width 40 -fg brown -bg white
	grid .frame.b$row$col -row $row -column $col -padx 10 -pady 10
	incr row
	incr tmp
		if {$row == 8} {
		set row 1
		incr col
		}
    	}
set row 8
button .frame.quit -image [image create photo -file [file join sysdata quitte.gif]] -command "menu_page2 [string range $what 5 5]" -fg brown -bg white
grid .frame.quit -row $row -column $col -sticky es
}

##########################################################################################
proc main {} {
global nbcol nbrow tabcell color1 out cellvisited Home basedir lhistory plateforme Home_data listdata niveau orig taillecell baseHome sysFont user
global typesuite reverse pas trace titre nbrow nbcol cur_cell nberreur progaide nomf trous freq array_pas hsol nombrexact pasmax indmax maxparcours freqlaby aplusb amoinsb aplusbplusc

set nberreur 0
set lhistory ""
set hsol 0
array unset array_pas
array unset tabcell
array unset cellvisited

set f [open [file join $Home_data $nomf] "r"]
 set listdata [gets $f]
 close $f

set titre [lindex [lindex $listdata $niveau] 0]
wm title . "[mc {Le bon chemin}] - $nomf - $titre - [mc {Utilisateur}] : [string map {.log \040} [file tail $user]]"
# typesuite : 0 arithm�tique 1 g�om�trique (indsuite <> 0 pour la g�om�trique) 2 aleaoire 3 labynormal 4 labydecomposition
set typesuite [lindex [lindex [lindex $listdata $niveau] 1] 0]
# taille : 0 petit 1 moyen 2 grand
set nbrow [lindex [lindex [lindex $listdata $niveau] 1] 1]
set nbcol [lindex [lindex [lindex $listdata $niveau] 1] 2]

#reverse : 0 � l'endroit 1 � l'envers
set reverse [lindex [lindex [lindex $listdata $niveau] 1] 3]
set pas [lindex [lindex [lindex $listdata $niveau] 1] 4]
set indsuite [lindex [lindex [lindex $listdata $niveau] 1] 5]
#trace 0 on ne voit pas le chemin parcouru 1 : on le voit
set trace [lindex [lindex [lindex $listdata $niveau] 1] 6]
#Exercice � trous?
set trous [lindex [lindex [lindex $listdata $niveau] 1] 7]
#Frequence des trous?
set freq [lindex [lindex [lindex $listdata $niveau] 1] 8]
#retrouver le nombre exact pour les suites al�atoires?
set nombrexact [lindex [lindex [lindex $listdata $niveau] 1] 9]
# raison max pour les suites al�atoires?
set pasmax [lindex [lindex [lindex $listdata $niveau] 1] 10]
# ind max pour les suites al�atoires?
set indmax [lindex [lindex [lindex $listdata $niveau] 1] 11]
# pas decimal pour les suites al�atoires?
set maxparcours [lindex [lindex [lindex $listdata $niveau] 1] 12]
# frequence des substititions par �critures num�riques pour labynombre
#if {[lindex [lindex [lindex $listdata $niveau] 1] 13] != ""} {set freqlaby [lindex [lindex [lindex $listdata $niveau] 1] 13]}
set freqlaby [lindex [lindex [lindex $listdata $niveau] 1] 13]
set aplusb [lindex [lindex [lindex $listdata $niveau] 1] 14]
set amoinsb [lindex [lindex [lindex $listdata $niveau] 1] 15]
set aplusbplusc [lindex [lindex [lindex $listdata $niveau] 1] 16]
set taillecell 50
set paramfont s
if {[winfo screenwidth .] == "1024"} {
set taillecell 50
set paramfont s
} elseif {[winfo screenwidth .] == "800"} {
set taillecell 40
set paramfont vs
} elseif {[winfo screenwidth .] == "1280"} {
set taillecell 50
set paramfont s
}
set largeur [expr $taillecell*$nbcol]
set hauteur [expr $taillecell*$nbrow]
set orig {10 10}
catch {destroy .framebottom}
catch {destroy .frame}
frame .frame -width [expr [winfo screenwidth .]] -height [expr [winfo screenheight .]]
pack .frame -side top -fill both -expand yes

canvas .frame.c -height [expr $hauteur + 2*[lindex $orig 1]] -width [expr $largeur + 2*[lindex $orig 0]] -bg yellow
pack .frame.c -expand true
.frame.c create rect [lindex $orig 0] [lindex $orig 1] [expr [lindex $orig 0] + $largeur ] [expr [lindex $orig 0] + $hauteur ] -width 2
focus -force .  
set indc 0
   for {set j 0} {$j < [expr $nbrow]} {incr j 1} {
	for {set i 0} {$i < [expr $nbcol]} {incr i 1} {
      .frame.c create rect [expr [lindex $orig 0] + $i*$taillecell] [expr [lindex $orig 1] + $j*$taillecell ]  [expr [lindex $orig 0] + ($i+1)*$taillecell]  [expr [lindex $orig 1] + ($j+1)*$taillecell]  -width 2 -fill gray -tags rect$indc
	.frame.c create text [expr [lindex $orig 0] + $i*$taillecell + $taillecell/2] [expr [lindex $orig 1] + $j*$taillecell + $taillecell/2] -text $indc -tags text$indc -font $sysFont($paramfont)
	incr indc
  }
  }
  for {set i 0} {$i <= [expr $nbcol*$nbrow]} {incr i} {
  set tabcell($i) -1
  }
  for {set i 0} {$i <= [expr $nbcol*$nbrow]} {incr i} {
  set cellvisited($i) 0
  }
##########################################################

if {$typesuite == 2} {
	for {set i 0} {$i <= [expr $nbcol*$nbrow]} {incr i} {
		if {$pasmax != $pas} { set pas [expr int(rand()*($pasmax - $pas +1)) + $pas]}
  	set array_pas($i) [expr int(rand()*$pas)+1]
  	}
}

if {$typesuite != 4} {set in [rand_border_cell $nbrow $nbcol]} else {set in [expr int(rand()*$nbcol)]}

if {$typesuite == 2 } {
if {$indmax != $indsuite} { set indsuite [expr int(rand()*($indmax - $indsuite +1)) + $indsuite]}
}
cell_next $in $indsuite


if {$typesuite != 4} {set out [compute_cell_out $nbrow $nbcol]} else {set out [expr ($nbrow-1)*$nbcol + int(rand()*$nbcol)]}

if {$in == $out} { interface ; return}
if {$reverse ==1} {
set tmp $in
set in $out
set out $tmp
}
set cur_cell $in
set cellvisited($in) 1

frame .framebottom 
pack .framebottom -side top -fill both -expand yes
text .framebottom.history -height 2
pack .framebottom.history -side left -padx 5
label .framebottom.binette -image [image create photo -file [file join sysdata pneutre.gif]]
pack .framebottom.binette -side left
button .framebottom.quit -image [image create photo -file [file join sysdata quitte.gif]] -command "destroy_tux; menu_page3 $nomf"
pack .framebottom.quit -side right
button .framebottom.recommence -image [image create photo again -file [file join sysdata again.gif] ] -state disabled -command "interface; main"
pack .framebottom.recommence -side right
button .framebottom.suite -image [image create photo suite -file [file join sysdata suite.gif]] -state disabled -command "incr niveau ; interface; main"
pack .framebottom.suite -side right
.framebottom.history configure -wrap word -width [expr $nbcol * 5]
lappend lhistory $tabcell($in)
if {$typesuite != 4} {.framebottom.history insert end $lhistory}
.framebottom.history configure -state disabled 
.frame.c itemconfigure [.frame.c find withtag rect$in] -fill yellow
.frame.c addtag curseur withtag rect$in
.frame.c itemconfigure [.frame.c find withtag rect$out] -fill green
set color1 "yellow"
set ext .gif
set coord [.frame.c coords [.frame.c find withtag rect$in]]
.frame.c create image [expr [lindex $coord 0] + $taillecell/2] [expr [lindex $coord 1] + $taillecell/2] -image [image create photo dep -file [file join sysdata depart$taillecell$ext] ]
set coord [.frame.c coords [.frame.c find withtag rect$out]]
.frame.c create image [expr [lindex $coord 0] + $taillecell/2] [expr [lindex $coord 1] + $taillecell/2] -image [image create photo arriv -file [file join sysdata arrive$taillecell$ext] ]

solve $in
set lhistory ""
set hsol 0
lappend lhistory $tabcell($in)
if {$typesuite == 3} {laby_nombre $in $out}
if {$typesuite == 4} {decomp_nombre $in $out}

if {$trous == 1} {cache_nombre}

focus -force .frame.c
bind .frame.c <Right> "move_cur 2"
bind .frame.c <Left> "move_cur 0"
bind .frame.c <Up> "move_cur 1"
bind .frame.c <Down> "move_cur 3"
	if { $plateforme == "unix" }  {
	bind .frame.c <KP_Right> "move_cur 2"
	bind .frame.c <KP_Left> "move_cur 0"
	bind .frame.c <KP_Down> "move_cur 1"
	bind .frame.c <KP_Up> "move_cur 3"
	}
init_tux
tux_commence $typesuite $pas $reverse
}

#################################################################################"
proc solve {in} {
global tab_sol reverse nbcol nbrow tabcell typesuite

array unset tab_sol
  for {set i 0} {$i <= [expr $nbcol*$nbrow]} {incr i} {
  set cellvisited($i) -1
  }
set cellvisited($in) 0
set h  0
set tab_sol($h) $in
set history([incr h])  $in
set pp  0

    while {$pp == 0} {
        while {$pp == 0} {
            set n 0
            for {set i 0} {$i <= 3} {incr i} {
		set cell [scanne_cell $in $i]
			if {$cell != -1} {
				if {$cellvisited($cell) == -1} {
					set dir($n) "$i $cell"
					incr n			
				} 
			}
			
		}

	if {$n==0} {
	break
	}

	if {$n == 1} {
	set i 0 
           } else {      
	set i  [expr int(rand()*$n)] 
	}

	set pcell [lindex [lindex [array get dir $i] 1] 1]
	set i [lindex [lindex [array get dir $i] 1] 0]

 	if {$reverse == 0} {
	if {$typesuite != 4} {set vcell [verif_cell $pcell $in]} else {set vcell [verif_cell_decomp_solve $pcell]}
	} else {
	if {$typesuite != 4} {set vcell [verif_reverse $pcell $in]} else {set vcell [verif_cell_decomp_solve $pcell]}
	}
	if {$vcell == 1 || $vcell == 2} {
	set tab_sol($h) $pcell
	
	if {$vcell == 2} {
	return
	}
	set in $pcell
      set history([incr h]) $in
	set cellvisited($in) 0
      } 
   }
        if {[expr $h -1] == 0} {break}
	  array unset tab_sol $h
        set in $history([incr h -1])
    }
}



##################################################################################
global plateforme ident niveau nomf user

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
set niveau 0
set nomf ""

source fonts.tcl
source path.tcl
source msg.tcl
source eval.tcl
source compagnon.tcl

initapp $plateforme
inithome
initlog $plateforme $ident
wm title . "[mc {Le bon chemin}] - [mc {Utilisateur}] : [string map {.log \040} [file tail $user]]"
wm resizable . 0 0

interface




