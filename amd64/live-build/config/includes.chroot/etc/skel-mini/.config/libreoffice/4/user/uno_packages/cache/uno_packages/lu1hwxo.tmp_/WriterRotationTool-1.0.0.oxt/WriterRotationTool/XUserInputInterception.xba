<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE script:module PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "module.dtd">
<script:module xmlns:script="http://openoffice.org/2000/script" script:name="XUserInputInterception" script:language="StarBasic">REM  *****  BASIC  *****

option explicit

&apos;**************************************************
&apos;AUTHOR		: T. VATAIRE
&apos;DATE		: 22/11/09
&apos;VERSION	: 0.1.0
&apos;**************************************************
&apos;This module contains functions relative to the
&apos;&quot;com.sun.star.awt.XUserInputInterception&quot; interface
&apos;**************************************************
&apos;Dependencies : Logger, Message, MessageOutput
&apos;**************************************************
&apos;LICENCE	: LGPL v3.0
&apos;**************************************************

private const STR_KEY_HANDLER_IFACE = &quot;com.sun.star.awt.XKeyHandler&quot;
private const STR_MOUSE_CLICK_HANDLER_IFACE = &quot;com.sun.star.awt.XMouseClickHandler&quot;
private const STR_USR_INPUT_INTERCEP_IFACE = &quot;com.sun.star.awt.XUserInputInterception&quot;
&apos;the name of the handlers used to (de)activate input devices
private const STR_MOUSE_CLICK_HANDLER_NAME = &quot;_bchMH_&quot;
private const STR_KEY_HANDLER_NAME = &quot;_bchKH_&quot;

&apos;a key handler and a mouse click handler used to intercept events of input devices
private XUserInputInterception_m_obj_currentKeyHandler as object
private XUserInputInterception_m_obj_currentMouseClickHandler as object

&apos;disable keyboard and mouse click events on an object implementing XUserInputInterception interface (usually an Xcontroller object)
&apos;registers a key handler and a mouse click handler to listen input events on &quot;UserInputInterceptionImpl&quot; object.
&apos;events are always consumed while these handlers are registered
&apos;param UserInputInterceptionImpl	: object	: an object which implements XUserInputInterception interface
&apos;return								: boolean	: true if handlers are successfully registered, false otherwise
function XUserInputInterception_lockInputDevices(UserInputInterceptionImpl as object) as boolean

	dim success as boolean
	
	success = (hasUnoInterfaces(UserInputInterceptionImpl, STR_USR_INPUT_INTERCEP_IFACE))
	if (success) then
		if (isNull(XUserInputInterception_m_obj_currentKeyHandler)) then
			XUserInputInterception_m_obj_currentKeyHandler = createUnoListener(STR_KEY_HANDLER_NAME, STR_KEY_HANDLER_IFACE)
		end if
		if (isNull(XUserInputInterception_m_obj_currentMouseClickHandler)) then
			XUserInputInterception_m_obj_currentMouseClickHandler = createUnoListener(STR_MOUSE_CLICK_HANDLER_NAME, STR_MOUSE_CLICK_HANDLER_IFACE)
		end if
		success = ((not isNull(XUserInputInterception_m_obj_currentKeyHandler)) and _
				   (not isNull(XUserInputInterception_m_obj_currentMouseClickHandler)))
		if (success) then
			UserInputInterceptionImpl.addKeyHandler(XUserInputInterception_m_obj_currentKeyHandler)
			UserInputInterceptionImpl.addMouseClickHandler(XUserInputInterception_m_obj_currentMouseClickHandler)
		else
			messageOutput_log(message_new(&quot;XUserInputInterception_lockInputDevices : unable to register all handlers.&quot;, _
										  INT_MESSAGE_TYPE_ERROR))
		end if
	else
		messageOutput_log(message_new(&quot;XUserInputInterception_lockInputDevices : illegal parameter : the object doesn&apos;t implement &lt;&quot; &amp; _
									  STR_USR_INPUT_INTERCEP_IFACE &amp; &quot;&gt; interface.&quot;, INT_MESSAGE_TYPE_ERROR))
	end if
	
	XUserInputInterception_lockInputDevices = success
	
end function

&apos;reactivates keyboard events after that the method &quot;XUserInputInterception_lockInputDevices&quot; have been called.
&apos;param UserInputInterceptionImpl	: object	: an object which implements XUserInputInterception interface
&apos;return								: boolean	: true if handlers are successfully unregistered, false otherwise
function XUserInputInterception_unlockInputDevices(UserInputInterceptionImpl as object) as boolean

	dim success as boolean
	
	success = (hasUnoInterfaces(UserInputInterceptionImpl, STR_USR_INPUT_INTERCEP_IFACE))
	if (success) then
		if (not isNull(XUserInputInterception_m_obj_currentKeyHandler)) then
			UserInputInterceptionImpl.removeKeyHandler(XUserInputInterception_m_obj_currentKeyHandler)
		end if
		if (not isNull(XUserInputInterception_m_obj_currentMouseClickHandler)) then
			UserInputInterceptionImpl.removeMouseClickHandler(XUserInputInterception_m_obj_currentMouseClickHandler)
		end if
	else
		messageOutput_log(message_new(&quot;XUserInputInterception_unlockInputDevices : illegal parameter : the object doesn&apos;t implement &lt;&quot; &amp; _
									  STR_USR_INPUT_INTERCEP_IFACE &amp; &quot;&gt; interface.&quot;, INT_MESSAGE_TYPE_ERROR))
	end if
	
	XUserInputInterception_unlockInputDevices = success
	
end function

</script:module>