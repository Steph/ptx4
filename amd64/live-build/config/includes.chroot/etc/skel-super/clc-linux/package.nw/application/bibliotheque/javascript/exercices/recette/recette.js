var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.recette = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.

var aNombre, sucre, barreG, barreD, etiquette;

// Référencer les ressources de l'exercice (images, sons)
// {
//      nom-de-la-ressource : chemin-du-fichier
// }

exo.oRessources = {
	txt:"recette/textes/recette_fr.json",
	memo:"recette/images/memo.png",
    illustration:"recette/images/illustration.png",
	boutonPlus:"recette/images/bouton_plus.png",
	boutonMoins:"recette/images/bouton_moins.png"
};

// Options par défaut de l'exercice (définir au moins exoName, totalQuestion, tempsExo et tempsQuestion)

exo.creerOptions = function() {
    var optionsParDefaut = {
        temps_exo:0,
		totalQuestion:10,
		temps_question:0,
		niveau:0
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
//Creation des donnees numeriques de l'exo
	/**********************
	* nA : nombre de personnes initial
	* qA : quantité initiale de la recette
	* coeff : coefficient
	* nB : Nombre final de personnes
	* qB : quantité à trouver
	* valGradTemp : valeur d'une graduation
	* soluce : la graduation solution
	******************/
	var i,j,k,nA,nB,qA,qB,valGradTemp,soluce,
		aNA,aQA,aNB,aQB,coeff,aGrad,aCoeff;

	aNombre = [];
	for (i=0;i<exo.options.totalQuestion;i++){
		//rapport entre les grandeurs = x2 x3 x4 ou x5
		if ( exo.options.niveau === 0 ) {
			nA = 3 + Math.floor(Math.random()*(5-3+1));
			aQA=[20,30,40,50];
			util.shuffleArray(aQA);
			qA = aQA[0];
			coeff = 2 + Math.floor(Math.random()*(5-2+1));
			nB = nA * coeff;
			qB = qA * coeff;
			if(Math.floor(qB/10)%5 === 0 ){
				valGradTemp=25;
			}
			else if(qB < 140){
				valGradTemp=10;
			} 
			else if(qB >= 140 ){
				valGradTemp=20;
			}
		}
		//rapport entre les grandeurs = 1/2,1/4,1/5
		else if ( exo.options.niveau == 1 ) { 
			aCoeff =[1/2,1/4,1/5];
			util.shuffleArray(aCoeff);
			coeff = aCoeff[i%3];
			nB = 3 + Math.floor(Math.random()*(5-3+1));
			aQB = [20,30,40,50];
			util.shuffleArray(aQB);
			qB = aQB[0];
			nA = nB/coeff;
			qA = qB/coeff;
			
			if(Math.floor(qB/10)%5 === 0 ){
				valGradTemp=25;
			}
			else if(qB < 140){
				valGradTemp=10;
			} 
			else if(qB >= 140 ){
				valGradTemp=20;
			}
		}
		// propriétés de la linéarite 1 + 1/2
		else if ( exo.options.niveau == 2 ) { 
			aNA = [4,6,8,10,12];
			util.shuffleArray(aNA);
			nA = aNA[i%5];
			aQA=[20,40,50,60,80,120,160];
			qA = aQA[i%7];
			coeff = 1.5;
			nB = nA * coeff;
			qB = qA * coeff;
			if(qB%5 === 0 && qB%10 !== 0){
				valGradTemp=25;
			}
			else if(qB < 140){
				valGradTemp=10;
			} 
			else if(qB >= 140 ){
				valGradTemp=20;
			}
		}
		// passage par l'unité
		else if ( exo.options.niveau == 3 ) { 
			var aQUnitaire = [10,20,25];
			var qUnitaire = aQUnitaire[i%3];
			aNA = [2,3,4,5];
			util.shuffleArray(aNA);
			nA = aNA[i%4];
			qA=nA*qUnitaire;
			nB = nA +1;
			qB = nB * qUnitaire;
			valGradTemp = qUnitaire;
		}
		soluce = qB/valGradTemp;
		aNombre.push([nA,qA,coeff,nB,qB,valGradTemp,soluce]);
		
	}

};

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigneGenerale);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
	var typeVerre = aNombre[q][5];
	var leftVerre, leftBarre, rightBarre, widthVerre;
	exo.keyboard.config({
		numeric:"disabled",
		arrow:"disabled"
	});
	// la recette
	var conteneurRecette = disp.createImageSprite(exo,"memo");
	exo.blocAnimation.append(conteneurRecette);
	conteneurRecette.css({left:-250,top:25});
	var titre = disp.createTextLabel(exo.txt.recette1).css({
		left:40,
		top:70,
		fontSize:20,
		fontWeight:"bold",
		color:"#5F96ED"
	});
	var sRecette  = exo.txt.recette21
				.replace('var1',String(aNombre[q][0]))
				.replace('var2',String(aNombre[q][1]));
	var recette = disp.createTextLabel(sRecette).css({
		left:40,
		top:110,
		fontSize:20,
		color:"#5F96ED"
	});
	conteneurRecette.append(titre,recette);
	// la question
	var sQuestion = exo.txt.question1.replace('var3',aNombre[q][3]);
	var question = disp.createTextLabel(sQuestion).css({
		left:30,
		top:300,
		width:250,
		fontSize:18,
		backgroundColor:"#176CED",
		padding:10,
		color:"white",
		display:'none'
	});
	exo.blocAnimation.append(question);

	// Le verre doseur
	switch(typeVerre){
		case 10:
			widthVerre = 150;
			positionCentraleGraduation = 25;
			leftVerre = 245+170;
			leftBarre = 245+170+2;
			rightBarre = 245+170+2+100;
			coef = 10;
			break;
		case 20:
			widthVerre = 200;
			positionCentraleGraduation = 50;
			leftVerre = 245+145;
			leftBarre = 245+145+2;
			rightBarre = 245+145+2+150;
			coef = 20;
			break;
		case 25:
			widthVerre = 225;
			positionCentraleGraduation = 125/2;
			leftVerre = 245+132;
			leftBarre = 245+132+2;
			rightBarre = 245+132+2+175;
			coef = 25;
			break;
	}
	var verre = disp.createEmptySprite();
	verre.css({
		left:leftVerre,
		top:30,
		width:widthVerre,
		height:270,
		borderColor:"#93D0FA",
		borderWidth:"0 2px 5px 2px",
		borderStyle:"solid",
		borderRadius:3,
		background:"#E2EDF5",
		padding:"0px 0px",
		overflow:"hidden",
	});
	exo.blocAnimation.append(verre);
	//Le sucre
	sucre = disp.createEmptySprite();
	sucre.addClass("sucre");
	sucre.css({
		background:"#FAFEFF",
		width:widthVerre,
		height:270,
		left:0,
		top:270,
		opacity:1
	});
	verre.append(sucre);
	sucre.data("graduation",0);
	//Graduations -> barreG, etiquette et barreD
	barreG = [];
	barreD = [];
	etiquette = [];
	var posLeft, posRight, posTop;
	posLeft = 245+170+2;
	posRight = 245+170+2+100;
	for(i=1;i<=14;i++){
		posTop = 300-i*18;//Hauteur
		//barreG
		barreG[i] = disp.createEmptySprite();
		barreG[i].css({width:50,height:1,left:leftBarre,top:posTop,"background-color":"#bbb"});//ccd1d1
		exo.blocAnimation.append(barreG[i]);
		//etiquette
		valeurAAfficher = coef * i;//15*coef - (coef * i);
		etiquette[i] = disp.createTextLabel(String(valeurAAfficher) + " g");
		etiquette[i].css({color:"#bbb","font-size":"12px"});//,right:230,top:posTop,"text-align":"right"});
		exo.blocAnimation.append(etiquette[i]);
		var positionAIndiquer = "right+"+positionCentraleGraduation+" center";
		etiquette[i].position({
			my:'center center',
			at:positionAIndiquer,
			of:barreG[i]
		});
		//barreD
		barreD[i] = disp.createEmptySprite();
		barreD[i].css({width:50,height:1,left:rightBarre,top:posTop,"background-color":"#bbb"});
		exo.blocAnimation.append(barreD[i]);
	}

	// Les boutons
	var conteneurBouton = disp.createEmptySprite();
	conteneurBouton.css({width:120});
	conteneurBouton.appendTo(exo.blocAnimation);
	conteneurBouton.css({width:120, left:leftVerre+(widthVerre-120)/2,top:320});
	//
	var boutonPlus = disp.createImageSprite(exo,"boutonPlus");
	conteneurBouton.append(boutonPlus);
	boutonPlus.on("mousedown.clc touchstart.clc",gestionClickPlus);
	boutonPlus.css({cursor:"pointer"});
	//
	var boutonMoins = disp.createImageSprite(exo,"boutonMoins");
	conteneurBouton.append(boutonMoins);
	boutonMoins.on("mousedown.clc touchstart.clc",gestionClickMoins);
	boutonMoins.css({left:120-38,cursor:"pointer"});
	
	//apparition de la recette
	conteneurRecette.transition({x:+290,delay:200},400,"linear",function(){question.show();});
	
	// gestion des boutons
	function gestionClickPlus(e) {
		e.preventDefault();
		var grad = sucre.data("graduation");
		if (grad < 14) {
			grad++;
			sucre.transition({y:"-=18"},100,'linear');
			sucre.data("graduation",grad);
		}
	}
	
	function gestionClickMoins(e) {
		e.preventDefault();
		var grad = sucre.data("graduation");
		if (grad > 0) {
			grad--;
			sucre.transition({y:"+=18"},100,'linear');
			sucre.data("graduation",grad);
		}
	}
	
};


// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    var q = exo.indiceQuestion;
	var soluce = aNombre[q][6];
	var graduationEleve = sucre.data("graduation");
    if (graduationEleve === 0) {
		return "rien"; 
    } else if( graduationEleve == soluce ) {
        return "juste";
    } else {
        return "faux";
    }
};

// Correction (peut rester vide)

exo.corriger = function() {
    var q = exo.indiceQuestion;
	var indiceReponseJuste = aNombre[q][6];
	barreG[indiceReponseJuste].css({"background-color":"red"});
	barreD[indiceReponseJuste].css({"background-color":"red"});
	etiquette[indiceReponseJuste].css({color:"red",fontWeight:"bold",fontSize:14});
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
//    var conteneur = disp.createOptConteneur();
//    exo.blocParametre.append(conteneur);
var controle;
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:4,
        nom:"temps_question",
        texte:exo.txt.option1
    });
    exo.blocParametre.append(controle);
    
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"niveau",
        texte:exo.txt.option2,
        aValeur:[0,1,2,3],
        aLabel:exo.txt.valeur2
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));