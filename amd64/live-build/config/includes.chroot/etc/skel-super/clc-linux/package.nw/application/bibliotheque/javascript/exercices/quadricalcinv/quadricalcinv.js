var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.quadricalcinv = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
//exo.tabletSupport="debug";
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre;
var aPlageA = [], aPlageB = [], aCouple = [], intId, aValeurT = [], aValeurTe = [];;
// Référencer les ressources de l'exercice (image, son)
exo.oRessources = { 
    illustration : "quadricalcinv/images/illustration.png",
    txt:"quadricalcinv/textes/quadricalcinv_fr.json"
}
// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:1,
        tempsExo:0,						
        nombre_a:"2-9",
        nombre_b:"2-9",
        totalTentative:10,
        operateur:[1000], //1 addition,10 soustraction,100 multiplication,1000 division euclidienne
        defilement:1000,
        acceleration:1000,
        nDecimalesA:0,
	nDecimalesB:0
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}
// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    //console.log(exo.options.plageA)
    aPlageA = util.getArrayNombre(exo.options.nombre_a);
    aPlageB = util.getArrayNombre(exo.options.nombre_b);
    var lengthA = aPlageA.length;
    var lengthB = aPlageB.length;
    // on genere tous les couples possibles 
    for( var i = 0 ; i < lengthA ; i++ ) {
        var nA = aPlageA[i];
        for ( var j = 0 ; j < lengthB ; j++ ) {
            var nB = aPlageB[j];
            if ( aCouple.indexOf([nA,nB]))
                aCouple.push([nA,nB]);
        }
    }
    aCouple.sort(function(){return Math.floor(Math.random()*3)-1});
    // on evite de manipuler un tableau trop long (100 couples maxi)
    if( aCouple.length > 100 ) {
        aCouple = aCouple.slice(0,exo.options.totalTentative);
    }
    // si les parametres ne génèrent pas assez de couples on répète
    else if ( aCouple.length < exo.options.totalTentative ) {
        var n = 0;
        while ( (exo.options.totalTentative - aCouple.length)>0 || n < 100) {
            n++;
            aCouple = aCouple.concat(aCouple.slice(0,exo.options.totalTentative - aCouple.length));
        }
    }
}
//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration)
}
//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    exo.btnValider.hide();
    exo.keyboard.config({
		numeric : "disabled",
		arrow : "enabled",
		large : "disabled"
    });
    //var indexOperateur = indexTentative % options.choixOperateur.length;
    //var operateur = options.choixOperateur[indexOperateur];
    //le conteneur
    var conteneur = disp.createEmptySprite();
    var paper = Raphael(conteneur[0],560,360)
    conteneur.html(paper.node);
    exo.blocAnimation.append(conteneur);
    conteneur.position({
        my:"center top",
        at:"center top+20",
        of:exo.blocAnimation
    });
    //on cree les briques et les etiquettes de la ligne de base
    var aBrique=[];
    var aValeurBase = updateLigneBase();
	console.log("aValeurBase",aValeurBase)
    //console.log(aValeurBase);
    for( var j = 0 ; j < 9 ; j++ ) {
        for( var i = 0 ; i < 7 ; i++ ) {
            var brique = {};
            brique.fond = paper.rect(i*80,j*40,80,40);
            brique.fond.attr("stroke","#333");
            brique.fond.attr("stroke-width",0.1);
            if(j==8){
                // la ligne de base
                brique.base = true;
                brique.valeur=aValeurBase[i][1];
                brique.texte=aValeurBase[i][0];
                console.log(brique.texte);
                brique.fond.attr("gradient","90-#FF9900-#FFBB00");
                brique.label = paper.text(i*80+40,j*40+20,brique.texte);
                brique.label.attr("font-size",16);
            } else {
                brique.fond.attr("fill","#ffffff");
                brique.base = false;
            }
            aBrique.push(brique);
            brique.indiceb = aBrique.length - 1;
        }
    }
    // on cree la brique active et son etiquette
    var alea = 5;
    var briqueActive = aBrique[3];
    briqueActive.valeur = updateBriqueActive().valeur;
    briqueActive.texte = updateBriqueActive().texte;
    briqueActive.label = paper.text(3*80+40,20,briqueActive.texte);
    briqueActive.label.attr("font-size",16);
    briqueActive.label.attr("fill","#fff");
    briqueActive.fond.attr("gradient","90-#02A001-#00CC00");;
    // on ajoute la gestion du clavier
    exo.baseContainer.on("keydown",gererClavier);
    //$(document).on("keyup",gererChute);
    // on démarre l'animation de la brique active
    var vitesse = exo.options.defilement;
    var count = 0;
    intId = setInterval(
        function(){
            bougerBriqueBas(briqueActive);
            evalBrique(briqueActive);
        },
        vitesse
    );
    exo.baseContainer.unload(function(e){
        console.log("unload");
        clearInterval(intId)
    })
    // 
    function gererClavier(e){
        if ( e.which == 37 && briqueActive.indiceb%7>0 && aBrique[briqueActive.indiceb-1].base == false) {
            bougerBriqueGauche(briqueActive);
            evalBrique(briqueActive);
            
        }
        else if ( e.which == 39 && briqueActive.indiceb%7<6 && aBrique[briqueActive.indiceb+1].base == false) {
            bougerBriqueDroite(briqueActive);
            evalBrique(briqueActive);
            
        }
        else if ( e.which == 40 && Math.floor(briqueActive.indiceb/7)>0 && Math.floor(briqueActive.indiceb/7)<7) {
            clearInterval(intId);
            tomberBrique(briqueActive);
            intId = setInterval(
                function(){
                    bougerBriqueBas(briqueActive);
                    evalBrique(briqueActive);
                    return false;
                },
                vitesse
            )
        }
        return false;
    }
    /*
    function gererChute(e){
        //console.log(e.which)
        if ( e.which == 40 ) {
            clearInterval(intId);
            tomberBrique(briqueActive);
            intId = setInterval(
                function(){
                    bougerBriqueBas(briqueActive);
                    evalBrique(briqueActive);
                },
                vitesse
            )
            return false;
        }
    }
    */
    //
    function bougerBriqueBas(b){   
        var valeur = b.valeur;
        var texte = b.texte;
        b.label.remove();
        b.fond.attr("fill","#fff");
        // la brique en dessous de la brique active n'est pas une brique de base
        if( aBrique[b.indiceb+7].base == false ) {
            // la brique en dessous devient brique active
            briqueActive = aBrique[b.indiceb+7];
            briqueActive.fond.attr("gradient","90-#02A001-#00CC00");;
            if (briqueActive.label != undefined)
                briqueActive.label.remove();
            briqueActive.valeur = valeur;
            briqueActive.texte = texte;
            var posX = briqueActive.fond.getBBox().x+40;
            var posY  = briqueActive.fond.getBBox().y+20;
            briqueActive.label = paper.text(posX,posY,briqueActive.texte);
            briqueActive.label.attr("font-size",16);
            briqueActive.label.attr("fill","#fff");
        }
    }
    //
    function bougerBriqueGauche(b){
        var valeur = b.valeur;
        var texte = b.texte;
        b.fond.attr("fill","#fff");
        b.label.remove();
        briqueActive = aBrique[b.indiceb-1];
        briqueActive.fond.attr("gradient","90-#02A001-#00CC00");;
        briqueActive.valeur = valeur;
        briqueActive.texte = texte;
        var posX = briqueActive.fond.getBBox().x+40;
        var posY  = briqueActive.fond.getBBox().y+20;
        briqueActive.label = paper.text(posX,posY,texte);
        briqueActive.label.attr("font-size",16);
        briqueActive.label.attr("fill","#fff");
    }
    //
    function bougerBriqueDroite(b){
        var valeur = b.valeur;
        var texte = b.texte;
        b.fond.attr("fill","#fff");
        b.label.remove();
        briqueActive = aBrique[b.indiceb+1];
        briqueActive.fond.attr("gradient","90-#02A001-#00CC00");;
        briqueActive.valeur = valeur;
        briqueActive.texte = texte;
        var posX = briqueActive.fond.getBBox().x+40;
        var posY  = briqueActive.fond.getBBox().y+20;
        briqueActive.label = paper.text(posX,posY,texte);
        briqueActive.label.attr("font-size",16);
        briqueActive.label.attr("fill","#fff");
    }
    function tomberBrique(b){
        var valeur = b.valeur;
        var texte = b.texte;
        b.label.remove();
        b.fond.attr("fill","#fff");
        // la brique en dessous de la brique active n'est pas une brique de base
        var n=1;
        while(aBrique[b.indiceb+7*n].base==false){
            n++
        };
        //
        briqueActive = aBrique[b.indiceb+7*(n-1)];
        briqueActive.fond.attr("gradient","90-#02A001-#00CC00");;
        if (briqueActive.label != undefined)
            briqueActive.label.remove();
        briqueActive.valeur = valeur;
        briqueActive.texte = texte;
        var posX = briqueActive.fond.getBBox().x+40;
        var posY  = briqueActive.fond.getBBox().y+20;
        briqueActive.label = paper.text(posX,posY,briqueActive.texte);
        briqueActive.label.attr("font-size",16);
        briqueActive.label.attr("fill","#fff");
    }
    //
    function evalBrique(b){
        //console.log("indiceTentative",exo.indiceTentative)
        $(document).off("keydown",gererClavier);
        var nextBrique = aBrique[b.indiceb+7];
        var chucky = b.indiceb+7;
        // perdu
        if(nextBrique.base == true && nextBrique.valeur != b.valeur){
            //console.log("perdu");
            exo.indiceTentative++;
            testFaute=true;
            nextBrique.fond.attr("gradient","90-#ff0000-#ff5500");
            nextBrique.label.remove();
            nextBrique.base = false;
            //b.fond.attr("fill","#ff9900");
            b.fond.attr("gradient","90-#FF9900-#FFBB00");
            b.base = true;
            b.valeur = nextBrique.valeur;
            b.label.remove();
            var posX = b.fond.getBBox().x+40;
            var posY  = b.fond.getBBox().y+20;
            b.label = paper.text(posX,posY,aValeurTe[chucky%7][0]);
            b.label.attr("font-size",16);
            // si on a epuisé toutes les tentatives fin du jeu
            if(exo.indiceTentative == exo.options.totalTentative) {
                clearInterval(intId);
                $(document).off("keydown",gererClavier);
                exo.terminerExercice();
                console.log("fin");
                return "fin";
            }
            //on replace la brique active en haut
            if(b.indiceb > 6) {
                briqueActive = aBrique[3];
                //briqueActive.valeur = b.valeur;
                briqueActive.label = paper.text(3*80+40,20,briqueActive.texte);
                briqueActive.label.attr("font-size",16);
                briqueActive.label.attr("fill","#fff");
                briqueActive.fond.attr("gradient","90-#02A001-#00CC00");
            }
            //C'est terminé y a plus de place
            else {
                clearInterval(intId);
                $(document).off("keydown",gererClavier);
                exo.terminerExercice();
                console.log("fin");
                return "fin";
            }
            $(document).on("keydown",gererClavier);
        }
        //gagné
        else if ( nextBrique.base == true && nextBrique.valeur == b.valeur ){
            console.log("gagne");
            exo.indiceTentative++;
            exo.score++;
            exo.refreshScore();
            // si on a epuisé toutes les tentatives fin du jeu
            if(exo.indiceTentative == exo.options.totalTentative) {
                clearInterval(intId);
                $(document).off("keydown",gererClavier);
                exo.terminerExercice();
                for (var i = 0 ; i < aBrique.length ; i++ ) {
                    var brique = aBrique[i];
                    if(brique.base == true) {
                        brique.fond.attr("fill","#ffff00");
                        brique.fond.animate({fill:"#ff9900"},700,function(e){
                            this.attr("gradient","90-#FF9900-#FFBB00")
                        });
                    }
                }
                b.fond.attr("fill","#fff");
                b.label.remove();
                console.log("fin");
                return "fin";
            }
            // on met à jour les etiquettes de la ligne de base
            var aValeurBase = updateLigneBase();
			console.log("aValeurBase",aValeurBase)
            var count = 0;
            for (var i = 0 ; i < aBrique.length ; i++ ) {
                var brique = aBrique[i];
                if(brique.base == true) {
                    brique.label.remove();
                    var posX = brique.fond.getBBox().x+40;
                    var posY  = brique.fond.getBBox().y+20;
                    brique.valeur = aValeurBase[count][1];
                    brique.label = paper.text(posX,posY,aValeurBase[count][0]);
                    brique.label.attr("font-size",16);
                    brique.fond.attr("fill","#ffff00");
                    brique.fond.animate({fill:"#ff9900"},700,function(e){
                        this.attr("gradient","90-#FF9900-#FFBB00")
                    });
                    //brique.fond.attr("gradient","90-#FF7700-#FF9900");
                    count++
                }
            }
            //on met a jour la brique active
            setTimeout(function(e){
                b.fond.attr("fill","#fff");
                b.label.remove();
                briqueActive = aBrique[3];
                briqueActive.label.remove();
                briqueActive.valeur = updateBriqueActive().valeur;
                briqueActive.texte = updateBriqueActive().texte;
                briqueActive.label = paper.text(3*80+40,20,briqueActive.texte);
                briqueActive.label.attr("font-size",16);
                briqueActive.label.attr("fill","#fff");
                briqueActive.fond.attr("gradient","90-#02A001-#00CC00");
                $(document).on("keydown",gererClavier);
            },200);
        }
        // pas arrive en bas
        else {
            $(document).on("keydown",gererClavier);
        }   
    }
    function updateLigneBase(aValeur){
        //console.log("exo.indiceTentative",exo.indiceTentative);
        //console.log("exo.options",exo.options);
        var indice = exo.indiceTentative%exo.options.operateur.length;
        //console.log("indice",indice)
        var coperateur = exo.options.operateur[indice];
        //console.log("op",coperateur);
        var nA = aCouple[exo.indiceTentative][0];
        var nB = aCouple[exo.indiceTentative][1];
        aValeurTe =[];
        //console.log("nA nB",nA,nB)
        //addition
        if ( coperateur == 1 ) {
            aValeur=[];
            aValeur.push([nA+" + "+nB,nA+nB]);//la solution
            aValeurT=[];//pour tester si on a un doublon
            aValeurT.push(nA+nB);
            // on choisit 6 autres propositions toutes différentes
            for ( var i = 0 ; i < aCouple.length ; i ++ ) {
                var valeur = aCouple[i][0]+aCouple[i][1];
                if( aValeurT.indexOf(valeur) < 0 ) {
                    aValeur.push([aCouple[i][0]+" + "+aCouple[i][1],valeur]);
                }
                if(aValeur.length==7)
                    break;
            }
        }
        // multiplication
        else if ( coperateur == 100 ) {
            aValeur=[];
            aValeur.push([nA+" x "+nB,nA*nB]);//la solution
            aValeurT=[];//pour tester si on a un doublon
            aValeurT.push(nA*nB);
            // on choisit 6 autres propositions toutes différentes
            for ( var i = 0 ; i < aCouple.length ; i ++ ) {
                var valeur = aCouple[i][0]*aCouple[i][1];
                if( aValeurT.indexOf(valeur) < 0 ) {
                    aValeur.push([aCouple[i][0]+" x "+aCouple[i][1],valeur]);
                }
                if(aValeur.length == 7)
                    break;
            }
        }
        // soustraction
        else if ( coperateur == 10 ) {
            aValeur=[];
            aValeur.push([Math.max(nA,nB)+" - "+Math.min(nA,nB),Math.abs(nA-nB)]);//la solution
            aValeurT=[];//pour tester si on a un doublon
            aValeurT.push(Math.abs(nA-nB));
            // on choisit 6 autres propositions toutes différentes
            for ( var i = 0 ; i < aCouple.length ; i ++ ) {
                var valeur = Math.max(aCouple[i][0],aCouple[i][1])-Math.min(aCouple[i][0],aCouple[i][1]);
                if( aValeurT.indexOf(valeur) < 0 ) {
                    aValeur.push([Math.max(aCouple[i][0],aCouple[i][1])+" - "+Math.min(aCouple[i][0],aCouple[i][1]),valeur]);
                }
                if(aValeur.length==7)
                    break;
            }
        }
        //division
        else if ( coperateur == 1000 ) {
            aValeur=[];
            aValeur.push([nA*nB+" : "+nA,nB]);//la solution
            aValeurT=[];//pour tester si on a un doublon
            aValeurT.push(nB);
            // on choisit 6 autres propositions toutes différentes
            for ( var i = 0 ; i < aCouple.length ; i ++ ) {
                var valeur = aCouple[i][0];
                if( aValeurT.indexOf(valeur) < 0 ) {
                    aValeur.push([aCouple[i][0]*aCouple[i][1]+" : "+aCouple[i][1],valeur]);
                }
                if(aValeur.length==7)
                    break;
            }
        }
        
        aValeur.sort(function(a,b){
            var res;
            a >= b ? res = 1 : res = -1;
            return res;
        });
        // si on n'a pas trouve 6 valeurs différentes on rajoute des valeurs supplémentaires
        while(aValeur.length<7){
            var valSup = aValeur[aValeur.length-1]+1;
            if( aValeur.indexOf(valSup) < 0 ) {
                aValeur.push(valSup);
            }
        }
        aValeurTe = aValeur;
        return aValeur;
    }
    
    function updateBriqueActive(){
        var indice = exo.indiceTentative;
        var coperateur = exo.options.operateur[exo.indiceTentative%exo.options.operateur.length];
        //alert("operateur"+coperateur)
        var nA = aCouple[indice][0];
        var nB = aCouple[indice][1];
        var valeur,texte;
        if ( coperateur == 1 ) {
            valeur = nA+nB;
            //texte = nA + " + " + nB;
            texte = valeur;
        }
        else if ( coperateur == 100 ) {
            valeur = nA*nB;
            //texte = nA + " x " + nB;
            texte = valeur;
        }
        else if ( coperateur == 10 ) {
            var max = Math.max(nA,nB);
            var min = Math.min(nA,nB);
            valeur = max - min;
            //texte = max + " - " +min;
            texte = valeur;
        }
        else if ( coperateur == 1000 ) {
            valeur = nB;
            //texte = nA*nB + " : " + nA;
            texte = valeur;
        }
        return {
            valeur:valeur,
            texte:texte
        }
    }
}

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
   
    return "juste"
    
}

// Correction (peut rester vide)

exo.corriger = function() {
    
}

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"totalTentative",
        texte:exo.txt.option1
    });
    exo.blocParametre.append(controle);
    //
    var controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:300,
        //taille:50,
        nom:"nombre_a",
        texte:exo.txt.option2
    });
    exo.blocParametre.append(controle);
    //
    var controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:300,
        //taille:50,
        nom:"nombre_b",
        texte:exo.txt.option3
    });
    exo.blocParametre.append(controle);
     //
    var controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:300,
        //taille:50,
        nom:"nDecimalesA",
        texte:exo.txt.option8
    });
    exo.blocParametre.append(controle);
    //
    var controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:300,
        //taille:50,
        nom:"nDecimalesB",
        texte:exo.txt.option9
    });
    exo.blocParametre.append(controle);
    var controle = new disp.createOptControl(exo,{
        type:"radio",
        nom:"defilement",
        texte:exo.txt.option4,
        aLabel : exo.txt.option5,
        aValeur : [1400,1000,800,600,400]
    });
    /*exo.blocParametre.append(controle);
    var controle = new disp.createOptControl(exo,{
        type:"radio",
        nom:"acceleration",
        texte:"Acceleration : ",
        aLabel : ["très lente","lente","moyenne","rapide","très rapide"],
        aValeur : [1400,1000,800,600,400]
    });*/
    exo.blocParametre.append(controle);
    var controle = new disp.createOptControl(exo,{
        type:"checkbox",
        nom:"operateur",
        texte:exo.txt.option6,
        aLabel : exo.txt.option7,
        aValeur : [1,10,100,1000]
    });
    exo.blocParametre.append(controle);
}
exo.unload = function(){
    console.log("unload")
    clearInterval(intId)
}
/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC));