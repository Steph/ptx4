(function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.mbrique = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre;
var arNombre,paperQuestion,aPile,aStore;
var creerBriqueSet = function(paper,largeur,valeur,couleur1,couleur2,etiquetteVisible) {
    var lblVisible = etiquetteVisible || "visible";
    var hauteur = 5+(valeur*2);
    var offsetPlot = 0;
    if (hauteur<40) offsetPlot = (40-hauteur-4.5)/2 ;
    var offsetPlaque = offsetPlot+4.5;
    var offsetLabel = offsetPlot+(4.5+hauteur)/2;
    var st = paper.set();
    st.push(paper.rect(0,0,largeur*20,40).attr({fill:"#ccc",stroke:"none",opacity:0}));//poignee pour faciliter le deplacement des petites briques sur tablette
    for(var i=0;i<largeur;i++){
        st.push(paper.rect(4+(i*20),offsetPlot,12,4.5).attr({fill:couleur2,stroke:"none"}));
    }
    st.push(paper.rect(0,offsetPlaque,largeur*20,hauteur).attr({fill:"90-"+couleur1+"-"+couleur2,stroke:"none"}));
    st.data={empile:false, hauteur:hauteur, top:offsetPlaque, valeur:valeur};
    
    if(lblVisible == "visible"){
        var label = paper.text(st.getBBox().width/2,st.getBBox().height/2,valeur);
        label.attr({"font-size":"14px","font-weight":"bold"});
        st.push(label);
    }
    // necessaire parce que raphael drag sur des elements text ne fonctionne pas dans IOS
    st.push(paper.rect(0,0,largeur*20,st.getBBox().height).attr({fill:"#fff",stroke:"none",opacity:0}));
    //
    st.attr("cursor","pointer");
    return st;
};

// Référencer les ressources de l'exercice (image, son)

exo.oRessources = { 
    illustration : "mbrique/images/illustration.png"
};

// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        temps_question:0,
        temps_exo:180,
        typeCalcul:4
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
    var nDizaineCible,cible,nDizaineA,nUniteA,nA,nB,nC;
    arNombre = [];
    creerDonnees(exo.options.typeCalcul,exo.options.totalQuestion);
    
    function creerDonnees(typeCalcul,max) {
        if (typeCalcul == 1) {
            //compléter à une dizaine <= 50
            for (i=0;i<max;i++) {
                nDizaineCible = 3 + Math.floor(Math.random()*(5-3+1));
                cible = nDizaineCible * 10;
                nDizaineA = 1 + Math.floor(Math.random()*((nDizaineCible-2)-1+1));
                nUniteA = 2 + Math.floor(Math.random()*(8-2+1));
                nA = nDizaineA*10 + nUniteA;
                nB = 10 - nUniteA;
                nC = cible - nA - nB;
                if(util.find([nA,nB,nC,cible],arNombre)>0) {
                    i-- ;
                } else {
                    arNombre.push([nA,nB,nC,cible]);
                }
            }
        } else if (typeCalcul == 2) {
            //compléter à une dizaine entre 50 et 90
            for (i=0;i<max;i++) {
                nDizaineCible = 5 + Math.floor(Math.random()*(9-5+1));
                cible = nDizaineCible * 10;
                nDizaineA = 1 + Math.floor(Math.random()*((nDizaineCible-2)-1+1));
                nUniteA = 2 + Math.floor(Math.random()*(8-2+1));
                nA = nDizaineA*10 + nUniteA;
                nB = 10 - nUniteA;
                nC = cible - nA - nB;
                if(util.find([nA,nB,nC,cible],arNombre)>0) {
                    i-- ;
                } else {
                    arNombre.push([nA,nB,nC,cible]);
                }
            }
        } else if (typeCalcul == 3) {
            //compléter à 100 à partir d'un nombre >= 50
            for (i=0;i<max;i++) {
                nDizaineCible = 10;
                cible = nDizaineCible * 10;
                nDizaineA = 5 + Math.floor(Math.random()*((nDizaineCible-2)-5+1));
                nUniteA = 2 + Math.floor(Math.random()*(8-2+1));
                nA = nDizaineA*10 + nUniteA;
                nB = 10 - nUniteA;
                nC = cible - nA - nB;
                if(util.find([nA,nB,nC,cible],arNombre)>0) {
                    i-- ;
                } else {
                    arNombre.push([nA,nB,nC,cible]);
                }
            }
        } else if (typeCalcul == 4) {
            //compléter à 100 à partir d'un nombre < 50
            for (i=0;i<max;i++) {
                nDizaineCible = 10;
                cible = nDizaineCible * 10;
                nDizaineA = 1 + Math.floor(Math.random()*(4-1+1));
                nUniteA = 2 + Math.floor(Math.random()*(8-2+1));
                nA = nDizaineA*10 + nUniteA;
                nB = 10 - nUniteA;
                nC = cible - nA - nB;
                if(util.find([nA,nB,nC,cible],arNombre)>0) {
                    i-- ;
                } else {
                    arNombre.push([nA,nB,nC,cible]);
                }
            }
        }
    }
    

    
};

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("Math brique");
    exo.blocConsigneGenerale.html("Poser des briques avec la souris<br />et calculer la hauteur de la construction.");
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.append(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var svgCnt = disp.createSvgContainer(735,430).css({top:20});
    paperQuestion = svgCnt.paper;
    exo.blocAnimation.append(svgCnt);
    
    aStore=[];
    var brique,offsetLeft,offsetTop;
    //Les briques jaunes
    for(i=0;i<8;i++) {
        j=i+2;
        brique = creerBriqueSet(paperQuestion,4,j,"#FFA109","#FFCB00");
        aStore.push(brique);
        offsetLeft = 20+90*(i%4);
        offsetTop = Math.floor(i/4)*40;
        brique.transform("T"+offsetLeft+","+offsetTop);
        brique.data.oX=offsetLeft;
        brique.data.oY=offsetTop;
        brique.drag(dragMove,dragStart,dragEnd,brique,brique,brique);
    }
    
    for(i=0;i<4;i++) {
        j=10+(i*10);
        brique = creerBriqueSet(paperQuestion,4,j,"#FFA109","#FFCB00");
        aStore.push(brique);
        offsetLeft = 20+90*(i%4);
        offsetTop = 90;
        brique.transform("T"+offsetLeft+","+offsetTop);
        brique.data.oX=offsetLeft;
        brique.data.oY=offsetTop;
        brique.drag(dragMove,dragStart,dragEnd,brique,brique,brique);
    }
    
    for(i=0;i<4;i++) {
        j=80-(i*10);
        brique = creerBriqueSet(paperQuestion,4,j,"#FFA109","#FFCB00");
        aStore.push(brique);
        offsetLeft = 20+90*(i%4);
        offsetTop = 140+(i*19);
        brique.transform("T"+offsetLeft+","+offsetTop);
        brique.data.oX=offsetLeft;
        brique.data.oY=offsetTop;
        brique.drag(dragMove,dragStart,dragEnd,brique,brique,brique);
    }

    //La zone de drop
    aPile=[];
    var valeurDepart=arNombre[exo.indiceQuestion][0];
    var stDrop = paperQuestion.set();
    var plaque = creerBriqueSet(paperQuestion,8,0,"#ccc","#ccc","hidden");
    plaque.transform("T0,"+(280-plaque.data.top-plaque.data.hauteur+5));
    stDrop.push(plaque);
    var empreinte = paperQuestion.rect(40,0,80,280).attr("stroke","none");
    stDrop.push(empreinte);
    var briqueBleue = creerBriqueSet(paperQuestion,4,valeurDepart,"#3366CC","#3399CC");
    briqueBleue.transform("T40,"+(280-briqueBleue.data.top-briqueBleue.data.hauteur));
    stDrop.push(briqueBleue);
    stDrop.transform("...T400,10");
    aPile.push(briqueBleue);
    var hauteurPile = briqueBleue.data.hauteur;
   
    // la consigne
    var stConsigne = paperQuestion.set();
    stConsigne.push(paperQuestion.rect(0,0,250,100,5).attr({fill:"#B7F300",stroke:"none"}));
    stConsigne.push(paperQuestion.text(125,35,"Pose des briques\npour que la hauteur égale").attr({"font-size":"20px"}));
    stConsigne.push(paperQuestion.text(125,75,arNombre[exo.indiceQuestion][3]).attr({"font-size":"32px","font-weight":"bold"}));
    stConsigne.transform("T"+((735-stConsigne.getBBox().width)/2)+",315");
    
    
    
    function dragMove(dx,dy){
        this.transform("...T"+(dx - (this.dx || 0))+","+(dy - (this.dy || 0)));
        this.dx = dx;
        this.dy = dy;
    }
    
    function dragStart(x,y){
        this.dx = this.dy = 0;
        this.toFront();
    }
    
    function dragEnd(e){
        var ebox = empreinte.getBBox();
        var sbox = this.getBBox();
        if( Raphael.isBBoxIntersect(ebox,sbox)){
            console.log("sur la pile");
            if(!this.data.empile){
                hauteurPile+=(this.data.hauteur);
                aPile.push(this);
            }
            this.transform("...T"+(ebox.x - sbox.x)+","+((ebox.y-sbox.y-this.data.top)+(ebox.height-hauteurPile)));
            this.data.empile=true;
            for(var i=0;i<aPile.length-1;i++){
                aPile[i].undrag();
            }
        } else{
            console.log("en dehors de la pile");
            if(this.data.empile){
                hauteurPile-=(this.data.hauteur);
                aPile.pop(this);
                var lastBrique = aPile[aPile.length-1];
                
                if(aPile.length>1) lastBrique.drag(dragMove,dragStart,dragEnd,lastBrique,lastBrique,lastBrique);
            }
            this.transform("...T"+(this.data.oX - sbox.x)+","+(this.data.oY-sbox.y));
            this.data.empile=false;
        }
    }
    
    
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    var nBrique = aPile.length;
	var total=0;
    var i;
	for(i=0;i<nBrique;i++) {
		total+= aPile[i].data.valeur;
	}
	if (nBrique<2) {
		return "rien";
	}
	else if (total == arNombre[exo.indiceQuestion][3]) {
        for( i = 0; i < aStore.length; i++ ){
            aStore[i].undrag();
        }
		return "juste";
	} else {
        for( i = 0; i < aStore.length; i++ ){
            aStore[i].undrag();
        }
		return "faux";
	}
};

// Correction (peut rester vide)

exo.corriger = function() {
    var stCor = paperQuestion.set();
    var plaque = creerBriqueSet(paperQuestion,8,0,"#ccc","#ccc","hidden");
    plaque.transform("T0,"+(280-plaque.data.top-plaque.data.hauteur+5));
    stCor.push(plaque);
    var briqueBleue = creerBriqueSet(paperQuestion,4,arNombre[exo.indiceQuestion][0],"#3366CC","#3399CC");
    briqueBleue.transform("T40,"+(280-briqueBleue.data.top-briqueBleue.data.hauteur));
    var hauteurPile = briqueBleue.data.hauteur;
    stCor.push(briqueBleue);
    var brique = creerBriqueSet(paperQuestion,4,arNombre[exo.indiceQuestion][1],"#FFA109","#FFCB00");
    brique.transform("T40,"+(280-hauteurPile-brique.data.top-brique.data.hauteur));
    hauteurPile+=brique.data.hauteur;
    stCor.push(brique);
    brique = creerBriqueSet(paperQuestion,4,arNombre[exo.indiceQuestion][2],"#FFA109","#FFCB00");
    brique.transform("T40,"+(280-hauteurPile-brique.data.top-brique.data.hauteur));
    stCor.push(brique);
    stCor.transform("...T570,10");
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions : "
    });
    exo.blocParametre.append(controle);
    //
   controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"temps_exo",
        texte:"Temps pour réaliser l'exercice (en secondes)  : "
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"temps_question",
        texte:"Temps pour réaliser l'exercice (en secondes)  : "
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"typeCalcul",
        texte:"Type de calcul : ",
        aLabel:[
            "compléter à une dizaine <= 50",
            "compléter à une dizaine entre 50 et 90",
            "compléter à 100 à partir d'un nombre >= 50",
            "compléter à 100 à partir d'un nombre < 50"
        ],
        largeur:500,
        aValeur:[1,2,3,4]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));