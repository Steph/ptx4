############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : calapa.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 08/02/2003
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: barque.tcl,v 1.1.1.1 2004/04/16 11:45:40 erics Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#calapa.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
global classe listdata indclasse tabbigbarques tabsmallbarques tbundobarques bigbarquepos smallbarquepos comptesmallbarques nbbarques flaginit nbpetites nbgrandes
#comptesmallbarques : incr�ment� � chaque fois qu'on a une petite barque plac�e, test� pour la fonctionnalit� nombre de barques � une place limit�
#nbbarques : nombre de barque total, variable utilis�e par verif pour s'assurer qu'il n'y a pas de barque vide � la fin du jeu gagnant
#classe : nombre (2, 3,4 ou 5) indiquant la nature des groupes
#indclasse : nombre d'animaux s�lectionn�s � la fois, utilis� pour d�terminer si le groupe est complet pour �tre mis manuellement dans la barque
#flaginit :drapeau de d�tection d�but de jeu, pour le placement et le comportement des boutons

global erreurselection mauvaisgroupe
#animal d�j� s�lectionn�
set erreurselection 0
#animaux mal group�s pour la mise en barque
set mauvaisgroupe 0


set tabbigbarques {}
set tabsmallbarques {}
set tbundobarques {}

set classe [lindex [lindex $listdata 0] 2]
if {$classe == ""} {
tk_messageBox -message [mc {Pas de donnees valides pour le scenarios selectionne.}] -type ok -title [mc {Barques}]
return}
set tmp 0
switch $classe {
5 {
set tmp 6
set smallbarquepos { {60 250} {110 250} {160 250} {210 250} {260 250} {310 250} {360 250} {410 250} {460 250} {510 250} {60 320} {110 320} {160 320} {210 320} {260 320} {310 320} {360 320} {410 320} {460 320} {510 320} {60 390} {110 390} {160 390} {210 390} {260 390} {310 390} {360 390} {410 390} {460 390} {510 390}}
set bigbarquepos {{60 250} {310 250} {60 320} {310 320} {60 390} {310 390}}
for {set i 0} {$i < 30} {incr i 1} {
lappend tabsmallbarques $i
}
}
4 {
set tmp 6
set smallbarquepos { {60 250} {110 250} {160 250} {210 250} {260 250} {310 250} {360 250} {410 250} {60 320} {110 320} {160 320} {210 320} {260 320} {310 320} {360 320} {410 320} {60 390} {110 390} {160 390} {210 390} {260 390} {310 390} {360 390} {410 390}}
set bigbarquepos {{60 250} {260 250} {60 320} {260 320} {60 390} {260 390}}
for {set i 0} {$i < 24} {incr i 1} {
lappend tabsmallbarques $i
}
}
3 {
set tmp 9
set bigbarquepos {{60 250} {210 250} {360 250} {60 320} {210 320} {360 320} {60 390} {210 390} {360 390}}
set smallbarquepos { {60 250} {110 250} {160 250} {210 250} {260 250} {310 250} {360 250} {410 250} {460 250} {60 320} {110 320} {160 320} {210 320} {260 320} {310 320} {360 320} {410 320} {460 320} {60 390} {110 390} {160 390} {210 390} {260 390} {310 390} {360 390} {410 390} {460 390}}
for {set i 0} {$i < 27} {incr i 1} {
lappend tabsmallbarques $i
}
}
2 {
set tmp 15
set smallbarquepos { {60 250} {110 250} {160 250} {210 250} {260 250} {310 250} {360 250} {410 250} {460 250} {510 250} {60 320} {110 320} {160 320} {210 320} {260 320} {310 320} {360 320} {410 320} {460 320} {510 320} {60 390} {110 390} {160 390} {210 390} {260 390} {310 390} {360 390} {410 390} {460 390} {510 390}}
set bigbarquepos { {60 250} {160 250} {260 250} {360 250} {460 250} {60 320} {160 320} {260 320} {360 320} {460 320} {60 390} {160 390} {260 390} {360 390} {460 390}}
for {set i 0} {$i < 30} {incr i 1} {
lappend tabsmallbarques $i
}
}
1 {
set tmp 30
set smallbarquepos { {60 250} {110 250} {160 250} {210 250} {260 250} {310 250} {360 250} {410 250} {460 250} {510 250} {60 320} {110 320} {160 320} {210 320} {260 320} {310 320} {360 320} {410 320} {460 320} {510 320} {60 390} {110 390} {160 390} {210 390} {260 390} {310 390} {360 390} {410 390} {460 390} {510 390}}
set bigbarquepos { {60 250} {110 250} {160 250} {210 250} {260 250} {310 250} {360 250} {410 250} {460 250} {510 250} {60 320} {110 320} {160 320} {210 320} {260 320} {310 320} {360 320} {410 320} {460 320} {510 320} {60 390} {110 390} {160 390} {210 390} {260 390} {310 390} {360 390} {410 390} {460 390} {510 390}}
#set bigbarquepos { {60 250} {160 250} {260 250} {360 250} {460 250} {60 320} {160 320} {260 320} {360 320} {460 320} {60 390} {160 390} {260 390} {360 390} {460 390}}
for {set i 0} {$i < 30} {incr i 1} {
lappend tabsmallbarques $i
}
}
}
set indclasse 0
set comptesmallbarques 0
set nbbarques 0
set nbpetites 0
set nbgrandes 0

set flaginit 0
for {set i 0} {$i < $tmp} {incr i 1} {
lappend tabbigbarques $i
}


	for {set i 0} {$i < [llength $tabsmallbarques]} {incr i 1} {
	set t1 [expr int(rand()*[llength $tabsmallbarques])]
	set t2 [expr int(rand()*[llength $tabsmallbarques])]
	set tmp [lindex $tabsmallbarques $t1]
	set tabsmallbarques [lreplace $tabsmallbarques $t1 $t1 [lindex $tabsmallbarques $t2]]
	set tabsmallbarques [lreplace $tabsmallbarques $t2 $t2 $tmp]
	}

	for {set i 0} {$i < [llength $tabbigbarques] } {incr i 1} {
	set t1 [expr int(rand()*[llength $tabbigbarques])]
	set t2 [expr int(rand()*[llength $tabbigbarques])]
	set tmp [lindex $tabbigbarques $t1]
	set tabbigbarques [lreplace $tabbigbarques $t1 $t1 [lindex $tabbigbarques $t2]]
	set tabbigbarques [lreplace $tabbigbarques $t2 $t2 $tmp]
	}



set ext .gif
image create photo bigbarque -file [file join images barque$classe$ext] 
image create photo smallbarque -file [file join images barque1.gif] 


#gestion de la barre des boutons suivant l'activit� (avec animaux, sans, communication)
proc placeboutons {c index} {
global classe listdata
set ext .gif
	if {$index == 0 || $index ==1 } {
	button .bframe.butclasse -image [image create photo -file [file join images butbarque$classe$ext] ] -background #ff80c0 -command "gerebarque $c grande"
	grid .bframe.butclasse -column 3  -padx 5 -row 0 -sticky w
	if {$classe !=1} {
	button .bframe.butbarque -image [image create photo -file [file join images butbarque1.gif] ] -background #ff80c0 -command "gerebarque $c petite"
	grid .bframe.butbarque -column 4  -padx 5 -row 0 -sticky w
	}
	button .bframe.poubelle -image [image create photo -file [file join images poubelle.gif] ] -background #ff80c0 -command "gerebarque $c undo"
	grid .bframe.poubelle -column 5  -padx 5 -row 0 -sticky w
	}

	if {$index ==2} {
	button .bframe.butok -image [image create photo -file [file join sysdata ok1.gif] ] -background #ff80c0 -command "departcom $c"
	grid .bframe.butok -column 2  -padx 5 -row 0 -sticky w
		if { [lindex [lindex $listdata 0] 5]==1} {
		button .bframe.butclasse -image [image create photo -file [file join images butbarque$classe$ext] ] -background #ff80c0 -command "gerebarque $c grande" -state disabled
		grid .bframe.butclasse -column 3  -padx 5 -row 0 -sticky w
		if {$classe !=1} {
		button .bframe.butbarque -image [image create photo -file [file join images butbarque1.gif] ] -background #ff80c0 -command "gerebarque $c petite" -state disabled
		grid .bframe.butbarque -column 4  -padx 5 -row 0 -sticky w
		}
		button .bframe.poubelle -image [image create photo -file [file join images poubelle.gif] ] -background #ff80c0 -command "gerebarque $c undo" -state disabled
		grid .bframe.poubelle -column 5  -padx 5 -row 0 -sticky w
		}
	}
}

#gestion du jeu et des d�placements
proc itemStartDrag {c x y} {
    global lastX lastY sourcecoord flag
    set sourcecoord [$c coords current]
    set lastX [$c canvasx $x]
    set lastY [$c canvasy $y]
    $c raise current
    .bframe.tete configure -image neutre
    }

proc highlight {c image gamma} {
$image configure -gamma $gamma
$c itemconfigure current -image $image
#update
#after 500
#$image configure -gamma 1.0
}


proc itemStopDrag {c x y} {
global indens serieencours classe listdata indclasse erreurselection 
#indens : tableaux des indens($i) qui d�nombrent les animaux de chaque serie (ind$serieencours)
#nbens : tableaux des nbens($i) total des animaux de chaque serie

#indclasse : nombre d'animaux s�lectionn�s � la fois, utilis� pour d�terminer si le groupe est complet pour �tre mis manuellement dans la barque
set tmp [$c itemcget current -image]
set id [lindex [$c gettags current] [lsearch -regexp [$c gettags current] uid*]]
highlight $c $tmp 2.5
	if {$id== "uidens(0)" || [lsearch [$c gettags current] compte] != -1 || ($serieencours!= "" && [string range $id 3 end] != $serieencours)} {
	highlight $c $tmp 0.01
	foreach imag [image names] {
			$imag configure -gamma 1.0
			}

	$c dtag uid$serieencours compte
	set serieencours ""
	set indclasse 0
	incr erreurselection
      .bframe.tete configure -image mal
	return
	}
incr indclasse
$c addtag compte withtag current
set serieencours [string range $id 3 end]
}

proc itemDrag {c x y} {
}

#placement manuel des animaux dans les barques
proc verif {c what} {
#what : petiteb ou grandeb
global nbens indens serieencours listdata indclasse classens classe nbbarques mauvaisgroupe erreurselection user nbpetites nbgrandes
$c addtag cible withtag current

	foreach imag [image names] {
	$imag configure -gamma 1.0
	}
set ciblepos [$c bbox cible]
	switch $what {
	petiteb { set compte 1}
	grandeb { set compte $classe}
	}

	if {$compte != $indclasse} {
	$c dtag uid$serieencours compte
	#set serieencours ""
	set indclasse 0
	$c dtag all cible
	incr mauvaisgroupe
	.bframe.tete configure -image mal
	} else {
	$c dtag $what current
	incr ind$serieencours $compte
	#comptitem : variable locale pour placer l'animal dans la grande barque les uns � c�t� des autres
	set comptitem 0
		foreach item [$c find withtag uid$serieencours] {
			if {[lsearch [$c gettags $item] compte] != -1} {
			$c coords $item [expr [lindex $ciblepos 0] + $comptitem*50 + 15] [expr [lindex $ciblepos 1] - 15]
			$c dtag $item compte
			$c dtag $item drag
			$c dtag $item anim1
			incr comptitem
			}
		}
	$c dtag cible $what
	incr nbbarques -1
	$c raise cible
	$c dtag all cible
	set indclasse 0
	.bframe.tete configure -image bien
	}

	if {$indens(1) == $nbens(1) &&  $nbbarques == 0} {
	$c dtag all drag
	############################################eval
	appendeval "\173[mc {Nombre d'erreurs de s�lection :}]$erreurselection\175\040\173[mc {Erreurs placement barques  :}]$mauvaisgroupe\175\040\173[mc {Nombre de grandes barques :}] $nbgrandes\175\040\173[mc {Nombre de petites barques :}]$nbpetites\175\040\173[mc {Exercice reussi}]\175" $user

	geresuite $c
	}
#set serieencours ""
}

proc gerebarque {c what} {
global classe listdata indclasse tabbigbarques tabsmallbarques tbundobarques bigbarquepos smallbarquepos comptesmallbarques nbbarques flaginit nbpetites nbgrandes
if {$flaginit == 0 && ([lindex [lindex $listdata 0] 1]==0 || [lindex [lindex $listdata 0] 1]==1) } {
button .bframe.butok -image [image create photo -file [file join sysdata ok1.gif] ] -background #ff80c0 -command "ok $c"
grid .bframe.butok -column 2  -padx 5 -row 0 -sticky w
}
if {$flaginit == 0 && [lindex [lindex $listdata 0] 1]==1} {
#set flaginit 1
foreach item [$c find withtag uidens(1)] {
$c itemconfigure $item -state hidden
}
}
set flaginit 1

	switch $what {
		grande {
			if {[llength $tabbigbarques] != 0} {
				for {set i 0} {$i < $classe } {incr i 1} {
				set ind [lsearch $tabsmallbarques [expr $classe*[lindex $tabbigbarques 0] + $i]]
				set tabsmallbarques [lreplace $tabsmallbarques $ind $ind]
				}
      		set idt [$c create image [lindex $bigbarquepos [lindex $tabbigbarques 0]] -image bigbarque -anchor "sw" -tags grandeb]
			lappend tbundobarques $idt\040[lindex $tabbigbarques 0]\040grande
			set tabbigbarques [lreplace $tabbigbarques 0 0]
			incr nbbarques
			incr nbgrandes
			}
		}

		petite { 
			if {[llength $tabsmallbarques] != 0} {
				set ind [lsearch $tabbigbarques [expr int([lindex $tabsmallbarques 0]/$classe)]]
				set tabbigbarques [lreplace $tabbigbarques $ind $ind]
				set idt [$c create image [lindex $smallbarquepos [lindex $tabsmallbarques 0]] -image smallbarque -anchor "sw" -tags petiteb]
				lappend tbundobarques $idt\040[lindex $tabsmallbarques 0]\040petite
				set tabsmallbarques [lreplace $tabsmallbarques 0 0]
			incr comptesmallbarques
			if {[lindex [lindex $listdata 0] 3] == 1 && $comptesmallbarques >= [expr $classe - 1]} {
			.bframe.butbarque configure -state disabled
			}
			incr nbbarques
			incr nbpetites
			}
		}

		undo {
			if {[llength $tbundobarques] != 0} {
				$c delete [lindex [lindex $tbundobarques end] 0]
				incr nbbarques -1
					switch  [lindex [lindex $tbundobarques end] 2] {
						petite {
						lappend tabsmallbarques [lindex [lindex $tbundobarques end] 1]
						incr comptesmallbarques -1
						if {$comptesmallbarques < [expr $classe - 1]} {
						.bframe.butbarque configure -state normal
						}
						incr nbpetites -1
						set indbase [expr [lindex [lindex $tbundobarques end] 1] - int(fmod([lindex [lindex $tbundobarques end] 1],$classe))]
						set comptemp 0
							for {set i 0} {$i < $classe } {incr i 1} {
								if {[lsearch $tabsmallbarques [expr $indbase + $i]] != -1} {
								incr comptemp
								}
							}
							if {$comptemp == $classe} {
							lappend tabbigbarques [expr int($indbase/$classe)]
							}
						}

						grande {
						incr nbgrandes -1
						lappend tabbigbarques [lindex [lindex $tbundobarques end] 1]
							for {set i 0} {$i < $classe } {incr i 1} {
							lappend tabsmallbarques [expr [lindex [lindex $tbundobarques end] 1]*$classe + $i]
							}
						}
					}
			set tbundobarques [lreplace $tbundobarques end end]
			}
		}
	}
}

# pour la situation de communication ou d'animaux masqu�s
proc departcom {c} {
global flaginit listdata classe
#if {$flaginit == 0 && [lindex [lindex $listdata 0] 1]==2} 
set flaginit 1
set ext .gif
.bframe.consigne configure -text [mc {Place les barques}]

foreach item [$c find withtag uidens(1)] {
$c itemconfigure $item -state hidden
}
.bframe.butok configure -command "ok $c"
if { [lindex [lindex $listdata 0] 5]==0} {
button .bframe.butclasse -image [image create photo -file [file join images butbarque$classe$ext] ] -background #ff80c0 -command "gerebarque $c grande"
grid .bframe.butclasse -column 3  -padx 5 -row 0 -sticky w
if {$classe !=1} {
button .bframe.butbarque -image [image create photo -file [file join images butbarque1.gif] ] -background #ff80c0 -command "gerebarque $c petite"
grid .bframe.butbarque -column 4  -padx 5 -row 0 -sticky w
}
button .bframe.poubelle -image [image create photo -file [file join images poubelle.gif] ] -background #ff80c0 -command "gerebarque $c undo"
grid .bframe.poubelle -column 5  -padx 5 -row 0 -sticky w
} else {
.bframe.butclasse configure -state normal
if {$classe !=1} {
.bframe.butbarque configure -state normal
}
.bframe.poubelle configure -state normal
}
}

#Gestion du bouton ok, suivant l'�tape du jeu indiqu�e par flaginit
proc ok {c} {
global listdata classe flaginit
.bframe.consigne configure -text [mc {Place les animaux dans les barques}]
if {$flaginit == 1} {
foreach item [$c find withtag uidens(1)] {
$c itemconfigure $item -state normal
}
}
#catch {destroy .bframe.butclasse}
#catch {destroy .bframe.butbarque}
.bframe.butclasse configure -state disabled
if {$classe !=1} {
.bframe.butbarque configure -state disabled
}
catch {destroy .bframe.poubelle}
#placement manuel des animaux dans les barques
	if {[lindex [lindex $listdata 0] 4] == 0} {
	.bframe.butok configure -command "ok2 $c"
	$c bind petiteb <ButtonRelease-1> "verif $c petiteb"
	$c bind grandeb <ButtonRelease-1> "verif $c grandeb"
	$c bind drag <ButtonRelease-1> "itemStopDrag $c %x %y"
	$c bind drag <ButtonPress-1> "itemStartDrag $c %x %y"
	$c bind drag <B1-Motion> "itemDrag $c %x %y"
	} else {
#placement automatique des animaux dans les barques
	verif2 $c
	}
}

#echec � l'exercice
proc ok2 {c} {
global erreurselection nbgrandes nbpetites user mauvaisgroupe
catch {destroy .bframe.butok} 
$c bind petiteb <ButtonRelease-1> ""
$c bind grandeb <ButtonRelease-1> ""

$c bind drag <ButtonRelease-1> ""
$c bind drag <ButtonPress-1> ""
$c bind drag <B1-Motion> ""
.bframe.consigne configure -text [mc {Essaie encore une fois}]
.bframe.tete configure -image mal
button .bframe.recommencer -image [image create photo -file [file join sysdata again2.gif] ] -background #ff80c0 -command "recommence $c"
grid .bframe.recommencer -column 2  -padx 20 -row 0 -sticky w
appendeval "\173[mc {Nombre d'erreurs de selection :}]$erreurselection\175\040\173[mc {Erreurs sur groupement pour barques :}]$mauvaisgroupe\175\040\173[mc {Nombre de grandes barques :}] $nbgrandes\175\040\173[mc {Nombre de petites barques :}] $nbpetites\175\040\173[mc {Echec}]\175" $user
}

#placement automatique des animaux
proc verif2 {c} {
global classe nbbarques erreurselection user nbpetites nbgrandes mauvaisgroupe
#animaux : liste temporaire pour �num�rer les animaux � placer
foreach animal [$c find withtag uidens(1)] {
lappend animaux $animal
}
#nbitem : nombre d'animaux correctement place 
set nbitem 0 

#indanimal : nombre total des animaux � placer
set indanimal [expr [llength $animaux] -1]
		foreach item [$c find withtag grandeb] {
			set ciblepos [$c bbox $item]
			#comptitem : variable locale pour placer l'animal dans la grande barque les uns � c�t� des autres
			set comptitem 0
				for {set i 0} {$i < $classe } {incr i 1} {
					if {$indanimal >=0} {
					set animal [lindex $animaux $indanimal]
					$c raise $animal
					$c coords $animal [expr [lindex $ciblepos 0] + $comptitem*50 + 15] [expr [lindex $ciblepos 1] - 15]
					incr indanimal -1
					incr comptitem
					incr nbitem
					$c dtag $animal uidens(1)
					$c dtag $animal drag
					$c dtag $animal anim1
					}
				}
		$c raise $item
		if {$comptitem == [expr $classe]} {incr nbbarques -1}
		}
		foreach item [$c find withtag petiteb] {
			set ciblepos [$c bbox $item]
					if {$indanimal >=0} {
					set animal [lindex $animaux $indanimal]
					$c raise $animal
					$c coords $animal [expr [lindex $ciblepos 0] + 15] [expr [lindex $ciblepos 1] - 15]
					incr nbitem
					incr indanimal -1
					$c dtag $animal uidens(1)
					$c dtag $animal drag
					$c dtag $animal anim1
					incr nbbarques -1
					}
		$c raise $item	
		}

if {$nbitem == [llength $animaux] && $nbbarques == 0} {
##############################eval
appendeval "\173[mc {Nombre d'erreurs de selection :}]$erreurselection\175\040\173[mc {Erreurs sur groupement pour barques :}]$mauvaisgroupe\175\040\173[mc {Nombre de grandes barques :}] $nbgrandes\175\040\173[mc {Nombre de petites barques :}] $nbpetites\175\040\173[mc {Exercice reussi}]\175" $user
geresuite $c
} else {
ok2 $c
}
}


placeboutons $c [lindex [lindex $listdata 0] 1]
