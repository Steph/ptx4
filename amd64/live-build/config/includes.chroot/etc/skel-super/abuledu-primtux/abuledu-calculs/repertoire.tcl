#
proc repertoire { n} {
global w2 tab_config nom_sce var_reper flag_af
if { $flag_af  == 1 }  {
set n $tab_config($nom_sce,diviseur_fixe) 
}
catch {destroy .top1 }
set w2 [toplevel .top1]
wm geometry $w2 180x320+550+150
wm title $w2 "[mc {R�pertoire de }]$n"
wm resizable $w2 no no
grab $w2 ; raise $w2
 #frame . -height 420 -width 420
#frame .frame -background #aaaaaa -height 50 -width 80
#pack .frame
##frame .frame1.trait_v -width 3 -height 550 -bg red
##place .frame1.trait_v -x 202 -y 55
label ${w2}.titre -text "Table de $n :" -font {arial 10}
place ${w2}.titre -x 45 -y 0 
for { set i 1} { $i <= 10} {incr i} {
label ${w2}.${i} -text "$i x $n = [expr $i * $n]" -font {arial 10}
place ${w2}.${i} -x 2 -y [expr $i * 20] 
frame ${w2}.trait_h${i} -width 180 -height 1 -bg #0f1245
place ${w2}.trait_h${i} -x 0 -y  [expr [expr $i * 20] + 1]
}
frame ${w2}.trait_h11 -width 180 -height 1 -bg #0f1245
place ${w2}.trait_h11 -x 0 -y  221
frame ${w2}.trait_v -width 1 -height 200 -bg #0f1245
place ${w2}.trait_v -x 120 -y 20

bind ${w2} <Button-1> "choix %x %y $n "
entry ${w2}.result -width 10 -font {arial 10} -justify center -bg red
place ${w2}.result -x 50 -y 230
${w2}.result insert 0 0
button ${w2}.mult10 -text [mc {*10}] -command " maj *"
place ${w2}.mult10 -x 50 -y 255
button ${w2}.div10 -text [mc {/10}] -command " maj \/"
place ${w2}.div10 -x 92 -y 255
button ${w2}.bon -text [mc {Valider}] -command "accepter $n"
place ${w2}.bon -x 67 -y 285
}

proc accepter {n} {
global w2 tab_config nom_sce
if {!$tab_config($nom_sce,flag_recup_valeur)  } {
destroy ${w2} ; return
} else {
.frame2.calcul_fois insert 0 [${w2}.result get]
.frame2.n_fois insert 0 [expr [${w2}.result get] / $n ]
destroy ${w2}
}
}
proc maj {oper} {
global w2 
set res [${w2}.result get]
${w2}.result delete 0 end
${w2}.result insert 0 [expr $res $oper 10]   
}

proc choix {X Y N} {
global w2 
#.frame2.n_fois insert 0 coucou
#set y [lindex 1 $place]
# .frame2.n_fois insert 0 $Y 
#focus -force .frame2.n_fois 
#$w2.result  insert 0 [expr 1 * $N]
incr Y -20
if {  $Y >= 5 && $Y  < 20 } {
  $w2.result delete 0 end ; $w2.result  insert 0 [expr 1 * $N]
return
}
if {  $Y >= 20 && $Y  < 40  } {
 $w2.result delete 0 end ; $w2.result  insert 0 [expr 2 * $N]
 return
}

if {  $Y >= 40 && $Y  < 60 } {
  $w2.result delete 0 end ; $w2.result  insert 0 [expr 3 * $N]
return
}
if {  $Y >= 60 && $Y  < 80  } {
 $w2.result delete 0 end ; $w2.result  insert 0 [expr 4 * $N]
 return
}
if {  $Y >= 80 && $Y  < 100 } {
  $w2.result delete 0 end ; $w2.result  insert 0 [expr 5 * $N]
return
}
if {  $Y >= 100 && $Y  < 120  } {
 $w2.result delete 0 end ; $w2.result  insert 0 [expr 6 * $N]
 return
}

if {  $Y >= 120 && $Y  < 140 } {
  $w2.result delete 0 end ; $w2.result  insert 0 [expr 7 * $N]
return
}
if {  $Y >= 140 && $Y  < 160  } {
 $w2.result delete 0 end ; $w2.result  insert 0 [expr 8 * $N]
 return
}

if {  $Y >= 160 && $Y  < 180 } {
  $w2.result delete 0 end ; $w2.result  insert 0 [expr 9 * $N]
return
}
if {  $Y >= 180 && $Y  < 200  } {
 $w2.result delete 0 end ; $w2.result  insert 0 [expr 10 * $N]
 return
}

}