 #!/bin/sh
#Calcul_en_arbre.tcl
#\
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Date    : 27/11/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean-Louis Sendral. 
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
source calculs.conf
source msg.tcl
source path.tcl
global hauteur largeur drapeau_ref 
set drapeau_ref 0
##bind . <F1> "showaide {carbre}"
bind . <1> {}
bind . <Double-Button-1> {}
# Frames
	wm title . [mc {titre_arbre}] ; wm geometry  . +0+0  
	set hauteur [expr [winfo screenheight .] / 1.5]
	set largeur  [expr [winfo screenwidth .] / 1.5]
  . configure -width $largeur -height $hauteur  
wm resizable . no yes
#-background #ffff80
set basedir [pwd]
# [file dir $argv0]
cd $basedir
set num_entry 1 
#les images pour les boutons
image create photo sauver_ps	-file [file join $basedir images "save_file1.gif"]
	image create photo plus	-file [file join $basedir images "plus.gif"]
	image create photo moins	-file [file join $basedir images "moins.gif"]
	image create photo multiplie	-file [file join  $basedir images "multiplie.gif"]
	image create photo divise	-file [file join $basedir images  "divise.gif"]

# frame haut, gauche, bas et droit (canvas)

      frame .frame_up -bd 2 -relief groove 
      ##-background orange
	frame .frame_left -bd 2 -relief groove 
	frame .frame_right -bd 2 -relief groove
	frame .frame_down -bd 2 -relief groove

	grid .frame_up -row 0 -column 0 -columnspan 2 -sticky ew 
	grid .frame_left -row 1 -column 0 -sticky ns 
	grid .frame_right -row 1 -column 1 -sticky news
      grid .frame_down -row 2 -column 0 -columnspan 2 -sticky ew

	grid rowconfigure . 0 -weight 0
	grid rowconfigure . 1 -weight 1
	grid rowconfigure . 2 -weight 0
	grid columnconfigure . 0 -weight 0
	grid columnconfigure . 1 -weight 1
# entr�e des noms, classe, des deu nombres et de l'op�ration
  label .frame_up.lbl_nom -text [mc {ton_nom} ] 
  entry .frame_up.nom -textvariable nom_elev -width 15 -relief sunken -justify center
  label .frame_up.lbl_classe  -text [mc {ta_classe}] -fg black
  entry .frame_up.classe -textvariable nom_classe -width 5 -relief sunken -justify center
  label .frame_up.nombre_un -text [mc {prem_nombre}]
  entry .frame_up.prem_nombre -textvariable nombre_un -width 5  -foreground red -justify center -font {Helvetica 20 bold}
 label .frame_up.nombre_deux  -text [mc {deux_nombre}] -fg black
  entry .frame_up.deux_nombre -textvariable nombre_deux -width 5 -foreground red -justify center -font {Helvetica 20 bold}
  label .frame_up.oper -text [mc {operation}]
  entry .frame_up.operation -textvariable operation -width 3  -foreground red -justify center -font {Helvetica 20 bold}
  button .frame_up.b1 -text [mc {quitter}] -command "exit" 
grid .frame_up.lbl_nom .frame_up.nom \
.frame_up.lbl_classe  .frame_up.classe \
.frame_up.nombre_un .frame_up.prem_nombre \
.frame_up.nombre_deux  .frame_up.deux_nombre \
.frame_up.oper .frame_up.operation .frame_up.b1

# Inside left frame
		# Sub-frames
		frame .frame_left.commandes -bd 2 -relief groove
            frame .frame_left.fins -bd 2 -relief groove
		pack .frame_left.commandes .frame_left.fins  -side top -padx 2 -pady 2

radiobutton .frame_left.commandes.plus -image plus  -indicatoron 0 -variable operation_now -value +   -command {   } 
radiobutton .frame_left.commandes.moins -image moins  -indicatoron 0 -variable operation_now -value \- -command {   } 
set operation_now +
#radiobutton .frame_left.commandes.multiplie -image multiplie  -indicatoron 0 -variable operation -value * -command {   } 
#radiobutton .frame_left.commandes.divise -image divise  -indicatoron 0 -variable operation -value \/ -command {   }
pack .frame_left.commandes.plus -pady 100
pack .frame_left.commandes.moins -pady 0
#grid .frame_left.commandes.multiplie au cas o�
#grid .frame_left.commandes.divise au cas o�
#grid .frame_left.fin_calcul
#grid  .frame_left.sauve_ps 
#le canvas et les �v�nements : lancer l'activit�, recommencer, exit, aide
# commencer l'arbre en demandant  un calcul,
 ##-scrollregion {0 0 2000 1000} 
canvas .frame_right.canvas -height [expr  $hauteur - 50] -width [expr $largeur -133] -background orange 
##-scrollregion {0 0 800 800} 
##-yscrollcommand {.frame_right.canvas.vsc  set}
##-xscrollcommand {.frame_right.canvas.hsc  set} 
##scrollbar .frame_right.canvas.vsc -bd 1 -orient vertical -command {}
##scrollbar .frame_right.canvas.hsc -bd 1 -orient horizontal -command {}
pack .frame_right.canvas -fill both -expand yes 
##pack .frame_right.canvas.vsc -side right -fill y
##.frame_right.canvas.vsc  configure -command  {.frame_right.canvas yview}
##.frame_right.canvas yview moveto  0.33
##pack .frame_right.canvas.hsc -side bottom -fill both 
## .frame_right.canvas.hsc  configure -command  {.frame_right.canvas xview}
##.frame_right.canvas.vsc  configure -command  {.frame_right.canvas yview}
.frame_right.canvas bind lignes <Button-1> {+ y_t_il_calcul %x %y}
button .frame_down.lancer -text [mc {lancer}] -command "activite"
button .frame_down.refaire -text [mc {refaire}] -command  "refaire"
button .frame_down.eff_ligne -text [mc {effacer_ligne}] -command  "effacer_ligne"
button .frame_down.imprime_ps -text [mc {imprimer}]  -command "imprime .frame_right.canvas "
button .frame_down.sauve_ps -text [mc {sauver_ps}]  -command "sauver_ps "
##button .frame_down.verif_expr -text [mc {veif_expr}]  -command "verif_expr $rep_expr-arith1 $noeud"

#button .frame_down.fin_calcul -text "FIN\?"  -foreground red -command "set fin_calcul 1 " 
button .frame_down.aide -text [mc {Aide}] -foreground green -command "exec $progaide  [pwd]/aide/calculs_en_arbre.html &"
##"cd $basedir ; exec netscape4 ./aide.html#carbre"
##"showaide {carbre}"
label .frame_down.info -text ""  -fg red
pack .frame_down.lancer -side left
pack .frame_down.refaire -side left
pack .frame_down.eff_ligne -side left
pack .frame_down.imprime_ps -side right
pack .frame_down.sauve_ps -side right
#pack .frame_down.fin_calcul -side right
pack .frame_down.aide -side right
##pack .frame_down.verif_expr -side right
pack .frame_down.info -side right
##foremer(4+500...)
proc   traite_liste  {liste_expr}   {
 if {   $liste_expr == "" } {
 return {}
 }
 if  {    [ llength  $liste_expr  ] == 1  }  {
   return [lindex $liste_expr 0]  }  else   {
    return  "\([lindex  $liste_expr  0]\+[traite_liste [lrange  $liste_expr    1 end]]\)"
}
}


#### procedure qui permet de d�marrer puis de poursuivre l'arbre de calcul ####
###############################################################################

proc y_t_il_calcul {sx sy } {
 global tab_extremite liste_fin_fleche tab_nombres milieux text_oper_now operation_now compteur_ligne num_entry num_butt liste_tout_expr prem_l1 prem_l2 ind_expr \
 sy_max drapeau_clic depart2y liste_chiffres rep button_suite   expre_arith     expr_nbre1  expr_nbre2   tab_expr     expr_prov
 set nombres1 {} ; set nombres2 {}  ; set expr_nbre2 {} ; set  expr_nbre1 {}
set expr_nbre2_bis {} ; set  expr_nbre1_bis {}
 # pour pr�voir l'affichage final
if { $sy > $sy_max } {
  set sy_max $sy
}
# r�cup�ration des objets du canvas autour de (sx,sy)peut �tre modifi�.
set id_lignes [.frame_right.canvas find overlapping [expr $sx - 7] [expr $sy - 7] [expr $sx + 7] [expr $sy + 7 ]]
set id_lignes_tags [.frame_right.canvas find withtag lignes] ; set bonne_liste_tags {}
#�limination des objets non lignes
foreach tag_a_tester $id_lignes {
   if { [lsearch -exact $id_lignes_tags $tag_a_tester] != -1 } {
 set bonne_liste_tags [linsert $bonne_liste_tags  end $tag_a_tester ]
	}
					} ; # fin foreach

set compteur_ligne 0 ; set liste_nombres {} ; # on est sur d'etre sur ligne foreach id $id_lignes
#.frame_up.nom insert end " $id_lignes :: $sx \;  $sy |" pour debug
# pour chaque branche de l'arbre aboutissant � (sx,sy) (s'il y en a plus de deux)
if { [llength $bonne_liste_tags ] >= 2 } {
if { [ loin $sx $sy   $liste_fin_fleche ] || $drapeau_clic == 0 } { 
return
}
 set drapeau_clic 0
 .frame_right.canvas bind lignes <Button-1> {}
 ## .frame_right.canvas.hsc  configure -command  {}
##.frame_right.canvas.vsc  configure -command  {} 
 foreach id $id_lignes {
set deb_ligney [expr int([lindex [ .frame_right.canvas coords $id] 0 ]) ] ; set  deb_lignex [expr int([lindex [ .frame_right.canvas coords $id] 1 ])]
#.frame_up.nom insert end "$deb_ligney : $deb_lignex " pour debug
if { $deb_ligney <  $milieux } { ; # premier nombre � gauche de l'�cran
set nombres11 [expr int([ lindex $tab_nombres([expr int($deb_ligney)],[expr int($deb_lignex)]) 0])]
  set  expr_nbre11  [ lrange  $tab_expr([expr int($deb_ligney)],[expr int($deb_lignex)]) 0 end]
##set liste_tout_expr [linsert ${liste_tout_expr} ${ind_expr} ${expr_nbre11} ]
##incr ind_expr 
##set expr_nbre11  [expr int( $tab_expr([expr int($deb_ligney)],[expr int($deb_lignex)] ) )]
set nombres1 [linsert $nombres1 end $nombres11]
##bon ordre dans les formations des expr
set expr_nbre1_bis    [linsert ${expr_nbre1_bis} end  $expr_nbre11]
        } else { ;# second nombre
set nombres12  [expr int([ lindex $tab_nombres([expr int($deb_ligney)],[expr int($deb_lignex)]) 0])]
 set expr_nbre12  [ lrange $tab_expr([expr int($deb_ligney)],[expr int($deb_lignex)]) 0 end]
##set liste_tout_expr [linsert ${liste_tout_expr} ${ind_expr} ${expr_nbre12} ]

## set expr_nbre12   [expr  int( $tab_expr([expr int($deb_ligney)],[expr int($deb_lignex)] )) ]
set nombres2  [linsert  $nombres2  end  $nombres12]
set expr_nbre2_bis      [linsert  ${expr_nbre2_bis} end  $expr_nbre12]
                }
#set index_search [lsearch -exact  $liste_chiffres [expr int([lindex $tab_nombres([expr int($deb_ligney)],[expr int($deb_lignex)]) 1]) ]  ] 
#set liste_chiffres [lreplace $liste_chiffres $index_search $index_search ]

            } ; #fin foreach
##retravailler expr_nbre1biset21 pour expr_nbre1 et2
set l_indice11 {} ; set l_indice21 {}
	foreach el ${expr_nbre1_bis} {
	set l_indice11 	[linsert $l_indice11 end [lsearch  ${liste_tout_expr} $el]]	
}
set l_indice1 [lsort -integer ${l_indice11}] ; set prem_l1 [lindex $l_indice1 0]
foreach el ${expr_nbre2_bis} {
  set l_indice21 [linsert $l_indice21 end [lsearch  ${liste_tout_expr} $el]]	
}
set l_indice2 [lsort -integer ${l_indice21}] ; set prem_l2 [lindex $l_indice2 0]
foreach ind ${l_indice1} {
set expr_nbre1 [linsert  ${expr_nbre1} end [lindex  ${liste_tout_expr} $ind ] ]
}
foreach ind ${l_indice2} {
set expr_nbre2 [linsert  ${expr_nbre2} end [lindex  ${liste_tout_expr} $ind ] ]
}

    # fen�tre r�ponse drapeau_clic pour �viter les clics pendant les calculs
incr num_entry ; ### set drapeau_clic 0
set repp [entry .frame_right.canvas."entry$num_entry" -width 7 -font {Helvetica 14 bold} -justify center ]
set repp1 [.frame_right.canvas create window $sx [expr $sy + 15]  -window .frame_right.canvas."entry$num_entry"  -tags rep]
set nbre1 [ somme $nombres1 ] ; set nbre2 [somme $nombres2 ]
incr num_butt ; unset id_lignes 
set text_oper_now [.frame_right.canvas create text  85 170 -text [ mc {choix_op}] -font {Helvetica 10}  -fill blue ]
clignote   [ mc {choix_op}] 3

#set oper_now [.frame_right.canvas create text  55 [expr $sy +40] -text "oh$operation_now"]

#bind Enter ne semble pas fonctionner !! d'ou le bouton suite pour le remplacer
#set button_suite [button .frame_right.canvas."butt$num_butt" -text Suite -command " tester $nbre1 $nbre2 $sx $sy " ] 
#.frame_right.canvas create window 20 50 -window .frame_right.canvas."butt$num_butt" -tags button_suite
focus -force $repp
bind $repp <Return> "tester $nbre1 $nbre2 $sx $sy "
bind $repp <KP_Enter> "tester $nbre1 $nbre2 $sx $sy "
#[.frame_right.canvas.entry  get]"
 #.frame_up.nom insert end [list $nombres1 : $nombres2 ]
} ; ###fin traitement

}
proc loin { lieu_x lieu_y liste } {
foreach pointe $liste {
		set lieux_x [lindex $pointe 0] ; set lieuy_y [lindex $pointe 1] 
	if { [expr $lieu_y < [expr $lieuy_y + 3] ] && [expr $lieu_y > [expr $lieuy_y - 3] ] \
 && [expr $lieu_x < [expr $lieux_x + 3] ] && [expr $lieu_x > [expr $lieux_x - 3] ] } {
		return 0
	   }
}
return 1
}
#### �venements d�clanch�s aux clics !!########

proc button-down { sx sy } {
global objet  premy premx lastx lasty drapeau_clic clicclic rien liste_chiffres liste_chiffres_en_cours resultat tab_nombres resultat
set lastx $sx ; set lasty $sy ; set rien 0
 if { $liste_chiffres  == {}  && $liste_chiffres_en_cours == {} } {
 .frame_down.info configure -text "[mc {mauvaise_fin}] $resultat " -font {Helvetica 12 bold} -foreground red
 after 5000 "refaire" ; # .frame_down.info configure -text "" ;  # pour la soustraction ?
	} else {
   			if {  [lsearch -exact $liste_chiffres [lindex $tab_nombres($sx,$sy) 1 ]] == -1 ||  [deja_pris $sx $sy ] ||  $drapeau_clic == 0 || $clicclic == 1 } {
	   set rien 1 ; return	} else  {
    if { $liste_chiffres_en_cours == {}  }  { ; # tracer la fl�che
	set objet [ .frame_right.canvas create line $sx $sy $sx $sy -width 3 -fill blue -arrow last -tags lignes]
set premy $sy ; set premx $sx
} else { ; # encours non vide comme liste nombres; rejoindre l'ext�mit� de la fl�che
		global finx finy fin_fleche_x fin_fleche_y 
		set objet [ .frame_right.canvas create line $sx $sy $finx $finy -width 2 -fill black  -tags lignes]
     	 
	}
}
}
}

###### ne sert que pour la fl�che. rien, drapeau pour les clics fous !!!#############
proc button-motion { nx ny } {
	global objet lastx lasty finx finy liste_chiffres_en_cours tab_nombres rien clicclic
if { $clicclic == 1 } {
set clicclic 0 ; return}
if {  $rien == 0 } {
if { $liste_chiffres_en_cours == {}  } {
.frame_right.canvas  coords $objet $lastx $lasty $nx $ny ; set finx $nx ; set finy $ny 
}
}
}
########### pour les maj des listes utilis�es
proc button-release { nx ny } {
global rien clicclic  nombre_fait objet objet1 liste_chiffres_en_cours liste_fin_fleche premy premx
 
if { $liste_chiffres_en_cours == {}  } {
if { [expr [expr abs($ny - $premy) < 70 ] && [expr abs($nx - $premx) < 70 ] ] || [expr abs($ny)  < 35 ]\
|| [expr abs($ny)  <= abs($premy) ] }  {
.frame_right.canvas delete $objet ; return}
if  { [trop_pres_fin_fleche $nx  $ny]}  {
.frame_right.canvas delete $objet
return
}
##[expr abs($ny - $premy) > 5[expr abs($nx - $premx) >5 ]
}
if { $clicclic == 1 } {
set clicclic 0 ; return}
if {  $rien == 0 } {
global objet lastx lasty  liste_chiffres_en_cours liste_chiffres  liste_en_cours_pixels  finx finy  tab_nombres nombre_fait
 set liste_chiffres_en_cours [linsert $liste_chiffres_en_cours 0 [lindex $tab_nombres([expr int($lastx)],[expr int($lasty)]) 1 ]   ]
set nombre_fait [expr int([lindex $tab_nombres([expr int($lastx)],[expr int($lasty)]) 1]) ]  
set index_search [lsearch -exact  $liste_chiffres [expr int([lindex $tab_nombres([expr int($lastx)],[expr int($lasty)]) 1]) ]  ] 
set liste_chiffres [lreplace $liste_chiffres $index_search $index_search ]
set liste_en_cours_pixels [linsert $liste_en_cours_pixels 0   $lastx ]
set liste_fin_fleche [linsert $liste_fin_fleche 0 [list $nx $ny] ]
.frame_down.eff_ligne configure -state normal
set objet1 $objet
set objet ""
}

}

proc deja_pris { sy sx } {
global liste_en_cours_pixels 
# 54 pour les noeuds donc non d�ja pris ? 1 �re ligne des chifres
if { $sx > 54 } {
	return 0
}
foreach pixely $liste_en_cours_pixels {
	if { $sy < [expr $pixely + 7] && $sy > [expr $pixely - 7] } {
		return 1
	   }
}
return 0
}
proc trop_pres_fin_fleche {sx  sy} {
global liste_fin_fleche
foreach fin $liste_fin_fleche { 
		if { $sx < [expr [ lindex  $fin 0  ] + 60]  && $sx > [ expr [  lindex  $fin 0 ] - 60]  && \
		$sy < [expr [   lindex  $fin 1  ] + 60]   &&  $sx > [  expr [ lindex  $fin 1   ] - 60] }  {
		return 1}
}
return 0
}
##############################################################################################

proc somme { liste } {
if { $liste == {} } {
    return 0
} else {
return [expr [lindex $liste  0] + [somme [lrange $liste 1 end] ]]
}
}

#############proc�dure pour continuer l'arbre de calcul (attente de la bonne r�ponse) ou finir############
#################################################################################################
proc tester { nbre1 nbre2 lieu_x lieu_y} {
global num_entry num_expr text_oper_now operation_now sy_max drapeau_clic liste_fin_fleche liste_chiffres_en_cours liste_tout_expr  prem_l1 prem_l2 ind_expr \
liste_en_cours_pixels liste_chiffres tab_nombres  tab_expr expr_prov button_suite bonne_rep  num_butt fin_calcul nom_elev resultat   \
 expre_arith   expr_nbre1  expr_nbre2 drapeau_ref
set oper $operation_now ; set noeud [ eval [list expr $nbre1 $oper  $nbre2 ]]
#set oper [.frame_up.operation get]
if { [ catch {   set expr_prov {}
   if   { $expr_nbre1 == {} }  {
                   set       expr_prov       [  traite_liste  $expr_nbre2 ]
##                        set   expre_arith   [ concat   $expre_arith  $expr_prov  ]
                                        }  elseif  { $expr_nbre2 == {}  }  {
                       set       expr_prov       [  traite_liste  $expr_nbre1 ]
##                         set   expre_arith    [concat   $expre_arith  $expr_prov  ]
                                       }   else  {
                                       if { $prem_l1 <= $prem_l2  } {
                set   expr_prov   [concat    \([  traite_liste  $expr_nbre1 ] ${oper}[  traite_liste  $expr_nbre2 ]\)   ] } else {
set    expr_prov   [concat    \([  traite_liste  $expr_nbre2 ] ${oper}[  traite_liste $expr_nbre1 ]\) ]  
}
}  }] } {
}

##     set   expre_arith   [concat   $expre_arith $expr_prov  ]

   set  expre_arith  $expr_prov
set rep [.frame_right.canvas."entry$num_entry"  get ]
if { ![teste_input1 $rep ] && $rep != 0 } {
.frame_right.canvas."entry$num_entry"  delete 0 end ; \
focus .frame_right.canvas."entry$num_entry"  ; return
}
if {  $rep == $noeud  } {
	if {$noeud == $resultat && $liste_chiffres == {} } {  ; #$fin_calcul==1&& # bon calcul c'est termin�
.frame_right.canvas."entry$num_entry" configure -state disable
.frame_right.canvas delete $text_oper_now
## .frame_right.canvas.hsc  configure -command  {.frame_right.canvas xview}
###le bon.frame_right.canvas.vsc  configure -command  {.frame_right.canvas yview} -scrollregion {0 0 800 800} 
if { $drapeau_ref == 0} {
.frame_right.canvas configure -yscrollcommand {.frame_right.canvas.vsc  set} -scrollregion {0 0 900 900} 
scrollbar .frame_right.canvas.vsc -bd 1 -orient vertical -command {.frame_right.canvas yview} 
pack .frame_right.canvas.vsc -side right -fill y
			} else {
			 .frame_right.canvas.vsc  configure -command  {.frame_right.canvas yview}
	}
##.frame_right.canvas.vsc  configure -command  {.frame_right.canvas yview} 
##.frame_right.canvas yview moveto  0.0
##-yscrollcommand {.frame_right.canvas.vsc  set}
##-xscrollcommand {.frame_right.canvas.hsc  set} 
##scrollbar .frame_right.canvas.vsc -bd 1 -orient vertical -command {}
incr num_expr
set lieu_x_expr 350 ; set lieu_y_expr $sy_max
set bonne_rep [.frame_right.canvas create text  $lieu_x [expr $lieu_y + 70]  -text "[mc {mess_bonne_rep} ] $nom_elev" -font {Helvetica 14} -fill blue ]
set pour_expr [.frame_right.canvas create text  260 [expr $lieu_y_expr + 100]  -text [ mc {expr_arith} ] -font {Helvetica 10} -fill black -tags rep ]
# pour obtenir l'expression arithm�tique du calcul g�n�ral
set liste_chiffres_en_cours  {}  ; set liste_en_cours_pixels {} 
set rep-expr-arith [entry .frame_right.canvas."entree$num_expr" -width 35 -font {Helvetica 10} ]
set rep_expr-arith1 [.frame_right.canvas create window 250 [expr $lieu_y_expr + 120]  -window .frame_right.canvas."entree$num_expr"  -tags rep]
  ##    ${rep-expr-arith}  insert  0   $expre_arith
button .frame_down.verif_expr -text [mc {verif_expr}]  -command " verif_expr ${rep-expr-arith} $noeud "
    pack .frame_down.verif_expr -side right
##.frame_down.verif_expr configure -state normal -foreground red
#-anchor s
return 1
} else { ; #bon calcul interm�diaire
 ## construction de l'expression parenth�s�e
 .frame_right.canvas bind lignes <Button-1> {+ y_t_il_calcul %x %y}
 .frame_right.canvas."entry$num_entry" configure  -state disable
set bonne_reponse [.frame_right.canvas create text  $lieu_x [expr $lieu_y + 70]  -text \
 "[ mc {mess_bonne_rep}]"  -font {Helvetica 14}  -fill blue ]
##debug${liste_tout_expr} 
after 3000 ".frame_right.canvas delete $bonne_reponse "
set item3  [.frame_right.canvas  create rectangle [expr $lieu_x - 3]  [expr $lieu_y + 29]  [expr $lieu_x + 3] [expr $lieu_y + 35]    -fill red  -width 1 -tags lignes2 ]

#l'arbre de calcul se developpe avec les m�mes �v�nements qu'au d�but
.frame_right.canvas bind $item3 <Button-1> {button-down %x %y}
.frame_right.canvas bind $item3 <B1-Motion> {button-motion %x %y}
.frame_right.canvas bind $item3 <ButtonRelease-1> {button-release  %x %y}
.frame_right.canvas delete $text_oper_now
.frame_down.eff_ligne configure -state disable
# maj  des listes apr�s bon calcul
set liste_chiffres_en_cours  {} 

# set liste_en_cours_pixels {}
if { $liste_chiffres != {} } {  ; # si vide c'est la fin ne pas abonder liste_chiffres
set liste_chiffres [concat $liste_chiffres    $noeud ]
   }
  entourer $noeud $noeud  [expr [expr int($lieu_x)]]  [expr [expr int($lieu_y)] + 32 ]  5
 entourer_expr   ${expr_prov}    [expr [expr int($lieu_x)]]  [expr [expr int($lieu_y)] + 32 ]  5
#.frame_right.canvas delete suite  ; .frame_right.canvas delete $button_suite
set liste_tout_expr [linsert ${liste_tout_expr} ${ind_expr} ${expr_prov} ]
incr ind_expr
}
set drapeau_clic 1
# mauvais calcul atente d'une autre r�ponse
} else { set mauvais [.frame_right.canvas create text  $lieu_x [expr $lieu_y + 50] -text [mc {mess_mauvais_rep} ] -font {Helvetica 14}  -fill red ]
after 3000 ".frame_right.canvas delete $mauvais"
clignote   [ mc {mauv_choix_op}] 3
.frame_right.canvas."entry$num_entry" delete 0 end 
set drapeau_clic 0
}

}
#############proc�dures pour entourer et s�curiser les lieux du clic ##############
####################################################################
proc  pointer_nombre  { nombre lieu_y lieu_x pas pas_pixel } {
global noeud clicclic  pas_decalage liste_tout_expr 
 set borne [expr [string length  $nombre] - 1]
for {set index 0 } { $index <= $borne }  { incr index } {
set noeud [ expr int([expr [string index $nombre $index ] * pow(10,[ expr [expr [string length  $nombre] -1] - $index ] )])] 
set chiffre [string index $nombre $index ] 
set sy_temp  [expr $lieu_y + [expr $index * $pas]] ; set sx_temp [expr $lieu_x + $pas_decalage ]
# on est sur un chiffre; on va l' entourer
entourer $chiffre $noeud $sy_temp $sx_temp $pas_pixel
 entourer_expr   $noeud  $sy_temp $sx_temp $pas_pixel
set liste_tout_expr [linsert ${liste_tout_expr} end $noeud ]
.frame_right.canvas  create text  [expr $lieu_y + [expr $index * $pas]] $lieu_x  -text $chiffre -font {Helvetica 20} -tags nombre
set item1 [ .frame_right.canvas  create rectangle [expr $sy_temp - 3] [expr $sx_temp - 3] [expr $sy_temp + 3] [expr $sx_temp + 3] -tags ancre  -fill red  -width 0 ]
.frame_right.canvas bind $item1 <Double-Button-1> { set clicclic 1}
.frame_right.canvas bind $item1 <Button-1> {button-down %x %y}
.frame_right.canvas bind $item1 <B1-Motion> {button-motion %x %y}
.frame_right.canvas bind $item1 <ButtonRelease-1> {button-release %x %y}

}
}

proc entourer { chiffre noeud lieu1_y lieu1_x pas_pix } {
global tab_nombres liste_fin_fleche
set liste_fin_fleche  [linsert $liste_fin_fleche 0 [list $lieu1_y $lieu1_x] ]
   for { set i  -$pas_pix } { $i <= $pas_pix } {incr i } {
		for { set  j  -$pas_pix } { $j <= $pas_pix } {incr j } {
				set tab_nombres([expr int($lieu1_y) +$i ],[expr int($lieu1_x) + $j]) [list $noeud $chiffre ]

				}
     }
}
 proc entourer_expr  { noeud lieu1_y lieu1_x pas_pix } {
##noeud vrai noeud ou expression
global tab_expr
   for { set i  -$pas_pix } { $i <= $pas_pix } {incr i } {
		for { set  j  -$pas_pix } { $j <= $pas_pix } {incr j } {
				set tab_expr([expr int($lieu1_y) +$i ],[expr int($lieu1_x) + $j])  $noeud

				}
     }
}




###########proc�dures pour recommencer############
proc refaire {} { 
global hauteur largeur drapeau_ref

update 

update
##destroy .frame_right.canvas.vsc 
if  { ! [catch  { .frame_right.canvas.vsc  configure -command  {} }  ]  }  {
set drapeau_ref 1
.frame_right.canvas yview moveto  0.0
 update idletasks
.frame_right.canvas configure -height [expr  $hauteur - 50] -width [expr $largeur -133] -scrollregion {0 0 800 800} 
}
destroy  .frame_down.verif_expr 
.frame_right.canvas bind lignes <Button-1> {+ y_t_il_calcul %x %y}
.frame_down.lancer configure -state normal
.frame_right.canvas delete with tags all
.frame_down.info configure -text "" 
.frame_up.operation configure -state normal
.frame_up.prem_nombre configure -state normal
.frame_up.deux_nombre configure -state normal
.frame_down.eff_ligne configure -state disable
 ##.frame_right.canvas.hsc  configure -command  {.frame_right.canvas xview}
##.frame_right.canvas.vsc  configure -command  {.frame_right.canvas yview}
}
 
proc effacer_ligne {} {
 ; # � am�liorer c.a.d r�_initialiser correctement si fleche enlever ds liste_fin_fleche
global objet objet1 drapeau_clic liste_chiffres_en_cours liste_chiffres liste_en_cours_pixels nombre_fait  liste_fin_fleche 
if { $drapeau_clic != 0 } {
set liste_chiffres_en_cours [lreplace $liste_chiffres_en_cours 0 0 ]
set liste_en_cours_pixels  [lreplace $liste_en_cours_pixels 0 0 ]
set liste_chiffres [linsert $liste_chiffres 0 $nombre_fait]
set liste_fin_fleche [lreplace  $liste_fin_fleche  0 0]
##liste_vrac_pixels ?? 
.frame_down.eff_ligne configure -state disable
 .frame_right.canvas delete $objet1
}
}

####initialisations
if { $tcl_platform(platform) == "unix" } {
set nom_elev $tcl_platform(user)
set nom_classe [lindex [exec  id -G -n ] 1]
set pas_decalage 10
} else {
 set nom_elev eleve
 set nom_classe classe
.frame_down.imprime_ps configure -state disable
.frame_down.sauve_ps configure -state disable
set pas_decalage 15
}
initlog $tcl_platform(platform) $nom_elev
.frame_down.eff_ligne configure -state disable
set num_butt 0 ; set num_expr 0
#set pas_decalage 
focus -force .frame_up.prem_nombre
##########################################################################
#### proc�dure pour lancer l'activit�####################################
proc activite {}  {
global operation clicclic nombre_un nombre_deux milieux resultat depart1 depart2 depart2y sy_max drapeau_clic pas tab_nombres liste_tout_expr ind_expr \
liste_chiffres liste_chiffres_en_cours liste_en_cours_pixels liste_vrac_pixels liste_fin_fleche num_butt num_entry fin_calcul     expre_arith 
set liste_chiffres_en_cours  {} ; set liste_chiffres {} ; set liste_en_cours_pixels {} ; set liste_fin_fleche {} ; set drapeau_clic 1 ; set liste_tout_expr {} ; set ind_expr 0
set expre_arith {}
###.frame_down.verif_expr configure -state disabled
set nombre_un [teste_input [.frame_up.prem_nombre  get]] ; set nombre_deux [teste_input [.frame_up.deux_nombre get] ]
set clicclic 0 ; set liste_vrac_pixels {}
#set num_entry 1 
##.frame_right.canvas.vsc   configure -command  {.frame_right.canvas yview moveto  0.0}
##.frame_right.canvas.vsc   -yview moveto 0
##.frame_right.canvas yview moveto  0.0
##.frame_right.canvas.vsc   configure -command  {.frame_right.canvas yview}
.frame_down.lancer configure -state disable
.frame_up.operation configure -state disable
.frame_up.prem_nombre configure -state disable
.frame_up.deux_nombre configure -state disable
if { [.frame_up.operation get] != "+" && [.frame_up.operation get] != "-" } {
	 set operation +
    } else {
       set operation [.frame_up.operation get]
}
set resultat [expr $nombre_un $operation $nombre_deux ] ; set fin_calcul 0 
for { set index 0 } {$index <= [expr [string length  $nombre_un]-1 ] } {incr index } {
 set liste_chiffres [linsert $liste_chiffres  end [ string index $nombre_un $index ]   ]
}
for {set index 0} {$index <= [expr [string length  $nombre_deux]-1] } {incr index } {
 set liste_chiffres [linsert $liste_chiffres  end [ string index $nombre_deux $index ]    ]
}
set depart1x 30
set depart1y [ expr [winfo width .frame_right.canvas ] / 4]
set depart2y [ expr  [ expr [winfo width .frame_right.canvas ] / 2] +  [ expr [winfo width .frame_right.canvas ] / 4]   ]
#set depart1y 200 
 set pas 20 ; set sy_max 40
set depart2x 30  ; set pas 20 ; set pas_pixel 5
# set depart2y 400
pointer_nombre $nombre_un $depart1y $depart1x $pas $pas_pixel
pointer_nombre $nombre_deux $depart2y $depart2x $pas $pas_pixel
#.frame_right.canvas  create text  [expr $depart1y + [expr $index * $pas]] $depart1x  -text [string index $nombre_un $index] -font {Helvetica 20 bold} -tags nombre
#set item1 [ .frame_right.canvas  create line [expr $depart1y + [expr $index  * $pas]] [expr $depart1x + 20] [expr $depart1y + [expr [expr $index * $pas] +2]] [expr $depart1x + 20]  -tags ancre  -fill red  -width 1 ]

#.frame_right.canvas bind $item1 <Button-1> {button-down %x %y}
#.frame_right.canvas bind $item1 <B1-Motion> {button-motion %x %y}

set depart11y [expr $depart1y + [expr  [expr [string length  $nombre_un]-1 ] * $pas ] ]
.frame_right.canvas  create text  [expr [expr $depart11y + $depart2y] / 2] $depart1x -text $operation  -font {Helvetica 20 bold}
set milieux [expr [expr $depart11y + $depart2y] / 2]
#.frame_right.canvas  bind ancre <Button-1> {button-down %x %y} ca a l'air de marcher!
}
proc teste_input { input} {
if  {[ regexp {(^[0-9]+$)} $input ] == 1 } {
            return $input
    } else {
return 0
}
}
proc teste_input1 { input} {
if  {[ regexp {(^\-*[0-9]+$)} $input ] == 1 } {
            return $input
    } else {
return 0
}
}
proc verif_expr  {f_expr noeud } {
global expre_arith
if { [catch {
    if  {[expr [ $f_expr get ]] !=  $noeud  } {
    tk_messageBox -message "[mc {err_parenth}] \n$expre_arith " -icon info -type ok
     }  else  {
     tk_messageBox -message "[mc {bon_expr}]\n$expre_arith" -icon info -type ok
      } } mess  ] } {
  tk_messageBox -message "[mc {err_form_parenth}] $mess\n [mc {bon_expres}] \n$expre_arith" -icon info -type ok
}
}

proc sauver_ps {} {
global basedir TRACEDIR_ARBRE LogHome
  set date [clock format [clock seconds] -format "%H%M%d%b"]
set TRACEDIR_ARBRE $LogHome
cd $TRACEDIR_ARBRE
global nom_elev nom_class
.frame_right.canvas  postscript -file "${nom_elev}$date.ps"
# -height 20c -width 20c  -file "$nom_elev.ps"
cd $basedir
}

############ � �tudier pour imprimer sous windows
proc imprime {c} {
if {[info exists env(PRINTER)]} {
    set psCmd "lpr -P$env(PRINTER)"
} else {
    set psCmd "lpr"
}

set postscriptOpts {-pageheight 190m -pagewidth 280m -pageanchor c}

if {[catch {eval exec $psCmd <<    \
		  {[eval $c postscript $postscriptOpts]}} msg]} {
		    tk_messageBox -message  \
		      "Error when printing: $msg" -icon error -type ok
		}
}
proc clignote { texte  nclic} {
global text_oper_now
 for  {set cl 1} {$cl <= $nclic} {incr cl} {
after 200
.frame_right.canvas itemconfigure ${text_oper_now}  -text  $texte  -font {Helvetica 10 bold}  -fill red 
 update
after 200
 .frame_right.canvas itemconfigure  ${text_oper_now} -text $texte  -font {Helvetica 10  bold}  -fill blue 
update
}

}
 


#proc main {}  {
#activite
#}
#main 
#.frame_right.canvas  postscript -file






