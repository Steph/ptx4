#!/bin/sh
#editeurp.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jl.sendral@free.fr
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean- Louis Sendral
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
###################"gestion du son
#"global sound
#"if {[catch {package require -exact snack 2.1}]} {
#"    set sound 0
#"} else {
#"    set sound 1
#"}
source calculs.conf
set bgcolor #aaaaaa
. configure  -height 460 -width 640
wm geometry . +0+0
. configure -background $bgcolor
wm resizable . 0 1
set basedir [file dir $argv0]
cd $basedir

bind . <F1> "showaide division"
global sysFont
source msg.tcl
source fonts.tcl
source repertoire.tcl
source config_sce.tcl
source path.tcl
set basedir [pwd]
#source interface.tcl
#source calculatrice.tcl
set indice_quot 1
set indice_dividende 1
set flag_verif 0 ; set cumul_quot 0
set nombre_sous_eff 0 ; set n_lance_calcul 0
set nom_elev  [lindex $argv 1 ] ;#init nom �l�ve et nom clase
set nom_classe [lindex $argv 2 ]
set nom_sce [lindex $argv 0 ]
##set nom_lieu [lindex $argv 3]
set fich_scena  [lindex $argv 4]
 set  DOSSIER_EXOS  [lindex $argv 3]
  set f [open [file join $DOSSIER_EXOS $fich_scena  ] "r"  ]
#set listdata [gets $f] ;#init de la liste des couples
while { [gets $f ligne] >=0 } {
              if { [lindex [split $ligne ":"] 0] == ${nom_sce}  } {
	set data_brut $ligne
	break
      }
}
close $f
cd $basedir
proc sup {a b} {
if { $a >= $b} {
return $a } else {
return $b
}
}
 ##set data_brut "fixe_ici1:1:0:0:0:1:f:::4586:26:::"
set donnees [split $data_brut ":" ]
set tab_config($nom_sce,flag_repertoire)  [lindex  $donnees 1]
set tab_config($nom_sce,flag_calcul_quot)  [lindex  $donnees 2]
set tab_config($nom_sce,flag_calcul_diff)  [lindex  $donnees 3]
set tab_config($nom_sce,flag_n_quot_noeud)  [lindex  $donnees 4]
set tab_config($nom_sce,flag_recup_valeur)  [lindex  $donnees 5]
set tab_config($nom_sce,flag_nombres_a_fixer)  [lindex  $donnees 6]
set flag_af 0
 proc teste_input { input} {
if  {[ regexp {(^[0-9]+$)} $input ] == 1 && ![ regexp {(^0+[1-9]+$)} $input] } {
            return $input
    } else {
return 0
}
}

if { $tab_config($nom_sce,flag_nombres_a_fixer) == "af"} {
set flag_af 1 ; set tab_config($nom_sce,flag_nombres_a_fixer) "f"
proc retiens_divis { } {
global divis_fix w4
set  divis_fix [${w4}.n_divis get ]
if { [teste_input $divis_fix ] } {
set divis_fix [${w4}.n_divis get ]
#${w4}.n_divis configure -state normal
#focus ${w4}.n_divis 
destroy ${w4}
} else {
${w4}.n_divis delete 0 end
focus ${w4}.n_divis 
return
}
}
proc retiens_divid { } {
global divid_fix w4
set  divid_fix [${w4}.n_divid get ]
if { [teste_input $divid_fix ] } {
set divid_fix [${w4}.n_divid get ]
${w4}.n_divis configure -state normal
focus ${w4}.n_divis 
} else {
${w4}.n_divid delete 0 end
focus ${w4}.n_divid 
return
}
}


catch {destroy .top3 }
set w4 [toplevel .top3]
wm geometry $w4 250x150+0+0 
wm resizable $w4 no no
wm title $w4 [mc {choix_divid_divis}]
grab ${w4}
raise ${w4} .
#focus ${w3}
label ${w4}.text_divid -text [ mc {dividende}] -font {arial 10}
place ${w4}.text_divid -x 15 -y 30 
entry ${w4}.n_divid -width 6 -font {arial 10} -justify center
place ${w4}.n_divid -x 90 -y 30 
focus ${w4}.n_divid 
bind ${w4}.n_divid <Return> "retiens_divid "
bind ${w4}.n_divid <KP_Enter> "retiens_divid "
label ${w4}.text_divis -text [mc {diviseur}] -font {arial 10}
place ${w4}.text_divis -x 15 -y 60 
entry ${w4}.n_divis -width 6 -font {arial 10} -justify center
place ${w4}.n_divis -x 90 -y 60 
${w4}.n_divis configure -state disabled
#focus ${w4}.n_divis
bind ${w4}.n_divis <Return> "retiens_divis "
bind ${w4}.n_divis <KP_Enter> "retiens_divis "
set divid_fix 0 ; set divis_fix 0 
tkwait window ${w4} 
}

if { $tab_config($nom_sce,flag_nombres_a_fixer)  == "f" } {
   if { $flag_af == 1} {
 set tab_config($nom_sce,diviseur_fixe) $divis_fix
set tab_config($nom_sce,dividende_fixe) $divid_fix
} else {
 set tab_config($nom_sce,diviseur_fixe) [lindex $donnees 10] 
set tab_config($nom_sce,dividende_fixe) [lindex $donnees 9] 
}
#hasard � controler
} else {
set min_divis [lindex  $donnees 8  ] ; set max_divis [lindex $donnees 12 ] 
set min_divid [lindex  $donnees 7 ] ; set max_divid [lindex $donnees 11 ] 
set n_chif_divis [expr  $min_divis + [ expr int([expr [expr $max_divis - $min_divis ]* rand()] )]]
set inter_divid [ sup $min_divid $n_chif_divis ]
if { $inter_divid >  $max_divid } {
set inter_divid $min_divid
}
set n_chif_divid [expr $inter_divid + [ expr int([expr [ expr $max_divid - $inter_divid ]* rand() ] ) ]]
#set puiss_divis1 [expr int(pow(10,[expr $n_chif_divis - 1])) ] 
set puiss_divis1 [expr int(pow(10,[expr [expr $n_chif_divid - $n_chif_divis] - 1]))]
set puiss_divis2 [expr int(pow(10,[expr $n_chif_divid - $n_chif_divis]))] 
if { $n_chif_divid == $n_chif_divis } {
set puiss_divis1  2
set puiss_divis2 10
}
set tab_config($nom_sce,diviseur_fixe) [expr $puiss_divis1 +  int([expr $puiss_divis2 - $puiss_divis1] * rand())]
#set puiss_divid1 [expr int(pow(10,[expr $n_chif_divid - 1])) ] 
set puiss_divid1 [expr int(pow(10,[expr $n_chif_divid - 1 ])) ]
set puiss_divid2 [expr int(pow(10, $n_chif_divid ))] 
set tab_config($nom_sce,dividende_fixe) [expr $puiss_divid1 +  int([expr $puiss_divid2 - $puiss_divid1] * rand())]
# set tab_config($nom_sce,diviseur_fixe) 25 
#set tab_config($nom_sce,dividende_fixe) 4589
}
set reste_partiel $tab_config($nom_sce,dividende_fixe) 
 
########## Construction des widgets de l'interface###########
wm geometry . +0+0
 wm title . [mc {a_ nous_division}]
 #frame . -height 420 -width 420
#######init des lieux origine : 70 115 210 115
set coin_gauche_x 70 ; set coin_gauche_y 60 ; set coin_droit_x 210 ; set coin_droit_y 60 
set pas_gauche 30 ; set pas_droit 30 

frame .frame2 -background #aaaaaa -height 640 -width 270
frame .frame1 -background #0f1245 -height 640 -width 440 
pack .frame2  .frame1 -side left
#pack .frame1 -side left -anchor n
#label .frame2.categorie -text [mc {repertoire}]
#place .frame2.categorie -x 10 -y 240
label .frame1.titre  -text  [mc {deroulement_calculs}] -font {Arial 18} -bg #aaaaaa
place .frame1.titre  -x 80 -y 5
frame .frame1.trait_h -width 150 -height 3 -bg red
place .frame1.trait_h -x 205 -y 95
frame .frame1.trait_v -width 3 -height 550 -bg red
### avant 100
place .frame1.trait_v -x 202 -y 55
entry .frame1.diviseur -width 10 -font {arial 14} -justify right
place .frame1.diviseur -x $coin_droit_x -y $coin_droit_y ; ###210 115
entry .frame1.dividende -width 10 -font {arial 14 } -justify right
place .frame1.dividende -x $coin_gauche_x  -y $coin_gauche_y ; ##70  115
button .frame2.ajouter -text [mc {acces_repertoire}] -command "repertoire $tab_config($nom_sce,diviseur_fixe)"
place .frame2.ajouter -x 40 -y 230 ; ##300
# .frame1.diviseur insert 0 35 ; .frame1.dividende insert 0 7895
label .frame2.question -text  "   [mc  {n_fois}]\n      $tab_config($nom_sce,diviseur_fixe) ?" -font {Arial 18} -bg #aaaaaa
place .frame2.question -x 0 -y 80 ; ###150
label .frame2.diviseur -text $tab_config($nom_sce,diviseur_fixe)  -font {Arial 18} -bg #aaaaaa
#place .frame2.diviseur -x 200 -y 150
label .frame2.signe_fois -text "*" -font {Arial 16} -bg #aaaaaa
place .frame2.signe_fois -x 10  -y 140
entry .frame2.n_fois -width 10 -font {Arial 16}
place .frame2.n_fois -x 40  -y 140
###debug  .frame2.n_fois  insert end    "$DOSSIER_EXOS  $fich_scena "

.frame1.diviseur insert 0 $tab_config($nom_sce,diviseur_fixe) ; .frame1.dividende insert 0 $tab_config($nom_sce,dividende_fixe)
label .frame2.signe_egal -text "=" -font {Arial 16} -bg #aaaaaa
place .frame2.signe_egal -x 10  -y 180
entry .frame2.calcul_fois -width 10 -font {arial 14}
place .frame2.calcul_fois -x 40  -y 180
bind .frame2.n_fois <Return> ".frame2.calcul_fois configure -state normal ; lance_calcul  "
bind .frame2.calcul_fois <Return> {verif_calcul [.frame1.diviseur get ] [.frame2.n_fois get ] [.frame2.calcul_fois get]  }
bind .frame2.n_fois <KP_Enter> ".frame2.calcul_fois configure -state normal ; lance_calcul  "
bind .frame2.calcul_fois <KP_Enter> {verif_calcul [.frame1.diviseur get ] [.frame2.n_fois get ] [.frame2.calcul_fois get]  }
focus -force .frame2.n_fois 
button .frame2.fin -text [mc {fin_calcul_quot}] -font {Helvetica 12 bold } -command " fin_calcul $cumul_quot"
place .frame2.fin -x 30 -y 350
#button .frame2.imprimer -text [mc {imprimer}] -command " imprimer ."
#place .frame2.imprimer -x 45 -y 430
.frame2.calcul_fois configure -state disabled

if { $tab_config($nom_sce,flag_calcul_quot)== 1 } {
entry .frame1.quot1 -width 10 -font {arial  14} -justify right  
place .frame1.quot1 -x $coin_droit_x   -y [expr [ expr $coin_droit_y + 25 ] + [expr 1 * 15]]
 .frame1.quot1 configure -state disabled
}
set flag_reste 0 
 if { $tab_config($nom_sce,flag_repertoire) == 0  } {
.frame2.ajouter configure -state disabled
}
catch {destroy .top2 }
set w3 [toplevel .top2]
wm geometry $w3 250x90+400+200 
wm resizable $w3 no no
wm title $w3 [mc {n_diff_prev}]
grab ${w3}
lower  . ${w3}  
#focus  ${w3}
label ${w3}.titre -text [mc {n_diff_prev}] -font {arial 10}
place ${w3}.titre -x 55 -y 10 
entry ${w3}.n_sous  -width 3 -font {arial 10} -justify center
place ${w3}.n_sous -x 100 -y 40 
focus  ${w3}.n_sous 
set nombre_sous 100000
bind ${w3}.n_sous <Return> "retiens_sous "
bind ${w3}.n_sous <KP_Enter> "retiens_sous "
#### if { $nombre_sous != 100000 } { destroy ${w3} } "

proc retiens_sous {} {
global nombre_sous w3
if { [teste_input [ ${w3}.n_sous get ]] } {
 set nombre_sous [${w3}.n_sous get ]
 destroy ${w3} 
 } else {
${w3}.n_sous  delete 0 end
focus ${w3}.n_sous 
return
}
}
tkwait window ${w3}
set nombre_sous_eff 0
###tkwait variable nombre_sous 
#.frame2.calcul_fois configure -state disabled
##############################lance calcul pour le nombre de fois#################
#################################################################################
proc lance_calcul { } {
global indice_quot ind_cal nombre_sous_eff  var_reper indice_dividende tab_config flag_verif nom_sce flag_af flag_reste reste_partiel n_lance_calcul
############Pour limiter le nombre de diff�rences
if {[.frame1.diviseur get ]  == 0} { exit}
if { [ regexp {(^0+[1-9]+$)} [.frame2.n_fois  get] ]  } {.frame2.n_fois delete 0 end ; return }
if { ![ teste_input1 [.frame2.n_fois get] ]  } {
	if { $flag_reste == 1 && $tab_config($nom_sce,flag_calcul_diff)== 0 } {
	.frame2.n_fois delete 0 end
	focus .frame1.div${ind_cal} 
	.frame2.n_fois configure -state disabled ; .frame2.calcul_fois configure -state disabled
	return }  else {
 .frame2.n_fois delete 0 end ; return
}
}
if { [.frame2.n_fois  get] == 0 || [regexp {(^0+[1-9]+$)} [.frame2.n_fois  get] ] } { .frame2.n_fois delete 0 end ; return }
if { $nombre_sous_eff == 7 } { tk_messageBox -message "[mc {der_diff}]" -title [mc {mess_erreur}] -icon error -type ok ; \
focus .frame2.n_fois ; return }
if { $nombre_sous_eff == 8 } { tk_messageBox -message " [mc {trop_diff}]" -title [mc {mess_erreur}] -icon error -type ok ; exit }
##tk_messageBox -message "$cumul_quot, n' est bon (ou pas termin�) \n\ \ \ \ \  Continue !." -title "Message" -icon error -type ok 
############ on est ds le calcul des restes + impossible de disabled
if { $flag_reste == 1 } {
 if { $tab_config($nom_sce,flag_calcul_diff)== 0 } {
focus .frame1.div${ind_cal} 
.frame2.n_fois delete 0 end
.frame2.n_fois configure -state disabled ; .frame2.calcul_fois configure -state disabled
.frame2.n_fois delete 0 end
return
}
#.frame2.n_fois configure -state disabled
}
#### pour divid divis  � fixer
if { $flag_af == 1 } {
 set tab_config($nom_sce,diviseur_fixe) [.frame1.diviseur get] 
set var_reper  [.frame1.diviseur get] 
set tab_config($nom_sce,dividende_fixe) [.frame1.dividende get] 
#### si premier calcul pour � fixer 
 if { $n_lance_calcul == 0} {
set reste_partiel $tab_config($nom_sce,dividende_fixe) 
.frame2.question configure -text  "    [mc {n_fois}]\n     $tab_config($nom_sce,diviseur_fixe) ?" -font {Arial 18} -bg #aaaaaa
# .frame2.diviseur configure -text [.frame1.diviseur get]
# $tab_config($nom_sce,diviseur_fixe) 
}
incr n_lance_calcul
}
.frame1.diviseur configure -state disabled ; .frame1.dividende configure -state disabled
if { $tab_config($nom_sce,flag_n_quot_noeud) } {
if { [regexp {^[0-9]0+$} [.frame2.n_fois get ]] == 0 && [.frame2.n_fois get] >= 10 } {
tk_messageBox -message "[.frame2.n_fois get ] , [mc {mess_noeud} ]" -title "Message" -icon error -type ok 
return  
}
}
.frame2.n_fois configure -state normal ; .frame2.calcul_fois delete 0 end
if { $tab_config($nom_sce,flag_recup_valeur) } {
if { [.frame2.calcul_fois cget -state ] == "disabled" } {
return
}
###recup valeur automatique
.frame2.calcul_fois insert 0 [expr [.frame2.n_fois get ] * [.frame1.diviseur get]]
set quot1_par [expr [.frame2.n_fois get ] * [.frame1.diviseur get]]
if { $quot1_par > $reste_partiel  } {
tk_messageBox -message "[.frame2.n_fois get ] , [mc {nbre_trop_grand}]" -title [mc {mess_erreur}] -icon error -type ok 
.frame2.calcul_fois delete 0 end 
.frame2.n_fois configure -state normal
.frame2.n_fois delete 0 end 
focus -force .frame2.n_fois
return
}
#.frame2.n_fois delete 0 end ; .frame2.calcul_fois delete 0 end ; focus -force .frame2.n_fois 
place_quotient $indice_quot
place_reste $indice_dividende $indice_quot $quot1_par
		} else { focus .frame2.calcul_fois  ; ### pas de recup
		}
.frame2.n_fois configure -state normal
#.frame2.calcul_fois configure -state disabled
}
############Pour tester l'�galit� a=bq##########################
################################################################
proc verif_calcul {diviseur n_fois calcul } {
global flag_verif indice_quot tab_config indice_dividende indice_quot flag_reste nom_sce reste_partiel
if { ![ teste_input1 [.frame2.calcul_fois get] ]  || [.frame2.n_fois get] == {} } {
 .frame2.calcul_fois delete 0 end ; return
}
.frame2.n_fois configure -state disabled ; set flag_verif 0
if { [expr $diviseur * ${n_fois} ] == $calcul } {
if { $calcul >  $reste_partiel } {
tk_messageBox -message "$n_fois , [mc {nbre_trop_grand}]" -title [mc {mess_erreur}] -icon error -type ok 
.frame2.calcul_fois delete 0 end 
.frame2.n_fois configure -state normal
.frame2.n_fois delete 0 end 
focus -force .frame2.n_fois
return
}
set flag_verif 1 ; set flag_reste 1 
place_quotient $indice_quot
.frame2.n_fois configure -state disabled 
place_reste $indice_dividende $indice_quot $calcul
.frame2.n_fois configure -state normal 
.frame2.n_fois delete 0 end ; .frame2.calcul_fois delete 0 end ; focus -force .frame2.n_fois 
.frame2.calcul_fois configure -state disabled 
# .frame2.n_fois configure -state disabled ;.frame2.calcul_fois configure -state disabled 
return 1 } else {
tk_messageBox -message "$calcul , [mc {mauvais_prod}]" -title [mc {mess_erreur}] -icon error -type ok 
.frame2.n_fois configure -state disabled
.frame2.calcul_fois delete 0 end ; focus .frame2.calcul_fois 
}
}
##################
proc verif_calcul1_partiel {reste quot ind } {
verif_calcul1 $reste $quot [.frame1.div${ind} get] $ind
}
#########Validation du calcul reste partiel quand non automatique
proc verif_calcul1 {reste0 reste1 input indice} {
global reste_partiel flag_reste
.frame2.n_fois configure -state disabled
.frame2.calcul_fois configure -state disabled
if { [expr $reste0 -$reste1 ] == $input } {
set flag_reste 0
.frame2.n_fois configure -state normal
.frame2.calcul_fois configure -state normal
.frame2.ajouter configure -state normal
set reste_partiel $input
# [expr $reste_partiel - $input] 
focus -force .frame2.n_fois
.frame1.div${indice} configure -state disabled
				} else {
tk_messageBox -message "${input}, [mc {mauvais_diff}]" -title [mc {mess_erreur}] -icon error -type ok
.frame1.div${indice} delete 0 end ; focus .frame1.div${indice} 
}
}

###############Pour enchainer le calcul des restes partiels###############
#########################################################################

proc place_reste {ind_div ind_qu quot_partiel} {
global tab_config reste_partiel indice_dividende indice_quot ind_cal nombre_sous_eff nom_sce flag_reste coin_gauche_x coin_gauche_y
set flag_reste 1
if { $tab_config($nom_sce,flag_calcul_diff) == 1 } {
entry .frame1.div${ind_div} -width 10 -font {arial  14} -justify right  
place .frame1.div${ind_div} -x $coin_gauche_x   -y [expr [expr $coin_gauche_y + 10 ] + [expr $ind_div * 30]]
set reste_partiel  [expr $reste_partiel - $quot_partiel]
.frame1.div${ind_div} insert 0 $reste_partiel ; incr indice_dividende ; incr nombre_sous_eff
.frame2.n_fois delete 0 end ; .frame2.calcul_fois delete 0 end ; focus -force .frame2.n_fois 
.frame1.div${ind_div} configure -state disabled
} else {
### � mettre dans lance_calcul
###if { $nombre_sous_eff = 14 } {tk_message dernier finis  }
###if { $nombre_sous_eff = 15 } {tk_message trop  ; exit  }
.frame2.n_fois delete 0 end
.frame2.calcul_fois delete 0 end
.frame2.calcul_fois configure -state disabled
.frame2.n_fois configure -state disabled
.frame2.ajouter configure -state disabled
entry .frame1.diff${ind_div} -width 10 -font {arial  14} -justify right  
place .frame1.diff${ind_div} -x $coin_gauche_x  -y [expr [expr  $coin_gauche_y + 10 ] + [expr $ind_div * 30]]
.frame1.diff${ind_div} insert 0 $quot_partiel
.frame1.diff${ind_div} configure -state disabled
#.frame1.diff${indice_dividende} configure -state disabled
label .frame1.${ind_qu}_signe_moins -text "-" -width 2 -font {arial 12 bold} -bg white
place .frame1.${ind_qu}_signe_moins -x [expr $coin_gauche_x  -25 ] -y [expr [expr $coin_gauche_y +13 ] + [expr $ind_div * 30]]
set ind_cal [expr $ind_div + 1]
entry .frame1.div${ind_cal} -width 10 -font {arial  14} -justify right  
place .frame1.div${ind_cal} -x $coin_gauche_x -y  [expr [expr $coin_gauche_y + 10 ] + [expr $ind_cal * 30]]
focus .frame1.div${ind_cal} 
incr  nombre_sous_eff
#set $quot_partiel 1000
#set input1 [.frame1.div${ind_cal}  get]
bind .frame1.div${ind_cal} <Return> "verif_calcul1_partiel $reste_partiel $quot_partiel  ${ind_cal}"  
bind .frame1.div${ind_cal} <KP_Enter> "verif_calcul1_partiel $reste_partiel $quot_partiel  ${ind_cal}"  
#set reste_partiel  [expr $reste_partiel - $quot_partiel] 
incr indice_dividende 2
#.frame1.div${ind_cal} configure -state disabled
#.frame2.n_fois delete 0 end ; .frame2.calcul_fois delete 0 end ; focus -force .frame2.n_fois 

}
}

proc place_quotient {ind  } {
global tab_config cumul_quot indice_quot nom_sce coin_droit_x coin_droit_y
#entry.frame2.quot${ind}           -tag quot
if { $tab_config($nom_sce,flag_calcul_quot) == 1 } {
#entry .frame1.quot${ind} -width 10 -font {arial  16} -justify right  
#place .frame1.quot${ind} -x 210  -y [expr 140 + [expr $ind * 15]]
.frame1.quot1 configure -state normal
set cumul_quot [ expr $cumul_quot + [.frame2.n_fois get] ]
.frame1.quot1 delete 0 end ; .frame1.quot1 insert 0 $cumul_quot 
#.frame2.n_fois delete 0 end ; .frame2.calcul_fois delete 0 end ; focus -force .frame2.n_fois 
.frame1.quot1 configure -state disabled

incr indice_quot
} else {
entry .frame1.quot${ind} -width 10 -font {arial  14} -justify right  
place .frame1.quot${ind} -x $coin_droit_x  -y [expr [expr $coin_droit_y + 12] + [expr $ind * 30]]
set cumul_quot [expr $cumul_quot + [.frame2.n_fois get]  ]
.frame1.quot${ind} insert 0 [.frame2.n_fois get]
.frame1.quot${ind} configure -state disabled
incr indice_quot
}
#.frame2.n_fois delete 0 end ; .frame2.calcul_fois delete 0 end ; focus -force .frame2.n_fois 

}
proc fin_calcul { cumul } {
global cumul_quot tab_config indice_quot nombre_sous nombre_sous_eff nom_sce reste_partiel
global coin_droit_x coin_droit_y
if { $cumul_quot == [expr [.frame1.dividende get]  / [.frame1.diviseur get]] } {
.frame2.fin configure  -state disabled
if { $nombre_sous == $nombre_sous_eff } {
set message " [mc {bon_nbre_diff}] ${nombre_sous} "
} else {
set message "ton pari �tait ${nombre_sous} diff�rences contre $nombre_sous_eff  . Tu peux faire mieux (ou trop rapide)"
}
#tk_messageBox -message "$cumul_quot, est bon \n\ \ \ \ \  Bravo !. \n $message" -title "Message" -icon info -type ok 
if {$tab_config($nom_sce,flag_calcul_quot) == 1 } {
if { $nombre_sous == $nombre_sous_eff } {
set message "[mc {bon_nbre_diff}] ${nombre_sous_eff} "
} else {
set message "Ton pari �tait ${nombre_sous} diff�rences contre $nombre_sous_eff . \n Tu peux faire mieux (ou trop rapide)"
}
message .frame2.bonne_fin -justify center -text " [mc {tu_as}] \n [.frame1.dividende get] = \( $cumul_quot \* [.frame1.diviseur get] \) + $reste_partiel\n  \
[mc {avec}] $reste_partiel \< [.frame1.diviseur get] "   -font {Helvetica 14 bold} -bg white -fg red -width 250 
#-font {Helvetica 14 bold} -bg white -fg red -width 250
place .frame2.bonne_fin -x 10 -y 400
tk_messageBox -message "$cumul_quot, [mc {bon_quot}] $message" -title "Message" -icon info -type ok 

} else {
 .frame2.fin configure  -state disabled
entry .frame1.quot${indice_quot} -width 10 -font {arial 14 bold} -justify right  
place .frame1.quot${indice_quot} -x $coin_droit_x  -y [expr [expr $coin_droit_y + 10 ] + [expr $indice_quot * 30]]
# set calcul_fin [.frame1.quot${indice_quot} get]
set depart_quot [expr $coin_droit_y + 12 ] ; ###126
for { set k 1 } { $k < ${indice_quot} } { incr k } {
label .frame1.p${k} -text "+" -bg white -font {arial 14}
place .frame1.p${k} -x $coin_droit_x   -y [expr [expr $depart_quot +3 ] + [expr $k * 30]]

}
.frame2.calcul_fois configure -state disabled
.frame2.n_fois configure -state disabled
focus .frame1.quot${indice_quot}
bind .frame1.quot${indice_quot} <Return> "verif2_calcul $cumul_quot $indice_quot"
bind .frame1.quot${indice_quot} <KP_Enter> "verif2_calcul $cumul_quot $indice_quot"

}
} else {
tk_messageBox -message "$cumul_quot,[mc {mauv_quot}]" -title "Message" -icon error -type ok 

}
}
#####################Validation du calcul du quotient qd fin de la division
proc verif2_calcul {total ind} {
global nombre_sous nombre_sous_eff reste_partiel
if { $total == [.frame1.quot${ind} get] } {
if { $nombre_sous == $nombre_sous_eff } {
set message "[mc {bon_nbre_diff}]${nombre_sous_eff} "
} else {
set message "[mc {pari}] ${nombre_sous} [mc {diff}] ${nombre_sous_eff} . \n [mc {mieux_faire}]"
}
message .frame2.bonne_fin -justify center -text "[mc {tu_as_donc}][.frame1.dividende get] = \( $total \* [.frame1.diviseur get] \) + $reste_partiel  \
\n [mc {avec}] $reste_partiel \< [.frame1.diviseur get] "   -font {Helvetica 14 bold} -bg white -fg red -width 250
place .frame2.bonne_fin -x 10 -y 450
tk_messageBox -message "            $total,[mc {bon_tot}]\n $message" -title "Message" -icon info -type ok 
} else {
tk_messageBox -message "[.frame1.quot${ind} get] [mc {mauv_tot}]" -title "Message" -icon error -type ok 
.frame1.quot${ind} delete 0 end ; focus .frame1.quot${ind}
}
}
proc sauver_ps {} {
global basedir TRACEDIRX_DIV
cd $TRACEDIRX_DIV
global nom_elev nom_class
.frame_right.canvas  postscript -height 20c -width 20c  -file "$nom_elev.ps"
cd $basedir
}

############ � �tudier pour imprimer sous windows
proc imprimer {c} {
if {[info exists env(PRINTER)]} {
    set psCmd "lpr -P$env(PRINTER)"
} else {
    set psCmd "lpr"
}

set postscriptOpts {-pageheight 190m -pagewidth 280m -pageanchor c}

if {[catch {eval exec $psCmd <<    \
		  {[eval $c postscript $postscriptOpts]}} msg]} {
		    tk_messageBox -message  \
		      "Error when printing: $msg" -icon error -type ok
		}
}
proc teste_input1 { input} {
if  {[ regexp {(^\-*[0-9]+$)} $input ] == 1 } {
            return $input
    } else {
return 0
}
}









