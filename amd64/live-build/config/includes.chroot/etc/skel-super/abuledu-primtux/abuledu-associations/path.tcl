#!/bin/sh
#menus.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : path.tcl
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    :
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    $Id: path.tcl,v 1.10 2006/05/21 10:15:29 david Exp $
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************



proc supnum {cate} {
if {[string match {[0-9]} [string index $cate end]] == 1} {
    set  cate [string range $cate 0 [expr [string length $cate] -2] ]
    supnum $cate
   } else {
return $cate
}
}

proc opencat {} {
global user sysFont Home repbasecat
catch {destroy .opencate}
variable getcat

toplevel .opencate -background grey -width 250 -height 400
#ajout eric, si on est sur plus de 640x480 on d�lace la popup sur
#la droite de la fenetre principale
set largeur [winfo screenwidth .]
if {$largeur > 640} {
  wm geometry .opencate +650+50
} else {
  wm geometry .opencate +50+50
}
wm transient .opencate .
wm title .opencate [mc {Choisir un fichier}]
frame .opencate.frame -background grey -width 250 -height 400
pack .opencate.frame -side top
label .opencate.frame.labobj -font $sysFont(s) -text [mc {Cliquer pour choisir une categorie.}] -background grey
grid .opencate.frame.labobj -row 0 -column 0
listbox .opencate.frame.list -yscrollcommand ".opencate.frame.scroll set"
scrollbar .opencate.frame.scroll -command ".opencate.frame.list yview"
grid .opencate.frame.list -row 1 -column 0 -sticky ew
grid .opencate.frame.scroll -in .opencate.frame -row 1 -column 1 -sticky ns
button .opencate.frame.but -text [mc {OK}] -command "setcat"
grid .opencate.frame.but -row 2 -column 0 -sticky ew 
catch {
foreach i [lsort [glob [file join $Home categorie $repbasecat *.cat]]] {
if {([string first non_ [file tail [lindex [split [lindex [split $i /] end] .] 0]]] != 0) && ([string match {[0-9]} [file tail [string index [lindex [split [lindex [split $i /] end] .] 0] end ]]] != 1)} {
.opencate.frame.list insert end [file tail [lindex [split [lindex [split $i /] end] .] 0]]
}
}
}
focus -force .opencate
set getcat [.opencate.frame.list get 0]
tkwait variable getcat
}

proc setcat {} {
variable getcat
if {[.opencate.frame.list get active] != ""} {
set getcat [.opencate.frame.list get active]
}
destroy .opencate
}


proc setwindowsusername {} {
    global user LogHome
    catch {destroy .utilisateur}
    toplevel .utilisateur -background grey -width 250 -height 100
    wm geometry .utilisateur +50+50
    frame .utilisateur.frame -background grey -width 250 -height 100
    pack .utilisateur.frame -side top
    label .utilisateur.frame.labobj -font {Helvetica 10} -text [mc {Quel est ton nom?}] -background grey
    pack .utilisateur.frame.labobj -side top 

    listbox .utilisateur.frame.listsce -yscrollcommand ".utilisateur.frame.scrollpage set" -width 15 -height 10
    scrollbar .utilisateur.frame.scrollpage -command ".utilisateur.frame.listsce yview" -width 7
    pack .utilisateur.frame.listsce .utilisateur.frame.scrollpage -side left -fill y -expand 1 -pady 10
    bind .utilisateur.frame.listsce <ButtonRelease-1> "verifnom %x %y"
    foreach i [lsort [glob [file join $LogHome *.log]]] {
    .utilisateur.frame.listsce insert end [string map {.log \040} [file tail $i]]
    }

    button .utilisateur.frame.ok -background gray75 -text [mc {Ok}] -command "interface; destroy .utilisateur"
    pack .utilisateur.frame.ok -side top -pady 70 -padx 10
}

proc verifnom {x y} {
    global env user LogHome

set ind [.utilisateur.frame.listsce index @$x,$y]
set nom [string trim [.utilisateur.frame.listsce get $ind]]
    if {$nom !=""} {
	set env(USERNAME) $nom
	set user [file join $LogHome $nom.log]
    }
}

proc init {plateforme} {
global Home basedir baseHome


if {![file exists [file join $Home]]} {
	file mkdir [file join $Home]
	file mkdir [file join $Home images]
	file mkdir [file join $Home categorie cat_francais]
	file copy -force [file join reglages] [file join $Home]
	file copy -force [file join images blank.gif] [file join $Home images]

}

if {![file exists [file join $Home sons]]} {
	file mkdir [file join $Home sons]
}

if {![file exists [file join $Home images blank.gif]]} {
file copy [file join sysdata blank.gif] [file join $Home images]
}

if {![file exists [file join $basedir images blank.gif]]} {
file copy [file join sysdata blank.gif] [file join $basedir images]
}

switch $plateforme {
    unix {
	if {![file exists [file join  $baseHome log]]} {
	file mkdir [file join  $baseHome log]
	}

    }
    windows {
	if {![file exists [file join associations log]]} {
	file mkdir [file join associations log]
	}
	
    	}
}

}


proc initlog {plateforme ident} {
global LogHome user Home baseHome
switch $plateforme {
    unix {
	set LogHome [file join $baseHome log]

    }
    windows {
	set LogHome [file join associations log]
    }
}

if {$ident != ""} {
     set user [file join $LogHome $ident.log]
     } else {
     set user [file join $LogHome associations.log]
     }
if {![file exists [file join $user]]} {
set f [open [file join $user] "w"]
close $f
}
}

proc inithome {} {
global baseHome basedir Home
variable repert
set f [open [file join $baseHome reglages repert.conf] "r"]
set repert [gets $f]
close $f

switch $repert {
0 {set Home $baseHome}
1 {set Home $basedir }
}
}

proc changerepbasecat {i} {
global Home baseHome
set f [open [file join $baseHome reglages associations.conf] "r"]
set tmp1 [gets $f]
set tmp2 [gets $f]
set tmp3 [gets $f]
set tmp4 [gets $f]
set tmp5 [gets $f]
close $f

set f [open [file join $baseHome reglages associations.conf] "w"]
puts $f "none"
puts $f $tmp2
puts $f $tmp3
puts $f $tmp4
puts $f $tmp5
puts $f $i
close $f

}

proc changehome {} {
global Home basedir baseHome
variable repertcat
variable repert
set f [open [file join $baseHome reglages repert.conf] "w"]
puts $f $repert
close $f

switch $repert {
0 {set Home $baseHome}
1 {set Home $basedir }
}

set f [open [file join $baseHome reglages associations.conf] "r"]
set tmp1 [gets $f]
set tmp2 [gets $f]
set tmp3 [gets $f]
set tmp4 [gets $f]
set tmp5 [gets $f]
set tmp6 [gets $f]
close $f
set repertcat $tmp6

}

global basedir Home baseHome iwish progaide abuledu prof

set abuledu 0
set prof 0
set basedir [file dir $argv0]
cd $basedir
if {$env(HOME) == "c:\\"} {
set Home [file join $basedir]

} else {
set Home [file join $env(HOME) leterrier associations]
}
set baseHome $Home



switch $tcl_platform(platform) {
    unix {
	set progaide runbrowser
	#set progaide dillo
	set iwish wish
	if {[lsearch [exec id -nG $env(USER)] "profs"] != -1} {set prof 1}
	#if {[string first "profs" $env(HOME)] != -1} {set prof 1}
	if {[file isdirectory [file join /etc abuledu]]} {
	    set abuledu 1
	}
	package require Img
	source sonlinux.tcl
    }
    windows {
	set progaide shellexec.exe
	set iwish wish
	source sonwindows.tcl
	package require Img
    }
}



