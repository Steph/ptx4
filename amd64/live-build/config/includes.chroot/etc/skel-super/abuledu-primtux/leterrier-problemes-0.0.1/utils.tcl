#!/bin/sh
#problemes.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2007 David Lucardi <davidlucardi@aol.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier:
#  Date    : 01/06/2007
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  David Lucardi 01/06/2007
# 
#  *************************************************************************
source fonts.tcl
source eval.tcl
source path.tcl

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}

initlog $plateforme $ident
inithome

wm protocol . WM_DELETE_WINDOW "quitte"

proc speaktexte {texte} {
global basedir
regsub -all {\(s\)} $texte "" texte
#catch {exec [file join $basedir cicero bulktalk.py] $texte &}
cd $basedir
}

proc quitte {} {
quitter
destroy .
}

##############################################
proc quitter {} {
global arg niv parcours page indparcours log nbverif Home
enregistreval $log
set nbverif 0
set page 1
incr indparcours
	if {$parcours == "none"} {
	catch {destroy .}
	} else {
	set f [open [file join  $Home problemes $parcours] "r"]
	set tmp [gets $f]
	close $f
		if {$indparcours < [llength $tmp]} {
		set arg [lindex [lindex $tmp $indparcours] 0]
		set niv [lindex [lindex $tmp $indparcours] 1]
		main
		} else {
		catch {destroy .}
		}
	}
}


proc continue {} {
global log nbverif
enregistreval $log
set log ""
set nbverif 0
.framebottom0.verif configure -command ""
main
}


proc verif {} {
global niveau operations flagok etapes page reponse nbverif log okdessinniv4 source
incr nbverif
lappend log "Essai n� $nbverif"
if {$niveau == 3} {
.frame.c itemconfigure canvas -state hidden
catch {
destroy button .framebottom0.corrige
if {[lindex $operations 0]!="0+0"} {.framebottom.ope1 configure -state normal}
for {set z 0} {$z < [llength $reponse]} {incr z} {
set repons [lindex $reponse $z]
for {set i 0} {$i < [llength [lindex $repons 1]]} {incr i} {
.framebottom$z.reponse$i configure -state normal
}
}
}
}
set flagok 1
if {($niveau <= 2 || ($niveau == 4 && $okdessinniv4 == 1)) && $source !=""} {verifdessin}
if {$niveau >= 2} {verifope}
if {$niveau >= 1} {verifrep}
	if {$flagok == 1} {
		if {$page < $etapes} {
		.framebottom0.verif configure -text "Continuer" -command "continue"
		incr page
		} else {
		.framebottom0.verif configure -text "Quitter" -command "quitter"
		}
	}

}



proc verifrep {} {
global reponse flagok log
set flag 0
	for {set z 0} {$z < [llength $reponse]} {incr z} {
	set repons [lindex $reponse $z]
		foreach ind [lindex $repons 0] {
		set result [lindex [lindex $repons 1] $ind]
		set resultq [.framebottom$z.reponse$ind get]
			if {$result != $resultq} {
			#tk_messageBox -message "Il y a une erreur dans le champ [expr $z +1] de la r�ponse."
			if {$resultq ==""} {set resultlog "vide"} else {set resultlog $resultq}
			lappend log [concat Erreur dans le champ [expr $z +1] de la r�ponse (r�ponse donn�e : $resultlog).]
			.framebottom$z.reponse$ind configure -bg red
			set flagok 0
			incr flag
			} else {
			.framebottom$z.reponse$ind configure -bg yellow
			} 
		}
	}
if {$flag == 0} {lappend log "La r�ponse est juste."} 
}

proc verifope {} {
#reponse
global operations flagok log
if {[lindex $operations 0] == "0+0"} {return}
set repl [.framebottom.ope1 get]

if {$repl == ""} {set replog "vide"} else {set replog $repl}
lappend log [concat Op�ration :  $replog .]
#test ecriture valide
	if {[catch {expr [string map {= ==} $repl]}] ==1} {
	.framebottom.ope1 configure -bg red
	set flagok 0
	#tk_messageBox -message "Erreur dans l'op�ration (champ vide?)"
	return
	}
#test ecriture donnant resultat
set ope $repl
set ind [string first  "=" $ope]
	if {$ind != -1} {
	set ope1 [string range $ope 0 [expr $ind -1]]
	regsub -all {[\040]} $ope1 "" ope1
	#regsub -all {[\(\)]} $ope1 "" ope1
	incr ind
	set result [string range $ope $ind end]
	set flagresult 0
	set flagope 0
	if {[lindex [lindex $operations 0] 0] != "libre"} {
		foreach operation $operations {
			if {$ope1 == $operation} {
			set flagope 1
				if {$result == [expr $operation]} {
				set flagresult 1
				}
			}
		}
	} else {
		foreach operation $operations {
			if {[expr $ope1] == [lindex $operation 1]} {
			set flagope 1
				if {$result == [lindex $operation 1]} {
				set flagresult 1
				}
			}
		}
	#if {$result == [lindex [lindex $operations 0] 1]} {
	#set flag 2
	#}
	}

		if {$flagope == 0} {
		.framebottom.ope1 configure -bg red
		set flagok 0
		#tk_messageBox -message "L'operation est fausse"
		return
		}
		if {$flagope == 1 && $flagresult == 0} {
		.framebottom.ope1 configure -bg green
		set flagok 0
		#tk_messageBox -message "Le resultat de l'operation est faux"
		return
		}

	} else {
	.framebottom.ope1 configure -bg red
	set flagok 0
	#tk_messageBox -message "Il y a une erreur dans l'op�ration (pas de signe =?)"
	return
	} 
#tk_messageBox -message "L'op�ration est juste."
.framebottom.ope1 configure -bg yellow
}
########################################################

proc tracecibleline {intervalcible taillerect orgxorig orgy orient labelcible quadri volatil} {
global cible sysFont
set orgx $orgxorig
	for {set k 0} {$k < [llength $cible]} {incr k 1} {
	set nbcibleplace  [lindex [lindex [lindex $cible $k] 2] 1]
	.frame.c create text [expr $orgx + [string length [lindex $labelcible $k]]*5] [expr $orgy - 30] -text [lindex $labelcible $k] -font $sysFont(s) -tags "canvas splab$k"
	.frame.c bind splab$k <ButtonRelease-1> "speaktexte \173[lindex $labelcible $k]\175"
	#set orgx [expr $orgxorig + $k*([lindex [lindex $cible $k] 1]+1)*$taillerect + $intervalcible]
		for {set i 0} {$i < [expr [lindex [lindex $cible $k] 1]]} {incr i 1} {
			for {set j 0} {$j < [expr [lindex [lindex $cible $k] 0]]} {incr j 1} {
			.frame.c create rect [expr $orgx + $i*$taillerect] [expr $orgy + $j*$taillerect ]  [expr $orgx + ($i+1)*$taillerect]  [expr $orgy + ($j+1)*	$taillerect]  -width 2 -fill grey -tags "rect$k$i$j canvas"
				if {$nbcibleplace != 0 && $nbcibleplace != ""} {
					if {$volatil == 0} {
					.frame.c create image [expr $orgx + $i*$taillerect + int($taillerect/2)] [expr $orgy + $j*$taillerect + int($taillerect/2)] -image [image create photo -file [file join sysdata [lindex [lindex [lindex $cible $k] 2] 0]]] -tags "place canvas"
					} else {
					.frame.c create image [expr $orgx + $i*$taillerect + int($taillerect/2)] [expr $orgy + $j*$taillerect + int($taillerect/2)] -image [image create photo -file [file join sysdata [lindex [lindex [lindex $cible $k] 2] 0]]] -tags "drag place [lindex [lindex $cible $k] 3] canvas"
					}
				incr nbcibleplace -1
				}

			}
		}
	set orgx [expr $orgx + ([lindex [lindex $cible $k] 1])*$taillerect + $intervalcible]
	}

set orgx $orgxorig 
	for {set k 0} {$k < [llength $cible]} {incr k 1} {
	#set orgx [expr $orgxorig + $k*([lindex [lindex $cible $k] 1]+1)*$taillerect + $intervalcible]
	.frame.c create rect [expr $orgx] [expr $orgy]  [expr $orgx + ([lindex [lindex $cible $k] 1])*$taillerect]  [expr $orgy + ([lindex [lindex $cible $k] 0])*$taillerect]  -width 2 -fill grey -outline blue -tags "cible$k canvas"
		if {$quadri == 1} {
			if {$orient == 0} {
				for {set i 0} {$i <= [expr [lindex [lindex $cible $k] 1]]} {incr i 1} {
					for {set j 0} {$j < [expr [lindex [lindex $cible $k] 0]]} {incr j 1} {
					.frame.c create line [expr $orgx + $i*$taillerect] [expr $orgy + $taillerect*($j)]  [expr $orgx + $i*$taillerect]  [expr $orgy + $taillerect*($j+1)] -width 2 -fill green -tags "line$k$i$j canvas"
					}
				}
			} else {
				for {set i 1} {$i <= [expr [lindex [lindex $cible $k] 1]]} {incr i 1} {
					for {set j 0} {$j <= [expr [lindex [lindex $cible $k] 0]]} {incr j 1} {
					.frame.c create line [expr $orgx + $taillerect*($i-1)] [expr $orgy + $j*$taillerect]  [expr $orgx + $taillerect*($i)]  [expr $orgy + $j*$taillerect] -width 2 -fill green -tags "line$k$i$j canvas"
					}
				}
			}
		}
	set orgx [expr $orgx + ([lindex [lindex $cible $k] 1])*$taillerect + $intervalcible]
	}
foreach item [.frame.c find withtag place] {.frame.c raise $item} 
}
###################################################

proc itemStartDrag {x y} {
global lastX lastY sourcecoord
set sourcecoord [.frame.c bbox current]
set lastX [expr [lindex $sourcecoord 0] + ([lindex $sourcecoord 2] - [lindex $sourcecoord 0])/2]
set lastY [expr [lindex $sourcecoord 1] + ([lindex $sourcecoord 3] - [lindex $sourcecoord 1])/2]
.frame.c raise current
}
#######################################################
proc itemDrag {x y} {
global lastX lastY
set x [.frame.c canvasx $x]
set y [.frame.c canvasy $y]
.frame.c move current [expr $x-$lastX] [expr $y-$lastY]
set lastX $x
set lastY $y
}
###################################################
proc tracedessin {nb orgsourcey orgsourcexorig taillerect} {
set ind 0
set decal 0
	for {set k 0} {$k < $nb} {incr k} {
	set orgy [expr $orgsourcey + $decal]
	.frametop.c1 create oval [expr $orgsourcexorig] [expr $orgy ] [expr $orgsourcexorig + $taillerect] [expr $orgy + $taillerect] -fill red -tags color
	set decal [expr $decal + $taillerect +10]
	incr ind
	if {$ind > 5} {
	set ind 0
	set decal 0
	set orgsourcexorig [expr $orgsourcexorig + $taillerect +10]
	}
	}
.frametop.c1 bind color <1> "changecolor"
}

proc changecolor {} {
set color [.frametop.c1 itemcget current -fill]
if {$color == "red"} {
.frametop.c1 itemconfigure current -fill blue
} else {
.frametop.c1 itemconfigure current -fill red
}
}
#########################################################
