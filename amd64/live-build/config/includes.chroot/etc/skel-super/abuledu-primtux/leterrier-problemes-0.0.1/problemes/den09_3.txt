set etapes 1
set niveaux {0 1 3}
::1
set niveau 1
set ope {{1 9} {1 9}}
set interope {{1 10 1} {1 10 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set volatil 0
set operations {{0+0}}
set enonce "La monnaie.\nPim a [expr $ope1*10 + $ope2] euros dans sa tirelire. Il a moins de 10 pi�ces.\nCombien a-t-il de billets et de pi�ces?"
set cible {{3 3 {} source0} {6 9 {} source1}}
set intervalcible 60
set taillerect 40
set orgy 50
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 700
set source {euros10.gif euros1.gif}
set orient 0
set labelcible {Billets Pi�ces}
set quadri 0
set ensembles [list [expr $ope1] [expr $ope2]]
set reponse [list [list {1 3} [list {Il a} [expr $ope1] {billet(s) et} [expr $ope2] pi�ce(s).]]]
set dessin ""
set canvash 300
set c1height 160
set opnonautorise {}
::
