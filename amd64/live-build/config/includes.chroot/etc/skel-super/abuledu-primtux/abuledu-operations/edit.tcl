############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editeur.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
#				200909 - Nathalie Seigne
# 				mailto : nathalie.seigne@oracle.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: edit.tcl,v 1.2 2004/11/14 15:38:27 david Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

	# Sourcing
	set plateforme $tcl_platform(platform)
	set ident $tcl_platform(user)

	source fonts.tcl
	source path.tcl
	source eval.tcl
	source msg.tcl

	# set ztype "TEST"
	# PARAMETRE pour l'editeur commun
	set ztitre "TEST"
	set zconf "text.conf"
	set ztype $::argv
	if {$ztype == "additions"} {
		set ztitre "Additions NEW"
		set zconf "additions.conf"
	}
	if {$ztype == "soustractions"} {
		set ztitre "Soustractions NEW"
		set zconf "soustractions.conf"
	}
	if {$ztype == "multiplications"} {
		set ztitre "Multiplications NEW"
		set zconf "multiplications.conf"
	}

	inithome
	initlog $plateforme $ident
	changehome

	set fichier [file join $basedir aide index.htm]
	bind . <F1> "exec $progaide \042$fichier\042 &"

	

	. configure -background blue
	frame .geneframe -background blue -height 300 -width 500
	pack .geneframe -side left


	# G�n�ration interface
	proc interface {} {
	global sysFont ztype zconf ztitre

	




	catch {destroy .geneframe}

	catch {destroy .leftframe}

	frame .leftframe -background blue -height 400 -width 250 
	pack .leftframe -side left -anchor n

	frame .geneframe -background blue -height 600 -width 500
	pack .geneframe -side top -anchor n -padx 30
	frame .geneframe2 -background blue -height 600 -width 500
	pack .geneframe2 -side top -anchor n -padx 30
	frame .geneframe3 -background blue -height 600 -width 500
	pack .geneframe3 -side top -anchor n -padx 30

	label .leftframe.lignevert1 -bg red -text "" -font {Arial 1}
	pack .leftframe.lignevert1 -side right -expand 1 -fill y

	# leftframe.frame1
	frame .leftframe.frame1 -background blue -width 250 
	pack .leftframe.frame1 -side top 


	label .leftframe.frame1.lab1 -foreground red -bg white -text $ztitre -font $sysFont(l)
	pack .leftframe.frame1.lab1 -side top -fill x -pady 5

	label .leftframe.frame1.lab2 -bg blue -foreground white -text [mc "Sc�nario"] -font $sysFont(l)
	pack .leftframe.frame1.lab2 -side top -fill x -expand 1 -pady 5


	listbox .leftframe.frame1.listsce -yscrollcommand ".leftframe.frame1.scrollpage set" -width 30 -height 15 -font $sysFont(ms)
	scrollbar .leftframe.frame1.scrollpage -command ".leftframe.frame1.listsce yview" -width 7
	pack .leftframe.frame1.listsce .leftframe.frame1.scrollpage -side left -fill y -expand 1 -pady 10
	bind .leftframe.frame1.listsce <ButtonRelease-1> "changelistsce %x %y"


	#### NOUVEAU
	button .leftframe.frame1.butup -text [mc "Up"] -command "upitem" -image [image create photo -file [file join sysdata arrow-up.xpm]] -activebackground white -font $sysFont(msb) -foreground black -background white
	pack .leftframe.frame1.butup -padx 10 -pady 10 -side top

	button .leftframe.frame1.butdown -text [mc "Down"] -command "downitem" -image [image create photo -file [file join sysdata arrow-down.xpm]] -activebackground white -font $sysFont(msb) -foreground black -background white
	pack .leftframe.frame1.butdown -padx 10 -pady 10 -side top
	### NOUVEAU



	# leftframe.frame2
	frame .leftframe.frame2 -background blue -width 250
	pack .leftframe.frame2 -side top 
	button .leftframe.frame2.but0 -text [mc "Nouveau"] -command "additem" -activebackground white -font $sysFont(msb) -foreground green -background white
	pack .leftframe.frame2.but0 -padx 50 -pady 10 -side left
	button .leftframe.frame2.but1 -text [mc "Supprimer"] -command "delitem" -activebackground white -font $sysFont(msb) -foreground red -background white
	pack .leftframe.frame2.but1 -padx 50 -pady 10 -side left


	# leftframe.frame3

	label .geneframe.lab0 -foreground white -text [mc ""] -font $sysFont(l) -bg blue
	pack .geneframe.lab0 -side top -fill x -pady 5
	label .geneframe.lab1 -foreground white -text [mc "S�rie d'op�rations"] -font $sysFont(l) -bg blue
	pack .geneframe.lab1 -side top -fill x -pady 5

	#label .leftframe.frame1.lab2 -bg blue -foreground white -text [mc "Sc�nario"] -font $sysFont(l)
	#pack .leftframe.frame1.lab2 -side top -fill x -expand 1 -pady 5

	listbox .geneframe.listsce -yscrollcommand ".geneframe.scrollpage set" -width 40 -height 10 -font $sysFont(ms)
	scrollbar .geneframe.scrollpage -command ".geneframe.listsce yview" -width 7
	pack .geneframe.listsce .geneframe.scrollpage -side left -fill y -expand 1 -pady 10
	bind .geneframe.listsce <ButtonRelease-1> "changelistpage %x %y"

	# Champ de texte pour saisir la nouvelle operation
	entry .geneframe2.entobj -background yellow -font $sysFont(ms) -width 40
	pack .geneframe2.entobj -side top -pady 20

	button .geneframe2.ajout -background white -text [mc "Ajouter"] -command "addope" -font $sysFont(msb) -foreground green
	pack .geneframe2.ajout -side left -padx 50

	button .geneframe2.supp -background gray75 -text [mc "Supprimer"] -command "delope" -font $sysFont(msb) -foreground red
	pack .geneframe2.supp -side left -padx 50



	# leftframe.frame3
	button .geneframe3.fin -text [mc "QUITTER"] -command "fin" -image [image create photo -file [file join sysdata mini-porte.gif]] -activebackground white -font $sysFont(msb) -foreground blue
	pack .geneframe3.fin -side right -padx 50 -pady 20

}




proc charge {index} {
	global listdata activelist indexpage totalpage Home indlist zconf

	set f [open [file join $Home reglages $zconf] "r"]
	set listdata [gets $f]
	close $f

	set totalpage [llength $listdata] 
	set indexpage $index
	.leftframe.frame1.listsce delete 0 end
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
		.leftframe.frame1.listsce insert end [lindex [lindex $listdata $k] 0]
	}
	set activelist [lindex [lindex $listdata $indexpage] 2]
	.leftframe.frame1.listsce selection set $indexpage

	.geneframe.listsce delete 0 end
	for {set k 0} {$k < [llength $activelist]} {incr k 1} {
		.geneframe.listsce insert end [lindex $activelist $k]
	}

	.geneframe.listsce selection set end
	set indlist 0

	# Pas de selection de la derniere serie
	.geneframe2.entobj delete 0 end

	# MAJ du champ scenarion avec le index i�me scenario
	.leftframe.frame1.lab2 configure -text [.leftframe.frame1.listsce get $index]
}




proc enregistre_sce {} {
	global indexpage listdata activelist totalpage Home indlist zconf

	# Pour eviter l'effacement des espaces
	set activelist [.leftframe.frame1.listsce get $indexpage]
	set tmp ""
	for {set i 0} { $i < [.geneframe.listsce index end] } {incr i 1} {
		set tmp "$tmp \{[.geneframe.listsce get $i]\}"
	}
	set tmp \173$tmp\175

	# Ajoute dun double-quote de chaque cote du nom et met 0 espace apr�s
	set car "\""
	set activelistnew $car$activelist$car\0400\040
	set activelist $activelistnew$tmp

	set listdata [lreplace $listdata $indexpage $indexpage $activelist]

	set f [open [file join $Home reglages $zconf] "w"]
	puts $f $listdata
	close $f
}


proc changelistsce {x y} {
	global ind indlist
	
	set ind [.leftframe.frame1.listsce index @$x,$y]

	# Memorisation du nom du scenario pour eviter le bug 321
	set scetext [.leftframe.frame1.listsce get @$x,$y]
	set indlist 0
	charge $ind
	.leftframe.frame1.lab2 configure -text $scetext
}

proc changelistpage {x y} {
	global indlist
	set indlist [.geneframe.listsce index @$x,$y]
	.geneframe2.entobj delete 0 end
	.geneframe2.entobj insert end [.geneframe.listsce get $indlist]
}

proc fin {} {
	enregistre_sce
	destroy .
}

proc save {} {
	enregistre_sce
}


proc delitem {} {
	global listdata indexpage totalpage Home zconf ztitre
	if {$totalpage < 2} {
		tk_messageBox -message [mc "Impossible de supprimer la fiche"] -type ok -title [mc $ztitre]
		return
	}
	set message ""

	# Espace dans le scenario
	set nline [lrange [lindex $listdata $indexpage] 0 0]
	set nline_size [string length $nline]
	set scen [string range $nline 1 [expr $nline_size - 2]]

	append message [mc "Voulez-vous vraiment supprimer la fiche ?  $scen"]

	set response [tk_messageBox -message $message -type yesno -title [mc $ztitre]]
	if {$response == "yes"} {
		set totalpage [expr $totalpage - 1]
		set listdata [lreplace $listdata $indexpage $indexpage]
     	if {$indexpage > 0} {
     		set indexpage [expr $indexpage - 1]						
		} 

		set f [open [file join $Home reglages $zconf] "w"]
		puts $f $listdata
		close $f
		charge $indexpage
	} 
}


proc additem {} {
	global listdata indexpage totalpage ztitre
	enregistre_sce

	catch {destroy .nomobj}
	toplevel .nomobj -background grey -width 250 -height 100
	wm geometry .nomobj +50+50

	# Affichage d'un titre sur la boite de message autre que nomobj
	set mess "Cr�ation d'un sc�nario $ztitre"
	wm title .nomobj $mess

	frame .nomobj.frame -background grey -width 250 -height 100
	pack .nomobj.frame -side top
	label .nomobj.frame.labobj -font {Helvetica 10} -text [mc "Nom du sc�nario : "] -background grey
	pack .nomobj.frame.labobj -side top

	# Change la taille de la fenetre de saisie du scenario
	entry .nomobj.frame.entobj -font {Helvetica 10} -width 50
	pack .nomobj.frame.entobj -side top 
	button .nomobj.frame.ok -background gray75 -text [mc "Ok"] -command "verifnomobj"
	pack .nomobj.frame.ok -side top -pady 10
}

proc verifnomobj {} {
	global nom listdata indexpage totalpage Home zconf

	set car "\""
	set ext .conf

	# Permet la saisie avec des espaces
	set nom [.nomobj.frame.entobj get]
	set nom1 $car$nom$car
	if {$nom1 !=""} {
		
		# Nvelle version
		if {$indexpage != $totalpage} {
			set listdata_new ""
			set iscen_new 0
			for {set iscen 0} {$iscen <= $totalpage} {incr iscen} {
				set scen [lindex $listdata $iscen]
				if {$iscen == $indexpage} {
					# Ajoute le scenario + le nouveau scenario
					lappend listdata_new $scen
					lappend listdata_new "$nom1 0"
					set iscen_new [expr $iscen + 1]
				} else {
					lappend listdata_new $scen
				}
			}
			incr totalpage
			
		} else {
			set iscen_new [expr $indexpage + 1]
			set indexpage $totalpage
			incr totalpage
			set listdata_new $listdata
			lappend listdata_new "$nom1 0"
		}

		set f [open [file join $Home reglages $zconf] "w"]
		puts $f $listdata_new
		close $f

		set listdata $listdata_new

		# Change le nom du scenario pour mettre le nouveau scenario
		.leftframe.frame1.lab2 configure -text $nom

		# charge $indexpage
		charge $iscen_new
	}
	catch {destroy .nomobj}
}




proc upitem {} {
	global nom listdata indexpage totalpage Home zconf

	set nline [lrange [lindex $listdata $indexpage] 0 0]
	set nline_size [string length $nline]
	set nom_select [string range $nline 1 [expr $nline_size - 2]]

	# Charge le scenario selectionn�
	charge $indexpage

	set scen_move [lindex $listdata $indexpage]

	set iscen_new $indexpage

	if {$indexpage > 0} {

		set listdata_new ""
		set iscen_new 0
		for {set iscen 0} {$iscen <= $totalpage} {incr iscen} {
			set scen [lindex $listdata $iscen]
			if {$iscen == [expr $indexpage - 1]} {
				# Ajoute le scenario + le nouveau scenario
				lappend listdata_new $scen_move
				set iscen_new $iscen
				lappend listdata_new $scen				
			} else {
				if {$iscen == $indexpage} {
					# Ce scenario a deja ete ajoute
				} else {
					lappend listdata_new $scen
				}
			}
		}
		

		set f [open [file join $Home reglages $zconf] "w"]
		puts $f $listdata_new
		close $f

		set listdata $listdata_new
		set indexpage $iscen_new

	} else {
		# Le scenario est dej� en premi�re position
	}

	# Change le nom du scenario pour mettre le nouveau scenario
	.leftframe.frame1.lab2 configure -text $nom_select

	# enregistre_sce

	# charge $indexpage
	charge $iscen_new
}

proc downitem {} {
	global nom listdata indexpage totalpage Home zconf

	set nline [lrange [lindex $listdata $indexpage] 0 0]
	set nline_size [string length $nline]
	set nom_select [string range $nline 1 [expr $nline_size - 2]]

	# Charge le scenario selectionn�
	charge $indexpage

	set scen_move [lindex $listdata $indexpage]

	set iscen_new $indexpage

	if {$indexpage >= 0 && $indexpage < $totalpage} {

		set listdata_new ""
		set iscen_new 0
		for {set iscen 0} {$iscen <= $totalpage} {incr iscen} {
			set scen [lindex $listdata $iscen]
			if {$iscen == $indexpage} {
				# Ce scenario a deja ete ajoute
			} else {
				if {$iscen == [expr $indexpage + 1]} {
					# Ajoute le scenario + le nouveau scenario
					lappend listdata_new $scen
					lappend listdata_new $scen_move
					set iscen_new $iscen	
				} else {
					lappend listdata_new $scen
				}
			} 
		}
		

		set f [open [file join $Home reglages $zconf] "w"]
		puts $f $listdata_new
		close $f

		set listdata $listdata_new
		set indexpage $iscen_new

	} else {
		# Le scenario est dej� en premi�re position
	}

	# Change le nom du scenario pour mettre le nouveau scenario
	.leftframe.frame1.lab2 configure -text $nom_select

	# enregistre_sce

	# charge $indexpage
	charge $iscen_new
}






proc addope {} {
	global indlist ztype

	# Remet en surbrillance la ligne selectionnee
	.geneframe.listsce selection set $indlist

	set tmp [.geneframe2.entobj get]
	if {$tmp !=""} {

		if {$ztype == "additions"} {
			if {[llength $tmp] <2} {
				set answer [tk_messageBox -message [mc "Au moins 2 nombres pour une addition !"] -type ok]
				return
			}
		}
		if {$ztype == "multiplications"} {
			if {[llength $tmp] !=2} {
      		set answer [tk_messageBox -message [mc "2 nombres pour une multiplication !"] -type ok] 
      		return
    		}
		}
		if {$ztype == "soustractions"} {
			if {[llength $tmp] !=2} {
      		set answer [tk_messageBox -message [mc "2 nombres pour une soustraction !"] -type ok] 
      		return
    		}
		}

		foreach item $tmp {

			if {[string is double [string map {, .} $item]] == 0} {
				set answer [tk_messageBox -message [mc "Veuillez entrer des nombres valides !"] -type ok] 
				return
			}
		}
		.geneframe.listsce insert [expr $indlist +1] $tmp

		# Vide la ligne jaune
		.geneframe2.entobj delete 0 end

		# Sauvegarde le scenarion � chaque ajout de serie
		enregistre_sce
	}
}

proc delope {} {
	global indlist
	.geneframe.listsce delete $indlist 
	set indlist 0
	.geneframe.listsce selection set $indlist

	.geneframe2.entobj delete 0 end
	.geneframe2.entobj insert end [.geneframe.listsce get $indlist]

	# Sauvegarde du scenario
	enregistre_sce 
}
interface
charge 0
