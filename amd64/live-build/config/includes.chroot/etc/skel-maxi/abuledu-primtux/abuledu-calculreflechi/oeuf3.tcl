#!/bin/sh
#oeuf3.tcl
#Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

############################################################################
# Copyright (C) 2006 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : oeuf3.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/12/2006
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version $Id: oeuf3.tcl,v 1.3 2006/12/02 07:02:19 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/12/2006
#
#
#########################################################################
####################################################""
#procÚdure principale
#######################################################"


proc main {c} {
global xpos ypos score erreurs Home listdata indexp niveau arg1 niveau iwish listeval global sysFont user ntux
set xpos 200
set ypos 40
$c delete all
set ntux 1

focus $c
image create photo fond -file [file join images fond3.jpg] 
$c configure -background Blue
$c create image 200 300 -image fond

image create photo tux1 -file [file join images tux_ufo1.gif]
image create photo tux2 -file [file join images tux_ufo2.gif]  
image create photo aster -file [file join images asteroide.gif] 
image create photo blank -file [file join images blank.gif] 
image create photo aster_brise -file [file join images aster_brise.gif] 
image create photo avant -file [file join images avant.gif] 
image create photo apres -file [file join images apres.gif] 
image create photo goon -file [file join images ok0.gif] 
image create photo quitte -file [file join images quitte.gif] 

$c create image 200 460 -tags tux -image tux$ntux

$c create image $xpos $ypos -tags aster
$c create text 200 30 -anchor c -font $sysFont(lb) -justify center  -tags texte -fill white

entry $c.text1 -width 10 -font $sysFont(l)
$c create window 210 570 -window $c.text1


$c.text1 configure -justify center

focus -force $c.text1


set f [open [file join $Home data $arg1] "r"]
set listdata [gets $f]
close $f
$c create text 210 540 -text ". / ." -anchor c -font $sysFont(t) -justify center  -tags numero -fill white
$c create text 70 515 -text [mc {Score : }] -anchor c -font $sysFont(t) -justify center  -tags score -fill white
$c create text 65 535 -text [mc {Erreurs : }] -anchor c -font $sysFont(t) -justify center  -tags erreurs -fill white
$c create text 65 570 -text "[mc {Niveau}]\n[expr $niveau +1] / [llength $listdata]" -anchor c -font $sysFont(s) -justify center  -tags niveau -fill white

button $c.but4 -image goon -width 38 -height 38 -command "go $c"
$c create window 375 520 -window $c.but4
button $c.but2 -image avant -command "changeniveau $c -1"
$c create window 20 570 -window $c.but2
button $c.but3 -image apres -command "changeniveau $c 1"
$c create window 110 570 -window $c.but3

button $c.but1 -image quitte -command "exec $iwish mathoeuf.tcl &;exit"
$c create window 375 575 -window $c.but1
wm title . "[mc {Calcul reflechi}] - [mc {Utilisateur}] : [string map {.log \040} [file tail $user]]"

}

proc go {c} {
global listeval niveau indexp arg1 score erreurs oldnb1 oldnb2 user
set oldnb1 ""
set oldnb2 ""
set listeval \173$arg1\175\040Niveau:[expr $niveau +1]
set indexp 0
set score 0
set erreurs 0
$c delete fini
$c itemconfigure score -text "[mc {Score : }]$score"
$c itemconfigure erreurs -text "[mc {Erreurs : }]$erreurs"
wm title . "[mc {Utilisateur}] : [string map {.log \040} [file tail $user]] - [mc {Type}] : [string map {.conf \040} [file tail $arg1]] [mc {Niveau}] : [expr $niveau +1]"

depart $c
}

proc verif {c} {
global stop expresultat resultat score erreurs total erritem
set tt [$c.text1 get]
if {$tt == $resultat} {
$c.text1 configure -state disabled
bind $c.text1 <Return> ""
bind $c.text1 <KP_Enter> ""
set stop 1
$c itemconfigure aster -image blank
#$c move texte 0 70
$c itemconfigure texte -text $expresultat
incr score
if {$erreurs > 0 && $erritem > 1} {incr erreurs -1}
majbilan
$c itemconfigure score -text "[mc {Score : }]$score"
$c itemconfigure erreurs -text "[mc {Erreurs : }]$erreurs"
update
after 2000
depart $c
return
} else {
bell
$c.text1 configure -bg red
update
after 600
$c.text1 configure -bg white
update
$c.text1 delete 0 end
incr erreurs
incr erritem
$c itemconfigure erreurs -text "[mc {Erreurs : }]$erreurs"
}

}

proc jeu {c} {
global xpos ypos stop erreurs expresultat timing erritem ntux
update
set coord [$c bbox aster]
if {[lindex $coord 3] > 420 } {
set stop 1
incr erreurs
#incr erritem
set erritem 2
majbilan
$c itemconfigure erreurs -text "[mc {Erreurs : }]$erreurs"
$c itemconfigure aster -image aster_brise
#$c move texte 0 70
$c itemconfigure texte -text $expresultat
$c.text1 configure -state disabled
bind $c.text1 <Return> ""
bind $c.text1 <KP_Enter> ""
update
after 2000
depart $c
return
}

if {$stop ==0} {
$c move aster  0 5
#$c move texte 0 -5
incr ntux
if {$ntux == 12} {set ntux 1} 
$c itemconfigure tux -image tux[expr int($ntux/6) +1]
update 
}
after $timing jeu  $c
}



proc depart {c} {
global xpos ypos operation resultat stop total indexp listdata niveau complement expresultat timing erritem erreurs score exo listeval iwish oldnb1 oldnb2 sysFont
variable repert
set stop 0
set erritem 0
set oper ""

after cancel jeu $c
set total [lindex [lindex $listdata $niveau] 3]
if {$indexp == $total} {
set stop 1
#$c delete aster
$c create text 200 200 -text [mc {C'est fini!}] -font $sysFont(l) -tags fini

set scor [expr $score - $erreurs]
if {$scor <0} {set scor 0}
set scor [expr $scor*100/$total]
set pourcent %
lappend listeval \040bilan\040$niveau\040$exo\040$repert\040$scor
enregistreval

bind $c.text1 <Return> ""
bind $c.text1 <KP_Enter> ""
update
return
}
incr indexp

set timing [lindex [lindex $listdata $niveau] 4]
set operande [lindex [lindex $listdata $niveau] 0]
set intervalle1 [ expr [lindex [lindex [lindex $listdata $niveau] 1] 1] - [lindex [lindex [lindex $listdata $niveau] 1] 0]]
set pas1 [lindex [lindex $listdata $niveau] 6]

set intervalle2 [ expr [lindex [lindex [lindex $listdata $niveau] 2] 1] - [lindex [lindex [lindex $listdata $niveau] 2] 0]]
set pas2 [lindex [lindex $listdata $niveau] 7]

for {set i 0} {$i <3} {incr i} {
set nb1 [expr int((int(rand()*$intervalle1) + [lindex [lindex [lindex $listdata $niveau] 1] 0]) / $pas1)*$pas1]
set nb2 [expr int((int(rand()*$intervalle2) + [lindex [lindex [lindex $listdata $niveau] 2] 0]) / $pas2)*$pas2]
if {$nb1 != $oldnb1 || $nb2 != $oldnb2} {break}
}
set oldnb1 $nb1
set oldnb2 $nb2
set complement [lindex [lindex $listdata $niveau] 5]


if {$complement == 0} {
if {$operande == "-"} {
if {$nb1 < $nb2} {
set tmp $nb1
set nb1 $nb2
set nb2 $tmp
}
}
set operation "$nb1 $operande $nb2"
set resultat [expr $operation]
set expresultat "$nb1 $operande $nb2 = $resultat"
} else {
if {$nb1 > $nb2} {
set tmp $nb1
set nb1 $nb2
set nb2 $tmp
}
if {$operande == "+"} {set oper "-"}
if {$operande == "-"} {set oper "+"}
if {$operande == "*"} {set oper "/"}

set resultat [expr int($nb2 $oper $nb1)]
set nb2 [expr $nb1 $operande $resultat]
set operation "$nb1 $operande ? = $nb2"

set expresultat "$nb1 $operande $resultat = $nb2"
}


$c.text1 configure -state normal
bind $c.text1 <Return> "verif $c"
bind $c.text1 <KP_Enter> "verif $c"

$c.text1 delete 0 end

$c itemconfigure aster -image aster 
$c itemconfigure texte -text $operation
$c coords aster $xpos $ypos
#$c coords texte 200 30 
$c itemconfigure numero -text "$indexp / $total"

jeu $c
}

proc majbilan {} {
global listeval erritem expresultat
            switch $erritem {
                0 {
                lappend listeval 1\040\173$expresultat\175
                }
                1 {
                lappend listeval 2\040\173$expresultat\175
                }
                default {
                lappend listeval 3\040\173$expresultat\175
                }
              }

}


###############################################################
#appel de la procÚdure principale
##############################################################"
main $c



