#!/bin/sh
#choix.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier: 
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean- Louis Sendral
#  @modifier  
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *****************************************

#############Pr�paration du calcul : nom, classe, choix du sc�nario ou de la s�quence.

###################"gestion du son
#"global sound
#"if {[catch {package require -exact snack 2.1}]} {
#"    set sound 0
#"} else {
#"    set sound 1
#"}

set basedir [pwd]
#[file dir $argv0]
cd $basedir
source calculs.conf
source path.tcl
  set basedir [pwd]
set  DOSSIER_EXOS1 [lindex $argv 0 ]
#source i18n
bind . <F1> "showaide capproche"
global sysFont
source msg.tcl
source fonts.tcl
set bgcolor orange
global scenario
. configure  -height 480 -width 640
wm geometry . +0+0 ; wm resizable . 1 1 ; wm title . "[mc {choix_sce}] pour calculs approch�s."
frame .frame -background $bgcolor -height 480 -width 340
grid .frame -column 0 -row 0

label .frame.lab_prenom -text [mc {nom}]  -background  $bgcolor
place .frame.lab_prenom -x 130 -y 5
entry .frame.ent_nom  -justify center
place .frame.ent_nom -x 80 -y 25
focus .frame.ent_nom
#set nom_elev $tcl_platform(user)
#[exec  id -u -n ]
#.frame.ent_nom insert end $nom_elev
#set nom_elev [.frame.ent_prenom get]

label .frame.lab_classe -text [mc {ta_classe}]   -background $bgcolor
place .frame.lab_classe -x 115 -y 50
entry .frame.ent_classe -justify center
place .frame.ent_classe -x 80  -y 70
#set nom_classe [.frame.ent_classe get]
if { $tcl_platform(platform) == "unix" } {
set nom_elev $tcl_platform(user)
set nom_classe [lindex [exec  id -G -n ] 1]
if { $nom_classe == {} } {
set nom_classe classe
}
} else {
 set nom_elev eleve
 set nom_classe classe
}
.frame.ent_nom insert end $nom_elev
.frame.ent_classe insert end  $nom_classe
label .frame.duree_saut -text [mc {duree_max} ] -background $bgcolor
place .frame.duree_saut -x 55 -y 100
entry .frame.ent_duree
place .frame.ent_duree -x 245  -y 100 -width 35
.frame.ent_duree insert end 2000
set duree_saut [.frame.ent_duree get]

label .frame.ent_text_approx -text [mc {approx}] -background  $bgcolor
place .frame.ent_text_approx -x 70 -y 120
entry .frame.ent_approx
place .frame.ent_approx -x 245 -y 120 -width 35
if { $tcl_platform(platform) == "unix" } {
listbox .frame.list1 -background #c0c0c0 -height 15 -width 40 -yscrollcommand ".frame.scroll1 set"
scrollbar .frame.scroll1 -command ".frame.list1 yview"
place .frame.list1 -x 10 -y 195
place .frame.scroll1 -x 280 -y 195 -height 245 } else {
listbox .frame.list1 -background #c0c0c0 -height 15 -width 40 -yscrollcommand ".frame.scroll1 set"
scrollbar .frame.scroll1 -command ".frame.list1 yview"
place .frame.list1 -x 10 -y 195
place .frame.scroll1 -x 257 -y 195 -height 245

}



label .frame.menu3 -text [mc {double_clic_sce_seq}] -background $bgcolor
place .frame.menu3 -x 10 -y 160
label .frame.scenario -text [mc {sce_seq}] -background  $bgcolor
place .frame.scenario -x 80 -y 175
#label .frame.menu1 -text "Quel est ton nom :" -background #ffff80
#place .frame.menu1 -x 80 -y 340
#"button .frame.ajouter -text Ajouter
#"place .frame.ajouter -x 180 -y 420
#entry .frame.text1
#place .frame.text1 -x 80 -y 370
#set nom_elev [.frame.text1 get]
bind .frame.list1 <Double-ButtonRelease-1> { calc }
#pour lister les sc�narios et les s�quences d�j� construits.

proc peuplelist1 {} {
global basedir DOSSIER_EXOS DOSSIER_EXOS1 choix_lieu    Home
cd $DOSSIER_EXOS1
    set ext .sce ; set ext1 .seq
    .frame.list1 delete 0 end
    catch {foreach i [lsort [glob [file join   *$ext]]] {
            .frame.list1 insert end  $i ; ##[file rootname $i]
        }
    }
catch {foreach i [lsort [glob [file join   *$ext1]]] {
        .frame.list1 insert end $i  ; ##[file rootname $i]
    }
}

}
peuplelist1
#pour lancer le ou les claculs##############

proc calculs {} {
return [.frame.list1 get active]
}

proc  calc {} {
global basedir DOSSIER_EXOS choix_lieu DOSSIER_EXOS1  iwish  Home
set fich [calculs]
  set f [open [file join $DOSSIER_EXOS1 $fich] "r"  ]
    set liste_sce [gets $f]
 if  {[file extension $fich] ==".sce" } {
    set liste_sce [list $fich]
} else  {
cd $DOSSIER_EXOS1
    set f [open [file join $fich] "r"  ]
    set liste_sce [gets $f]
}
set len_liste [llength $liste_sce] ; set compt 1
foreach exo  $liste_sce {
    if {[file extension [lindex $liste_sce 0]] != ".sce" } {
        set exo "${exo}.sce"
    }
##cd  $DOSSIER_EXOS1
if {  $compt !=  $len_liste  }  {
incr compt
#"/usr/share/abuledu/applications/abuledu-calculenpluie"
exec  $iwish  [file join $basedir pluie_app1.tcl] [file join $DOSSIER_EXOS1 $exo ] [.frame.ent_nom  get] [.frame.ent_classe get] [.frame.ent_duree get] [.frame.ent_approx get] $DOSSIER_EXOS1 
set rep [tk_messageBox  -message "[mc {exo_suivant}]" -type yesno -icon question  -default no -title "Exercice suivant?" ]
 if {  $rep  == "no"  }  { break }
}  else   {
exec  $iwish  [file join $basedir pluie_app1.tcl] [file join $DOSSIER_EXOS1 $exo ]  [.frame.ent_nom  get] [.frame.ent_classe get] [.frame.ent_duree get]  [.frame.ent_approx get] $DOSSIER_EXOS1 &  ; destroy .
		}
}  
}

;#"return [.frame.list1 get active]







