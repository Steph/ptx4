# tab_config sc�narios a nous la division
set tab_config(sce1,flag_calculatrice) 1 
set tab_config(sce1,flag_repertoire) 1 
set tab_config(sce1,flag_recup_valeur) 0
set tab_config(sce1,flag_calcul_quot) 1
set tab_config(sce1,flag_calcul_diff) 1 
set tab_config(sce1,flag_nombres_fixes) 1 ; # et donc sa n�gation
set tab_config(sce1,diviseur_fixe) 25 
set tab_config(sce1,dividende_fixe) 4589
set tab_config(sce1,n_chiffres_dividende) {2 5} 
set tab_config(sce1,n_chiffres_quotient) {2 3}
set tab_config(sce1,sens_ecriture) "\<\<" ;# sinon >>
set tab_config(sce1,flag_n_quot_noeud) 0
