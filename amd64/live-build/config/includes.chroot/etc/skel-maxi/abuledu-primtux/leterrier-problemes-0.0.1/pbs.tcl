############################################################################
# Copyright (C) 2007 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editeur.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: editparcours.tcl,v 1.1.1.1 2004/04/16 11:45:45 erics Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

set listpb ""
foreach item {den adsous ligneg multip} {
set ind 0
lappend listpb "$item\n"
foreach i [glob [file join  problemes  $item*.txt]] {
set f [open [file join  problemes [file tail $i]] "r"]
set tmp [gets $f]
eval $tmp
set tmp [gets $f]
eval $tmp
	while {$tmp != "::1"} {
	set tmp [gets $f]
	}

	while {$tmp != "::"} {
	set tmp [gets $f]
	if {$tmp != "::"} {catch {eval $tmp}}
	}
close $f
set tmp [file tail $i]
regsub -all {.txt} $tmp "" tmp
set indp [string range $tmp end end]
set indtit [string first "." $enonce]
set titre [string range $enonce 0 $indtit]
lappend listpb "$ind - $enonce\n"
incr ind
}

tk_messageBox -message $listpb
set f [open [file join  problemes sommaire.txt] "a"]
foreach item $listpb {
puts $f $item
}
}
close $f