set etapes 1
set niveaux {0 1 2 4}
::1
set niveau 2
set ope {{4 3}}
set interope {{3 15 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1])*3 + [lindex [lindex $ope 0] 0]]
set volatil 0
set operations [list [list [expr $ope1 ]+[expr $ope1]+[expr $ope1 ]] [list [expr $ope1]*3] [list 3*[expr $ope1]] [list [expr $ope1*3 ]/3]]
set editenon "([expr $ope1*3] �toiles sont repr�sent�es, $ope1 pour chacun.)"
set enonce "On partage.\nTim, Tom et Vincent veulent se partager ces �toiles.\nIls en veulent exactement autant l'un que l'autre \net il doit rester le moins possible d'�toiles."
set cible {{4 4 {} source0} {4 4 {} source0} {4 4 {} source0}}
set intervalcible 20
set taillerect 25
set orgy 50
set orgxorig 50
set orgsourcey 50
set orgsourcexorig 550
#set source {{etoile.gif 14 4 6}}
set source [list [list etoile.gif [expr $ope1*3 + int(rand()*3)] 10 7]]
set orient 1
set labelcible {Pim Pam Poum}
set quadri 0
#set reponse {{{1} {{Il y a} 8 {�toiles pour chacun.}}}}
set reponse [list [list {1} [list {Il y a} [expr $ope1] {�toiles pour Tim.}]] [list {1} [list {Il y a} [expr $ope1] {�toiles pour Tom.}]] [list {1} [list {Il y a} [expr $ope1] {�toiles pour Vincent.}]]]
#set ensembles {8 8}
set ensembles [list [expr $ope1] [expr $ope1] [expr $ope1]]
set canvash 300
set c1height 160
set opnonautorise {0}
::