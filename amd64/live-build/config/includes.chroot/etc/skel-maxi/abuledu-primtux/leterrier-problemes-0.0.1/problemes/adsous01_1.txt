set etapes 1
set niveaux {0 1 2 3 4 5}
::1
set niveau 1
set ope {{5 5} {5 5}}
set interope {{1 20 1} {1 20 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set volatil 0
#set operations {{11+15} {15+11}}
set operations [list [list [expr $ope1 ]+[expr $ope2]] [list [expr $ope2]+[expr $ope1]]]
set enonce "La classe de Tom.\nDans la classe de Tom, il y a $ope1 filles et $ope2 gar�ons.\nCombien y a-t-il d'�l�ves dans la classe de Tom?"
set cible {{5 4 {} source0} {5 4 {} source1}}
set intervalcible 60
set taillerect 45
set orgy 40
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 600
set source {fille.gif garcon.gif}
set orient 0
set labelcible {Filles Gar�ons}
set quadri 0
set reponse [list [list {1} [list {Il y a} [expr $ope1 + $ope2] {enfants dans la classe de Tom.}]]]
#set ensembles {11 15}
set ensembles [list [expr $ope1] [expr $ope2]]
set canvash 300
set c1height 160
set opnonautorise {}
::
