#!/bin/sh
#path.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : path.tcl
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    $Id: path.tcl,v 1.2 2004/11/14 15:38:27 david Exp $
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************
proc setwindowsusername {} {
global user gettext
catch {destroy .utilisateur}
toplevel .utilisateur -background grey -width 250 -height 100
wm geometry .utilisateur +50+50
frame .utilisateur.frame -background grey -width 250 -height 100
pack .utilisateur.frame -side top
label .utilisateur.frame.labobj -font {Helvetica 10} -text [mc "Quel est ton nom ?"] -background grey
pack .utilisateur.frame.labobj -side top 
entry .utilisateur.frame.entobj -font {Helvetica 10} -width 10
pack .utilisateur.frame.entobj -side top 
button .utilisateur.frame.ok -background gray75 -text "Ok" -command "verifnom"
pack .utilisateur.frame.ok -side top -pady 10
}

proc verifnom {} {
global env
set nom [string tolower [string trim [string map {\040 ""} [.utilisateur.frame.entobj get]]]]
if {$nom !=""} {
set env(USER) $nom
}
catch {destroy .utilisateur}
}

proc init {plateforme} {
global Home basedir baseHome


if {![file exists [file join $Home]]} {
	file mkdir [file join $Home]
	file mkdir [file join $Home reglages]
	foreach item {additions.conf soustractions.conf divisions.conf multiplications.conf repert.conf regsoust.conf} {
	file copy -force [file join $basedir reglages $item] [file join $Home reglages]
	}

}


switch $plateforme {
    unix {
	if {![file exists [file join  $baseHome log]]} {
	file mkdir [file join  $baseHome log]
	}

    }
    windows {
	if {![file exists [file join operations log]]} {
	file mkdir [file join operations log]
	}
	
    	}
}

}


proc initlog {plateforme ident} {
global LogHome user Home baseHome
switch $plateforme {
    unix {
	set LogHome [file join $baseHome log]

    }
    windows {
	set LogHome [file join operations log]
    }
}

if {$ident != ""} {
     set user [file join $LogHome $ident.log]
     } else {
     set user [file join $LogHome operations.log]
     }
}

proc inithome {} {
global baseHome basedir Home
variable repert
set f [open [file join $baseHome reglages repert.conf] "r"]
set repert [gets $f]
close $f

switch $repert {
0 {set Home $baseHome}
1 {set Home $basedir }
}
}


proc changehome {} {
global Home basedir baseHome
variable repert
set f [open [file join $baseHome reglages repert.conf] "w"]
puts $f $repert
close $f

switch $repert {
0 {set Home $baseHome}
1 {set Home $basedir }
}
}

global basedir Home baseHome iwish progaide envir abuledu prof
set abuledu 0
set prof 0
set basedir [file dir $argv0]
cd $basedir
if {$env(HOME) == "c:\\"} {
set Home [file join $basedir]

} else {
set Home [file join $env(HOME) leterrier operations]
}
set baseHome $Home



switch $tcl_platform(platform) {
    unix {
	if {[lsearch [exec id -nG $env(USER)] "leterrier"] != -1} {set prof 1}
	#if {[file isdirectory [file join /etc abuledu]]} { set abuledu 1}
	set abuledu 1
	set progaide runbrowser
	
	#set progaide dillo
	set iwish wish
	}
    windows {
	set progaide shellexec.exe
	set iwish wish
	}
	}


