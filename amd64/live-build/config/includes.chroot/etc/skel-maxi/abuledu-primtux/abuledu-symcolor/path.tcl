global iwish progaide basedir Home Home_data baseHome abuledu prof
set abuledu 0
set prof 0

set basedir [file dir $argv0]
cd $basedir
if {$env(HOME) == "c:\\"} {
set Home $basedir
} else {
set Home [file join $env(HOME) leterrier symcolor]
}

set baseHome $Home
#set Home_data [file join $env(HOME) leterrier symcolor data]

set Home_data [file join $Home data]

switch $tcl_platform(platform) {
    unix {
	set progaide runbrowser
	#set progaide dillo
	set iwish wish
	}
    windows {
	set progaide shellexec.exe
	set iwish wish
	}
	}


proc initapp {plateforme} {
global basedir Home baseHome
lappend auto_path $basedir
cd $basedir


if {![file exists [file join $Home]]} {
	file mkdir [file join $Home]
	file copy -force [file join reglages] [file join $Home] 
	file copy -force [file join data] [file join $Home]

}

switch $plateforme {
    unix {
	if {![file exists [file join  $baseHome log]]} {
	file mkdir [file join  $baseHome log]
	}

    }
    windows {
	if {![file exists [file join symcolor log]]} {
	file mkdir [file join symcolor log]
	}
	
    	}
}

}

proc setwindowsusername {} {
global user
catch {destroy .utilisateur}
toplevel .utilisateur -background grey -width 250 -height 100
wm geometry .utilisateur +50+50
frame .utilisateur.frame -background grey -width 250 -height 100
pack .utilisateur.frame -side top
label .utilisateur.frame.labobj -text [mc {Quel est ton nom?}] -background grey
pack .utilisateur.frame.labobj -side top 
entry .utilisateur.frame.entobj -width 10
pack .utilisateur.frame.entobj -side top 
button .utilisateur.frame.ok -background gray75 -text [mc {OK}] -command "verifnom"
pack .utilisateur.frame.ok -side top -pady 10
}

proc verifnom {} {
global env
set nom [string tolower [string trim [string map {\040 ""} [.utilisateur.frame.entobj get]]]]
if {$nom !=""} {
set env(USER) $nom
}
catch {destroy .utilisateur}
}

proc initlog {plateforme ident} {
global LogHome user Home baseHome
switch $plateforme {
    unix {
	set LogHome [file join $baseHome log]

    }
    windows {
	set LogHome [file join symcolor log]
    }
}

if {$ident != ""} {
     set user [file join $LogHome $ident.log]
     } else {
     set user [file join $LogHome symcolor.log]
     }
}

proc inithome {} {
global baseHome basedir Home Home_data
variable repertconf
set f [open [file join $baseHome reglages repert.conf] "r"]
set repertconf [gets $f]
close $f

switch $repertconf {
0 {set Home $baseHome}
1 {set Home $basedir }
}
set Home_data [file join $Home data]
}

proc changehome {} {
    global Home basedir baseHome Home_data
    variable repertconf
 

    set f [open [file join $baseHome reglages repert.conf] "w"]
    #on synchronise les 2 variables en attendant
    puts $f $repertconf
    close $f
    switch $repertconf {
	0 {set Home $baseHome}
	1 {set Home $basedir}
    }
set Home_data [file join $Home data]
}

