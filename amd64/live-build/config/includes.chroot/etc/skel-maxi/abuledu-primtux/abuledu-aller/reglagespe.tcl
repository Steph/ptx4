############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : $$$
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#reglages.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

#source msg.tcl
global sysFont Home Homeconf exo strtext listexo repert oldexo
catch {destroy .w1}
toplevel .w1
set basedir [file dir $argv0]
cd $basedir

set exo none
set oldexo $exo
wm title .w1 [mc {Reglages specifiques pour l'exercice}]
.w1 configure -background Steelblue2
set font $sysFont(l)

frame .w1.frametop1 -width 132 -background grey95
grid .w1.frametop1 -row 0 -column 0
label .w1.frametop1.labeltop1 -text [mc {Choix de l'exercice}] -background grey95 -width 25
grid .w1.frametop1.labeltop1 -column 0 -row 0 -sticky news
frame .w1.frametop2 -width 360
grid .w1.frametop2 -row 0 -column 1
label .w1.frametop2.labeltop2 -text [mc {Parametrages}] -background Steelblue2
grid .w1.frametop2.labeltop2 -column 0 -row 0

frame .w1.frameleft -height 320 -width 180 
grid .w1.frameleft -column 0 -row 1

set i 0
    foreach menu {Closure Reconstitution {Phrases melangees} {Mots melanges} {Texte a corriger} {Phrases sans espaces} {Phrases incompletes} {Exercice 1} {Exercice 2} {Flash} {Rapido} {Ponctuation1} {Ponctuation2} {Dict�e}} {
    label .w1.frameleft.l$i -text [lindex [lindex $listexo $i] 1]  -width 29
    place .w1.frameleft.l$i -x 0 -y [expr 20*$i]
    bind .w1.frameleft.l$i <Any-Enter> "pushEnter $i"
    bind .w1.frameleft.l$i <Any-Leave> "pushLeave $i"
    bind .w1.frameleft.l$i <ButtonRelease-1> "select $i"
    incr i
    }
frame .w1.frame1 -background Steelblue2 -height 320 -width 360
grid .w1.frame1 -column 1 -row 1

###########################################################
proc select {i} {
global sysFont exo
set list {closure reconstitution phrase mot faute espace incomplet bisque1 bisque2 bisque3 bisque4 ponctuation1 ponctuation2 dictee}
    for {set k 0} {$k < 14} {incr k 1} {
    .w1.frameleft.l$k configure -background grey95
    }
.w1.frameleft.l$i configure -background Steelblue2
set exo [lindex $list $i]
interfac [lindex $list $i]
}
##############################################################

proc pushEnter {i} {
.w1.frameleft.l$i configure -cursor hand2
}

##########################################################
proc pushLeave {i} {
.w1.frameleft.l$i configure -cursor left_ptr
}

##########################################################"""

proc chargedonnees {exo} {
global sysFont listexo Home Homeconf tabaide tablongchamp tablistevariable tabstartdirect tablecture_mot tablecture_mot_cache
variable sel7
variable sel9
	if {$exo != "ponctuation1" && $exo != "ponctuation2"} {
		foreach i {1 2 3 4 5 6 8} {
		variable sel$i
		}
      set sel1 $tabaide($exo)
	set sel4 $tablongchamp($exo)
	set sel5 $tablistevariable($exo)
	set sel6 $tabstartdirect($exo)
	set sel8 $tablecture_mot($exo)
	}
set ind [lsearch {closure reconstitution phrase mot faute espace incomplet bisque1 bisque2 bisque3 bisque4 ponctuation1 ponctuation2 dictee} $exo]
	if {$ind != -1} {
	set sel7 [lindex [lindex $listexo $ind] 4]
	} 
	set sel9 $tablecture_mot_cache($exo)

}
########################################################""




proc interfac {exo} {
global sysFont listexo oldexo tabaide tablongchamp tablistevariable tabstartdirect tablecture_mot tablecture_mot_cache
foreach i {1 2 3 4 5 6 7 8 9} {
variable sel$i
}
variable substexte
#set substexte 1

if {$oldexo != "none" && $oldexo != "ponctuation1" && $oldexo != "ponctuation2"} {
set tabaide($oldexo) $sel1
set tablongchamp($oldexo) $sel4
set tablistevariable($oldexo) $sel5
set tabstartdirect($oldexo) $sel6
set tablecture_mot($oldexo) $sel8
}

if {$oldexo != "none"} {
set tablecture_mot_cache($oldexo) $sel9
set ind [lsearch {closure reconstitution phrase mot faute espace incomplet bisque1 bisque2 bisque3 bisque4 ponctuation1 ponctuation2 dictee} $oldexo]
if {$ind != -1} {

set listexo [lreplace $listexo $ind $ind \173[lindex [lindex $listexo $ind] 0]\175\040\173[lindex [lindex $listexo $ind] 1]\175\040\173[lindex [lindex $listexo $ind] 2]\175\040[lindex [lindex $listexo $ind] 3]\040$sel7\040$[lindex [lindex $listexo $ind] 5]\040[lindex [lindex $listexo $ind] 6]]
}
} 


set oldexo $exo
catch {destroy .w1.frame1}
frame .w1.frame1 -background Steelblue2 -height 280 -width 360
grid .w1.frame1 -column 1 -row 1

switch $exo {
    closure {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel5 -text [mc {La liste d'aide est variable}] -variable sel5 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel5 -x 40 -y 180
            .w1.frame1.sel5 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel4 -text [mc {Les champs de texte sont de longueur fixe}] -variable sel4 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel4 -x 40 -y 160
            .w1.frame1.sel4 select
            checkbutton .w1.frame1.sel8 -text [mc {Lecture du texte}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel8 -x 40 -y 200
            checkbutton .w1.frame1.sel9 -text [mc {Lecture des mots � trouver}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel9 -x 40 -y 220
            }
    reconstitution {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel5 -text [mc {La liste d'aide est variable}] -variable sel5 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel5 -x 40 -y 180
            .w1.frame1.sel5 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel4 -text [mc {Les champs de texte sont de longueur fixe}] -variable sel4 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel4 -x 40 -y 160
            .w1.frame1.sel4 select
            checkbutton .w1.frame1.sel8 -text [mc {Lecture du texte}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel8 -x 40 -y 200
            checkbutton .w1.frame1.sel9 -text [mc {Lecture des mots � trouver}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel9 -x 40 -y 220
            }
    mot {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel4 -text [mc {Les champs de texte sont de longueur fixe}] -variable sel4 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel4 -x 40 -y 160
            .w1.frame1.sel4 select
            checkbutton .w1.frame1.sel8 -text [mc {Lecture des mots}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel8 -x 40 -y 180
            checkbutton .w1.frame1.sel9 -text [mc {Lecture de la phrase}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel9 -x 40 -y 200


            }

    phrase {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel8 -text [mc {Lecture des phrases}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel8 -x 40 -y 120
            checkbutton .w1.frame1.sel9 -text [mc {Lecture du texte}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel9 -x 40 -y 200

            }
    faute {
            label .w1.frame1.menu1 -text [mc {Reponse :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel6 -text [mc {Les marques de textes sont visibles.}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
		checkbutton .w1.frame1.checksubst -text [mc {Substituer au texte}] -variable substexte -relief flat -activebackground grey95 -background Steelblue2
		place .w1.frame1.checksubst -x 40 -y 140
            checkbutton .w1.frame1.sel8 -text [mc {Lecture des mots}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel8 -x 40 -y 200
            checkbutton .w1.frame1.sel9 -text [mc {Lecture du texte}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel9 -x 40 -y 220

            }
    espace {
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel9 -text [mc {Lecture des phrases}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel9 -x 40 -y 40

            }

    incomplet {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel9 -text [mc {Lecture des phrases}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel9 -x 40 -y 120
            }
    bisque1  {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel5 -text [mc {La liste d'aide est variable}] -variable sel5 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel5 -x 40 -y 180
            .w1.frame1.sel5 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel4 -text [mc {Les champs de texte sont de longueur fixe}] -variable sel4 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel4 -x 40 -y 160
            .w1.frame1.sel4 select
            checkbutton .w1.frame1.sel9 -text [mc {Lecture du texte}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel9 -x 40 -y 200
            }

    bisque2  {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel5 -text [mc {La liste d'aide est variable}] -variable sel5 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel5 -x 40 -y 180
            .w1.frame1.sel5 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel4 -text [mc {Les champs de texte sont de longueur fixe}] -variable sel4 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel4 -x 40 -y 160
            .w1.frame1.sel4 select
            checkbutton .w1.frame1.sel9 -text [mc {Lecture du texte}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel9 -x 40 -y 200
            }
      bisque3 {
            label .w1.frame1.menu1 -text [mc {Vitesse :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Rapide}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Moyenne}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Lente}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            #checkbutton .w1.frame1.sel5 -text [mc {La liste d'aide est variable}] -variable sel5 -relief flat -background Steelblue2 -activebackground grey95
            #place .w1.frame1.sel5 -x 40 -y 180
            #.w1.frame1.sel5 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            #checkbutton .w1.frame1.sel4 -text [mc {Les champs de texte sont de longueur fixe}] -variable sel4 -relief flat -background Steelblue2 -activebackground grey95
           	#place .w1.frame1.sel4 -x 40 -y 160
            #.w1.frame1.sel4 select
            #checkbutton .w1.frame1.sel9 -text [mc {Lecture du texte}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            #place .w1.frame1.sel9 -x 40 -y 200
            }
       bisque4 {
            label .w1.frame1.menu1 -text [mc {Temps :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {10 s}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {20 s}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {30 s}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            #checkbutton .w1.frame1.sel5 -text [mc {La liste d'aide est variable}] -variable sel5 -relief flat -background Steelblue2 -activebackground grey95
            #place .w1.frame1.sel5 -x 40 -y 180
            #.w1.frame1.sel5 select
            #checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey95
            #place .w1.frame1.sel6 -x 40 -y 20
            #.w1.frame1.sel6 select
            #checkbutton .w1.frame1.sel4 -text [mc {Les champs de texte sont de longueur fixe}] -variable sel4 -relief flat -background Steelblue2 -activebackground grey95
            #place .w1.frame1.sel4 -x 40 -y 160
            #.w1.frame1.sel4 select
            #checkbutton .w1.frame1.sel9 -text [mc {Lecture du texte}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            #place .w1.frame1.sel9 -x 40 -y 200
            }

    
    ponctuation1 {
            checkbutton .w1.frame1.sel9 -text [mc {Lecture du texte}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel9 -x 40 -y 40

            }
    ponctuation2 {
            checkbutton .w1.frame1.sel9 -text [mc {Lecture du texte}] -variable sel9 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel9 -x 40 -y 40
            }

   
	dictee {
            label .w1.frame1.menu1 -text [mc {Correction apr�s :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Le premier essai}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {le deuxi�me essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Le troisi�me essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select


	}
}
            checkbutton .w1.frame1.sel7 -text [mc {Exercice active}] -variable sel7 -relief flat -background Steelblue2 -activebackground grey95
            place .w1.frame1.sel7 -x 40 -y 240


chargedonnees $exo
}
##################################################################################"


    button .w1.frameleft.ok -text [mc {Ok}] -command "enregistrefin"
    place .w1.frameleft.ok -x 70 -y 290

proc enregistrefin {} {
global tabaide tablongchamp tablistevariable tabstartdirect exo listexo tablecture_mot tablecture_mot_cache
variable sel7
variable sel8
variable sel9

	if {$exo != "none" && $exo != "ponctuation1" && $exo != "ponctuation2"} {
		foreach i {1 2 3 4 5 6} {
		variable sel$i
		}
	set tabaide($exo) $sel1
	set tablongchamp($exo) $sel4
	set tablistevariable($exo) $sel5
	set tabstartdirect($exo) $sel6
	set tablecture_mot($exo) $sel8
	}
	if {$exo != "none"} {
	set tablecture_mot_cache($exo) $sel9
	}

set ind [lsearch {closure reconstitution phrase mot faute espace incomplet bisque1 bisque2 bisque3 bisque4 ponctuation1 ponctuation2 dictee} $exo]
if {$ind != -1} {
set listexo [lreplace $listexo $ind $ind \173[lindex [lindex $listexo $ind] 0]\175\040\173[lindex [lindex $listexo $ind] 1]\175\040\173[lindex [lindex $listexo $ind] 2]\175\040[lindex [lindex $listexo $ind] 3]\040$sel7\040$[lindex [lindex $listexo $ind] 5]\040[lindex [lindex $listexo $ind] 6]]
} 

destroy .w1
}



