#!/bin/sh
#reconnaitre.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************
global sysFont nbreu essais auto couleur aide longchamp xcol ycol listevariable categorie startdirect user Homeconf repertoire tabaide tablistevariable tablongchamp tabstartdirect initrep Home baseHome lecture_mot lecture_mot_cache tablecture_mot tablecture_mot_cache textelu

source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl

proc cancelkey {A} {
set A ""
#focus .
}

#variables
#nbreu : nombre d'items effectues
#essais : total des essais effectues
#auto : flag de detection du mode de fonctionnement 
#couleur : couleur associ�e � l'exercice
#aide : pr�cise � quel moment doit intervenir l'aide
#longchamp : pr�cise si les champs sont de longueur variable

set couleur [lindex $argv 0]
set nbreu 0
set essais 0
set auto 0
set aide 2
set longchamp 1
set xcol 0
set ycol 0
set listevariable 1
set categorie ""
set startdirect 1
set filuser [lindex $argv 1]
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
initlog $plateforme $ident
inithome

#interface
. configure -background black -width 640 -height 480
wm geometry . +52+0


frame .menu -height 40
pack .menu -side bottom -fill both

button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main .text"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right


tux_commence

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both
#recup�ration des options de reglages
catch {
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
close $f
}

set initrep [file join $Home textes $repertoire]

bind .text <ButtonRelease-1> "lire"
bind .text <Any-Enter> ".text configure -cursor target"
bind .text <Any-Leave> ".text configure -cursor left_ptr"
bind . <KeyPress> "cancelkey %A"


#chargement du texte avec d�tection du mode
#set auto [charge .text $categorie]
set t .text
$t delete 1.0 end
source [file join $Home textes $repertoire $categorie]
set textelu [.text  get 1.0 "end - 1 chars"]

focus .text
set aide $tabaide($couleur)
set longchamp $tablongchamp($couleur)
set listevariable $tablistevariable($couleur)
set startdirect $tabstartdirect($couleur)
set lecture_mot $tablecture_mot($couleur)
set lecture_mot_cache $tablecture_mot_cache($couleur)

#.text configure -state disabled
wm title . "[mc {Exercice}] $categorie - [lindex [lindex $listexo [expr [string index $couleur end] +6]] 1]"


label .menu.consigne -text "[lindex [lindex $listexo [expr [string index $couleur end] + 6] ] 1] - [mc {Observe}]" -justify center
pack .menu.consigne -side left -fill both -expand 1


proc main {t} {
#liste principale de phrases contenant les mots sans ponctuation, liste de mots � cacher
#listessai : tableau pour tenir � jour les essais sur chaque mot
#texte : le texte initial
#longmot : longueur maximale des champs de texte

global sysFont plist listemotscaches listessai texte nbmotscaches auto longchamp longmot listexo couleur user startdirect
set nbmotscaches 0
.menu.consigne configure -text [lindex [lindex $listexo [expr [string index $couleur end] + 6] ] 2]

button .menu.b2 -image [image create photo corriger -file [file join sysdata corriger.gif]] -command "abandon $t"
pack .menu.b2 -side left


#catch {destroy .menu.b1}
#button .menu.b1 -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"
#pack .menu.b1 -side right
.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"

set what [mc {Clique sur les mots convenables ou clique sur la bouee pour abandonner et voir la correction.}]
if {$startdirect == 0} {set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"}
tux_exo $what

#bind .menu  <Destroy> "fin"
$t configure -state normal

    pauto $t
    
    $t configure -state disabled -selectbackground white -selectforeground black
bind .text <ButtonRelease-1> ""
bind .text <Any-Enter> ""
bind .text <Any-Leave> ""
}

proc pauto {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches couleur aide longchamp longmot listeaide iwish filuser lecture_mot lecture_mot_cache
set nbmotscaches 0
set listemots {}
set listemotscaches {}

# Construction de la liste des mots � cacher, � partir des tags
set liste [$t tag ranges $couleur]
    for {set i 0} {$i < [llength $liste]} {incr i 2} {
    incr nbmotscaches
    set str [$t get [lindex $liste $i] [lindex $liste [expr $i + 1]]]
    regsub -all \"+ $str "" str
    lappend listemotscaches $str        
    }

 set longmot 0
        foreach mot $listemotscaches {
            if {$longmot < [string length $mot]} {
            set longmot  [string length $mot]
            }
        }

    if {$nbmotscaches == 0} {
    set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
    exec $iwish aller.tcl $filuser &
    exit
    }


#On marque le texte pour substituer les mots � cacher par des champs de texte
    for {set i 0} {$i < [llength $liste]} {incr i 1} {
    $t mark set cur$i [lindex $liste $i]
    }
# On op�re la substitution
    for {set i 0} {$i < [llength $listemotscaches]} {incr i 1} {           
    $t tag add ent$i cur[expr $i*2] cur[expr ($i*2) +1]
    
    set listessai($i) 0
    }

set listeaide $listemotscaches

$t tag add text 1.0 end
$t tag bind text <ButtonRelease-1> "hit $t"
$t tag configure rouge -background red
$t tag configure jaune -background yellow

if {$lecture_mot_cache == "0" } {
catch {destroy .menu.bb1} 
}

}


proc hit {t} {
global sysFont longchamp longmot listemotscaches essais motfaux nbreu aide listeaide listevariable listexo couleur

$t configure -state normal
set ind [lsearch [$t tag names current] "ent*"] 
    if {$ind != -1} {
    set nom [lindex [$t tag names current] $ind]
    set num [string map {ent ""} $nom]
    set motfaux [$t get $nom.first $nom.last]
    #$t delete $nom.first $nom.last
    $t tag add jaune $nom.first $nom.last

        foreach tag [$t tag names current] {
        if {$tag != "jaune"} {
        catch {$t tag remove $tag $nom.first $nom.last}
        }
        }
    incr essais
    incr nbreu
    tux_reussi
    #catch {destroy .menu.lab}
    #label .menu.lab -text [format [mc {%1$s mot(s) trouve(s) sur %2$s.}] $nbreu [llength $listemotscaches]]
    #pack .menu.lab
    .menu.consigne configure -text "[lindex [lindex $listexo [expr [string index $couleur end] + 6] ] 2]. [format [mc {%1$s mot(s) trouve(s) sur %2$s.}] $nbreu [llength $listemotscaches]]"
    testefin $nbreu $essais

    return
    } else {
    $t tag add rouge current
    incr essais
    tux_echoue1
    }
$t configure -state disabled

}


proc testefin {nbreu essais} {
global sysFont listemotscaches user categorie listexo couleur
    if {$nbreu >= [llength $listemotscaches]} {
    catch {destroy .menu.lab}
    catch {destroy .menu.consigne}

    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s) pour %2$s mot(s).}] $essais $nbreu]
    set str1 [lindex [lindex $listexo [expr [string index $couleur end] -1]] 1]
set score [expr ($nbreu*100)/([llength $listemotscaches] +($essais- $nbreu))]
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}

    #enregistreval $str1 $categorie $str2 $user
    label .menu.lab -text $str0$str2
    pack .menu.lab
    .text tag remove text 1.0 end
    bell
        }
}



if {$startdirect == 0 } {
main .text
}

proc abandon {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches auto longchamp longmot
global sysFont longchamp longmot listemotscaches essais motfaux nbreu
set score [expr ($nbreu*100)/([llength $listemotscaches] +($essais- $nbreu))]
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}

set cur 0.0
$t configure -state normal
catch {destroy .menu.b2}
catch {destroy .menu.lab}

set listtag [$t tag names]
    foreach tg $listtag {
catch {
       set ind [lsearch $tg "ent*"]
       if {$ind != -1} {

          set nom $tg
          set num [string map {ent ""} $nom]
          set motfaux [$t get $nom.first $nom.last]
          #set cur [$t search $motfaux $cur end]
          set cur [$t index $nom.first]

          $t delete $nom.first $nom.last
          $t insert $cur [lindex $listemotscaches $num]
          $t tag add violet $cur "$cur + [string length [lindex $listemotscaches $num]] char"


        $t tag bind text <ButtonRelease-1> {}

       }
}
}
$t tag configure violet -background pink

$t configure -state disabled

}

proc fin {} {
global sysFont categorie user essais listemotscaches nbreu listexo couleur iwish filuser aide startdirect repertoire lecture_mot_cache
variable repertconf
  set score [expr ($nbreu*100)/([llength $listemotscaches] +($essais- $nbreu))]

    #set str1 [lindex [lindex $listexo [expr [string index $couleur end] -1]] 1]


    set str2 [format [mc {%1$s essai(s) pour %2$s mot(s) sur %3$s.}] $essais $nbreu [llength $listemotscaches]]

switch $startdirect {
1 {set startconf [mc {Le texte est visible au debut}]}
0 {set startconf [mc {Le texte n'est pas visible au debut}]}
}

switch $aide {
1 { set aideconf [mc {Au debut}]}
2 { set aideconf [mc {Apres le premier essai}]}
3 { set aideconf [mc {Apres le deuxieme essai}]}
}
switch $lecture_mot_cache {
1 { set lectmotcacheconf [mc {Le texte peut �tre entendu.}]}
0 { set lectmotcacheconf [mc {Le texte ne peut pas �tre entendu.}]}
}

set exoconf [mc {Parametres :}]
set exoconf "$exoconf $startconf - "
set exoconf "$exoconf Aide : $aideconf"
set exoconf "$exoconf  Son : $lectmotcacheconf"

    enregistreval [lindex [lindex $listexo [expr [string index $couleur end] +6]] 1] \173$categorie\175 $str2 $score $repertconf [expr [string index $couleur end] +6] $user $exoconf $repertoire
exec $iwish aller.tcl $filuser &
exit
}



