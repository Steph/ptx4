var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.chocolat2 = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var stage,k,Longueur,Largeur,Bouche,Fond,Tab_Carre,Plaque,BoucheO,BoucheF,Annuler,LargeurSoluce,LongueurSoluce,miam,Proposition,Touchable,Soluce;
var masse=[200,240,300,600];

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        tempsExo:180,
		num_tablette:4
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
     txt : "chocolat2/textes/chocolat2_fr.json",	
	 illustration:"chocolat2/images/illustration.png",
	 carre: "chocolat2/images/Choco_Plein.png",
	 glouton:"chocolat2/images/glouton.png",
	 glouton2:"chocolat2/images/glouton2.png",
	 cancel:"chocolat2/images/Cancel.png",
	 faux:"chocolat2/images/Faux.png",
	 morceau:"chocolat2/images/morceau.png"
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {	
	   
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
	 var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    // on ajoute un element canvas au bloc animation 
    // Attention ses dimensions doivent être fixées avec .attr(width:xx,height:xx)
    // et pas avec .css({width:xx,height:yy})
    var canvas = $("<canvas id='cnv'></canvas>");
    canvas.attr({width:735,height:350});
    canvas.css({position:'absolute',top:90});
    exo.blocAnimation.append(canvas);
    //on cree la scene avec createjs
    stage = new createjs.Stage("cnv");
	stage.enableMouseOver(1);
	// on demande à la scene de se redessiner chaque fois que le ticker envoie un evenement "tick"
	exo.aStage.push(stage); // pour qu'on puise appeler Touch.disable() au déchargement de l'exercice
	createjs.Ticker.timingMode = createjs.Ticker.RAF; // pour une animation Tween plus fluide
    createjs.Ticker.addEventListener("tick", stage); // pour que la scene soit redessinée à chaque "tick"
	createjs.Ticker.addEventListener("tick", createjs.Tween); // pour que Tween fonctionne après un Ticker.reset() lancé au déchargement de l'exercice;
	createjs.Touch.enable(stage);// pour compatibilité avec les tablettes tactiles
	
	
	var num_tablette;
	if(exo.options.num_tablette==4){
		num_tablette= Math.floor(Math.random() * 4);
	} else {
		num_tablette=exo.options.num_tablette;
		
	}

//Les données pour les 5 tablettes unetablette=[L, H , [nbCarres, Lsoluce, Hsoluce], ... ]
	var aTablettes = [];
	aTablettes[0] = [8, 5, [[20, 4, 5], [10, 2, 5], [30, 6, 5], [8, 8, 1], [24, 8, 3]]];
	aTablettes[1] = [9, 4, [[18, 9, 2], [9, 9, 1], [27, 9, 3], [12, 3, 4], [24, 6, 4]]];
	aTablettes[2] = [8, 6, [[24, 4, 6], [12, 2, 6], [36, 6, 6], [16, 8, 2], [32, 8, 4]]];
	aTablettes[3] = [9, 8, [[36, 9, 4], [18, 9, 2], [54, 9, 6], [24, 3, 8], [48, 6, 8]]];

	//quelques raccourcis utiles
	Largeur = aTablettes[num_tablette][0];
	Longueur = aTablettes[num_tablette][1];
	Soluce = aTablettes[num_tablette][2][exo.indiceQuestion][0];
	LargeurSoluce = aTablettes[num_tablette][2][exo.indiceQuestion][1];
	LongueurSoluce = aTablettes[num_tablette][2][exo.indiceQuestion][2];
	Proposition=0;
	miam=true;
	Touchable=true;
	k=Math.min(1,Math.min(535/(Longueur*50),300/(50*Largeur)));  
		
	Tab_Carre=[];

    var i,j;
	Plaque=new createjs.Container();
	for(i=0;i<Longueur;i++){
		for(j=0;j<Largeur;j++){
			//var uri=exo.getURI("carre")
			var bitmap = new createjs.Bitmap(exo.getURI("carre"));
			bitmap.x=i*50;
			bitmap.y=j*50;
			Plaque.addChild(bitmap);
			Tab_Carre.push(bitmap);
			
		}
		}
	Plaque.scaleX=Plaque.scaleY=k;
	Plaque.y=20;
	Plaque.x=(735-56*Longueur*k)/2;
	stage.addChild(Plaque);	
	
	// le bouton Annuler
	Annuler = new createjs.Container();
	var f = new createjs.Bitmap(exo.getURI("cancel"));
	Annuler.addChild(f);
	Annuler.x=50;
	Annuler.y=150;
	Annuler.addEventListener("click",Cancel);
	stage.addChild(Annuler);

	// le bouton valider
	exo.btnValider.css({left:600});
	
	// le glouton
	Bouche=new createjs.Container();
	Bouche.cursor = "pointer";
	BoucheF=new createjs.Bitmap(exo.getURI("glouton"));	
	BoucheO=new createjs.Bitmap(exo.getURI("glouton2"));
	Bouche.addChild(BoucheF);
	Bouche.addChild(BoucheO);
	Bouche.regX=29.5;
	Bouche.regY=26;
	BoucheO.x=-5;
	BoucheO.visible=false;	
	stage.addChild(Bouche);
	Bouche.x=100;
	Bouche.y=80;
	
	Bouche.addEventListener("pressmove",cliquer);
	Bouche.addEventListener("mousedown",startMordre);
	Bouche.addEventListener("pressup",stopMordre);
	
	// la consigne
	var text, frac;
	var p=pgcd(Longueur*Largeur,Soluce);
	var nume=Soluce/p;
	var deno=Longueur*Largeur/p;
	switch(nume/deno){
		case 0.5: frac="la moitié";break;
		case 0.25: frac="le quart";break;
		default: frac=nume+"/"+deno;
	}
	
	text=exo.txt.t2+masse[num_tablette]+" g.<br>"+exo.txt.t1+(Soluce*masse[num_tablette]/(Largeur*Longueur))+" g.";
	var etiquetteConsigne = disp.createTextLabel(text);
	exo.blocAnimation.append(etiquetteConsigne);
    etiquetteConsigne.css({
        fontSize:18,
        fontWeight:200,
		padding:10,
		textAlign:"center",
		border:"1px solid #ccc"
    });
	etiquetteConsigne.position({
		my:"center top",
		at:"center top+30",
		of:exo.blocAnimation
	});

	/* fonctions utiles */
	function pgcd(a,b){
		while (a!= b){
			if (a>b){ 
				a-=b;
			}
			else {
				b-=a;
			}
		}
		return a;
    }

    function startMordre(){
    	miam =true;
    	mordre();
    }

	function stopMordre(){
		miam = false;
		BoucheO.visible=true;
		BoucheF.visible=false;
	}

	function mordre(){
		if(BoucheO.visible === true){
			BoucheO.visible = false;
			BoucheF.visible = true;	
		}
		else {
			BoucheO.visible = true;	
			BoucheF.visible = false;
		}
		if(miam)
			exo.setTimeout(mordre, 200);
	}


	function Dist(a,b,X,Y){
		return((X-a)*(X-a)+(Y-b)*(Y-b));
	}

	function cliquer(e){
		//e.target.alpha=0.5;
		if(Touchable){
			Bouche.x=e.stageX;
			Bouche.y=e.stageY;
			
			for(var i=0;i<Tab_Carre.length;i++){
				if(Dist(Bouche.x-Plaque.x,Bouche.y-Plaque.y,(Tab_Carre[i].x+25)*k,(Tab_Carre[i].y+25)*k)<400*k && Tab_Carre[i].alpha==1)
				{
					Tab_Carre[i].alpha=0.3;
					Proposition++;
					var cass = new createjs.Bitmap(exo.getURI("morceau"));
					cass.regX=50;
					cass.regY=50;
					cass.x=Tab_Carre[i].x+25;
					cass.y=Tab_Carre[i].y+25;
					Plaque.addChild(cass);
					createjs.Tween.get(cass).to({scaleX:1.2,scaleY:1.2}, 200).call(destroy);	 
					break;
				}
			}	
		}
		
	}

	function Cancel(e){
		e.preventDefault();
		if(Touchable){
			for(var i=0; i<Tab_Carre.length; i++){
				Tab_Carre[i].alpha=1;
				Proposition=0;
			}
		}
	}

	function destroy(){
		Plaque.removeChild(this);
	}

	/* fin fonctions utiles */
};


// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    // on met le ticker en pause pour arreter l'animation
   // createjs.Ticker.setPaused(true);
	
	if(Proposition==Soluce){
    return "juste";}
	return "faux";
};

// Correction (peut rester vide)
exo.corriger = function() {
	stage.removeChild(Annuler);
	var i,j;
	Touchable=false;
	var PlaqueS = new createjs.Container();	
	for(i=0; i<Longueur; i++){
		for(j=0; j<Largeur; j++){
			var bitmap = new createjs.Bitmap(exo.getURI("carre"));
			bitmap.x=i*50;
			bitmap.y=j*50;
			PlaqueS.addChild(bitmap);
			
			if(i<LongueurSoluce && j<LargeurSoluce){
				bitmap.alpha=0.3;
			}		
		}
	}
	
	PlaqueS.scaleX=PlaqueS.scaleY=k/1.25;
	PlaqueS.y=20;
	PlaqueS.x=1000;
	stage.addChild(PlaqueS);	
	
	createjs.Tween.get(Plaque).to({scaleX:k/1.25,scaleY:k/1.25,x:30}, 1000).call(handleComplete);
	createjs.Tween.get(Bouche).to({x:100,y:300}, 1000).call(handleComplete);
	function handleComplete(){
		var croix=new createjs.Bitmap(exo.getURI("faux"));
		croix.scaleX=50*Longueur/215;
		croix.scaleY=50*Largeur/215;
		croix.x=croix.y=3;
		Plaque.addChild(croix);
		createjs.Tween.get(PlaqueS).to({x:Longueur*25*k+Plaque.x+150}, 1000);
	}
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;
	
	   
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:exo.txt.max_essais,
        aLabel:exo.txt.label_essais,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:72,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.temps_max
    });
    exo.blocParametre.append(controle);
	//
	  controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"num_tablette",
        texte:exo.txt.num_tablette,
        aLabel:exo.txt.label_tablette,
        aValeur:[0,1,2,3,4]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));