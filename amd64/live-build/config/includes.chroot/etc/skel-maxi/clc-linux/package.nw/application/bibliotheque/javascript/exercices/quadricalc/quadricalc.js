var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.quadricalc = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci

//exo.tabletSupport="debug";
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre;
var aPlageA = [], aPlageB = [], aCouple = [], intId;

// Référencer les ressources de l'exercice (image, son)

exo.oRessources = { 
    illustration : "quadricalc/images/illustration.png"
};

// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:1,
        tempsExo:0,
        plageA:"2",
        plageB:"3;4;5;6;7;8;9;10;12;15;20;25;30;40;50",
        totalTentative:10,
        choixOperateur:[1000], //1 addition,10 multiplication,100 soustraction,1000 division euclidienne
        choixVitesse:1000
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    aPlageA = util.getArrayNombre(""+exo.options.plageA);
    aPlageB = util.getArrayNombre(""+exo.options.plageB);
    var lengthA = aPlageA.length;
    var lengthB = aPlageB.length;
    // on genere tous les couples possibles 
    for( var i = 0 ; i < lengthA ; i++ ) {
        var nA = aPlageA[i];
        for ( var j = 0 ; j < lengthB ; j++ ) {
            var nB = aPlageB[j];
            aCouple.push([nA,nB]);
        }
    }
    util.shuffleArray(aCouple);
    // on evite de manipuler un tableau trop long (100 couples maxi)
    if( aCouple.length > 100 ) {
        aCouple = aCouple.slice(0,exo.options.totalTentative);
    }
    // si les parametres ne génèrent pas assez de couples on répète
    else if ( aCouple.length < exo.options.totalTentative ) {
        var n = 0;
        while ( (exo.options.totalTentative - aCouple.length)>0 || n < 100) {
            n++;
            aCouple = aCouple.concat(aCouple.slice(0,exo.options.totalTentative - aCouple.length));
        }
    }
};

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("Le quadricalc");
    exo.blocConsigneGenerale.html("Utilise les flèches du clavier pour diriger le calcul vers le bon résultat.");
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.append(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    exo.btnValider.hide();
    exo.keyboard.config({
        numeric:"disabled",
        large:"disabled"
    });
    //var indexOperateur = indexTentative % options.choixOperateur.length;
    //var operateur = options.choixOperateur[indexOperateur];
    //le conteneur
    var conteneur = disp.createEmptySprite();
    var paper = Raphael(conteneur[0],560,360);
    conteneur.html(paper.node);
    exo.blocAnimation.append(conteneur);
    conteneur.position({
        my:"center top",
        at:"center top+20",
        of:exo.blocAnimation
    });
    
    //on cree les briques et les etiquettes de la ligne de base
    var aBrique=[];
    var aValeurBase = updateLigneBase();
    for( var j = 0 ; j < 9 ; j++ ) {
        for( var i = 0 ; i < 7 ; i++ ) {
            var brique = {};
            brique.fond = paper.rect(i*80,j*40,80,40);
            brique.fond.attr("stroke","#333");
            brique.fond.attr("stroke-width",0.1);
            if(j==8){
                // la ligne de base
                brique.base = true;
                brique.valeur=aValeurBase[i];
                brique.fond.attr("gradient","90-#FF9900-#FFBB00");
                brique.label = paper.text(i*80+40,j*40+20,brique.valeur);
                brique.label.attr("font-size",16);
            } else {
                brique.fond.attr("fill","#ffffff");
                brique.base = false;
            }
            aBrique.push(brique);
            brique.indice = aBrique.length - 1;
        }
    }
    
    // on cree la brique active et son etiquette
    
    var alea = 5;
    var briqueActive = aBrique[3];
    briqueActive.valeur = updateBriqueActive().valeur;
    briqueActive.texte = updateBriqueActive().texte;
    briqueActive.label = paper.text(3*80+40,20,briqueActive.texte);
    briqueActive.label.attr("font-size",16);
    briqueActive.label.attr("fill","#fff");
    briqueActive.fond.attr("gradient","90-#02A001-#00CC00");
    
    // on ajoute la gestion du clavier
    exo.baseContainer.on("keydown",gererClavier);
    //$(document).on("keyup",gererChute);
    
    // on démarre l'animation de la brique active
    var vitesse = exo.options.choixVitesse;
    var count = 0;
    intId = exo.setInterval(
        function(){
            bougerBriqueBas(briqueActive);
            evalBrique(briqueActive);
        },
        vitesse
    );
    
    
    // 
    function gererClavier(e){
        if ( e.which == 37 && briqueActive.indice%7>0 && aBrique[briqueActive.indice-1].base === false) {
            bougerBriqueGauche(briqueActive);
            evalBrique(briqueActive);
            
        }
        else if ( e.which == 39 && briqueActive.indice%7<6 && aBrique[briqueActive.indice+1].base === false) {
            bougerBriqueDroite(briqueActive);
            evalBrique(briqueActive);
            
        }
        else if ( e.which == 40 && Math.floor(briqueActive.indice/7)>0 && Math.floor(briqueActive.indice/7)<7) {
            exo.clearInterval(intId);
            tomberBrique(briqueActive);
            intId = exo.setInterval(
                function(){
                    bougerBriqueBas(briqueActive);
                    evalBrique(briqueActive);
                    return false;
                },
                vitesse
            );
        }
        return false;
        
    }
    //
    function bougerBriqueBas(b){
        
        var valeur = b.valeur;
        var texte = b.texte;
        b.label.remove();
        b.fond.attr("fill","#fff");
        // la brique en dessous de la brique active n'est pas une brique de base
        if( aBrique[b.indice+7].base === false ) {
            // la brique en dessous devient brique active
            briqueActive = aBrique[b.indice+7];
            briqueActive.fond.attr("gradient","90-#02A001-#00CC00");
            if (briqueActive.label !== undefined)
                briqueActive.label.remove();
            briqueActive.valeur = valeur;
            briqueActive.texte = texte;
            var posX = briqueActive.fond.getBBox().x+40;
            var posY  = briqueActive.fond.getBBox().y+20;
            briqueActive.label = paper.text(posX,posY,briqueActive.texte);
            briqueActive.label.attr("font-size",16);
            briqueActive.label.attr("fill","#fff");
        }
    }
    
    //
    function bougerBriqueGauche(b){
        var valeur = b.valeur;
        var texte = b.texte;
        b.fond.attr("fill","#fff");
        b.label.remove();
        briqueActive = aBrique[b.indice-1];
        briqueActive.fond.attr("gradient","90-#02A001-#00CC00");
        briqueActive.valeur = valeur;
        briqueActive.texte = texte;
        var posX = briqueActive.fond.getBBox().x+40;
        var posY  = briqueActive.fond.getBBox().y+20;
        briqueActive.label = paper.text(posX,posY,texte);
        briqueActive.label.attr("font-size",16);
        briqueActive.label.attr("fill","#fff");
    }
    
    //
    function bougerBriqueDroite(b){
        var valeur = b.valeur;
        var texte = b.texte;
        b.fond.attr("fill","#fff");
        b.label.remove();
        briqueActive = aBrique[b.indice+1];
        briqueActive.fond.attr("gradient","90-#02A001-#00CC00");
        briqueActive.valeur = valeur;
        briqueActive.texte = texte;
        var posX = briqueActive.fond.getBBox().x+40;
        var posY  = briqueActive.fond.getBBox().y+20;
        briqueActive.label = paper.text(posX,posY,texte);
        briqueActive.label.attr("font-size",16);
        briqueActive.label.attr("fill","#fff");
    }
    
    function tomberBrique(b){
        
        var valeur = b.valeur;
        var texte = b.texte;
        b.label.remove();
        b.fond.attr("fill","#fff");
        // la brique en dessous de la brique active n'est pas une brique de base
        var n=1;
        while(aBrique[b.indice+7*n].base===false){
            n++;
        }
        
        briqueActive = aBrique[b.indice+7*(n-1)];
        briqueActive.fond.attr("gradient","90-#02A001-#00CC00");
        if (briqueActive.label !== undefined)
            briqueActive.label.remove();
        briqueActive.valeur = valeur;
        briqueActive.texte = texte;
        var posX = briqueActive.fond.getBBox().x+40;
        var posY  = briqueActive.fond.getBBox().y+20;
        briqueActive.label = paper.text(posX,posY,briqueActive.texte);
        briqueActive.label.attr("font-size",16);
        briqueActive.label.attr("fill","#fff");
    }
    
    //
    function evalBrique(b){
        $(document).off("keydown",gererClavier);
        var nextBrique = aBrique[b.indice+7];
        // perdu
        if(nextBrique.base === true && nextBrique.valeur != b.valeur){
            exo.indiceTentative++;
            nextBrique.fond.attr("gradient","90-#ff0000-#ff5500");
            nextBrique.label.remove();
            nextBrique.base = false;
            //b.fond.attr("fill","#ff9900");
            b.fond.attr("gradient","90-#FF9900-#FFBB00");
            b.base = true;
            b.valeur = nextBrique.valeur;
            b.label.remove();
            var posX = b.fond.getBBox().x+40;
            var posY  = b.fond.getBBox().y+20;
            b.label = paper.text(posX,posY,nextBrique.valeur);
            b.label.attr("font-size",16);
            // si on a epuisé toutes les tentatives fin du jeu
            if(exo.indiceTentative == exo.options.totalTentative) {
                exo.clearInterval(intId);
                //$(document).off("keydown",gererClavier);
                exo.terminerExercice();
                return "fin";
            }
            //on replace la brique active en haut
            if(b.indice > 6) {
                briqueActive = aBrique[3];
                //briqueActive.valeur = b.valeur;
                briqueActive.label = paper.text(3*80+40,20,briqueActive.texte);
                briqueActive.label.attr("font-size",16);
                briqueActive.label.attr("fill","#fff");
                briqueActive.fond.attr("gradient","90-#02A001-#00CC00");
                
            }
            //C'est terminé y a plus de place
            else {
                exo.clearInterval(intId);
                //$(document).off("keydown",gererClavier);
                exo.terminerExercice();
                return "fin";
            }
            //$(document).on("keydown",gererClavier);
        }
        //gagné
        else if ( nextBrique.base === true && nextBrique.valeur == b.valeur ){
            exo.indiceTentative++;
            exo.score++;
            exo.refreshScore();
            // si on a epuisé toutes les tentatives fin du jeu
            if(exo.indiceTentative == exo.options.totalTentative) {
                exo.clearInterval(intId);
                $(document).off("keydown",gererClavier);
                exo.terminerExercice();
                for (var i = 0 ; i < aBrique.length ; i++ ) {
                    var brique = aBrique[i];
                    if(brique.base === true) {
                        brique.fond.attr("fill","#ffff00");
                        brique.fond.animate({fill:"#ff9900"},700,function(e){
                            this.attr("gradient","90-#FF9900-#FFBB00");
                        });
                    }
                }
                b.fond.attr("fill","#fff");
                b.label.remove();
                return "fin";
            }
            // on met à jour les etiquettes de la ligne de base
            var aValeurBase = updateLigneBase();
            var count = 0;
            for (var i = 0 ; i < aBrique.length ; i++ ) {
                var brique = aBrique[i];
                if(brique.base === true) {
                    brique.label.remove();
                    var posX = brique.fond.getBBox().x+40;
                    var posY  = brique.fond.getBBox().y+20;
                    brique.valeur = aValeurBase[count]
                    brique.label = paper.text(posX,posY,brique.valeur);
                    brique.label.attr("font-size",16);
                    brique.fond.attr("fill","#ffff00");
                    brique.fond.animate({fill:"#ff9900"},700,function(e){
                        this.attr("gradient","90-#FF9900-#FFBB00")
                    });
                    //brique.fond.attr("gradient","90-#FF7700-#FF9900");
                    count++;
                }
            }
            
            //on met a jour la brique active
            exo.setTimeout(function(e){
                b.fond.attr("fill","#fff");
                b.label.remove();
                briqueActive = aBrique[3];
                briqueActive.label.remove();
                briqueActive.valeur = updateBriqueActive().valeur;
                briqueActive.texte = updateBriqueActive().texte;
                briqueActive.label = paper.text(3*80+40,20,briqueActive.texte);
                briqueActive.label.attr("font-size",16);
                briqueActive.label.attr("fill","#fff");
                briqueActive.fond.attr("gradient","90-#02A001-#00CC00");
                //$(document).on("keydown",gererClavier);
            },200);
        }
        // pas encore arrivé en bas
        else {
            $(document).on("keydown",gererClavier);
        }
        
    }
    
    function updateLigneBase(aValeur){
        var indice = exo.indiceTentative%exo.options.choixOperateur.length;
        var operateur = exo.options.choixOperateur[indice];
        var nA = aCouple[exo.indiceTentative][0];
        var nB = aCouple[exo.indiceTentative][1];
        //addition
        if ( operateur == 1 ) {
            aValeur=[];
            aValeur.push(nA+nB);//la solution
            // on choisit 6 autres propositions toutes différentes
            for ( var i = 0 ; i < aCouple.length ; i ++ ) {
                var valeur = aCouple[i][0]+aCouple[i][1];
                if( aValeur.indexOf(valeur) < 0 ) {
                    aValeur.push(valeur);
                }
                if(aValeur.length==7)
                    break;
            }
        }
        // multiplication
        else if ( operateur == 10 ) {
            aValeur=[];
            aValeur.push(nA*nB);//la solution
            // on choisit 6 autres propositions toutes différentes
            for ( var i = 0 ; i < aCouple.length ; i ++ ) {
                var valeur = aCouple[i][0]*aCouple[i][1];
                if( aValeur.indexOf(valeur) < 0 ) {
                    aValeur.push(valeur);
                }
                if(aValeur.length == 7)
                    break;
            }
        }
        // soustraction
        else if ( operateur == 100 ) {
            aValeur=[];
            aValeur.push(Math.abs(nA-nB));//la solution
            // on choisit 6 autres propositions toutes différentes
            for ( var i = 0 ; i < aCouple.length ; i ++ ) {
                var valeur = Math.abs(aCouple[i][0]-aCouple[i][1]);
                if( aValeur.indexOf(valeur) < 0 ) {
                    aValeur.push(valeur);
                }
                if(aValeur.length==7)
                    break;
            }
        }
        //division
        else if ( operateur == 1000 ) {
            aValeur=[];
            aValeur.push(nB);//la solution
            // on choisit 6 autres propositions toutes différentes
            for ( var i = 0 ; i < aCouple.length ; i ++ ) {
                var valeur = aCouple[i][1];
                if( aValeur.indexOf(valeur) < 0 ) {
                    aValeur.push(valeur);
                }
                if(aValeur.length==7)
                    break;
            }
        }
        
        aValeur.sort(function(a,b){
            var res = a >= b ? 1 : -1;
            return res;
        });
        // si on n'a pas trouve 6 valeurs différentes on rajoute des valeurs supplémentaires
        while(aValeur.length<7){
            var valSup = aValeur[aValeur.length-1]+1;
            if( aValeur.indexOf(valSup) < 0 ) {
                aValeur.push(valSup);
            }
        }
        return aValeur;
    }
    
    function updateBriqueActive(){
        var indice = exo.indiceTentative;
        var operateur = exo.options.choixOperateur[exo.indiceTentative%exo.options.choixOperateur.length];
        var nA = aCouple[indice][0];
        var nB = aCouple[indice][1];
        var valeur,texte;
        if ( operateur == 1 ) {
            valeur = nA+nB;
            texte = nA + " + " + nB;
        }
        else if ( operateur == 10 ) {
            valeur = nA*nB;
            texte = nA + " x " + nB;
        }
        else if ( operateur == 100 ) {
            var max = Math.max(nA,nB);
            var min = Math.min(nA,nB);
            valeur = max - min;
            texte = max + " - " +min;
        }
        else if ( operateur == 1000 ) {
            valeur = nB;
            texte = nA*nB + " : " + nA;
        }
        return {
            valeur:valeur,
            texte:texte
        };
    }
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
   
    return "juste";
    
};

// Correction (peut rester vide)

exo.corriger = function() {
    
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle;
     controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"totalTentative",
        texte:"Nombre de calculs : (minimum 10)"
    });
    exo.blocParametre.append(controle);
    //
    controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:300,
        //taille:50,
        nom:"plageA",
        texte:"Nombre a : "
    });
    exo.blocParametre.append(controle);
    //
    controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:300,
        //taille:50,
        nom:"plageB",
        texte:"Nombre b : "
    });
    exo.blocParametre.append(controle);
    
    controle = new disp.createOptControl(exo,{
        type:"radio",
        nom:"choixVitesse",
        texte:"Vitesse de chute : ",
        aLabel : ["très lente","lente","moyenne","rapide","très rapide"],
        aValeur : [1400,1000,800,600,400]
    });
    exo.blocParametre.append(controle);
    
    controle = new disp.createOptControl(exo,{
        type:"checkbox",
        nom:"choixOperateur",
        texte:"Choix de l'opération : ",
        aLabel : ["addition","multiplication","soustraction","division"],
        aValeur : [1,10,100,1000]
    });
    exo.blocParametre.append(controle);
    
};
/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));